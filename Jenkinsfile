#!groovy

node {

  emailRecipients = 'krjura@gmail.com'

  try {

    build()

    notificationBuildSuccessful()

  } catch(err) {
    notificationBuildFailed()
    throw err
  }
}

def build() {

  resolveProperties()

  withCredentials([
    usernamePassword(credentialsId: 'MAVEN2_KRJURA_ORG_CREDENTIALS', passwordVariable: 'REPOSITORY_PASSWORD', usernameVariable: 'REPOSITORY_USERNAME')
  ]) {
    withEnv([
      'GRADLE_USER_HOME=/opt/cache/gradle',
      'ENVIRONMENT=jenkins']) {
        withDockerRegistry([credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', url: "https://docker.krjura.org"]) {
          docker.image('docker.krjura.org/life/postgres:9.5.9').withRun(" --mount type=tmpfs,destination=/opt/tmpfs ") { database ->
          docker.image('docker.krjura.org/life/rabbit-mq:v1').withRun { rabbit ->
          docker.image('docker.krjura.org/life/java-build-env:v10').inside("--link=${database.id}:database --link=${rabbit.id}:rabbit -v life-gradle-cache:${GRADLE_USER_HOME}") {

            stage('Initialize') {
              checkout scm
            }

            stage('Prepare Database') {
              sh './build-utils --module services/backend-birthdays --file etc/init/docker/wait-postgres-up.sh --op script'
              sh './build-utils --module services/backend-birthdays --file etc/init/docker/initialize-postgres-tablespace.sh --op script'

              sh './build-utils --module services/backend-birthdays --file etc/init/docker/initialize-docker-database.sh --op script'
              sh './build-utils --module services/authentication-service --file etc/init/docker/initialize-docker-database.sh --op script'
              sh './build-utils --module services/notification-service --file etc/init/docker/initialize-docker-database.sh --op script'
              sh './build-utils --module infrastructure/spring-cloud-gateway --file etc/init/docker/initialize-docker-database.sh --op script'
            }

            stage('Build, Test, Deploy') {

              sh './gradlew --version'

              if(params.CLEAN_ALL) {
                sh './gradlew clean'
              }

              // execute services/backend-birthdays
              sh    './build-utils ' +
                    '--module services:backend-birthdays,services:authentication-service,services:notification-service,infrastructure:spring-cloud-gateway ' +
                    '--branch ${BRANCH_NAME} --env $ENVIRONMENT --op publishProject'

              // run asciidoctor where applicable
              sh './gradlew --no-daemon -Dspring.profiles.active=test,jenkins :services:authentication-service:asciidoctor :services:backend-birthdays:asciidoctor'
            }

          }}}
        }

        // build docker images if required
        stage('Build Dockers') {

          // checkout version project
          dir ('versions') {
            git credentialsId: 'jenkins@development_support', url: 'git@bitbucket.org:krjura/life-versions.git'
          }

          withDockerRegistry([credentialsId: 'DOCKER_KRJURA_ORG_CREDENTIALS', url: "https://docker.krjura.org"]) {

            // backend-birthdays
            dockerServiceRelease("backend-birthdays", "services")

            // authentication-service
            dockerServiceRelease("authentication-service", "services")

            // notification-service
            dockerServiceRelease("notification-service", "services")

            // spring-cloud-gateway
            dockerServiceRelease("spring-cloud-gateway", "infrastructure")
          }

          // commit changes if any
          sh './build-versions --op=commit --directory=versions --branch=$BRANCH_NAME'
        }
      }
    }
}

def dockerServiceRelease(String moduleName, String group) {
  def buildPropertiesDefaults = [released: false]
  props = readProperties defaults: buildPropertiesDefaults, file:"${WORKSPACE}/${group}/${moduleName}/build/${moduleName}-build.properties"

  RELEASED=props['released']

  if(RELEASED == 'true') {
    VERSION=props['project.version']
    IMAGE_TAG="docker.krjura.org/life/${moduleName}:${VERSION}"

    buildImage = docker.build("${IMAGE_TAG}", "${WORKSPACE}/${group}/${moduleName}")
    buildImage.push();

    sh "docker rmi ${IMAGE_TAG}"

    // add new version to versions project
    sh './build-versions --op=add --directory=versions --module=' + moduleName + ' --branch=$BRANCH_NAME --version=' + VERSION
  }
}

def notificationBuildFailed() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  mail to: emailRecipients,
    subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has FAILED",
    body: "Please go to ${BUILD_URL} for details."
}

def notificationBuildSuccessful() {
  if(env.BRANCH_NAME != "master") {
    return;
  }

  if( currentBuild.previousBuild == null ) {
    return
  }

  if (currentBuild.previousBuild.result == 'FAILURE') {
    mail to: emailRecipients,
      subject: "Job '${JOB_NAME}' build ${BUILD_DISPLAY_NAME} has RECOVERED",
      body: "Please go to ${BUILD_URL} for details."
  }
}

def resolveProperties() {
  def config = []

  // make sure cleanup is done on a regular basis
  config.add(
    buildDiscarder(
      logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '10', numToKeepStr: '5')))

  config.add(disableConcurrentBuilds())

  config.add(durabilityHint('PERFORMANCE_OPTIMIZED'))

  config.add(
    parameters([
        booleanParam(defaultValue: false, description: 'Should gradle clean on start', name: 'CLEAN_ALL')
    ])
  )

  properties(config)
}