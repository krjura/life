package org.krjura.life.spring.cloud.gateway.services;

import org.krjura.life.events.TokenRevokedEvent;

public interface LoginUpdatesService {

    void processTokenRevokedEvent(TokenRevokedEvent event);
}
