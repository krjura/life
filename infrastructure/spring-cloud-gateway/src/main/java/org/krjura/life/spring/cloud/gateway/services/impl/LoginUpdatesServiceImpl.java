package org.krjura.life.spring.cloud.gateway.services.impl;

import org.krjura.life.commons.time.FormattingUtils;
import org.krjura.life.events.TokenRevokedEvent;
import org.krjura.life.spring.cloud.gateway.model.repository.TokenRepository;
import org.krjura.life.spring.cloud.gateway.services.LoginUpdatesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.UUID;

@Component
public class LoginUpdatesServiceImpl implements LoginUpdatesService {

    private static final Logger logger = LoggerFactory.getLogger(LoginUpdatesServiceImpl.class);

    private final TokenRepository tokenRepository;

    public LoginUpdatesServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public void processTokenRevokedEvent(TokenRevokedEvent event) {

        logger.info("Processing TokenRevokedEvent");

        UUID tokenId = UUID.fromString(event.getTokenId());
        ZonedDateTime expiresAt = FormattingUtils.to(event.getExpiresAt());
        ZonedDateTime revokedAt = FormattingUtils.to(event.getRevokedAt());

        this.tokenRepository.addTokenInfo(tokenId, expiresAt, revokedAt);
    }
}
