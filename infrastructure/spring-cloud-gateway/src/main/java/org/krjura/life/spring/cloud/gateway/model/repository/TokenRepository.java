package org.krjura.life.spring.cloud.gateway.model.repository;

import org.krjura.life.spring.cloud.gateway.model.TokenInfo;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

public interface TokenRepository {

    void addTokenInfo(UUID token, ZonedDateTime expiresAt, ZonedDateTime revokedAt);

    Optional<TokenInfo> findTokenById(UUID tokenId);
}
