package org.krjura.life.spring.cloud.gateway.services;

import biz.db.dev.life.jwt.core.JwtDetails;
import org.krjura.life.spring.cloud.gateway.ex.TokenException;

public interface TokenService {

    void verifyToken(JwtDetails jwtDetails) throws TokenException;
}
