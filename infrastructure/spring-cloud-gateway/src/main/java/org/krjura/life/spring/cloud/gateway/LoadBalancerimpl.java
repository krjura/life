package org.krjura.life.spring.cloud.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

@Service
public class LoadBalancerimpl implements LoadBalancerClient {

    private static final Logger logger = LoggerFactory.getLogger(LoadBalancerimpl.class);

    private final Object LOCK = new Object();

    private final DiscoveryClient discoveryClient;

    private int count = 0;

    public LoadBalancerimpl(DiscoveryClient discoveryClient) {
        Objects.requireNonNull(discoveryClient);

        this.discoveryClient = discoveryClient;
    }

    @Override
    public <T> T execute(String serviceId, LoadBalancerRequest<T> request) throws IOException {
        return null;
    }

    @Override
    public <T> T execute(String serviceId, ServiceInstance serviceInstance, LoadBalancerRequest<T> request) throws IOException {
        return null;
    }

    @Override
    public URI reconstructURI(ServiceInstance instance, URI original) {
        String schema = instance.isSecure() ? "https" : "http";

        try {
            return new URI(
                    schema,
                    original.getUserInfo(),
                    instance.getHost(), instance.getPort(),
                    original.getPath(), original.getQuery(), original.getFragment());
        } catch ( URISyntaxException e) {
            logger.warn("Cannot build new uri from original " + original, e);
            return original;
        }
    }

    @Override
    public ServiceInstance choose(String serviceId) {
        List<ServiceInstance> instances = this.discoveryClient.getInstances(serviceId);

        if(instances.isEmpty()) {
            return null;
        }

        return instances.get(nextSelected(instances.size()));
    }

    private int nextSelected(int size) {
        return nextCount() % size;
    }

    private int nextCount() {
        synchronized (LOCK) {
            this.count++;

            if(this.count > 1_000_000) {
                this.count = 0;
            }

            return this.count;
        }
    }
}
