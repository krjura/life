package org.krjura.life.spring.cloud.gateway.cloud.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This will be loaded to application context
 */
@Configuration
@ConditionalOnProperty(value = "org.krjura.cloud.tools.consul.config.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({PropertySourceLocator.class})
public class ConsulClientAutoConfigure {

    @Bean
    public ConsulClientConfig consulClientConfig() {
        return new ConsulClientConfig();
    }
}
