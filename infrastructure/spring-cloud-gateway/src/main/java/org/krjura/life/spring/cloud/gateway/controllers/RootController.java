package org.krjura.life.spring.cloud.gateway.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.Rendering;

@Controller
public class RootController {

    @GetMapping(value = "/")
    public Rendering redirect() {
        return Rendering.redirectTo("/portal/").build();
    }
}
