package org.krjura.life.spring.cloud.gateway.config;

import biz.db.dev.life.jwt.core.config.JwtProps;
import org.krjura.life.spring.cloud.gateway.filters.global.JwtGlobalFilter;
import org.krjura.life.spring.cloud.gateway.services.TokenService;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public GlobalFilter JwtGlobalFilter(JwtProps jwtProps, TokenService tokenService) {
        return new JwtGlobalFilter(jwtProps, tokenService);
    }
}
