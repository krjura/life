package org.krjura.life.spring.cloud.gateway.services.impl;

import org.krjura.life.events.TokenRevokedEvent;
import org.krjura.life.ier.EventRecorder;
import org.krjura.life.json.utils.JsonUtils;
import org.krjura.life.spring.cloud.gateway.enums.InternalEventNames;
import org.krjura.life.spring.cloud.gateway.enums.QueueNames;
import org.krjura.life.spring.cloud.gateway.services.AmqpReceiverService;
import org.krjura.life.spring.cloud.gateway.services.LoginUpdatesService;
import org.krjura.life.validations.ex.ValidationException;
import org.krjura.life.validations.utils.EventValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
public class AmqpReceiverServiceImpl implements AmqpReceiverService {

    private static final Logger logger = LoggerFactory.getLogger(LoginUpdatesServiceImpl.class);

    private static final EventValidator EVENT_VALIDATOR = new EventValidator();

    private static final String CONST_TOKEN_REVOKED_EVENT = "TokenRevokedEvent";

    private final LoginUpdatesService loginUpdatesService;

    public AmqpReceiverServiceImpl(LoginUpdatesService loginUpdatesService) {
        Objects.requireNonNull(loginUpdatesService);

        this.loginUpdatesService = loginUpdatesService;
    }

    @RabbitListener(bindings =
    @QueueBinding(
            value = @Queue(value = QueueNames.LOGIN_UPDATES_QUEUE, durable = "true"),
            exchange = @Exchange(value = QueueNames.LOGIN_UPDATES_EXCHANGE, type = ExchangeTypes.FANOUT),
            key = QueueNames.LOGIN_UPDATES_RK)
    )
    public void processOrder(Message message) {

        try {
            switch (message.getMessageProperties().getType()) {
                case CONST_TOKEN_REVOKED_EVENT:

                    TokenRevokedEvent event = JsonUtils.to(message.getBody(), TokenRevokedEvent.class);
                    EVENT_VALIDATOR.validateAndThrow(event);

                    loginUpdatesService.processTokenRevokedEvent(event);

                    break;
                default:
                    EventRecorder.instance().publish(InternalEventNames.AMQP_UNKNOWN_EVENT.name(), message);
                    logger.info("received event of type {}. Ignoring it", message.getMessageProperties().getType());
            }
        } catch (IOException | ValidationException e) {
            logger.warn("Cannot convert object of type " + message.getMessageProperties().getType(), e);
            EventRecorder.instance().publish(InternalEventNames.AMQP_RECEIVER_EXCEPTION.name(), e);
        }
    }
}
