package org.krjura.life.spring.cloud.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties( prefix = "life.gateway")
public class GatewayConfig {

    private Long revokedTokenCacheExpireAfter;

    private Long getRevokedTokenCacheMaximumSize;

    public long getRevokedTokenCacheExpireAfter() {
        return revokedTokenCacheExpireAfter;
    }

    public void setRevokedTokenCacheExpireAfter(Long revokedTokenCacheExpireAfter) {
        this.revokedTokenCacheExpireAfter = revokedTokenCacheExpireAfter;
    }

    public long getGetRevokedTokenCacheMaximumSize() {
        return getRevokedTokenCacheMaximumSize;
    }

    public void setGetRevokedTokenCacheMaximumSize(Long getRevokedTokenCacheMaximumSize) {
        this.getRevokedTokenCacheMaximumSize = getRevokedTokenCacheMaximumSize;
    }
}
