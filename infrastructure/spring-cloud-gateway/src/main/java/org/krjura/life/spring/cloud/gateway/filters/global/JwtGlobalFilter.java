package org.krjura.life.spring.cloud.gateway.filters.global;

import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.JwtTokenBuilder;
import biz.db.dev.life.jwt.core.JwtTokenInfo;
import biz.db.dev.life.jwt.core.config.JwtProps;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.keystore.DefaultKeyStoreProvider;
import biz.db.dev.life.jwt.core.keystore.JwtKeyStore;
import biz.db.dev.life.jwt.core.utils.JwtConstants;
import biz.db.dev.life.jwt.core.utils.JwtHelper;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.krjura.life.commons.utils.ArrayUtils;
import org.krjura.life.commons.utils.RootCause;
import org.krjura.life.spring.cloud.gateway.ex.TokenException;
import org.krjura.life.spring.cloud.gateway.services.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class JwtGlobalFilter implements GlobalFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtGlobalFilter.class);

    private static final String CONST_X_GATEWAY_JWT_TOKEN = "X-GATEWAY-JWT-TOKEN";
    private static final String CONST_X_TERMINATED_BY = "X-TERMINATED-BY";
    private static final String CONST_GATEWAY = "GATEWAY";

    private static final UUID CONST_ANONYMOUS_TENANT_ID = UUID.fromString("fc9af5a5-58f7-42f8-b850-82559a4544f8");
    private static final UUID CONST_ANONYMOUS_USER_ID = UUID.fromString("fc9af5a5-58f7-42f8-b850-82559a4544f8");
    private static final String[] CONST_ANONYMOUS_GRANTS = new String[] {"ROLE_ANONYMOUS"};

    private final JwtProps jwtProps;
    private final TokenService tokenService;

    private final JwtKeyStore jwtKeyStore;

    public JwtGlobalFilter(JwtProps jwtProps, TokenService tokenService) {
        Objects.requireNonNull(jwtProps);
        Objects.requireNonNull(tokenService);

        this.jwtProps = jwtProps;
        this.tokenService = tokenService;

        this.jwtKeyStore = new DefaultKeyStoreProvider(jwtProps.getKeyStore());
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        logger.debug("in the JwtGlobalFilter");

        Optional<JwtDetails> jwtDetailsOptional;

        try {
            jwtDetailsOptional = extractJwtDetails(exchange);
        } catch (TokenExpiredException e) {
            return checkJwtError(exchange, chain, e);
        } catch (JwtProcessingException e) {
            logger.warn("Cannot process user token {}", e.getMessage());
            return checkJwtError(exchange, chain, e);
        }

        if(! jwtDetailsOptional.isPresent()) {
            return chain.filter(exchange);
        }

        JwtDetails jwtDetails = jwtDetailsOptional.get();

        try {
            this.tokenService.verifyToken(jwtDetails);
        } catch ( TokenException e) {
            logger.warn("token with id {} is not valid because: {}", jwtDetails.getTokenId(), e.getMessage());

            // do not let the user into the system if jwt token is not valid
            exchange.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
            exchange.getResponse().getHeaders().add(CONST_X_TERMINATED_BY, CONST_GATEWAY);
            return exchange.getResponse().setComplete();
        }

        // create gateway signed token
        Optional<JwtTokenInfo> jwtTokenInfoOptional = makeJwtTokenInfo(jwtDetails);

        if(! jwtTokenInfoOptional.isPresent()) {
            return chain.filter(exchange);
        }

        return chain.filter(addJwtTokenToRequest(exchange, jwtTokenInfoOptional.get()));
    }

    private Mono<Void> checkJwtError(ServerWebExchange exchange, GatewayFilterChain chain, Throwable e) {
        // if token has expires then let it into the system
        // otherwise to not let invalid tokens in
        if(RootCause.isRootCause(e, TokenExpiredException.class)) {
            return chain.filter(exchange);
        } else {
            exchange.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
            exchange.getResponse().getHeaders().add(CONST_X_TERMINATED_BY, CONST_GATEWAY);
            makeAnonymousJwtCookie().ifPresent(responseCookie -> exchange.getResponse().addCookie(responseCookie));
            return exchange.getResponse().setComplete();
        }
    }

    private Optional<JwtDetails> extractJwtDetails(ServerWebExchange exchange) throws JwtProcessingException {
        HttpCookie cookie = exchange
                .getRequest().getCookies().getFirst(JwtConstants.COOKIE_PARAM_NAME);

        if(cookie == null) {
            return Optional.empty();
        }

        return Optional.of(JwtHelper.processToken(cookie.getValue(), this.jwtKeyStore));
    }

    private ServerWebExchange addJwtTokenToRequest(ServerWebExchange exchange, JwtTokenInfo jwtTokenInfo) {
        ServerHttpRequest.Builder builder = exchange.getRequest().mutate();

        builder.header(CONST_X_GATEWAY_JWT_TOKEN, jwtTokenInfo.getToken());

        return exchange.mutate().request(builder.build()).build();
    }

    private Optional<JwtTokenInfo> makeJwtTokenInfo(JwtDetails jwtDetails) {
        try {
            String defaultKeyId = this.jwtProps.getKeyStore().getDefaultKeyId();
            String privateKey = this.jwtProps.getKeyStore().getKeys().get(defaultKeyId).getPrivateKey();
            String sanitizedPrivateKey = JwtHelper.sanitizePrivateKey(privateKey);

            JwtTokenInfo info = JwtTokenBuilder
                    .create()
                    .expiry(this.jwtProps.getKeyStore().getDefaultExpiry())
                    .tokenId(jwtDetails.getTokenId())
                    .tenantId(jwtDetails.getTenantId())
                    .userId(jwtDetails.getUserId())
                    .grants(ArrayUtils.toArray(jwtDetails.getGrants()))
                    .keyId(defaultKeyId)
                    .privateKey(sanitizedPrivateKey)
                    .build();

            return Optional.ofNullable(info);
        } catch (JwtProcessingException e) {
            logger.warn("Cannot generate gateway jwt token", e);
            return Optional.empty();
        }
    }

    private Optional<ResponseCookie> makeAnonymousJwtCookie() {
        try {
            String defaultKeyId = this.jwtProps.getKeyStore().getDefaultKeyId();
            String privateKey = this.jwtProps.getKeyStore().getKeys().get(defaultKeyId).getPrivateKey();
            String sanitizedPrivateKey = JwtHelper.sanitizePrivateKey(privateKey);

            JwtTokenInfo info = JwtTokenBuilder
                    .create()
                    .expiry(this.jwtProps.getKeyStore().getDefaultExpiry())
                    .tokenId(UUID.randomUUID().toString())
                    .tenantId(CONST_ANONYMOUS_TENANT_ID)
                    .userId(CONST_ANONYMOUS_USER_ID)
                    .grants(CONST_ANONYMOUS_GRANTS)
                    .keyId(defaultKeyId)
                    .privateKey(sanitizedPrivateKey)
                    .build();

            return Optional.of(
                    ResponseCookie.from(JwtConstants.COOKIE_PARAM_NAME, info.getToken()).httpOnly(true).build()
            );
        } catch (JwtProcessingException e) {
            logger.warn("Cannot generate gateway jwt token", e);
            return Optional.empty();
        }
    }
}
