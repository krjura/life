package org.krjura.life.spring.cloud.gateway.enums;

public enum InternalEventNames {

    AMQP_RECEIVER_EXCEPTION,
    AMQP_UNKNOWN_EVENT

    ;
}
