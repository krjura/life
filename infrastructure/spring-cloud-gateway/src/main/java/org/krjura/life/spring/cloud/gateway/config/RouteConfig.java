package org.krjura.life.spring.cloud.gateway.config;

import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.gateway.filter.LoadBalancerClientFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfig {

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/api/v1/auth/**")
                        .filters(f -> f.retry(retryConfig -> retryConfig.setSeries()))
                        .uri("lb://authentication-service")
                )
                .route(r -> r.path("/api/v1/birthdays/**")
                        .filters(f -> f.retry(retryConfig -> retryConfig.setSeries()))
                        .uri("lb://backend-birthdays")
                )
                .route(r -> r.path("/portal/**")
                        .filters(f -> f.retry(retryConfig -> retryConfig.setSeries()))
                        .uri("lb://frontend")
                )
                .build();
    }

    @Bean
    public LoadBalancerClientFilter loadBalancerClient(LoadBalancerClient loadBalancerClient) {
        return new LoadBalancerClientFilter(loadBalancerClient);
    }
}
