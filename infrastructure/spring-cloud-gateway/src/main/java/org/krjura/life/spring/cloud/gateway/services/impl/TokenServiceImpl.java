package org.krjura.life.spring.cloud.gateway.services.impl;

import biz.db.dev.life.jwt.core.JwtDetails;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.krjura.life.spring.cloud.gateway.config.GatewayConfig;
import org.krjura.life.spring.cloud.gateway.ex.TokenException;
import org.krjura.life.spring.cloud.gateway.model.TokenInfo;
import org.krjura.life.spring.cloud.gateway.model.repository.TokenRepository;
import org.krjura.life.spring.cloud.gateway.services.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class TokenServiceImpl implements TokenService {

    private static final Logger logger = LoggerFactory.getLogger(TokenServiceImpl.class);

    private final TokenRepository repository;

    private final Cache<UUID, TokenInfo> revokedTokenCache;

    public TokenServiceImpl(GatewayConfig gatewayConfig, TokenRepository repository) {
        Objects.requireNonNull(gatewayConfig);
        Objects.requireNonNull(repository);

        this.repository = repository;

        this.revokedTokenCache = Caffeine.newBuilder()
                .expireAfterWrite(gatewayConfig.getRevokedTokenCacheExpireAfter(), TimeUnit.MINUTES)
                .maximumSize(gatewayConfig.getGetRevokedTokenCacheMaximumSize())
                .build();
    }


    public void verifyToken(JwtDetails jwtDetails) throws TokenException {
        Optional<TokenInfo> tokenInfoOptional = findToken(jwtDetails);

        if(tokenInfoOptional.isPresent()) {
            throw new TokenException("token has been revoked");
        }
    }

    private Optional<TokenInfo> findToken(JwtDetails jwtDetails) {
        UUID tokenId = UUID.fromString(jwtDetails.getTokenId());

        TokenInfo tokenInfo = this.revokedTokenCache.getIfPresent(tokenId);

        if(tokenInfo != null) {
            logger.debug("token with id {} was loaded from cache", tokenId);
            return Optional.of(tokenInfo);
        }

        Optional<TokenInfo> tokenInfoOptional = repository
                .findTokenById(UUID.fromString(jwtDetails.getTokenId()));

        tokenInfoOptional.ifPresent(info -> this.revokedTokenCache.put(tokenId, info));

        return tokenInfoOptional;
    }

}
