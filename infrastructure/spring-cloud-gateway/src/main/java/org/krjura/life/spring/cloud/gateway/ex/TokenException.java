package org.krjura.life.spring.cloud.gateway.ex;

public class TokenException extends Exception {

    public TokenException(String s) {
        super(s);
    }

    public TokenException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
