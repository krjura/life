package org.krjura.life.spring.cloud.gateway.enums;

public class QueueNames {

    public static final String LOGIN_UPDATES_EXCHANGE = "exchange.login-updates";
    public static final String LOGIN_UPDATES_QUEUE = "gateway.queue.login-updates";
    public static final String LOGIN_UPDATES_RK = "login.updates";
}
