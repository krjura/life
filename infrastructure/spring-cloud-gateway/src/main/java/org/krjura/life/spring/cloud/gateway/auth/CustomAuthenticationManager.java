package org.krjura.life.spring.cloud.gateway.auth;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.List;
import java.util.Objects;

public class CustomAuthenticationManager implements AuthenticationManager {

    private final List<AuthenticationProvider> authenticationProviders;

    public CustomAuthenticationManager(List<AuthenticationProvider> authenticationProviders) {
        Objects.requireNonNull(authenticationProviders);

        this.authenticationProviders = authenticationProviders;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        for(AuthenticationProvider provider : this.authenticationProviders) {
            Authentication generated = provider.authenticate(authentication);

            if(generated != null) {
                return generated;
            }
        }

        throw new ProviderNotFoundException("provider not found");
    }
}
