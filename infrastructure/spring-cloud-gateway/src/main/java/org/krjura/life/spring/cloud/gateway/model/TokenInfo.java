package org.krjura.life.spring.cloud.gateway.model;

import java.time.ZonedDateTime;
import java.util.UUID;

public class TokenInfo {

    private UUID token;

    private ZonedDateTime expiresAt;

    private ZonedDateTime revokedAt;

    public TokenInfo(UUID token, ZonedDateTime expiresAt, ZonedDateTime revokedAt) {
        this.token = token;
        this.expiresAt = expiresAt;
        this.revokedAt = revokedAt;
    }

    public UUID getToken() {
        return token;
    }

    public ZonedDateTime getExpiresAt() {
        return expiresAt;
    }

    public ZonedDateTime getRevokedAt() {
        return revokedAt;
    }
}
