package org.krjura.life.spring.cloud.gateway.cloud.ex;

public class ConsulConfigurationException extends RuntimeException {

    public ConsulConfigurationException(String s) {
        super(s);
    }

    public ConsulConfigurationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
