package org.krjura.life.spring.cloud.gateway.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

    @GetMapping(path = "status")
    public String status() {
        return "OK";
    }
}
