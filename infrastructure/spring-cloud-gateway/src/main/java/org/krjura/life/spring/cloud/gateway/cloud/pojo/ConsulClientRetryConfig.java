package org.krjura.life.spring.cloud.gateway.cloud.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "org.krjura.cloud.tools.consul.config.retry")
public class ConsulClientRetryConfig {

    private boolean retryWhenFailed = true;

    private Integer numberOfRetries = 5;

    private Long pauseBetweenRetries = 5000L;

    public boolean isRetryWhenFailed() {
        return retryWhenFailed;
    }

    public void setRetryWhenFailed(boolean retryWhenFailed) {
        this.retryWhenFailed = retryWhenFailed;
    }

    public Integer getNumberOfRetries() {
        return numberOfRetries;
    }

    public void setNumberOfRetries(Integer numberOfRetries) {
        this.numberOfRetries = numberOfRetries;
    }

    public Long getPauseBetweenRetries() {
        return pauseBetweenRetries;
    }

    public void setPauseBetweenRetries(Long pauseBetweenRetries) {
        this.pauseBetweenRetries = pauseBetweenRetries;
    }
}