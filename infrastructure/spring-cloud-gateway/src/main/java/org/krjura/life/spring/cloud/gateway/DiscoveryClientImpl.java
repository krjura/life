package org.krjura.life.spring.cloud.gateway;

import org.krjura.cloud.tools.consul.client.ConsulClient;
import org.krjura.cloud.tools.consul.client.ex.ConsulException;
import org.krjura.cloud.tools.consul.client.pojo.health.response.ConsulHealthServiceInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.DefaultServiceInstance;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class DiscoveryClientImpl implements DiscoveryClient {

    private static final Logger logger = LoggerFactory.getLogger(DiscoveryClientImpl.class);

    private final ConsulClient client;

    private final Map<String, List<ServiceInstance>> serviceInstanceCache;

    private List<String> serviceCacheEntry;

    public DiscoveryClientImpl(Environment env) {
        this.serviceInstanceCache = new ConcurrentHashMap<>();

        String consulHost = env.getProperty("spring.cloud.consul.host", "127.0.0.1");
        String consulPort = env.getProperty("spring.cloud.consul.port", "8500");
        String token = env.getProperty("spring.cloud.consul.token");

        this.client = new ConsulClient("http://" + consulHost + ":" + consulPort, token);
    }

    @Override
    public String description() {
        return "custom discovery client";
    }

    @Scheduled(fixedDelay = 10_000, initialDelay = 30_000)
    public void updateCaches() {
        updateServiceCache();
        updateInstanceCache();
    }

    private void updateInstanceCache() {
        for(String serviceId : serviceInstanceCache.keySet()) {

            try {
                this.serviceInstanceCache.put(serviceId, fetchInstances(serviceId));
                logger.debug("updated service instance cache for service id {}", serviceId);
            } catch (Exception e) {
                logger.warn("cannot update service instance cache for service id " + serviceId, e);
            }
        }
    }

    private void updateServiceCache() {
        try {
            this.serviceCacheEntry = fetchServices();
            logger.debug("Updated services cache");
        } catch ( Exception e) {
            logger.warn("Cannot update services cache");
        }
    }

    @Override
    public List<ServiceInstance> getInstances(String serviceId) {

        try {
            if(!this.serviceInstanceCache.containsKey(serviceId)) {
                this.serviceInstanceCache.put(serviceId, fetchInstances(serviceId));
            }

            return this.serviceInstanceCache.getOrDefault(serviceId, Collections.emptyList());
        } catch (ConsulException e) {
            logger.warn("Cannot fetch consul instances for service id " + serviceId, e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<String> getServices() {
        try {

            if(this.serviceCacheEntry == null) {
                this.serviceCacheEntry = fetchServices();
            }

            return this.serviceCacheEntry;

        } catch (ConsulException e) {
            logger.warn("Cannot fetch consul services");
            return Collections.emptyList();
        }
    }

    private List<String> fetchServices() throws ConsulException {
        return new ArrayList<>(this
                .client
                .catalog()
                .services()
                .getServices()
                .keySet());
    }

    private List<ServiceInstance> fetchInstances(String serviceId) throws ConsulException {
        return this
                .client
                .health()
                .passingServices(serviceId)
                .getServices()
                .stream()
                .map(this::mapServiceInstance)
                .collect(Collectors.toList());
    }

    private DefaultServiceInstance mapServiceInstance(ConsulHealthServiceInfoResponse instance) {
        return new DefaultServiceInstance(
                instance.getService().getId(),
                instance.getService().getAddress(),
                instance.getService().getPort(),
                false,
                instance.getService().getMeta());
    }
}