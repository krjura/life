package org.krjura.life.spring.cloud.gateway.model.repository.impl;

import org.krjura.life.spring.cloud.gateway.model.TokenInfo;
import org.krjura.life.spring.cloud.gateway.model.repository.TokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Repository
public class TokenRepositoryImpl implements TokenRepository {

    private static final Logger logger = LoggerFactory.getLogger(TokenRepositoryImpl.class);

    private static final TokenInfoMapper TOKEN_INFO_MAPPER = new TokenInfoMapper();

    private static final String QUERY_ADD_TOKEN =
            "INSERT INTO token_info (token_id, expires_at, revoked_at) VALUES (?, ?, ?)";

    private static final String QUERY_FIND_TOKEN_BY_ID =
            "SELECT token_id, expires_at, revoked_at FROM token_info WHERE token_id = ?";

    private final JdbcTemplate jdbcTemplate;

    public TokenRepositoryImpl(JdbcTemplate jdbcTemplate) {
        Objects.requireNonNull(jdbcTemplate);

        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void addTokenInfo(UUID token, ZonedDateTime expiresAt, ZonedDateTime revokedAt) {
        this.jdbcTemplate.update(
                QUERY_ADD_TOKEN,
                token,
                toTimestamp(expiresAt),
                toTimestamp(revokedAt)
        );
    }

    @Override
    public Optional<TokenInfo> findTokenById(UUID tokenId) {

        try {
            return Optional.of(this
                    .jdbcTemplate.queryForObject(QUERY_FIND_TOKEN_BY_ID, new Object[]{tokenId}, TOKEN_INFO_MAPPER));
        } catch ( EmptyResultDataAccessException e ) {
            return Optional.empty();
        }
    }

    private static class TokenInfoMapper implements RowMapper<TokenInfo> {

        @Override
        public TokenInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new TokenInfo(
                    (UUID) rs.getObject("token_id"),
                    toZonedDateTime(rs.getTimestamp("expires_at")),
                    toZonedDateTime(rs.getTimestamp("revoked_at"))
            );
        }
    }

    private static Timestamp toTimestamp(ZonedDateTime timestamp) {
        return new Timestamp(timestamp.toInstant().toEpochMilli());
    }

    private static ZonedDateTime toZonedDateTime(Timestamp timestamp) {
        if(timestamp == null) {
            return null;
        }

        return ZonedDateTime.ofInstant(timestamp.toInstant(), ZoneOffset.UTC);
    }
}