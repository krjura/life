package org.krjura.life.spring.cloud.gateway.config;

import org.krjura.life.commons.enums.Privileges;
import org.krjura.life.spring.cloud.gateway.auth.CustomAuthenticationManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.ReactiveAuthenticationManagerAdapter;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

import java.util.List;

@Configuration
public class WebSecurityConfig {

    @Bean
    public AuthenticationManager customAuthenticationManager(List<AuthenticationProvider> providers) {
        return new CustomAuthenticationManager(providers);
    }

    @Bean
    public ReactiveAuthenticationManager reactiveAuthenticationManagerAdapter(
            AuthenticationManager authenticationManager) {
        return new ReactiveAuthenticationManagerAdapter(authenticationManager);
    }

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http
                .authorizeExchange()
                    .pathMatchers("/actuator/health").permitAll()
                    .pathMatchers("/actuator").hasAuthority(Privileges.MONITORING.name())
                    .pathMatchers("/actuator/**").hasAuthority(Privileges.MONITORING.name())
                    .anyExchange().permitAll()
                .and()
                    .httpBasic()
                .and()
                    .csrf()
                    .disable()
                .build();
    }

}
