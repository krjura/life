package org.krjura.life.spring.cloud.gateway.events;

import org.krjura.life.events.BasicEvent;
import org.krjura.life.events.EventMetadata;

public class UnknownEvent extends BasicEvent {

    public UnknownEvent(EventMetadata metadata) {
        super(metadata);
    }
}
