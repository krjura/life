package org.krjura.life.spring.cloud.gateway;

import biz.db.dev.life.exceptions.WaitingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.krjura.life.events.BasicEvent;
import org.krjura.life.ier.EventRecorder;
import org.krjura.life.ier.Recorder;
import org.krjura.life.json.utils.JsonUtils;
import org.krjura.life.spring.cloud.gateway.enums.QueueNames;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ProjectTest
public abstract class TestBase {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    Recorder recorder;

    @Before
    public void initTestBase() {
        this.recorder = EventRecorder.instance().record();
    }

    @After
    public void destroyTestBase() {
        this.recorder.unsubscribe();
    }

    protected <T> T fromJson(String content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz);
    }

    protected <T> T fromJson(byte[] content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz);
    }

    protected  <T> T waitUntilThereIsAMessageInLoginUpdatesQueue(Class<T> clazz) throws IOException {
        Message message = this.rabbitTemplate.receive(QueueNames.LOGIN_UPDATES_QUEUE, 30000);

        if(message == null) {
            throw new WaitingException();
        }

        return fromJson(message.getBody(), clazz);
    }

    protected void sendEventToLoginUpdatesQueue(BasicEvent event) throws JsonProcessingException {
        byte[] messageData = JsonUtils.from(event).getBytes();

        MessageProperties properties = MessagePropertiesBuilder
                .newInstance()
                .setContentType(MediaType.APPLICATION_JSON_VALUE)
                .setCorrelationId(event.getMetadata().getCorrelationId())
                .setMessageId(event.getMetadata().getMessageId())
                .setType(event.getClass().getSimpleName())
                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                .build();

        Message message = MessageBuilder
                .withBody(messageData)
                .andProperties(properties)
                .build();


        this.rabbitTemplate.send(QueueNames.LOGIN_UPDATES_EXCHANGE, QueueNames.LOGIN_UPDATES_RK, message);
    }
}
