package org.krjura.life.spring.cloud.gateway;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.awaitility.Awaitility;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.krjura.life.commons.time.FormattingUtils;
import org.krjura.life.events.EventMetadata;
import org.krjura.life.events.TokenRevokedEvent;
import org.krjura.life.ier.EventRecorder;
import org.krjura.life.ier.InternalEvent;
import org.krjura.life.ier.Recorder;
import org.krjura.life.spring.cloud.gateway.enums.InternalEventNames;
import org.krjura.life.spring.cloud.gateway.events.UnknownEvent;
import org.krjura.life.spring.cloud.gateway.model.TokenInfo;
import org.krjura.life.spring.cloud.gateway.model.repository.TokenRepository;
import org.krjura.life.validations.ex.ValidationException;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class TokenRevocationEventTest extends TestBase {

    @Autowired
    private TokenRepository tokenRepository;

    @Test
    public void amqpReceive_unknownEvent() throws JsonProcessingException {
        EventMetadata metadata = new EventMetadata(null, null, null, null);

        sendEventToLoginUpdatesQueue(new UnknownEvent(metadata));

        this.recorder.waitUntilThereAreEvents(1);

        List<InternalEvent> events = this.recorder.getEvents();
        assertThat(events).hasSize(1);

        assertThat(events.get(0).getName()).isEqualTo(InternalEventNames.AMQP_UNKNOWN_EVENT.name());
        assertThat(events.get(0).getValue()).isInstanceOf(Message.class);
    }

    @Test
    public void tokenRevocationEvent_notValid_ignored() throws JsonProcessingException {
        EventMetadata metadata = new EventMetadata(null, null, null, null);

        TokenRevokedEvent event = new TokenRevokedEvent(metadata, null, null, null);

        sendEventToLoginUpdatesQueue(event);

        this.recorder.waitUntilThereAreEvents(1);

        List<InternalEvent> events = this.recorder.getEvents();
        assertThat(events).hasSize(1);

        assertThat(events.get(0).getName()).isEqualTo(InternalEventNames.AMQP_RECEIVER_EXCEPTION.name());
        assertThat(events.get(0).getValue()).isInstanceOf(ValidationException.class);
    }

    @Test
    public void tokenRevocationEvent_savedInTokenInfoTable() throws JsonProcessingException {

        // when
        ZonedDateTime now = ZonedDateTime.now();
        UUID tokenIdUuid = UUID.randomUUID();
        String tokenId = tokenIdUuid.toString();
        String messageId = tokenIdUuid.toString();
        String timestamp = FormattingUtils.from(now);

        EventMetadata metadata = new EventMetadata(messageId, messageId, "1.0.0", timestamp);

        TokenRevokedEvent event = new TokenRevokedEvent(metadata, tokenId, timestamp, timestamp);

        sendEventToLoginUpdatesQueue(event);

        // then

        // wait until data is in the database
        Awaitility
                .await()
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> tokenRepository.findTokenById(tokenIdUuid).isPresent());

        //  check token values
        Optional<TokenInfo> tokenInfoOptional = tokenRepository.findTokenById(tokenIdUuid);
        assertThat(tokenInfoOptional).isPresent();

        TokenInfo tokenInfo = tokenInfoOptional.get();
        assertThat(tokenInfo.getToken()).isEqualTo(tokenIdUuid);
        assertThat(tokenInfo.getRevokedAt().toInstant().toEpochMilli()).isEqualTo(now.toInstant().toEpochMilli());
        assertThat(tokenInfo.getExpiresAt().toInstant().toEpochMilli()).isEqualTo(now.toInstant().toEpochMilli());
    }
}
