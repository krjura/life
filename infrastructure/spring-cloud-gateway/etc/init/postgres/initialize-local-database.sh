#!/bin/bash

set -o xtrace

source etc/init/postgres/client-local.sh

psql -f etc/liquidbase/init/create-databases.sql