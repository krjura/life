#!/bin/bash

set -o xtrace

source etc/init/postgres/client-docker.sh

for i in {1..10}
do
    if psql -c "select 1" > /dev/null
    then
        echo "Postgres is up"
        exit 0;
    else
        echo "Postgres is down"
        sleep 1
    fi
done