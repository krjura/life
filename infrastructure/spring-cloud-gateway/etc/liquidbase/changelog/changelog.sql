--changeset v20180710073700
CREATE TABLE token_info (
  token_id UUID NOT NULL,
  expires_at TIMESTAMP WITH TIME ZONE NOT NULL,
  revoked_at TIMESTAMP WITH TIME ZONE NOT NULL,
  CONSTRAINT pk_token_revocation PRIMARY KEY (token_id)
)