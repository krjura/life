package main

import (
	"flag"
	"log"
	"io/ioutil"
	"os"
	"strings"
	"./git"
)

func main() {
    var op = flag.String("op", "", "op to execute");
    var directory = flag.String("directory", "", "directory to use");
    var module = flag.String("module", "", "module to use");
	var branch = flag.String("branch", "", "branch to use");
	var version = flag.String("version", "", "version to use");

	flag.Parse()

    switch *op {
        case "add":
            addOperation(*directory, *module, *branch, *version)
        case "commit":
            commitOperation(*directory, *branch)
        default:
        		log.Print("Invalid op provided. Please check the manual");
    }
}

func commitOperation(directory string, branch string) {
    checkVar("directory", directory)
    checkVar("branch", branch)

    if(branch != "master") {
        log.Print("Refusing to commit code on a non master branch")
        os.Exit(0) // not an error
    }

    var result = git.CheckGitStatus(directory)

    if(result) {
        log.Print("Changes where detected. Committing and pushing code")
        git.CommitAndPush(directory)
    } else {
        log.Print("No changes where detected")
    }
}

func addOperation(directory string, module string, branch string, version string) {
    checkVar("directory", directory)
    checkVar("module", module)
    checkVar("branch", branch)
    checkVar("version", version)

    var commit = git.GetGitCommit()

    var contentArgs = []string{"module=", module, "\n", "branch=", branch, "\n", "version=", version, "\n", "commit=", commit}
    var content = strings.Join(contentArgs, "")
    log.Print("content was \n", content)

    writeFile(directory, module, branch, version, content)
}

func checkVar(name string, value string) {
    if len(value) == 0 {
    	log.Print("value for ", name, " is not defined");
        os.Exit(1)
    }
}

func writeFile(directory string, module string, branch string, version string, content string) {

    d1 := []byte(content)

    var targetDir = directory + "/" + module + "/" + branch
    var target = targetDir + "/" + version

    mkdirError := os.MkdirAll(targetDir, 0777)

    if mkdirError != nil {
    	log.Println("Cannot create target directory", targetDir, ". Error was '", mkdirError, "'")
    	os.Exit(1)
    }

    writeError := ioutil.WriteFile(target, d1, 0644)

    if writeError != nil {
    	log.Println("Cannot write target file. Error was: '", writeError, "'")
    	os.Exit(1)
    }
}