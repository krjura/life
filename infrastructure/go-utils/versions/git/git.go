package git

import (
    "bytes"
	"log"
	"os"
	"os/exec"
)

func GetGitCommit() string {
    cmd := exec.Command("git", "rev-parse", "HEAD")

    // save output
    var out bytes.Buffer
    cmd.Stdout = &out
    cmd.Stderr = os.Stderr

    err := cmd.Run()

    if err != nil {
    	log.Println("Cannot fetch git commit. Error was: '", err, "'")
    	os.Exit(1)
    }

    return out.String()
}

func CheckGitStatus(directory string) bool {
    cmd := exec.Command("git", "status", "--short")
    cmd.Dir = directory

    // save output
    var out bytes.Buffer
    cmd.Stdout = &out
    cmd.Stderr = os.Stderr

    err := cmd.Run()

    if err != nil {
        log.Println("Cannot execute git status. Error was: '", err, "'")
        os.Exit(1)
    }

    var result = out.String()
    length := len(result)
    log.Print("The following changes where detected: \n", result)

    return length > 0
}

func CommitAndPush(directory string) {
    GitAddAll(directory)
    GitCommit(directory)
    GitPush(directory)
}

func GitAddAll(directory string) {
    cmd := exec.Command("git", "add", ".")

    cmd.Dir = directory
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr

    err := cmd.Run()
    if err != nil {
        log.Println("Cannot add files to git. Errors where: '", err, "'")
    	os.Exit(1)
    }

    log.Println("Added all files to git project")
}

func GitCommit(directory string) {
    cmd := exec.Command("git", "commit", "-m", "adding changes to version files")

    cmd.Dir = directory
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr

    err := cmd.Run()
    if err != nil {
        log.Println("Cannot commit files to git. Errors where: '", err, "'")
    	os.Exit(1)
    }

    log.Println("Committing all changes")
}

func GitPush(directory string) {
    cmd := exec.Command("git", "push", "--set-upstream", "origin", "master")

    cmd.Dir = directory
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr

    err := cmd.Run()
    if err != nil {
        log.Println("Cannot push files to git. Errors where: '", err, "'")
    	os.Exit(1)
    }

    log.Println("Pushed all changes to remote")
}