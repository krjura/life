#!/bin/bash

CURRENT_DIR=$(pwd)
CURRENT_UUID=$(id -u $USERNAME)
CURRENT_GUID=$(id -g $USERNAME)

COMMAND="docker run -v $CURRENT_DIR:/opt/workspace -u $CURRENT_UUID:$CURRENT_GUID -it golang:1.9-stretch bash"

echo $COMMAND
$COMMAND


