package gradle

import (
    "log"
    "strings"
)

func GenerateSpringProfile(env string) string {

	var springProfile = "test"

	if env == "jenkins" {
		springProfile = "test,jenkins"
	} else {
		springProfile = "test"
	}

	log.Print("spring.profiles.active = ", springProfile)

	return springProfile
}

func GenerateGradleBuildCommand(springProfile string, module string) []string {
    var args []string

    args = append(args, "--no-daemon")
    args = append(args, "-Dspring.profiles.active=" + springProfile)
    args = parseModuleString(args, module, "build")

	return args
}

func GenerateGradlePublishProjectCommand(gitBranch string, springProfile string, module string) []string {

    var operation string

    if gitBranch == "master" {
        log.Print("Project is on master branch. Project will also be publish if required")
    	operation = "publishProject"
    } else {
        log.Print("Project is on " + gitBranch + " branch. Project will not be published")
    	operation = "build"
    }

    var args []string
    args = append(args, "--no-daemon")
    args = append(args, "-Dspring.profiles.active=" + springProfile)
    args = parseModuleString(args, module, operation)

	return args
}

func GenerateGradleInstallDistCommand(springProfile string, module string) []string {
    var args []string

    args = append(args, "--no-daemon")
    args = append(args, "-Dspring.profiles.active=" + springProfile)
    args = parseModuleString(args, module, "installDist")

	return args
}

func GenerateGradlePublishCommand(gitBranch string, springProfile string, module string) []string {
    var operation string

    if gitBranch == "master" {
        log.Print("Project is on master branch. Project will also be published if required")
    	operation = "publish"
    } else {
        log.Print("Project is on " + gitBranch + " branch. Project will not be published")
    	operation = "build"
    }

    var args []string

    args = append(args, "--no-daemon")
    args = append(args, "-Dspring.profiles.active=" + springProfile)
    args = parseModuleString(args, module, operation)

	return args
}

func parseModuleString(args []string, module string, operation string) []string {
    modules := strings.Split(module, ",")

    for _, element := range modules {
        args = append(args, element + ":" + operation)
    }

    return args
}