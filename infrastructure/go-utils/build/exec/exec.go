package exec

import (
	"os/exec"
	"log"
	"os"
)

func ExecuteCommandAndExit(command string, directory string, args[] string) {
	cmd := exec.Command(command, args...)

	log.Println("Running command '", command, "' with arguments '", args, "'")

	//cmd.Dir = directory;
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		log.Println("error was: '", err, "'")
		os.Exit(1)
	} else {
		log.Println("command completed")
	}
}

func ExecuteScript(directory string, file string) {
	cmd := exec.Command(file)

	log.Println("Running command '", file, "'")

	cmd.Dir = directory;
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		log.Println("error was: '", err, "'")
		os.Exit(1)
	} else {
		log.Println("command completed")
	}
}