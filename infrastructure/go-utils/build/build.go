package main

import (
	"flag"
	"os"
	"log"
	"./gradle"
	"./exec"
)

func main() {
	var module = flag.String("module", "", "module to use");
	var env = flag.String("env", "", "env type");
	var branch = flag.String("branch", "", "branch");
	var op = flag.String("op", "build", "what am I supposed to do now?")
	var file = flag.String("file", "", "file to use")

	flag.Parse()

	verifyModule(*module)

	springProfile := gradle.GenerateSpringProfile(*env)
	gitBranch := *branch

	switch *op {
	case "build":
		executeGradleBuild(springProfile, *module)
	case "publish":
		executeGradlePublish(gitBranch, springProfile, *module)
	case "installDist":
	    executeGradleInstallDist(springProfile, *module)
	case "publishProject":
		verifyBranch(gitBranch)
		executeGradlePublishProject(gitBranch, springProfile, *module)
	case "script":
		verifyFile(*file)
		exec.ExecuteScript(*module, *file)
	default:
		log.Print("Invalid op provided. Please check the manual");
	}
}

func executeGradleInstallDist(springProfile string, module string) {
	log.Print("Executing gradle installDist")
	args := gradle.GenerateGradleInstallDistCommand(springProfile, module)
	exec.ExecuteCommandAndExit("./gradlew", module, args)
}

func executeGradlePublishProject(gitBranch string, springProfile string, module string) {
	log.Print("Executing gradle publishProject")
	args := gradle.GenerateGradlePublishProjectCommand(gitBranch, springProfile, module)
	exec.ExecuteCommandAndExit("./gradlew", module, args)
}

func executeGradlePublish(gitBranch string, springProfile string, module string) {
	log.Print("Executing gradle publish")
	args := gradle.GenerateGradlePublishCommand(gitBranch, springProfile, module)
	exec.ExecuteCommandAndExit("./gradlew", module, args)
}

func executeGradleBuild(springProfile string, module string) {
	log.Print("Executing gradle build")
	args := gradle.GenerateGradleBuildCommand(springProfile, module)
	exec.ExecuteCommandAndExit("./gradlew", module, args)
}

func verifyModule(module string) {

	if len(module) == 0 {
		log.Print("module is not defined");
		os.Exit(1)
	}
}

func verifyFile(file string) {

	if len(file) == 0 {
		log.Print("file is not defined");
		os.Exit(1)
	}
}

func verifyBranch(gitBranch string) {

	if len(gitBranch) == 0 {
		log.Print("gitBranch is not defined");
		os.Exit(1)
	}
}