#!/bin/bash

docker run -d --hostname local --name rabbit-mq -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 15672:15672 -p 25672:25672 docker.krjura.org/life/rabbit-mq:v1