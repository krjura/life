FROM centos:centos7

LABEL "maintainer"="tb2b-devel@ingemark.com" \
	"software"="consul" \
	"consul-version"="0.9.2"

RUN yum -y update \
	&& yum -y install sudo less vim-enhanced telnet wget unzip \
	&& yum clean all \
	&& rm -rf /var/cache/* /tmp/* /var/tmp/*

RUN wget -P /opt/consul https://releases.hashicorp.com/consul/0.9.2/consul_0.9.2_linux_amd64.zip \
	&& unzip -d /opt/consul /opt/consul/consul_0.9.2_linux_amd64.zip \
	&& rm /opt/consul/consul_0.9.2_linux_amd64.zip

COPY scripts/consul.config /opt/consul/consul.config
COPY scripts/run.sh /opt/consul/run.sh

RUN yum -y update \
	&& yum -y install python-setuptools \
	&& easy_install supervisor \
	&& yum clean all \
	&& rm -rf /var/cache/* /tmp/* /var/tmp/*

COPY scripts/supervisord.conf /etc/supervisor/supervisord.conf

EXPOSE 8500

CMD ["/usr/bin/supervisord"]