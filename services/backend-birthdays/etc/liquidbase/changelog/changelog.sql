--liquibase formatted sql

--changeset kjurasovic:v20171022151900
CREATE TABLE birthday (
  id  BIGSERIAL NOT NULL,

  birthday_day INTEGER NOT NULL,
  birthday_month INTEGER NOT NULL,
  birthday_year INTEGER NOT NULL,

  person_info VARCHAR(100) NOT NULL,
  note VARCHAR(200) NOT NULL,

  CONSTRAINT pk_birthday PRIMARY KEY (id)
);
--rollback DROP TABLE birthday


--changeset kjurasovic:20171029192100
ALTER TABLE birthday ADD COLUMN user_id INTEGER NOT NULL DEFAULT 0;
--rollback ALTER TABLE DROP COLUMN user_id


--changeset kjurasovic:20171110195600
CREATE TABLE birthday_reminder (
  id  BIGSERIAL NOT NULL,
  birthday_id BIGINT NOT NULL,

  type VARCHAR(16) NOT NULL,
  units INTEGER NOT NULL,
  remind_at TIME WITHOUT TIME ZONE NOT NULL,
  repeatable BOOLEAN NOT NULL,

  CONSTRAINT pk_birthday_reminder PRIMARY KEY (id),
  CONSTRAINT fk_birthday_reminder_birthday FOREIGN KEY (birthday_id) REFERENCES birthday (id)
);
--rollback DROP TABLE birthday_reminder

--changeset kjurasovic:20171204205400
ALTER TABLE birthday DROP COLUMN user_id;
ALTER TABLE birthday ADD COLUMN user_id UUID NOT NULL DEFAULT '520e3d66-fa71-4f24-9c40-24e6874dc9dd';
--rollback ALTER TABLE birthday DROP COLUMN user_id;
--rollback ALTER TABLE birthday ADD COLUMN user_id INTEGER NOT NULL DEFAULT 0;

--changeset kjurasovic:20180210113600

ALTER TABLE birthday ADD COLUMN born_on DATE;
ALTER TABLE birthday ADD COLUMN next_birthday DATE;

UPDATE birthday SET born_on = make_date(birthday_year, birthday_month, birthday_day), next_birthday = make_date(birthday_year, birthday_month, birthday_day);

ALTER TABLE birthday DROP COLUMN birthday_day;
ALTER TABLE birthday DROP COLUMN birthday_month;
ALTER TABLE birthday DROP COLUMN birthday_year;

ALTER TABLE birthday ALTER COLUMN born_on SET NOT NULL;
ALTER TABLE birthday ALTER COLUMN next_birthday SET NOT NULL;

--changeset kjurasovic:20180211134300

CREATE TABLE INT_LOCK  (
  LOCK_KEY CHAR(36),
  REGION VARCHAR(100),
  CLIENT_ID CHAR(36),
  CREATED_DATE TIMESTAMP NOT NULL,
  constraint LOCK_PK primary key (LOCK_KEY, REGION)
);

--changeset kjurasovic:20180227205200

CREATE TABLE cluster_tools_keep_alive  (
  node_id VARCHAR(255) NOT NULL,
  last_updated TIMESTAMP WITH TIME ZONE,

  CONSTRAINT pk_cluster_tools_keep_alive PRIMARY KEY (node_id)
);

--changeset kjurasovic:20180228222100
DROP TABLE INT_LOCK;

CREATE TABLE cluster_lock  (
  lock_key VARCHAR(36),
  client_id VARCHAR(36),
  created_date TIMESTAMP WITH TIME ZONE NOT NULL,
  constraint pk_cluster_lock primary key (lock_key)
);

--changeset kjurasovic:20180603131200

CREATE TABLE cluster_tools_request_queue (
  id UUID NOT NULL,

  request_type VARCHAR(100) NOT NULL,
  request_data BYTEA NOT NULL,
  request_metadata BYTEA NOT NULL,

  node_id VARCHAR(255) NULL,
  assigned_on TIMESTAMP WITH TIME ZONE NULL,

  state VARCHAR(16) NOT NULL,
  execute_on TIMESTAMP WITH TIME ZONE NOT NULL,
  retries INT NOT NULL,

  CONSTRAINT pk_cluster_tools_request_queue PRIMARY KEY (id)
);

CREATE UNIQUE INDEX idx_cluster_tools_request_queue_execute ON cluster_tools_request_queue (state, execute_on);

--changeset kjurasovic:v20180911205900
ALTER TABLE cluster_lock ADD COLUMN node_id VARCHAR(255) NULL;

--changeset kjurasovic:v20180918094500
DROP INDEX idx_cluster_tools_request_queue_execute;
CREATE INDEX idx_cluster_tools_request_queue_execute ON cluster_tools_request_queue (state, execute_on);