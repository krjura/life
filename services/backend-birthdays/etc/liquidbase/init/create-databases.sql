DROP DATABASE IF EXISTS birthdays;
CREATE DATABASE birthdays ENCODING 'UTF-8';

DROP DATABASE IF EXISTS birthdays_tst;
CREATE DATABASE birthdays_tst ENCODING 'UTF-8';

DROP USER IF EXISTS birthdays;
CREATE USER birthdays WITH PASSWORD 'birthdays';

DROP USER IF EXISTS birthdays_tst;
CREATE USER birthdays_tst WITH PASSWORD 'birthdays_tst';