#!/usr/bin/env bash

# Since bash does not propagate signals properly we need to do this so docker can stop the container properly
_term() {
  echo "Caught SIGTERM signal in docker-entrypoint.sh!"
  MAIN_PID=$(cat $MAIN_PID_FILE)
  kill $MAIN_PID 2>/dev/null
  # must wait for the script to exit
  wait $RUN_SCRIPT_PID
}

trap _term SIGTERM

#Start application
export MAIN_PID_FILE=$APPLICATION_HOME/tmp/application.pid
bin/standalone.sh &
RUN_SCRIPT_PID=$!
wait $RUN_SCRIPT_PID