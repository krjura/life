package biz.db.dev.life.birtdays.controllers.pojo.birthdays.request;

import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;
import org.krjura.life.validations.BasicEnumConstraint;
import org.krjura.life.validations.PositiveIntegerConstraint;
import org.krjura.life.validations.TimeConstraint;

import javax.validation.constraints.NotNull;

public class BirthdayReminderRequest {

    @BasicEnumConstraint
    private BirthdayReminderType type;

    @PositiveIntegerConstraint
    private Integer units;

    @TimeConstraint
    private String remindAt;

    @NotNull
    private Boolean repeatable;

    public BirthdayReminderType getType() {
        return type;
    }

    public void setType(BirthdayReminderType type) {
        this.type = type;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public Boolean getRepeatable() {
        return repeatable;
    }

    public void setRepeatable(Boolean repeatable) {
        this.repeatable = repeatable;
    }

    // fluent

    public BirthdayReminderRequest type(final BirthdayReminderType type) {
        setType(type);
        return this;
    }

    public BirthdayReminderRequest units(final Integer units) {
        setUnits(units);
        return this;
    }

    public BirthdayReminderRequest remindAt(final String remindAt) {
        setRemindAt(remindAt);
        return this;
    }

    public BirthdayReminderRequest repeatable(final Boolean repeatable) {
        setRepeatable(repeatable);
        return this;
    }
}
