package biz.db.dev.life.birtdays.model;

import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.time.LocalTime;

@Entity
@Table(name = "birthday_reminder")
@NamedQueries({
        @NamedQuery(
                name = "BirthdayReminder.findByBirthdayId",
                query = "SELECT br FROM BirthdayReminder br " +
                        "INNER JOIN br.birthday b " +
                        "WHERE b.id = :birthdayId " +
                        "AND b.userId = :userId"
        ),
        @NamedQuery(
                name = "BirthdayReminder.findById",
                query = "SELECT br FROM BirthdayReminder br " +
                        "INNER JOIN br.birthday b " +
                        "WHERE br.id = :reminderId " +
                        "AND b.id = :birthdayId " +
                        "AND b.userId = :userId"
        ),
        @NamedQuery(
                name = "BirthdayReminder.deleteById",
                query = "DELETE FROM BirthdayReminder br " +
                        "WHERE EXISTS (" +
                        " SELECT b.id " +
                        " FROM Birthday b " +
                        " WHERE b.id = :birthdayId " +
                        " AND br.id = :reminderId " +
                        " AND b.userId = :userId)"
        ),
        @NamedQuery(
                name = "BirthdayReminder.deleteByBirthdayId",
                query = "DELETE FROM BirthdayReminder br " +
                        "WHERE EXISTS (" +
                        " SELECT b.id " +
                        " FROM Birthday b " +
                        " WHERE br.birthdayId = :birthdayId " +
                        " AND b.id = :birthdayId " +
                        " AND b.userId = :userId)"
        )
})
public class BirthdayReminder {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column(name = "birthday_id", nullable = false)
    private Long birthdayId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "birthday_id", insertable = false, updatable = false)
    private Birthday birthday;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 16)
    private BirthdayReminderType type;

    @Column(name = "units", nullable = false)
    private Integer units;

    @Column(name = "remind_at", nullable = false)
    private LocalTime remindAt;

    @Column(name = "repeatable", nullable = false)
    private boolean repeatable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBirthdayId() {
        return birthdayId;
    }

    public void setBirthdayId(Long birthdayId) {
        this.birthdayId = birthdayId;
    }

    public Birthday getBirthday() {
        return birthday;
    }

    public void setBirthday(Birthday birthday) {
        this.birthday = birthday;
    }

    public BirthdayReminderType getType() {
        return type;
    }

    public void setType(BirthdayReminderType type) {
        this.type = type;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public LocalTime getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(LocalTime remindAt) {
        this.remindAt = remindAt;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public void setRepeatable(boolean repeatable) {
        this.repeatable = repeatable;
    }

    // fluent
    public BirthdayReminder id(final Long id) {
        setId(id);
        return this;
    }

    public BirthdayReminder birthdayId(final Long birthdayId) {
        setBirthdayId(birthdayId);
        return this;
    }

    public BirthdayReminder birthday(final Birthday birthday) {
        setBirthday(birthday);
        return this;
    }

    public BirthdayReminder type(final BirthdayReminderType type) {
        setType(type);
        return this;
    }

    public BirthdayReminder units(final Integer units) {
        setUnits(units);
        return this;
    }

    public BirthdayReminder remindAt(final LocalTime remindAt) {
        setRemindAt(remindAt);
        return this;
    }

    public BirthdayReminder repeatable(final boolean repeatable) {
        setRepeatable(repeatable);
        return this;
    }
}
