package biz.db.dev.life.birtdays.controllers;

import biz.db.dev.life.birtdays.controllers.pojo.birthdays.request.BirthdayReminderRequest;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayReminderResponse;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayRemindersResponse;
import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.services.BirthdayReminderService;
import biz.db.dev.life.birtdays.services.pojo.PagingRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.AddNewBirthdayReminderServiceRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.UpdateBirthdayReminderServiceRequest;
import biz.db.dev.life.birtdays.utils.JwtUtils;
import biz.db.dev.life.birtdays.utils.SupplierUtil;
import biz.db.dev.life.exceptions.ResourceDoesNotExists;
import biz.db.dev.life.exceptions.ServiceException;
import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPostMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPutMapping;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Controller
public class BirthdayReminderController {

    private final BirthdayReminderService birthdayReminderService;

    public BirthdayReminderController(BirthdayReminderService birthdayReminderService) {
        Assert.notNull(birthdayReminderService, "BirthdayReminderService cannot be null");

        this.birthdayReminderService = birthdayReminderService;
    }

    @JsonPostMapping(path = "/api/v1/birthdays/{birthdayId}/reminders")
    public ResponseEntity<BirthdayReminderResponse> createBirthdayReminder(
            @PathVariable( name = "birthdayId") Long birthdayId,
            @RequestBody @Valid BirthdayReminderRequest request)
            throws ServiceException {

        UUID userId = JwtUtils.extractUserId();

        AddNewBirthdayReminderServiceRequest serviceRequest = new AddNewBirthdayReminderServiceRequest()
                .userId(userId)
                .birthdayId(birthdayId)
                .type(request.getType())
                .units(request.getUnits())
                .remindAt(request.getRemindAt())
                .repeatable(request.getRepeatable());

        BirthdayReminder reminder = this.birthdayReminderService.addBirthdayReminder(serviceRequest);

        return new ResponseEntity<>(new BirthdayReminderResponse(reminder), HttpStatus.CREATED);
    }

    @JsonGetMapping(path = "/api/v1/birthdays/{birthdayId}/reminders/{reminderId}")
    public ResponseEntity<BirthdayReminderResponse> readBirthdayReminder(
            @PathVariable( name = "birthdayId") Long birthdayId,
            @PathVariable( name = "reminderId") Long reminderId) throws ResourceDoesNotExists {

        UUID userId = JwtUtils.extractUserId();

        BirthdayReminder reminder = this.birthdayReminderService
                .findById(userId, birthdayId, reminderId)
                .orElseThrow(SupplierUtil
                        .throwResourceNotFoundWhenReminderNotFound(userId, birthdayId, reminderId));

        return ResponseEntity.ok(new BirthdayReminderResponse(reminder));
    }

    @JsonGetMapping(path = "/api/v1/birthdays/{birthdayId}/reminders")
    public ResponseEntity<BirthdayRemindersResponse> readBirthdayReminders(
            @PathVariable( name = "birthdayId") Long birthdayId,
            Pageable pageable) {

        UUID userId = JwtUtils.extractUserId();

        PagingRequest pagingRequest = new PagingRequest(pageable, 15);

        List<BirthdayReminder> reminders = this
                .birthdayReminderService
                .findByBirthdayId(userId, birthdayId, pagingRequest);

        return ResponseEntity.ok(new BirthdayRemindersResponse(reminders, pagingRequest));
    }

    @DeleteMapping(path = "/api/v1/birthdays/{birthdayId}/reminders/{reminderId}")
    public ResponseEntity<Void> deleteBirthdayReminder(
            @PathVariable( name = "birthdayId") Long birthdayId,
            @PathVariable( name = "reminderId") Long reminderId) {

        UUID userId = JwtUtils.extractUserId();

        this.birthdayReminderService.deleteById(userId, birthdayId, reminderId);

        return ResponseEntity.noContent().build();
    }

    @JsonPutMapping(path = "/api/v1/birthdays/{birthdayId}/reminders/{reminderId}")
    public ResponseEntity<BirthdayReminderResponse> updateBirthdayReminder(
            @PathVariable( name = "birthdayId") Long birthdayId,
            @PathVariable( name = "reminderId") Long reminderId,
            @RequestBody @Valid BirthdayReminderRequest request) throws ServiceException {

        UUID userId = JwtUtils.extractUserId();

        UpdateBirthdayReminderServiceRequest serviceRequest = new UpdateBirthdayReminderServiceRequest()
                .userId(userId)
                .birthdayId(birthdayId)
                .reminderId(reminderId)
                .type(request.getType())
                .units(request.getUnits())
                .remindAt(request.getRemindAt())
                .repeatable(request.getRepeatable());

        BirthdayReminder reminder = this.birthdayReminderService.updateBirthdayReminder(serviceRequest);

        return ResponseEntity.ok(new BirthdayReminderResponse(reminder));
    }
}