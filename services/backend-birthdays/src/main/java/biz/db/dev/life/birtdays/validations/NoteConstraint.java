package biz.db.dev.life.birtdays.validations;

import org.hibernate.validator.constraints.Length;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = { })
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention( RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@NotNull
@Length( min = 1, max = 200)
public @interface NoteConstraint {

    String message() default "life.NoteConstraint.message";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
