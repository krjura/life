package biz.db.dev.life.birtdays.controllers;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class DocumentationController {

    private static final String CONST_DOCS_API = "docs/api/main.html";

    @GetMapping(path = "/api/v1/birthdays/docs/api")
    public ResponseEntity<FileSystemResource> docsIndex() {
        Path path = Paths.get("docs/api/main.html");

        if(!Files.exists(path)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new FileSystemResource(CONST_DOCS_API));
    }
}
