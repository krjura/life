package biz.db.dev.life.birtdays.controllers.pojo.birthdays.response;

import biz.db.dev.life.birtdays.model.Birthday;

import java.util.List;
import java.util.stream.Collectors;

public class BirthdayResponses {

    private List<BirthdayResponse> content;

    public BirthdayResponses() {
        // mapping
    }

    public BirthdayResponses(List<Birthday> birthdays) {
        this.content = birthdays
                .stream()
                .map(BirthdayResponse::new)
                .collect(Collectors.toList());
    }

    public List<BirthdayResponse> getContent() {
        return content;
    }

    public void setContent(List<BirthdayResponse> content) {
        this.content = content;
    }
}
