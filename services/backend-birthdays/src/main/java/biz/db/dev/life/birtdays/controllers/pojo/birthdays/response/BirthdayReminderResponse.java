package biz.db.dev.life.birtdays.controllers.pojo.birthdays.response;

import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;

import java.time.format.DateTimeFormatter;

public class BirthdayReminderResponse {

    private Long id;

    private Long birthdayId;

    private BirthdayReminderType type;

    private Integer units;

    private String remindAt;

    private boolean repeatable;

    public BirthdayReminderResponse() {
        // mappers
    }

    public BirthdayReminderResponse(BirthdayReminder reminder) {
        this.id = reminder.getId();
        this.birthdayId = reminder.getBirthdayId();
        this.type = reminder.getType();
        this.units = reminder.getUnits();
        this.remindAt = reminder.getRemindAt().format(DateTimeFormatter.ISO_LOCAL_TIME);
        this.repeatable = reminder.isRepeatable();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBirthdayId() {
        return birthdayId;
    }

    public void setBirthdayId(Long birthdayId) {
        this.birthdayId = birthdayId;
    }

    public BirthdayReminderType getType() {
        return type;
    }

    public void setType(BirthdayReminderType type) {
        this.type = type;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public void setRepeatable(boolean repeatable) {
        this.repeatable = repeatable;
    }
}
