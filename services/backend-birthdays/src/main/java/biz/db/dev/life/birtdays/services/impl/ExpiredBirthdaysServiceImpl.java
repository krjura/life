package biz.db.dev.life.birtdays.services.impl;

import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.birtdays.services.ExpiredBirthdaysService;
import biz.db.dev.life.birtdays.utils.DateUtils;
import org.krjura.life.cluster.tools.locking.services.LocalLockingService;
import org.krjura.life.commons.dao.DefaultPagingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.locks.Lock;

@Service
public class ExpiredBirthdaysServiceImpl implements ExpiredBirthdaysService {

    private static Logger logger = LoggerFactory.getLogger(ExpiredBirthdaysServiceImpl.class);

    private static final String LOCK_EXPIRED_BIRTHDAYS = "ExpiredBirthdaysService";

    private final BirthdayRepository birthdayRepository;

    private final LocalLockingService localLockingService;

    public ExpiredBirthdaysServiceImpl(
            BirthdayRepository birthdayRepository,
            LocalLockingService localLockingService) {

        Assert.notNull(birthdayRepository, "BirthdayRepository cannot be null");
        Assert.notNull(localLockingService, "LocalLockingService cannot be null");

        this.birthdayRepository = birthdayRepository;
        this.localLockingService = localLockingService;
    }

    /**
     * Next birthday should always be updated to the latest value
     * In order to prevent large bursts on the database we will limit max entries to 50
     * this should allow us to update 50 * 60min * 25h * 365d ~ 26M entries over the course of year
     */
    @Override
    @Scheduled(fixedDelay = 60000, initialDelay = 30000)
    public void onSchedule() {

        try {
            localLockingService.doWithLock(LOCK_EXPIRED_BIRTHDAYS, this::updateBirthdays);
        } catch ( Exception e) {
            logger.warn("Cannot update expired birthdays", e);
        }
    }

    private void updateBirthdays() {
        LocalDate now = LocalDate.now();

        DefaultPagingRequest defaultRequest = new DefaultPagingRequest(0, 50);
        PageRequest pageRequest = PageRequest.of(defaultRequest.getPage(), defaultRequest.getPageSize());

        List<Birthday> birthdays = this.birthdayRepository.findWhereNextBirthdayAfter(now, pageRequest);
        logger.info("Found {} birthdays in need of next birthday update", birthdays.size());

        birthdays
            .stream()
            .filter(birthday -> birthday.getNextBirthday().isBefore(now))
            .forEach(birthday -> {
                this.birthdayRepository.updateNextBirthday(birthday.getId(), DateUtils.next(birthday.getBornOn()));
            });
    }
}
