package biz.db.dev.life.birtdays.controllers.pojo.birthdays.response;

import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.utils.DateUtils;

import java.util.UUID;

public class BirthdayResponse {

    private Long id;

    private UUID userId;

    private long bornOn;

    private long nextBirthday;

    private String personInfo;

    private String note;

    public BirthdayResponse() {
        // hibernate / jackson
    }

    public BirthdayResponse(Birthday birthday) {
        this.id = birthday.getId();
        this.userId = birthday.getUserId();
        this.bornOn = DateUtils.toEpocMilis(birthday.getBornOn());
        this.nextBirthday = DateUtils.toEpocMilis(birthday.getNextBirthday());
        this.personInfo = birthday.getPersonInfo();
        this.note = birthday.getNote();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public long getBornOn() {
        return bornOn;
    }

    public void setNextBirthday(long nextBirthday) {
        this.nextBirthday = nextBirthday;
    }

    public long getNextBirthday() {
        return nextBirthday;
    }

    public void setBornOn(long bornOn) {
        this.bornOn = bornOn;
    }

    public String getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(String personInfo) {
        this.personInfo = personInfo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
