package biz.db.dev.life.birtdays.controllers.pojo.birthdays.response;

import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.services.pojo.PagingRequest;

import java.util.List;
import java.util.stream.Collectors;

public class BirthdayRemindersResponse {

    private List<BirthdayReminderResponse> content;

    private PagingResponse paging;

    public BirthdayRemindersResponse() {
        // mappers
    }

    public BirthdayRemindersResponse(List<BirthdayReminder> reminders, PagingRequest pagingRequest) {
        this.content = reminders
                .stream()
                .map(BirthdayReminderResponse::new)
                .collect(Collectors.toList());
        this.paging = new PagingResponse(pagingRequest, reminders.size());
    }

    public List<BirthdayReminderResponse> getContent() {
        return content;
    }

    public void setContent(List<BirthdayReminderResponse> content) {
        this.content = content;
    }

    public PagingResponse getPaging() {
        return paging;
    }

    public void setPaging(PagingResponse paging) {
        this.paging = paging;
    }
}
