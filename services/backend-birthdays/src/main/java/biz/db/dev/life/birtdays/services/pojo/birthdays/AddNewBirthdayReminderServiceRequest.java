package biz.db.dev.life.birtdays.services.pojo.birthdays;

import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;

import java.util.UUID;

public class AddNewBirthdayReminderServiceRequest {

    private UUID userId;

    private Long birthdayId;

    private BirthdayReminderType type;

    private Integer units;

    private String remindAt;

    private Boolean repeatable;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Long getBirthdayId() {
        return birthdayId;
    }

    public void setBirthdayId(Long birthdayId) {
        this.birthdayId = birthdayId;
    }

    public BirthdayReminderType getType() {
        return type;
    }

    public void setType(BirthdayReminderType type) {
        this.type = type;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public String getRemindAt() {
        return remindAt;
    }

    public void setRemindAt(String remindAt) {
        this.remindAt = remindAt;
    }

    public Boolean getRepeatable() {
        return repeatable;
    }

    public void setRepeatable(Boolean repeatable) {
        this.repeatable = repeatable;
    }

    // fluent
    public AddNewBirthdayReminderServiceRequest userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public AddNewBirthdayReminderServiceRequest birthdayId(final Long birthdayId) {
        setBirthdayId(birthdayId);
        return this;
    }

    public AddNewBirthdayReminderServiceRequest type(final BirthdayReminderType type) {
        setType(type);
        return this;
    }

    public AddNewBirthdayReminderServiceRequest units(final Integer units) {
        setUnits(units);
        return this;
    }

    public AddNewBirthdayReminderServiceRequest remindAt(final String remindAt) {
        setRemindAt(remindAt);
        return this;
    }

    public AddNewBirthdayReminderServiceRequest repeatable(final Boolean repeatable) {
        setRepeatable(repeatable);
        return this;
    }
}
