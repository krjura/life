package biz.db.dev.life.birtdays.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name = "birthday")
@NamedQueries({
        @NamedQuery(
                name = "Birthday.findAll",
                query = "FROM Birthday"
        ),
        @NamedQuery(
                name = "Birthday.deleteBirthday",
                query = "DELETE FROM Birthday WHERE userId = :userId AND id = :id"
        ),
        @NamedQuery(
                name = "Birthday.findBirthday",
                query = "FROM Birthday WHERE userId = :userId AND id = :id"
        ),
        @NamedQuery(
                name = "Birthday.findBirthdayByUserId",
                query = "FROM Birthday b WHERE b.userId = :userId " +
                        "AND ( :searchTerm IS NULL OR b.personInfo LIKE :searchTerm ) " +
                        "ORDER BY nextBirthday ASC"
        ),
        @NamedQuery(
                name = "Birthday.findWhereNextBirthdayAfter",
                query = "FROM Birthday b " +
                        "WHERE b.nextBirthday < :threashold"
        ),
        @NamedQuery(
                name = "Birthday.updateNextBirthday",
                query = "UPDATE Birthday b " +
                        "SET b.nextBirthday = :nextBirthday " +
                        "WHERE b.id = :id"
        )
})
public class Birthday {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column(name = "user_id", nullable = false)
    private UUID userId;

    @Column(name = "born_on", nullable = false)
    private LocalDate bornOn;

    @Column(name = "next_birthday", nullable = false)
    private LocalDate nextBirthday;

    @Column(name = "person_info", length = 100, nullable = false)
    private String personInfo;

    @Column(name = "note", length = 200, nullable = false)
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Birthday id(Long id) {
        this.id = id;

        return this;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Birthday userId(UUID usedId) {
        this.userId = usedId;

        return this;
    }

    public LocalDate getBornOn() {
        return bornOn;
    }

    public void setBornOn(LocalDate bornOn) {
        this.bornOn = bornOn;
    }

    public Birthday bornOn(LocalDate bornOn) {
        this.bornOn = bornOn;

        return this;
    }

    public LocalDate getNextBirthday() {
        return nextBirthday;
    }

    public void setNextBirthday(LocalDate nextBirthday) {
        this.nextBirthday = nextBirthday;
    }

    public Birthday nextBirthday(LocalDate nextBirthday) {
        this.nextBirthday = nextBirthday;

        return this;
    }

    public String getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(String personInfo) {
        this.personInfo = personInfo;
    }

    public Birthday personInfo(String personInfo) {
        this.personInfo = personInfo;

        return this;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Birthday note(String note) {
        this.note = note;

        return this;
    }
}
