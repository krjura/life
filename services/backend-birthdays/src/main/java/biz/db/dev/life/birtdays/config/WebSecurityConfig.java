package biz.db.dev.life.birtdays.config;

import biz.db.dev.life.jwt.core.config.JwtProps;
import biz.db.dev.life.jwt.core.filters.JwtAuthenticationFilter;
import org.krjura.life.commons.enums.Privileges;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.util.Objects;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtProps jwtProps;

    private final AuthenticationManager authenticationManager;

    public WebSecurityConfig(JwtProps jwtProps, AuthenticationManager authenticationManager) {
        Objects.requireNonNull(jwtProps);
        Objects.requireNonNull(authenticationManager);

        this.jwtProps = jwtProps;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/actuator/health").permitAll()
                    .antMatchers("/actuator").hasAnyAuthority(Privileges.MONITORING.name())
                    .antMatchers("/actuator/**").hasAnyAuthority(Privileges.MONITORING.name())
                    .antMatchers("/api/v1/auth/docs/**").permitAll()
                    .antMatchers("/**").hasAuthority(Privileges.PRIV_USER.name());

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);

        http.addFilterBefore(
                new JwtAuthenticationFilter(this.jwtProps, this.authenticationManager),
                BasicAuthenticationFilter.class);

        http
                .csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());

        http
                .httpBasic()
                .authenticationEntryPoint(new Http403ForbiddenEntryPoint());

        http.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
    }
}