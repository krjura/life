package biz.db.dev.life.birtdays.services;

import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.services.pojo.PagingRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.AddNewBirthdayReminderServiceRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.UpdateBirthdayReminderServiceRequest;
import biz.db.dev.life.exceptions.ServiceException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BirthdayReminderService {

    BirthdayReminder addBirthdayReminder(AddNewBirthdayReminderServiceRequest serviceRequest) throws ServiceException;

    Optional<BirthdayReminder> findById(UUID userId, Long birthdayId, Long reminderId);

    List<BirthdayReminder> findByBirthdayId(UUID userId, Long birthdayId, PagingRequest pagingRequest);

    void deleteById(UUID userId, Long birthdayId, Long reminderId);

    void deleteByBirthdayId(UUID userId, Long birthdayId);

    BirthdayReminder updateBirthdayReminder(UpdateBirthdayReminderServiceRequest serviceRequest)
            throws ServiceException;
}
