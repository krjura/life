package biz.db.dev.life.birtdays.services;

import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.services.pojo.birthdays.BirthdayServiceRequest;
import org.krjura.life.commons.dao.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BirthdayService {

    void addBirthday(BirthdayServiceRequest request);

    void deleteBirthday(UUID userId, Long id);

    Optional<Birthday> findBirthday(UUID userId, Long id);

    Optional<Birthday> updateBirthday(Long id, BirthdayServiceRequest request);

    List<Birthday> listBirthdays(UUID userId, String searchTerm, Pageable pageable);
}
