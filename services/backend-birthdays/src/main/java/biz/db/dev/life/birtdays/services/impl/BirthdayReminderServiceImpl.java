package biz.db.dev.life.birtdays.services.impl;

import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.model.repository.BirthdayReminderRepository;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.birtdays.services.BirthdayReminderService;
import biz.db.dev.life.birtdays.services.pojo.PagingRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.AddNewBirthdayReminderServiceRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.UpdateBirthdayReminderServiceRequest;
import biz.db.dev.life.birtdays.utils.SupplierUtil;
import biz.db.dev.life.exceptions.ResourceDoesNotExists;
import biz.db.dev.life.exceptions.ServiceException;
import org.krjura.life.commons.time.FormattingUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BirthdayReminderServiceImpl implements BirthdayReminderService {

    private final BirthdayRepository birthdayRepository;

    private final BirthdayReminderRepository reminderRepository;

    public BirthdayReminderServiceImpl(
            BirthdayRepository birthdayRepository,
            BirthdayReminderRepository reminderRepository) {

        Assert.notNull(birthdayRepository, "BirthdayRepository cannot be null");
        Assert.notNull(reminderRepository, "BirthdayReminderRepository cannot be null");

        this.birthdayRepository = birthdayRepository;
        this.reminderRepository = reminderRepository;
    }

    @Override
    @Transactional
    public BirthdayReminder addBirthdayReminder(AddNewBirthdayReminderServiceRequest serviceRequest)
            throws ResourceDoesNotExists {

        Birthday birthday =  this
                .birthdayRepository
                .findBirthday(serviceRequest.getUserId(), serviceRequest.getBirthdayId())
                .orElseThrow(SupplierUtil.throwResourceNotFound(serviceRequest));


        BirthdayReminder birthdayReminder = new BirthdayReminder()
                .birthdayId(birthday.getId())
                .type(serviceRequest.getType())
                .units(serviceRequest.getUnits())
                .remindAt(FormattingUtils.parseLocalTime(serviceRequest.getRemindAt()))
                .repeatable(serviceRequest.getRepeatable());

        return this.reminderRepository.save(birthdayReminder);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BirthdayReminder> findById(UUID userId, Long birthdayId, Long reminderId) {
        return this.reminderRepository.findById(userId, birthdayId, reminderId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BirthdayReminder> findByBirthdayId(UUID userId, Long birthdayId, PagingRequest pagingRequest) {

        Pageable pageable = PageRequest.of(pagingRequest.getPage(), pagingRequest.getMaxPerPage());

        return this.reminderRepository.findByBirthdayId(userId, birthdayId, pageable);
    }

    @Override
    @Transactional
    public void deleteById(UUID userId, Long birthdayId, Long reminderId) {
        this.reminderRepository.deleteById(userId, birthdayId, reminderId);
    }

    @Override
    @Transactional
    public void deleteByBirthdayId(UUID userId, Long birthdayId) {
        this.reminderRepository.deleteByBirthdayId(userId,  birthdayId);
    }

    @Override
    @Transactional
    public BirthdayReminder updateBirthdayReminder(UpdateBirthdayReminderServiceRequest serviceRequest)
            throws ServiceException {

        BirthdayReminder reminder = findById(
                serviceRequest.getUserId(), serviceRequest.getBirthdayId(), serviceRequest.getReminderId())
                .orElseThrow(SupplierUtil.throwResourceNotFound(serviceRequest));

        reminder
                .type(serviceRequest.getType())
                .units(serviceRequest.getUnits())
                .remindAt(FormattingUtils.parseLocalTime(serviceRequest.getRemindAt()))
                .repeatable(serviceRequest.getRepeatable());

        return this.reminderRepository.save(reminder);
    }
}
