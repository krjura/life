package biz.db.dev.life.birtdays.services.pojo;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.Assert;

public class PagingRequest {

    private Integer maxPerPage;

    private Integer page;

    private Integer offset;

    public PagingRequest() {
        this.maxPerPage = Integer.MAX_VALUE;
        this.page = 0;
        this.offset = 0;
    }

    public PagingRequest(Pageable pageable, Integer maxPerPage) {
        Assert.notNull(pageable, "Pageable cannot be null");
        Assert.notNull(maxPerPage, "maxPerPage cannot be null");

        int limitedMaxPerPage = Math.min(pageable.getPageSize(), maxPerPage);
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), limitedMaxPerPage);

        this.maxPerPage = pageRequest.getPageSize();
        this.page = pageRequest.getPageNumber();
        this.offset = (int) pageRequest.getOffset();
    }

    public Integer getMaxPerPage() {
        return maxPerPage;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getOffset() {
        return offset;
    }
}
