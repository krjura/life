package biz.db.dev.life.birtdays.controllers.pojo.birthdays.response;

import biz.db.dev.life.birtdays.services.pojo.PagingRequest;

public class PagingResponse {

    private Integer maxPerPage;

    private Integer pageNumber;

    private Integer numberOfResults;

    public PagingResponse() {
        // mapping
    }

    public PagingResponse(PagingRequest pagingRequest, Integer numberOfResults) {
        this.maxPerPage = pagingRequest.getMaxPerPage();
        this.pageNumber = pagingRequest.getPage();
        this.numberOfResults = numberOfResults;
    }


    public Integer getMaxPerPage() {
        return maxPerPage;
    }

    public void setMaxPerPage(Integer maxPerPage) {
        this.maxPerPage = maxPerPage;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getNumberOfResults() {
        return numberOfResults;
    }

    public void setNumberOfResults(Integer numberOfResults) {
        this.numberOfResults = numberOfResults;
    }

    // fluid
    public PagingResponse maxPerPage(final Integer maxPerPage) {
        setMaxPerPage(maxPerPage);
        return this;
    }

    public PagingResponse pageNumber(final Integer pageNumber) {
        setPageNumber(pageNumber);
        return this;
    }

    public PagingResponse results(final Integer results) {
        setNumberOfResults(results);
        return this;
    }
}
