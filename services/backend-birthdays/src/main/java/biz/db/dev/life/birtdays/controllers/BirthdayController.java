package biz.db.dev.life.birtdays.controllers;

import biz.db.dev.life.birtdays.controllers.pojo.birthdays.request.BirthdayRequest;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayResponse;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayResponses;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.services.BirthdayService;
import biz.db.dev.life.birtdays.services.pojo.birthdays.BirthdayServiceRequest;
import biz.db.dev.life.birtdays.utils.JwtUtils;
import biz.db.dev.life.exceptions.ServiceException;
import org.krjura.life.commons.dao.DefaultPagingRequest;
import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPostMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPutMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Controller
public class BirthdayController {

    private final BirthdayService birthdayService;

    public BirthdayController(BirthdayService birthdayService) {
        Assert.notNull(birthdayService, "BirthdayService cannot be null");

        this.birthdayService = birthdayService;
    }

    @JsonGetMapping(path = "/api/v1/birthdays/{id}")
    public ResponseEntity<BirthdayResponse> readBirthday(@PathVariable( value = "id") Long id) {
        UUID userId = JwtUtils.extractUserId();

        Optional<Birthday> birthdayOptional = this.birthdayService.findBirthday(userId, id);

        if(birthdayOptional.isPresent()) {
            return ResponseEntity.ok(new BirthdayResponse(birthdayOptional.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @JsonGetMapping(path = "/api/v1/birthdays")
    public ResponseEntity<BirthdayResponses> listBirthday(
            @RequestParam(value = "searchTerm", required = false) String searchTerm,
            DefaultPagingRequest pagingRequest) throws ServiceException {

        UUID userId = JwtUtils.extractUserId();

        return ResponseEntity.ok(new BirthdayResponses(this
                .birthdayService
                .listBirthdays(userId, searchTerm, pagingRequest)));
    }

    @JsonPostMapping(path = "/api/v1/birthdays")
    public ResponseEntity<Void> createBirthday(@RequestBody @Valid BirthdayRequest request) throws ServiceException {
        UUID userId = JwtUtils.extractUserId();

        BirthdayServiceRequest serviceRequest = new BirthdayServiceRequest()
                .userId(userId)
                .date(request.getBirthday())
                .personInfo(request.getPersonInfo())
                .note(request.getNote());

        this.birthdayService.addBirthday(serviceRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/api/v1/birthdays/{id}")
    public ResponseEntity<Void> deleteBirthday(@PathVariable( value = "id") Long id) {
        UUID userId = JwtUtils.extractUserId();

        this.birthdayService.deleteBirthday(userId, id);

        return ResponseEntity.noContent().build();
    }

    @JsonPutMapping(path = "/api/v1/birthdays/{id}")
    public ResponseEntity<BirthdayResponse> updateBirthday(
            @PathVariable( value = "id") Long id,
            @RequestBody @Valid BirthdayRequest request) throws ServiceException {

        UUID userId = JwtUtils.extractUserId();

        BirthdayServiceRequest serviceRequest = new BirthdayServiceRequest()
                .userId(userId)
                .date(request.getBirthday())
                .personInfo(request.getPersonInfo())
                .note(request.getNote());

        Optional<Birthday> birthdayOptional = this.birthdayService.updateBirthday(id, serviceRequest);

        if(birthdayOptional.isPresent()) {
            return ResponseEntity.ok(new BirthdayResponse(birthdayOptional.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
