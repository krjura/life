package biz.db.dev.life.birtdays.controllers.pojo.birthdays.request;

import biz.db.dev.life.birtdays.validations.NoteConstraint;
import biz.db.dev.life.birtdays.validations.PersonInfoConstraint;
import org.krjura.life.validations.DateConstraint;

public class BirthdayRequest {

    @DateConstraint
    private String birthday;

    @PersonInfoConstraint
    private String personInfo;

    @NoteConstraint
    private String note;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(String personInfo) {
        this.personInfo = personInfo;
    }

    // fluent
    public BirthdayRequest birthday(final String birthday) {
        setBirthday(birthday);
        return this;
    }

    public BirthdayRequest personInfo(final String personInfo) {
        setPersonInfo(personInfo);
        return this;
    }

    public BirthdayRequest note(final String note) {
        setNote(note);
        return this;
    }
}
