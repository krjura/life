package biz.db.dev.life.birtdays.model.enums;

public enum BirthdayReminderType {

    DAYS,
    WEEKS,
    MONTHS
}
