package biz.db.dev.life.birtdays.model.repository;

import biz.db.dev.life.birtdays.model.BirthdayReminder;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BirthdayReminderRepository extends CrudRepository<BirthdayReminder, Long> {

    @Query("SELECT br " +
            "FROM BirthdayReminder br " +
            "INNER JOIN br.birthday b " +
            "WHERE b.id = :birthdayId AND b.userId = :userId")
    @Transactional(readOnly = true)
    List<BirthdayReminder> findByBirthdayId(
            @Param("userId") UUID userId, @Param("birthdayId") Long birthdayId, Pageable pageable);

    @Query("SELECT br " +
            "FROM BirthdayReminder br " +
            "INNER JOIN br.birthday b " +
            "WHERE br.id = :reminderId AND b.id = :birthdayId AND b.userId = :userId")
    @Transactional(readOnly = true)
    Optional<BirthdayReminder> findById(
            @Param("userId") UUID userId, @Param("birthdayId") Long birthdayId, @Param("reminderId") Long reminderId);

    @Query("DELETE FROM BirthdayReminder br " +
            "WHERE EXISTS (" +
            "SELECT b.id " +
            "FROM Birthday b " +
            "WHERE b.id = :birthdayId AND br.id = :reminderId AND b.userId = :userId)")
    @Modifying
    @Transactional
    void deleteById(
            @Param("userId") UUID userId, @Param("birthdayId") Long birthdayId, @Param("reminderId") Long reminderId);

    @Query("DELETE FROM BirthdayReminder br " +
            "WHERE EXISTS (" +
            "SELECT b.id " +
            "FROM Birthday b " +
            "WHERE br.birthdayId = :birthdayId AND b.id = :birthdayId AND b.userId = :userId)")
    @Modifying
    @Transactional
    void deleteByBirthdayId(@Param("userId") UUID userId, @Param("birthdayId") Long birthdayId);
}
