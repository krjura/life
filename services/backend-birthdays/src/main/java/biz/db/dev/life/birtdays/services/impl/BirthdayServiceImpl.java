package biz.db.dev.life.birtdays.services.impl;

import biz.db.dev.life.birtdays.ex.DaoException;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.birtdays.services.BirthdayReminderService;
import biz.db.dev.life.birtdays.services.BirthdayService;
import biz.db.dev.life.birtdays.services.pojo.birthdays.BirthdayServiceRequest;
import biz.db.dev.life.birtdays.utils.DateUtils;
import org.krjura.life.commons.dao.Pageable;
import org.krjura.life.commons.time.FormattingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BirthdayServiceImpl implements BirthdayService {

    private static final Logger logger = LoggerFactory.getLogger(BirthdayServiceImpl.class);

    private final BirthdayRepository birthdayRepository;

    private final BirthdayReminderService reminderService;

    public BirthdayServiceImpl(BirthdayRepository birthdayRepository, BirthdayReminderService reminderService) {
        Assert.notNull(birthdayRepository, "BirthdayRepository cannot be null");
        Assert.notNull(reminderService, "BirthdayReminderService cannot be null");

        this.birthdayRepository = birthdayRepository;
        this.reminderService = reminderService;
    }

    @Override
    @Transactional
    public void addBirthday(BirthdayServiceRequest request) {

        LocalDate localDate = FormattingUtils.parseLocalDate(request.getDate());
        LocalDate nextBirthday = DateUtils.next(localDate);

        Birthday birthday = new Birthday()
                .userId(request.getUserId())
                .bornOn(localDate)
                .nextBirthday(nextBirthday)
                .personInfo(request.getPersonInfo())
                .note(request.getNote());


        try {
            this.birthdayRepository.save(birthday);
        } catch ( Exception e) {
            logger.warn("Cannot save data to database", e);

            throw new DaoException("Cannot save data to database", e);
        }
    }

    @Override
    @Transactional
    public void deleteBirthday(UUID userId, Long birthdayId) {
        this.reminderService.deleteByBirthdayId(userId, birthdayId);
        this.birthdayRepository.deleteBirthday(userId, birthdayId);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Birthday> findBirthday(UUID userId, Long id) {
        return this.birthdayRepository.findBirthday(userId, id);
    }

    @Override
    @Transactional
    public Optional<Birthday> updateBirthday(Long id, BirthdayServiceRequest request) {

        Optional<Birthday> birthdayOptional = this.birthdayRepository.findBirthday(request.getUserId(), id);

        if(!birthdayOptional.isPresent()) {
            return Optional.empty();
        }

        LocalDate localDate = FormattingUtils.parseLocalDate(request.getDate());
        LocalDate nextBirthday = DateUtils.next(localDate);

        Birthday birthday = birthdayOptional
                .get()
                .bornOn(localDate)
                .nextBirthday(nextBirthday)
                .personInfo(request.getPersonInfo())
                .note(request.getNote());

        return Optional.of(this.birthdayRepository.save(birthday));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Birthday> listBirthdays(UUID userId, String searchTerm, Pageable pageable) {
        PageRequest pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());

        String searchTermString = searchTerm == null ? null : "%" + searchTerm + "%";
        return this.birthdayRepository.findByUserId(userId, searchTermString, pageRequest);
    }
}