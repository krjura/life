package biz.db.dev.life.birtdays.utils;

import biz.db.dev.life.birtdays.i18n.LocalizationKeys;
import biz.db.dev.life.birtdays.services.pojo.birthdays.AddNewBirthdayReminderServiceRequest;
import biz.db.dev.life.birtdays.services.pojo.birthdays.UpdateBirthdayReminderServiceRequest;
import biz.db.dev.life.exceptions.ResourceDoesNotExists;

import java.util.UUID;
import java.util.function.Supplier;

public class SupplierUtil {

    private SupplierUtil() {
        // util
    }

    public static Supplier<ResourceDoesNotExists> throwResourceNotFound(
            final AddNewBirthdayReminderServiceRequest serviceRequest) {

        return () -> new ResourceDoesNotExists(
                "birthdayId",
                serviceRequest.getBirthdayId().toString(),
                LocalizationKeys.CREATE_BIRTHDAY_REMINDER_NOT_FOUND,
                "birthday not found for user with id of " + serviceRequest.getUserId() +
                        " and birthday id of " + serviceRequest.getBirthdayId());
    }

    public static Supplier<ResourceDoesNotExists> throwResourceNotFoundWhenReminderNotFound(
            UUID userId, Long birthdayId, Long reminderId) {

        return () -> new ResourceDoesNotExists(
                "reminderId",
                reminderId.toString(),
                LocalizationKeys.CREATE_BIRTHDAY_REMINDER_NOT_FOUND,
                "reminder not found for user with id of " + userId +
                        " and birthday id of " + birthdayId +
                        " and reminder if of " + reminderId);
    }

    public static Supplier<ResourceDoesNotExists> throwResourceNotFound(UpdateBirthdayReminderServiceRequest serviceRequest) {
        return () -> new ResourceDoesNotExists(
                "reminderId",
                serviceRequest.getReminderId().toString(),
                LocalizationKeys.CREATE_BIRTHDAY_REMINDER_NOT_FOUND,
                "reminder not found for user with id of " + serviceRequest.getUserId() +
                        " and birthday id of " + serviceRequest.getBirthdayId() +
                        " and reminder if of " + serviceRequest.getReminderId());
    }
}
