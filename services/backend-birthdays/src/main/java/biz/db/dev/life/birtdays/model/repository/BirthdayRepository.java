package biz.db.dev.life.birtdays.model.repository;

import biz.db.dev.life.birtdays.model.Birthday;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BirthdayRepository extends CrudRepository<Birthday, Long> {

    @Query("SELECT b FROM Birthday b")
    @Transactional(readOnly = true)
    List<Birthday> findAll();

    @Query("DELETE FROM Birthday WHERE userId = :userId AND id = :id")
    @Modifying
    @Transactional
    void deleteBirthday(@Param("userId") UUID userId, @Param("id") Long id);

    @Query("SELECT b FROM Birthday b WHERE b.userId = :userId AND b.id = :id")
    @Transactional(readOnly = true)
    Optional<Birthday> findBirthday(@Param("userId") UUID userId, @Param("id") Long id);

    @Query("FROM Birthday b " +
            "WHERE b.userId = :userId AND ( :searchTerm IS NULL OR b.personInfo LIKE :searchTerm) " +
            "ORDER BY nextBirthday ASC")
    @Transactional(readOnly = true)
    List<Birthday> findByUserId(@Param("userId") UUID userId, @Param("searchTerm") String searchTerm, Pageable pageable);

    @Query("FROM Birthday b WHERE b.nextBirthday < :threshold")
    @Transactional(readOnly = true)
    List<Birthday> findWhereNextBirthdayAfter(@Param("threshold") LocalDate threshold, Pageable pagingRequest);

    @Query("UPDATE Birthday b SET b.nextBirthday = :next WHERE b.id = :id")
    @Modifying
    @Transactional
    void updateNextBirthday(@Param("id") Long id, @Param("next") LocalDate next);
}
