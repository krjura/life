package biz.db.dev.life.birtdays.services.pojo.birthdays;

import java.util.UUID;

public class BirthdayServiceRequest {

    private UUID userId;

    private String date;

    private String personInfo;

    private String note;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(String personInfo) {
        this.personInfo = personInfo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    //fluent

    public BirthdayServiceRequest userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public BirthdayServiceRequest date(final String date) {
        setDate(date);
        return this;
    }

    public BirthdayServiceRequest personInfo(final String personInfo) {
        setPersonInfo(personInfo);
        return this;
    }

    public BirthdayServiceRequest note(final String note) {
        setNote(note);
        return this;
    }

}
