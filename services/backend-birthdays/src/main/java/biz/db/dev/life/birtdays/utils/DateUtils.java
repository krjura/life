package biz.db.dev.life.birtdays.utils;

import java.time.LocalDate;
import java.time.ZoneOffset;

public class DateUtils {

    private DateUtils() {
        // mapping
    }

    public static long toEpocMilis(LocalDate date) {
        return date.atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    public static LocalDate next(LocalDate localDate) {
        LocalDate now = LocalDate.now();

        // take current year and copy month and day to get next birthday
        LocalDate next = LocalDate.of(now.getYear(), localDate.getMonth(), localDate.getDayOfMonth());

        // if the result is still before current day increase by year
        if(next.isBefore(now)) {
            next = next.plusYears(1);
        }

        return next;
    }
}