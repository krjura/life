package biz.db.dev.life.birtdays.ex;

import org.hibernate.service.spi.ServiceException;

public class DaoException extends ServiceException {

    public DaoException(String s) {
        super(s);
    }

    public DaoException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
