package biz.db.dev.life.birtdays;


import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.mvc.error.handlers.MvcErrorHandlers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackageClasses = {
        Birthday.class
})
@ComponentScan( basePackageClasses = {
        BirthdaysRoot.class,
        MvcErrorHandlers.class
})
@EnableScheduling
public class BirthdaysRoot {

    public static void main(String[] args) {
        BirthdaysRoot application = new BirthdaysRoot();
        application.runSpring(args);
    }

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(BirthdaysRoot.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }
}
