package biz.db.dev.life.birtdays.integration.controllers;

import biz.db.dev.life.birtdays.support.TestBase;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayReminderResponse;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayRemindersResponse;
import biz.db.dev.life.birtdays.i18n.LocalizationKeys;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;
import biz.db.dev.life.birtdays.model.repository.BirthdayReminderRepository;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorDetails;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponse;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatus;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatusCode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

public class BirthdayReminderControllerTest extends TestBase {

    private static final String DEFAULT_USER_ID_STRING = "e133b63a-8e89-445a-b0d7-54d98bcac0d8";

    @Autowired
    private BirthdayRepository birthdayRepository;

    @Autowired
    private BirthdayReminderRepository reminderRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void createBirthdayReminderSuccessfully() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();

        String content = contentOf("requests/valid/BirthdayReminderRequest-v1-valid-1.json");

        // execute
        mockMvc.perform(
                post("/api/v1/birthdays/" + birthday.getId() + "/reminders")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andReturn();

        // verify
        List<BirthdayReminder> reminders = reminderRepository.findByBirthdayId(
                birthday.getUserId(),
                birthday.getId(),
                PageRequest.of(0, Integer.MAX_VALUE));

        assertThat(reminders, hasSize(1));
        BirthdayReminder reminder = reminders.get(0);

        assertThat(reminder.getBirthdayId(), is(equalTo(birthday.getId())));
        assertThat(reminder.getType(), is(equalTo(BirthdayReminderType.DAYS)));
        assertThat(reminder.getUnits(), is(equalTo(1)));
        assertThat(reminder.getRemindAt(), is(equalTo(LocalTime.of(9, 0, 0))));
    }

    @Test
    public void createBirthdayReminderWhenNotAuthorized() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();

        String content = contentOf("requests/valid/BirthdayReminderRequest-v1-valid-1.json");

        // execute
        mockMvc.perform(
                post("/api/v1/birthdays/" + birthday.getId() + "/reminders")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void createBirthdayReminderWhenBirthdayCannotBeFound() throws Exception {
        String content = contentOf("requests/valid/BirthdayReminderRequest-v1-valid-1.json");

        // execute
        MvcResult mvcResult = mockMvc.perform(
                post("/api/v1/birthdays/1/reminders")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isNotFound())
                .andReturn();

        // validate
        ErrorResponse response = fromJson(mvcResult.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(response, is(notNullValue()));
        assertThat(response.getStatus(), is(equalTo(ErrorResponseStatus.ERROR)));
        assertThat(response.getStatusCode(), is(equalTo(ErrorResponseStatusCode.BAD_REQUEST.getCode())));
        assertThat(response.getStatusDescription(), is(equalTo("birthday not found for user with id of e133b63a-8e89-445a-b0d7-54d98bcac0d8 and birthday id of 1")));

        assertThat(response.getData(), is(notNullValue()));
        assertThat(response.getData().getDetails(), hasSize(1));

        ErrorDetails errorDetails = response.getData().getDetails().get(0);

        assertThat(errorDetails.getMessage(), is(notNullValue()));
        assertThat(errorDetails.getReason(), is(equalTo(LocalizationKeys.CREATE_BIRTHDAY_REMINDER_NOT_FOUND)));
        assertThat(errorDetails.getAttributeName(), is(equalTo("birthdayId")));
        assertThat(errorDetails.getAttributeValues(), hasSize(1));
        assertThat(errorDetails.getAttributeValues().get(0), is(equalTo("1")));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void updateBirthdayReminderSuccessfully() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder reminder = createDefaultBirthdayReminder(birthday);

        // execute
        String content = contentOf("requests/valid/BirthdayReminderRequest-update-v1-valid-1.json");

        mockMvc.perform(
                put("/api/v1/birthdays/" + birthday.getId() + "/reminders/" + reminder.getId())
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk());

        // verify
        List<BirthdayReminder> reminders = reminderRepository.findByBirthdayId(
                birthday.getUserId(),
                birthday.getId(),
                PageRequest.of(0, Integer.MAX_VALUE));

        assertThat(reminders, hasSize(1));
        BirthdayReminder updatedReminder = reminders.get(0);

        assertThat(updatedReminder.getBirthdayId(), is(equalTo(birthday.getId())));
        assertThat(updatedReminder.getType(), is(equalTo(BirthdayReminderType.WEEKS)));
        assertThat(updatedReminder.getUnits(), is(equalTo(2)));
        assertThat(updatedReminder.getRemindAt(), is(equalTo(LocalTime.of(10, 0, 0))));
        assertThat(updatedReminder.isRepeatable(), is(equalTo(false)));
    }

    @Test
    public void updateBirthdayReminderSuccessfullyWhenNotAuthorized() throws Exception {
        // execute
        String content = contentOf("requests/valid/BirthdayReminderRequest-update-v1-valid-1.json");

        mockMvc.perform(
                put("/api/v1/birthdays/1/reminders/1")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void updateBirthdayReminderSuccessfullyWhenNotFound() throws Exception {
        // execute
        String content = contentOf("requests/valid/BirthdayReminderRequest-update-v1-valid-1.json");

        mockMvc.perform(
                put("/api/v1/birthdays/1/reminders/1")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readBirthdayReminder() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder birthdayReminder = createDefaultBirthdayReminder(birthday);

        // execute
        MvcResult mvcResult = mockMvc.perform(
                get("/api/v1/birthdays/" + birthday.getId() + "/reminders/" + birthdayReminder.getId()))
                .andExpect(status().isOk())
                .andReturn();

        // verify
        BirthdayReminderResponse response = fromJson(
                mvcResult.getResponse().getContentAsString(), BirthdayReminderResponse.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getBirthdayId(), is(equalTo(birthday.getId())));
        assertThat(response.getId(), is(equalTo(birthdayReminder.getId())));
        assertThat(response.getRemindAt(), is(equalTo("09:00:00")));
        assertThat(response.getType(), is(equalTo(BirthdayReminderType.DAYS)));
        assertThat(response.getUnits(), is(equalTo(1)));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readBirthdayRemindersAll() throws Exception {
        Birthday birthday = createDefaultBirthday();

        BirthdayReminder reminderOne = createCustomBirthdayReminder(birthday, 5, 9);
        BirthdayReminder reminderTwo = createCustomBirthdayReminder(birthday, 10, 10);

        // execute
        MvcResult mvcResult = mockMvc.perform(
                get("/api/v1/birthdays/" + birthday.getId() + "/reminders"))
                .andExpect(status().isOk())
                .andReturn();

        BirthdayRemindersResponse response = fromJson(
                mvcResult.getResponse().getContentAsString(), BirthdayRemindersResponse.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getContent(), hasSize(2));
        assertThat(response.getPaging().getMaxPerPage(), is(equalTo(15)));
        assertThat(response.getPaging().getNumberOfResults(), is(equalTo(2)));
        assertThat(response.getPaging().getPageNumber(), is(equalTo(0)));

        BirthdayReminderResponse firstResponse = response.getContent().get(0);
        assertThat(firstResponse, is(notNullValue()));
        assertThat(firstResponse.getBirthdayId(), is(equalTo(birthday.getId())));
        assertThat(firstResponse.getId(), is(equalTo(reminderOne.getId())));
        assertThat(firstResponse.getRemindAt(), is(equalTo("09:00:00")));
        assertThat(firstResponse.getType(), is(equalTo(BirthdayReminderType.DAYS)));
        assertThat(firstResponse.getUnits(), is(equalTo(5)));

        BirthdayReminderResponse secondResponse = response.getContent().get(1);
        assertThat(secondResponse, is(notNullValue()));
        assertThat(secondResponse.getBirthdayId(), is(equalTo(birthday.getId())));
        assertThat(secondResponse.getId(), is(equalTo(reminderTwo.getId())));
        assertThat(secondResponse.getRemindAt(), is(equalTo("10:00:00")));
        assertThat(secondResponse.getType(), is(equalTo(BirthdayReminderType.DAYS)));
        assertThat(secondResponse.getUnits(), is(equalTo(10)));
    }
    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readBirthdayReminderWhenNotExists() throws Exception {
        // execute
        mockMvc.perform(
                get("/api/v1/birthdays/1/reminders/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void readBirthdayReminderWhenNotLoggedIn() throws Exception {
        // execute
        mockMvc.perform(
                get("/api/v1/birthdays/1/reminders/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteBirthdayReminder() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder birthdayReminder = createDefaultBirthdayReminder(birthday);

        // execute
        mockMvc.perform(
                delete("/api/v1/birthdays/" + birthday.getId() + "/reminders/" + birthdayReminder.getId())
                        .with(csrf()))
                .andExpect(status().isNoContent());

        // verify
        List<BirthdayReminder> reminders = this
                .reminderRepository.findByBirthdayId(
                        birthday.getUserId(),
                        birthday.getId(),
                        PageRequest.of(0, Integer.MAX_VALUE));

        assertThat(reminders, hasSize(0));
    }

    @Test
    public void deleteBirthdayReminderWhenNotAuthorized() throws Exception {
        // execute
        mockMvc.perform(
                delete("/api/v1/birthdays/1/reminders/1"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteBirthdayReminderWhenNotFound() throws Exception {
        // execute
        mockMvc.perform(
                delete("/api/v1/birthdays/1/reminders/1").with(csrf()))
                .andExpect(status().isNoContent());
    }

    private BirthdayReminder createDefaultBirthdayReminder(Birthday birthday) {
        BirthdayReminder reminder = new BirthdayReminder()
                .birthdayId(birthday.getId())
                .type(BirthdayReminderType.DAYS)
                .units(1)
                .remindAt(LocalTime.of(9, 0, 0))
                .repeatable(true);

        return this.reminderRepository.save(reminder);
    }

    private BirthdayReminder createCustomBirthdayReminder(Birthday birthday, Integer units, Integer hour) {
        BirthdayReminder reminder = new BirthdayReminder()
                .birthdayId(birthday.getId())
                .type(BirthdayReminderType.DAYS)
                .units(units)
                .remindAt(LocalTime.of(hour, 0, 0))
                .repeatable(true);

        return this.reminderRepository.save(reminder);
    }

    private Birthday createDefaultBirthday() {
        LocalDate bornOn = LocalDate.of(1981, 9, 5);
        LocalDate nextBirthday = bornOn.plusYears(1);

        Birthday birthday = new Birthday()
                .userId(UUID.fromString(DEFAULT_USER_ID_STRING))
                .bornOn(bornOn)
                .nextBirthday(nextBirthday)
                .personInfo("my name")
                .note("My birthday");

        return this.birthdayRepository.save(birthday);
    }
}
