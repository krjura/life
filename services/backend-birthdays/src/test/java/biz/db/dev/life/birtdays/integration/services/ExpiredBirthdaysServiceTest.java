package biz.db.dev.life.birtdays.integration.services;

import biz.db.dev.life.birtdays.services.ExpiredBirthdaysService;
import biz.db.dev.life.birtdays.support.TestBase;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;

public class ExpiredBirthdaysServiceTest extends TestBase {

    @Autowired
    private BirthdayRepository birthdayRepository;

    @Autowired
    private ExpiredBirthdaysService expiredBirthdaysService;

    @Test
    public void checkExpiredBirthdays() {

        // given
        LocalDate now = LocalDate.now();

        Birthday birthday = new Birthday()
                .userId(UUID.randomUUID())
                .bornOn(LocalDate.of(1900, 1, 1))
                .nextBirthday(LocalDate.of(1900, 1, 1))
                .personInfo("Demo")
                .note("demo");

        birthday = this.birthdayRepository.save(birthday);

        // when
        expiredBirthdaysService.onSchedule();

        // then
        Optional<Birthday> fromDbOptional = this.birthdayRepository.findBirthday(birthday.getUserId(), birthday.getId());

        assertThat(fromDbOptional.isPresent(), is(equalTo(true)));
        Birthday fromDb = fromDbOptional.get();

        boolean result = fromDb.getNextBirthday().isAfter(now) || fromDb.getNextBirthday().isEqual(now);
        assertTrue(result);
    }
}
