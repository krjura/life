package biz.db.dev.life.birtdays.documenation.api;

import biz.db.dev.life.birtdays.controllers.pojo.birthdays.request.BirthdayRequest;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ExceptionDocumentation extends DocumentationMvcTestBase {

    private static final String DEFAULT_USER_ID_STRING = "8f3df841-ec10-4e96-8bfa-206b9fc67d3c";

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void createBirthdayWithBadRequest() throws Exception {
        BirthdayRequest request = new BirthdayRequest()
                .birthday(null)
                .personInfo(null)
                .note(null);

        String content = toJson(request);

        getMockMvc().perform(
                post("/api/v1/birthdays")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andDo(document(
                        "errors_bad_request",
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("status")
                                        .type(JsonFieldType.STRING)
                                        .description("Status of the request. Can be ERROR or SUCCESS"),
                                PayloadDocumentation
                                        .fieldWithPath("statusCode")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Status code of the request"),
                                PayloadDocumentation
                                        .fieldWithPath("statusDescription")
                                        .type(JsonFieldType.STRING)
                                        .attributes()
                                        .description("Human readable description of the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data")
                                        .type(JsonFieldType.OBJECT)
                                        .description("Wrapper object for error details"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[]")
                                        .type(JsonFieldType.ARRAY)
                                        .description("List of all error details"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].reason")
                                        .type(JsonFieldType.STRING)
                                        .description("Localized key of the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].attributeName")
                                        .type(JsonFieldType.STRING)
                                        .description("Name of the attribute causing the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].attributeValues")
                                        .type(JsonFieldType.ARRAY)
                                        .description("List of values causing the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].message")
                                        .type(JsonFieldType.STRING)
                                        .description("Human readable description of the errors")
                        )
                ));
    }

    @Test
    public void userInfo() throws Exception {
        BirthdayRequest request = new BirthdayRequest()
                .birthday(null)
                .personInfo(null)
                .note(null);

        String content = toJson(request);

        getMockMvc().perform(
                post("/api/v1/birthdays")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(document(
                        "error_forbidden"
                ))
                .andExpect(status().isForbidden());
    }
}
