package biz.db.dev.life.birtdays.support;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@SpringBootTest
@ActiveProfiles( resolver = ProjectActiveProfilesResolver.class)
public @interface ProjectTest {

}
