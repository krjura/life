package biz.db.dev.life.birtdays.integration.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
public class DatabaseCleanerServiceImpl {

    @Autowired
    private EntityManager em;

    @Transactional
    public void deleteAll() {
        em.createNativeQuery("TRUNCATE birthday CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE birthday_reminder CASCADE").executeUpdate();
    }
}