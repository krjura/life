package biz.db.dev.life.birtdays.integration.controllers;

import biz.db.dev.life.birtdays.support.TestBase;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayResponse;
import biz.db.dev.life.birtdays.controllers.pojo.birthdays.response.BirthdayResponses;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;
import biz.db.dev.life.birtdays.model.repository.BirthdayReminderRepository;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.birtdays.utils.DateUtils;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

public class BirthdayControllerTest extends TestBase {

    private static final String DEFAULT_USER_ID_STRING = "8f3df841-ec10-4e96-8bfa-206b9fc67d3c";

    @Autowired
    private BirthdayReminderRepository birthdayReminderRepository;

    @Autowired
    private BirthdayRepository birthdayRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void listBirthdaysBySearchTerm() throws Exception {
        // given
        Birthday birthday = createDefaultBirthday();

        // when
        MvcResult result = mockMvc.perform(
                get("/api/v1/birthdays?searchTerm=name"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        BirthdayResponses responses = fromJson(result, BirthdayResponses.class);
        assertThat(responses.getContent(), is(notNullValue()));
        assertThat(responses.getContent().size(), is(equalTo(1)));

        BirthdayResponse response = responses.getContent().get(0);
        assertThat(response.getUserId().toString(), is(equalTo(DEFAULT_USER_ID_STRING)));
        assertThat(response.getPersonInfo(), is(equalTo("my name")));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void addBirthday() throws Exception {
        String content = contentOf("requests/valid/BirthdayRequest-v1-valid-1.json");

        // execute
        mockMvc.perform(
                post("/api/v1/birthdays")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated());

        // verify
        List<Birthday> birthdays = birthdayRepository.findAll();
        assertThat(birthdays, hasSize(1));

        Birthday birthday = birthdays.get(0);
        assertThat(birthday, is(notNullValue()));
        assertThat(birthday.getNote(), is(equalTo("Krešimir Jurasović birthday")));
        assertThat(birthday.getBornOn(), is(equalTo(birthday.getBornOn())));
        assertThat(birthday.getNextBirthday(), is(equalTo(birthday.getNextBirthday())));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void addBirthdayWithInvalidRequest() throws Exception {
        String content = contentOf("requests/invalid/BirthdayRequest-v1-invalid-1.json");

        // verify
        mockMvc.perform(
                post("/api/v1/birthdays")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteBirthday() throws Exception {

        // prepare data
        Birthday birthday = createDefaultBirthday();

        // execute and verify
        mockMvc.perform(
                delete("/api/v1/birthdays/" + birthday.getId()).with(csrf()))
                .andExpect(status().isNoContent());

        // verify database
        List<Birthday> birthdays = this.birthdayRepository.findAll();
        assertThat(birthdays, hasSize(0));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteBirthdayWithReminder() throws Exception {

        // prepare data
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder reminder = createDefaultBirthdayReminder(birthday);

        Birthday secondBirthday = createDefaultBirthday();
        BirthdayReminder secondReminder = createDefaultBirthdayReminder(secondBirthday);

        // execute and verify
        mockMvc.perform(
                delete("/api/v1/birthdays/" + birthday.getId()).with(csrf()))
                .andExpect(status().isNoContent());

        // verify database for first birthday
        Optional<Birthday> birthdayFromDbOptional =
                this.birthdayRepository.findBirthday(birthday.getUserId(), birthday.getId());

        assertThat(birthdayFromDbOptional.isPresent(), is(equalTo(false)));

        Optional<BirthdayReminder> reminderFromDbOptional =
                this.birthdayReminderRepository.findById(birthday.getUserId(), birthday.getId(), reminder.getId());

        assertThat(reminderFromDbOptional.isPresent(), is(equalTo(false)));

        // verify database for second birthdays
        Optional<Birthday> secondBirthdayFromDbOptional =
                this.birthdayRepository.findBirthday(secondBirthday.getUserId(), secondBirthday.getId());

        assertThat(secondBirthdayFromDbOptional.isPresent(), is(equalTo(true)));

        Optional<BirthdayReminder> secondReminderFromDbOptional =
                this.birthdayReminderRepository.findById(secondBirthday.getUserId(), secondBirthday.getId(), secondReminder.getId());

        assertThat(secondReminderFromDbOptional.isPresent(), is(equalTo(true)));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteNonExistingBirthday() throws Exception {

        // execute and verify
        mockMvc.perform(
                delete("/api/v1/birthdays/1").with(csrf()))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readExistingBirthday() throws Exception {
        // prepare data
        Birthday birthday = createDefaultBirthday();

        // execute
        MvcResult result = mockMvc.perform(
                get("/api/v1/birthdays/" + birthday.getId()))
                .andExpect(status().isOk())
                .andReturn();

        // verify
        BirthdayResponse response = fromJson(result, BirthdayResponse.class);

        assertThat(response.getUserId(), is(equalTo(birthday.getUserId())));
        assertThat(response.getBornOn(), is(equalTo(DateUtils.toEpocMilis(birthday.getBornOn()))));
        assertThat(response.getNextBirthday(), is(equalTo(DateUtils.toEpocMilis(birthday.getNextBirthday()))));
        assertThat(response.getNote(), is(equalTo(birthday.getNote())));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readNonExistingBirthday() throws Exception {
        // execute and verify
        mockMvc.perform(
                get("/api/v1/birthdays/2"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void updateExistingBirthday() throws Exception {
        // prepare data
        Birthday birthday = createDefaultBirthday();

        // verify
        MvcResult result = mockMvc.perform(
                put("/api/v1/birthdays/" + birthday.getId())
                        .content(contentOf("requests/valid/BirthdayRequest-update-v1-valid-1.json"))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        // verify response

        LocalDate updateBornOn = LocalDate.of(1981, 9, 8);
        LocalDate updatedNextBirthday = DateUtils.next(updateBornOn);

        BirthdayResponse responseBirthday = fromJson(result, BirthdayResponse.class);
        assertThat(responseBirthday.getPersonInfo(), is(equalTo("Krešimir Jurasović")));
        assertThat(responseBirthday.getNote(), is(equalTo("Ivan's birthday")));
        assertThat(responseBirthday.getBornOn(), is(equalTo(DateUtils.toEpocMilis(updateBornOn))));
        assertThat(responseBirthday.getNextBirthday(), is(equalTo(DateUtils.toEpocMilis(updatedNextBirthday))));

        // verify database
        List<Birthday> birthdays = this.birthdayRepository.findAll();

        assertThat(birthdays, is(notNullValue()));
        assertThat(birthdays, hasSize(1));

        Birthday updatedBirthday = birthdays.get(0);

        assertThat(updatedBirthday.getPersonInfo(), is(equalTo("Krešimir Jurasović")));
        assertThat(updatedBirthday.getNote(), is(equalTo("Ivan's birthday")));
        assertThat(updatedBirthday.getBornOn(), is(equalTo(updateBornOn)));
        assertThat(updatedBirthday.getNextBirthday(), is(equalTo(updatedNextBirthday)));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void updateNonExistingBirthday() throws Exception {
        // verify
        mockMvc.perform(
                put("/api/v1/birthdays/0")
                        .content(contentOf("requests/valid/BirthdayRequest-update-v1-valid-1.json"))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isNotFound());
    }

    private BirthdayReminder createDefaultBirthdayReminder(Birthday birthday) {
        BirthdayReminder birthdayReminder = new BirthdayReminder()
                .birthdayId(birthday.getId())
                .birthday(birthday)
                .type(BirthdayReminderType.DAYS)
                .units(1)
                .remindAt(LocalTime.of(9, 0, 0))
                .repeatable(true);

        return this.birthdayReminderRepository.save(birthdayReminder);
    }

    private Birthday createDefaultBirthday() {
        LocalDate bornOn = LocalDate.of(1981, 9, 5);
        LocalDate nextBirthday = bornOn.plusYears(1);

        Birthday birthday = new Birthday()
                .userId(UUID.fromString(DEFAULT_USER_ID_STRING))
                .bornOn(bornOn)
                .nextBirthday(nextBirthday)
                .personInfo("my name")
                .note("My birthday");

        return this.birthdayRepository.save(birthday);
    }
}
