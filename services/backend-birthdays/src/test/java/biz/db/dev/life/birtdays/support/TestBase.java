package biz.db.dev.life.birtdays.support;

import biz.db.dev.life.birtdays.integration.services.DatabaseCleanerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(SpringJUnit4ClassRunner.class)
@ProjectTest
public abstract class TestBase {

    @Autowired
    private DatabaseCleanerServiceImpl databaseCleanerService;

    @Autowired
    protected WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @Before
    public void beforeTestBase() {
        this.databaseCleanerService.deleteAll();

        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    public String contentOf(String fileName) throws URISyntaxException, IOException {
        return new String(Files.readAllBytes(Paths.get(getClass().getClassLoader()
                .getResource(fileName)
                .toURI())));
    }

    public <T> T fromJson(MvcResult result, Class<T> clazz) throws IOException {
        return fromJson(result.getResponse().getContentAsString(), clazz);
    }

    public <T> T fromJson(String content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz);
    }
}
