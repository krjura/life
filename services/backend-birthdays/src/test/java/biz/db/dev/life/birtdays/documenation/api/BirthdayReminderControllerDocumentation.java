package biz.db.dev.life.birtdays.documenation.api;

import biz.db.dev.life.birtdays.controllers.pojo.birthdays.request.BirthdayReminderRequest;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.BirthdayReminder;
import biz.db.dev.life.birtdays.model.enums.BirthdayReminderType;
import biz.db.dev.life.birtdays.model.repository.BirthdayReminderRepository;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestDocumentation;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BirthdayReminderControllerDocumentation extends DocumentationMvcTestBase {

    private static final String DEFAULT_USER_ID_STRING = "e133b63a-8e89-445a-b0d7-54d98bcac0d8";

    private static final String DESCRIPTION_ID = "Id of the reminder entry";
    private static final String DESCRIPTION_BIRTHDAY_ID = "Id of the birthday entry";
    private static final String DESCRIPTION_TYPE = "Type of reminder. Can be DAYS, WEEKS, MONTHS";
    private static final String DESCRIPTION_UNITS = "Number of units used to calculated reminder days. It is calculated by reducing birthday day by type * units";
    private static final String DESCRIPTION_REMIND_AT = "Time when the reminder should be sent. Should be in the format HH:mm:ss";
    private static final String DESCRIPTION_REPEATABLE = "Signals if the reminder should be repeated the next year";
    private static final String DESCRIPTION_PATH_BIRTHDAY_ID = "Id of the birthday";
    private static final String DESCRIPTION_PATH_REMINDER_ID = "Id of the reminder";

    @Autowired
    private BirthdayRepository birthdayRepository;

    @Autowired
    private BirthdayReminderRepository reminderRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readBirthdayRemindersAll() throws Exception {
        Birthday birthday = createDefaultBirthday();

        createCustomBirthdayReminder(birthday, 5, 9);
        createCustomBirthdayReminder(birthday, 10, 10);

        // execute
        getMockMvc().perform(
                get("/api/v1/birthdays/{birthdayId}/reminders?page={page}&maxPerPage={maxPerPage}", birthday.getId(), 0, 10))
                .andExpect(status().isOk())
                .andDo(document(
                        "list_reminders_for_birthday",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId()
                        ),
                        RequestDocumentation.requestParameters(
                                RequestDocumentation
                                        .parameterWithName("page")
                                        .description("Page number"),
                                RequestDocumentation
                                        .parameterWithName("maxPerPage")
                                        .description("Maximum number of entries per page. Maximum is 10.")
                        ),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("content[]")
                                        .type(JsonFieldType.ARRAY)
                                        .description("List of birthday entries matching the search query"),
                                PayloadDocumentation
                                        .fieldWithPath("content[].id")
                                        .type(JsonFieldType.NUMBER)
                                        .description(DESCRIPTION_ID),
                                PayloadDocumentation
                                        .fieldWithPath("content[].birthdayId")
                                        .type(JsonFieldType.NUMBER)
                                        .description(DESCRIPTION_BIRTHDAY_ID),
                                PayloadDocumentation
                                        .fieldWithPath("content[].type")
                                        .type(JsonFieldType.STRING)
                                        .description(DESCRIPTION_TYPE),
                                PayloadDocumentation
                                        .fieldWithPath("content[].units")
                                        .type(JsonFieldType.NUMBER)
                                        .description(DESCRIPTION_UNITS),
                                PayloadDocumentation
                                        .fieldWithPath("content[].remindAt")
                                        .type(JsonFieldType.STRING)
                                        .description(DESCRIPTION_REMIND_AT),
                                PayloadDocumentation
                                        .fieldWithPath("content[].repeatable")
                                        .type(JsonFieldType.BOOLEAN)
                                        .description(DESCRIPTION_REPEATABLE),
                                PayloadDocumentation
                                        .fieldWithPath("paging")
                                        .type(JsonFieldType.OBJECT)
                                        .description("Object for paging information"),
                                PayloadDocumentation
                                        .fieldWithPath("paging.maxPerPage")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Maximum entries per page"),
                                PayloadDocumentation
                                        .fieldWithPath("paging.pageNumber")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Current page number"),
                                PayloadDocumentation
                                        .fieldWithPath("paging.numberOfResults")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Number of results in the page")
                        )))
                .andReturn();
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void createBirthdayReminderSuccessfully() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();

        BirthdayReminderRequest request = new BirthdayReminderRequest()
                .type(BirthdayReminderType.DAYS)
                .units(1)
                .remindAt("09:00:00")
                .repeatable(true);

        String content = toJson(request);

        // execute
        getMockMvc().perform(
                post("/api/v1/birthdays/{birthdayId}/reminders", birthday.getId())
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andDo(document(
                        "create_reminders_for_a_birthday",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId()
                        ),
                        PayloadDocumentation.requestFields(
                                payloadDocumentationForType(),
                                payloadDocumentationForUnits(),
                                payloadDocumentationForRemindAt(),
                                payloadDocumentationForRepeatable()),
                        PayloadDocumentation.responseFields(
                                payloadDocumentationForId(),
                                payloadDocumentationForBirthdayId(),
                                payloadDocumentationForType(),
                                payloadDocumentationForUnits(),
                                payloadDocumentationForRemindAt(),
                                payloadDocumentationForRepeatable()
                        )));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readBirthdayReminder() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder birthdayReminder = createDefaultBirthdayReminder(birthday);

        // execute
        getMockMvc().perform(
                get("/api/v1/birthdays/{birthdayId}/reminders/{reminderId}", birthday.getId(), birthdayReminder.getId()))
                .andExpect(status().isOk())
                .andDo(document(
                        "read_specific_reminder",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId(),
                                requestPathDocumentationReminderId()),
                        PayloadDocumentation.responseFields(
                                payloadDocumentationForId(),
                                payloadDocumentationForBirthdayId(),
                                payloadDocumentationForType(),
                                payloadDocumentationForUnits(),
                                payloadDocumentationForRemindAt(),
                                payloadDocumentationForRepeatable()
                        )
                ));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteBirthdayReminder() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder birthdayReminder = createDefaultBirthdayReminder(birthday);

        // execute
        getMockMvc().perform(
                delete("/api/v1/birthdays/{birthdayId}/reminders/{reminderId}", birthday.getId(), birthdayReminder.getId())
                        .with(csrf()))
                .andExpect(status().isNoContent())
                .andDo(document(
                        "delete_specific_reminder",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId(),
                                requestPathDocumentationReminderId())
                ));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void updateBirthdayReminderSuccessfully() throws Exception {
        // prepare
        Birthday birthday = createDefaultBirthday();
        BirthdayReminder reminder = createDefaultBirthdayReminder(birthday);

        // execute
        BirthdayReminderRequest request = new BirthdayReminderRequest()
                .type(BirthdayReminderType.DAYS)
                .units(1)
                .remindAt("09:00:00")
                .repeatable(true);

        String content = toJson(request);

        getMockMvc().perform(
                put("/api/v1/birthdays/{birthdayId}/reminders/{reminderId}", birthday.getId(), reminder.getId())
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andDo(document(
                        "update_specific_reminder",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId(),
                                requestPathDocumentationReminderId()
                        ),
                        PayloadDocumentation.requestFields(
                                payloadDocumentationForType(),
                                payloadDocumentationForUnits(),
                                payloadDocumentationForRemindAt(),
                                payloadDocumentationForRepeatable()
                        ),
                        PayloadDocumentation.responseFields(
                                payloadDocumentationForId(),
                                payloadDocumentationForBirthdayId(),
                                payloadDocumentationForType(),
                                payloadDocumentationForUnits(),
                                payloadDocumentationForRemindAt(),
                                payloadDocumentationForRepeatable()
                        )
                ));
    }

    private ParameterDescriptor requestPathDocumentationForBirthdayId() {
        return RequestDocumentation
                .parameterWithName("birthdayId")
                .description(DESCRIPTION_PATH_BIRTHDAY_ID);
    }

    private ParameterDescriptor requestPathDocumentationReminderId() {
        return RequestDocumentation
                .parameterWithName("reminderId")
                .description(DESCRIPTION_PATH_REMINDER_ID);
    }

    private FieldDescriptor payloadDocumentationForId() {
        return PayloadDocumentation
                .fieldWithPath("id")
                .type(JsonFieldType.NUMBER)
                .description(DESCRIPTION_ID);
    }

    private FieldDescriptor payloadDocumentationForBirthdayId() {
        return PayloadDocumentation
                .fieldWithPath("birthdayId")
                .type(JsonFieldType.NUMBER)
                .description(DESCRIPTION_BIRTHDAY_ID);
    }

    private FieldDescriptor payloadDocumentationForType() {
        return PayloadDocumentation
                .fieldWithPath("type")
                .type(JsonFieldType.STRING)
                .description(DESCRIPTION_TYPE);
    }

    private FieldDescriptor payloadDocumentationForUnits() {
        return PayloadDocumentation
                .fieldWithPath("units")
                .type(JsonFieldType.NUMBER)
                .description(DESCRIPTION_UNITS);
    }

    private FieldDescriptor payloadDocumentationForRemindAt() {
        return PayloadDocumentation
                .fieldWithPath("remindAt")
                .type(JsonFieldType.STRING)
                .description(DESCRIPTION_REMIND_AT);
    }

    private FieldDescriptor payloadDocumentationForRepeatable() {
        return PayloadDocumentation
                .fieldWithPath("repeatable")
                .type(JsonFieldType.BOOLEAN)
                .description(DESCRIPTION_REPEATABLE);
    }

    private Birthday createDefaultBirthday() {
        LocalDate bornOn = LocalDate.of(1980, 1, 1);
        LocalDate nextBirthday = bornOn.plusYears(1);

        Birthday birthday = new Birthday()
                .userId(UUID.fromString(DEFAULT_USER_ID_STRING))
                .bornOn(bornOn)
                .nextBirthday(nextBirthday)
                .personInfo("John Doe")
                .note("My birthday");

        return this.birthdayRepository.save(birthday);
    }

    private BirthdayReminder createDefaultBirthdayReminder(Birthday birthday) {
        BirthdayReminder reminder = new BirthdayReminder()
                .birthdayId(birthday.getId())
                .type(BirthdayReminderType.DAYS)
                .units(1)
                .remindAt(LocalTime.of(9, 0, 0))
                .repeatable(true);

        return this.reminderRepository.save(reminder);
    }

    private void createCustomBirthdayReminder(Birthday birthday, Integer units, Integer hour) {
        BirthdayReminder reminder = new BirthdayReminder()
                .birthdayId(birthday.getId())
                .type(BirthdayReminderType.DAYS)
                .units(units)
                .remindAt(LocalTime.of(hour, 0, 0))
                .repeatable(true);

        this.reminderRepository.save(reminder);
    }
}
