package biz.db.dev.life.birtdays.documenation.api;

import biz.db.dev.life.birtdays.controllers.pojo.birthdays.request.BirthdayRequest;
import biz.db.dev.life.birtdays.model.Birthday;
import biz.db.dev.life.birtdays.model.repository.BirthdayRepository;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestDocumentation;

import java.time.LocalDate;
import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BirthdaysControllerDocumenation extends DocumentationMvcTestBase {

    private static final String DEFAULT_USER_ID_STRING = "8f3df841-ec10-4e96-8bfa-206b9fc67d3c";

    private static final String DESCRIPTION_NOTE = "Description of the event and notes";
    private static final String DESCRIPTION_PERSONAL_INFO = "Name and surname of the person";
    private static final String DESCRIPTION_NEXT_BIRTHDAY = "Date of the next birthday in epoc millisecond";
    private static final String DESCRIPTION_BORN_ON = "Date of the birthday in epoc millisecond";
    private static final String DESCRIPTION_USER_ID = "Id of the user assigned to this birthdays";
    private static final String DESCRIPTION_ID = "Id of the birthday entry";

    private static final String DESCRIPTION_PATH_BIRTHDAY_ID = "Id of the birthday";

    @Autowired
    private BirthdayRepository birthdayRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void listBirthdaysBySearchTerm() throws Exception {
        // given
        createDefaultBirthday();

        // when
        getMockMvc().perform(
                get("/api/v1/birthdays?searchTerm={searchTerm}&page={page}&maxPerPage={maxPerPage}", "John", 0, 10))
                .andExpect(status().isOk())
                .andDo(document(
                        "list_birthdays_by_search_term",
                        RequestDocumentation.requestParameters(
                                RequestDocumentation
                                        .parameterWithName("searchTerm")
                                        .description("Value to search for"),
                                RequestDocumentation
                                        .parameterWithName("page")
                                        .description("Page number"),
                                RequestDocumentation
                                        .parameterWithName("maxPerPage")
                                        .description("Maximum number of entries per page. Maximum is 10.")
                        ),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("content[]")
                                        .type(JsonFieldType.ARRAY)
                                        .description("List of birthday entries matching the search query"),
                                PayloadDocumentation
                                        .fieldWithPath("content[].id")
                                        .type(JsonFieldType.NUMBER)
                                        .description(DESCRIPTION_ID),
                                PayloadDocumentation
                                        .fieldWithPath("content[].userId")
                                        .type(JsonFieldType.STRING)
                                        .description(DESCRIPTION_USER_ID),
                                PayloadDocumentation
                                        .fieldWithPath("content[].bornOn")
                                        .type(JsonFieldType.NUMBER)
                                        .description(DESCRIPTION_BORN_ON),
                                PayloadDocumentation
                                        .fieldWithPath("content[].nextBirthday")
                                        .type(JsonFieldType.NUMBER)
                                        .description(DESCRIPTION_NEXT_BIRTHDAY),
                                PayloadDocumentation
                                        .fieldWithPath("content[].personInfo")
                                        .type(JsonFieldType.STRING)
                                        .description(DESCRIPTION_PERSONAL_INFO),
                                PayloadDocumentation
                                        .fieldWithPath("content[].note")
                                        .type(JsonFieldType.STRING)
                                        .description(DESCRIPTION_NOTE)
                        )))
                .andReturn();
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void addBirthday() throws Exception {
        BirthdayRequest request = new BirthdayRequest()
                .birthday("2018-09-01")
                .personInfo("John Doe")
                .note("My birthday");

        String content = toJson(request);

        // execute
        getMockMvc().perform(
                post("/api/v1/birthdays")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andDo(document(
                        "create_birthday",
                        PayloadDocumentation.requestFields(
                                payloadDocumentationOfBirthdayDate(),
                                payloadDocumentationOfPersonalInfo(),
                                payloadDocumentationOfNote()
                )));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void readExistingBirthday() throws Exception {
        // prepare data
        Birthday birthday = createDefaultBirthday();

        // execute
        getMockMvc().perform(
                get("/api/v1/birthdays/{birthdayId}", birthday.getId()))
                .andExpect(status().isOk())
                .andDo(document(
                        "read_birthday",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId()
                        ),
                        PayloadDocumentation.responseFields(
                            payloadDocumentationOfId(),
                            payloadDocumentationOfUserId(),
                            payloadDocumentationOfBornOn(),
                            payloadDocumentationOfNextBirthday(),
                            payloadDocumentationOfPersonalInfo(),
                            payloadDocumentationOfNote()
                    )))
                .andReturn();
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void deleteBirthday() throws Exception {

        // prepare data
        Birthday birthday = createDefaultBirthday();

        // execute and verify
        getMockMvc().perform(
                delete("/api/v1/birthdays/{birthdayId}", birthday.getId()).with(csrf()))
                .andExpect(status().isNoContent())
                .andDo(document(
                        "delete_birthday",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId()
                        )
                ));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING)
    public void updateExistingBirthday() throws Exception {
        // prepare data
        Birthday birthday = createDefaultBirthday();

        BirthdayRequest request = new BirthdayRequest()
                .birthday("2018-09-01")
                .personInfo("John Doe")
                .note("My birthday");

        String content = toJson(request);

        // verify
        getMockMvc().perform(
                put("/api/v1/birthdays/{birthdayId}", birthday.getId())
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andDo(document(
                        "update_birthday",
                        RequestDocumentation.pathParameters(
                                requestPathDocumentationForBirthdayId()
                        ),
                        PayloadDocumentation.requestFields(
                                payloadDocumentationOfBirthdayDate(),
                                payloadDocumentationOfPersonalInfo(),
                                payloadDocumentationOfNote()
                        ),
                        PayloadDocumentation.responseFields(
                                payloadDocumentationOfId(),
                                payloadDocumentationOfUserId(),
                                payloadDocumentationOfBornOn(),
                                payloadDocumentationOfNextBirthday(),
                                payloadDocumentationOfPersonalInfo(),
                                payloadDocumentationOfNote()
                        )
                ))
                .andReturn();
    }

    private ParameterDescriptor requestPathDocumentationForBirthdayId() {
        return RequestDocumentation
                .parameterWithName("birthdayId")
                .description(DESCRIPTION_PATH_BIRTHDAY_ID);
    }

    private FieldDescriptor payloadDocumentationOfId() {
        return PayloadDocumentation
                .fieldWithPath("id")
                .type(JsonFieldType.NUMBER)
                .description(DESCRIPTION_ID);
    }

    private FieldDescriptor payloadDocumentationOfUserId() {
        return PayloadDocumentation
                .fieldWithPath("userId")
                .type(JsonFieldType.STRING)
                .description(DESCRIPTION_USER_ID);
    }

    private FieldDescriptor payloadDocumentationOfBornOn() {
        return PayloadDocumentation
                .fieldWithPath("bornOn")
                .type(JsonFieldType.NUMBER)
                .description(DESCRIPTION_BORN_ON);
    }

    private FieldDescriptor payloadDocumentationOfNextBirthday() {
        return PayloadDocumentation
                .fieldWithPath("nextBirthday")
                .type(JsonFieldType.NUMBER)
                .description(DESCRIPTION_NEXT_BIRTHDAY);
    }

    private FieldDescriptor payloadDocumentationOfPersonalInfo() {
        return PayloadDocumentation
                .fieldWithPath("personInfo")
                .type(JsonFieldType.STRING)
                .description(DESCRIPTION_PERSONAL_INFO);
    }

    private FieldDescriptor payloadDocumentationOfNote() {
        return PayloadDocumentation
                .fieldWithPath("note")
                .type(JsonFieldType.STRING)
                .description(DESCRIPTION_NOTE);
    }

    private FieldDescriptor payloadDocumentationOfBirthdayDate() {
        return PayloadDocumentation
                .fieldWithPath("birthday")
                .type(JsonFieldType.STRING)
                .description("Date in ISO 8601 date format YYYY-MM-dd");
    }

    private Birthday createDefaultBirthday() {
        LocalDate bornOn = LocalDate.of(1980, 9, 5);
        LocalDate nextBirthday = bornOn.plusYears(1);

        Birthday birthday = new Birthday()
                .userId(UUID.fromString(DEFAULT_USER_ID_STRING))
                .bornOn(bornOn)
                .nextBirthday(nextBirthday)
                .personInfo("John Doe")
                .note("My birthday");

        return this.birthdayRepository.save(birthday);
    }
}
