package org.krjura.life.notifications.controller;

import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.krjura.life.notifications.TestBase;
import org.krjura.life.notifications.controller.pojo.response.UserNotificationEmailResponse;
import org.krjura.life.notifications.model.UserEmailConfiguration;
import org.krjura.life.notifications.model.repository.UserEmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserEmailControllerTest extends TestBase {

    private static final String DEFAULT_USER_ID_STRING = "0c80a3c1-dc4e-45b7-9a91-fdfda5589da4";

    @Autowired
    private UserEmailRepository userEmailRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void updateUserEmailNotAuthenticated() throws Exception {
        // execute

        String content = contentOf("requests/valid/UserNotificationEmailRequest-v1-valid.json");

        mockMvc.perform(
                post("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_USER"})
    public void updateUserEmailInvalidUserId() throws Exception {
        String userId = UUID.randomUUID().toString();

        // execute
        String content = contentOf("requests/valid/UserNotificationEmailRequest-v1-valid.json");

        mockMvc.perform(
                post("/api/v1/users/" + userId + "/emails")
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_USER"})
    public void updateUserEmailSuccessfully() throws Exception {
        // given

        // when

        String content = contentOf("requests/valid/UserNotificationEmailRequest-v1-valid.json");

        mockMvc.perform(
                post("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails")
                    .content(content)
                    .contentType(MediaType.APPLICATION_JSON)
                    .with(csrf()))
                .andExpect(status().isNoContent());

        // then
        Optional<UserEmailConfiguration> userEmailConfigurationOptional = this
                .userEmailRepository.findByUserId(UUID.fromString(DEFAULT_USER_ID_STRING));

        assertThat(userEmailConfigurationOptional.isPresent(), is(equalTo(true)));

        UserEmailConfiguration userEmailConfiguration = userEmailConfigurationOptional.get();

        assertThat(userEmailConfiguration.getUserId(), is(equalTo(UUID.fromString(DEFAULT_USER_ID_STRING))));
        assertThat(userEmailConfiguration.getUserEmail(), is(equalTo("demo@example.com")));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_USER"})
    public void updateUserEmailWhenAlreadyExistForAnotherUser() throws Exception {
        // given
        UserEmailConfiguration otherUserEmail = new UserEmailConfiguration();
        otherUserEmail.setUserEmail("demo@example.com");
        otherUserEmail.setUserId(UUID.randomUUID());

        this.userEmailRepository.save(otherUserEmail);

        // when

        String content = contentOf("requests/valid/UserNotificationEmailRequest-v1-valid.json");

        mockMvc.perform(
                post("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_USER"})
    public void updateUserEmailWhenAlreadyExistForSameUser() throws Exception {
        // given
        UserEmailConfiguration otherUserEmail = new UserEmailConfiguration();
        otherUserEmail.setUserEmail("demo2@example.com");
        otherUserEmail.setUserId(UUID.fromString(DEFAULT_USER_ID_STRING));

        this.userEmailRepository.save(otherUserEmail);

        // when

        String content = contentOf("requests/valid/UserNotificationEmailRequest-v1-valid.json");

        mockMvc.perform(
                post("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isNoContent());

        // then
        Optional<UserEmailConfiguration> userEmailConfigurationOptional = this
                .userEmailRepository.findByUserId(UUID.fromString(DEFAULT_USER_ID_STRING));

        assertThat(userEmailConfigurationOptional.isPresent(), is(equalTo(true)));

        UserEmailConfiguration userEmailConfiguration = userEmailConfigurationOptional.get();

        assertThat(userEmailConfiguration.getUserId(), is(equalTo(UUID.fromString(DEFAULT_USER_ID_STRING))));
        assertThat(userEmailConfiguration.getUserEmail(), is(equalTo("demo@example.com")));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void readUserEmailNotAuthenticated() throws Exception {
        // execute
        mockMvc.perform(
                get("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, grants = {"PW_USER"})
    public void readUserEmailInvalidUserId() throws Exception {
        String userId = UUID.randomUUID().toString();

        // execute
        mockMvc.perform(
                get("/api/v1/users/" + userId + "/emails"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, grants = {"PW_USER"})
    public void readUserEmailSuccessfully() throws Exception {

        // given
        UserEmailConfiguration otherUserEmail = new UserEmailConfiguration();
        otherUserEmail.setUserEmail("demo@example.com");
        otherUserEmail.setUserId(UUID.fromString(DEFAULT_USER_ID_STRING));

        this.userEmailRepository.save(otherUserEmail);

        // when
        MvcResult mvcResult = mockMvc.perform(
                get("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails"))
                .andExpect(status().isOk())
                .andReturn();

        // then
        UserNotificationEmailResponse response = fromJson(mvcResult, UserNotificationEmailResponse.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getEmail(), is(equalTo("demo@example.com")));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, grants = {"PW_USER"})
    public void readUserEmailNotFound() throws Exception {

        // when
        mockMvc.perform(
                get("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void deleteUserEmailNotAuthenticated() throws Exception {
        // execute
        mockMvc.perform(
                delete("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, grants = {"PW_USER"})
    public void deleteUserEmailInvalidUserId() throws Exception {
        String userId = UUID.randomUUID().toString();

        // execute
        mockMvc.perform(
                delete("/api/v1/users/" + userId + "/emails"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, grants = {"PW_USER"})
    public void deleteUserEmailSuccessfully() throws Exception {

        // given
        UserEmailConfiguration otherUserEmail = new UserEmailConfiguration();
        otherUserEmail.setUserEmail("demo@example.com");
        otherUserEmail.setUserId(UUID.fromString(DEFAULT_USER_ID_STRING));

        this.userEmailRepository.save(otherUserEmail);

        // then
        mockMvc.perform(
                delete("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails").with(csrf()))
                .andExpect(status().isNoContent());

        // when
        Optional<UserEmailConfiguration> userEmailConfigurationOptional = this
                .userEmailRepository.findByUserId(UUID.fromString(DEFAULT_USER_ID_STRING));

        assertThat(userEmailConfigurationOptional.isPresent(), is(equalTo(false)));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, grants = {"PW_USER"})
    public void deleteUserEmailWhenNotFound() throws Exception {
        // then
        mockMvc.perform(
                delete("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/emails")
                        .with(csrf()))
                .andExpect(status().isNoContent());
    }
}
