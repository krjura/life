package org.krjura.life.notifications.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
public class DatabaseCleanerServiceImpl {

    @Autowired
    private EntityManager em;

    @Transactional
    public void deleteAll() {
        em.createNativeQuery("TRUNCATE notification_request CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE user_email_configuration CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE user_notification CASCADE").executeUpdate();
    }
}