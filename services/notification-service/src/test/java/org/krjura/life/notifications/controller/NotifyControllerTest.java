package org.krjura.life.notifications.controller;

import biz.db.dev.life.jwt.core.test.WithMockJwt;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorDetails;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponse;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatus;
import org.junit.Test;
import org.krjura.life.notifications.TestBase;
import org.krjura.life.notifications.controller.pojo.response.NotifyRequestStatusResponse;
import org.krjura.life.notifications.controller.pojo.response.NotifyResponse;
import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.NotifyStatus;
import org.krjura.life.notifications.model.NotificationRequest;
import org.krjura.life.notifications.model.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NotifyControllerTest extends TestBase {

    private static final String DEFAULT_USER_ID_STRING = "0c80a3c1-dc4e-45b7-9a91-fdfda5589da4";

    @Autowired
    private NotificationRepository notificationRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void checkNotAuthenticated() throws Exception {
        String content = contentOf("requests/valid/NotifyRequest-v1-valid.json");

        // execute
        mockMvc.perform(
                post("/api/v1/notify")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {"PW_NS_NOTIFY"})
    public void checkValidNotifyRequest() throws Exception {
        String content = contentOf("requests/valid/NotifyRequest-v1-valid.json");

        // execute
        MvcResult mvcResult = mockMvc.perform(
                post("/api/v1/notify")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        // verify
        NotifyResponse response = fromJson(mvcResult, NotifyResponse.class);
        assertThat(response, is(notNullValue()));

        Optional<NotificationRequest> notificationRequestOptional = notificationRepository
                .findNotificationByRequestId(response.getRequestId());

        assertThat(notificationRequestOptional.isPresent(), is(equalTo(true)));

        NotificationRequest notificationRequest = notificationRequestOptional.get();

        assertThat(notificationRequest.getRequestId(), is(equalTo(response.getRequestId())));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {"PW_NS_NOTIFY"})
    public void checkInvalidNotifyRequest() throws Exception {
        String content = contentOf("requests/invalid/NotifyRequest-v1-invalid.json");

        // execute
        MvcResult mvcResult = mockMvc.perform(
                post("/api/v1/notify")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = fromJson(mvcResult, ErrorResponse.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getStatus(), is(equalTo(ErrorResponseStatus.ERROR)));
        assertThat(response.getStatusCode(), is(equalTo(400)));
        assertThat(response.getStatusDescription(), is(notNullValue()));
        assertThat(response.getData(), is(notNullValue()));
        assertThat(response.getData().getDetails(), is(notNullValue()));
        assertThat(response.getData().getDetails().size(), is(equalTo(3)));

        List<ErrorDetails> errors =  response.getData().getDetails();

        assertThat(errors.get(0).getReason(), is(equalTo("life.NotifyContentConstraint.message")));
        assertThat(errors.get(0).getAttributeName(), is(equalTo("content")));
        assertThat(errors.get(0).getAttributeValues().size(), is(equalTo(1)));
        assertThat(errors.get(0).getAttributeValues().get(0), is(nullValue()));
        assertThat(errors.get(0).getMessage(), is(notNullValue()));

        assertThat(errors.get(1).getReason(), is(equalTo("life.BasicEnumConstraint.message")));
        assertThat(errors.get(1).getAttributeName(), is(equalTo("format")));
        assertThat(errors.get(1).getAttributeValues().size(), is(equalTo(1)));
        assertThat(errors.get(1).getAttributeValues().get(0), is(nullValue()));
        assertThat(errors.get(1).getMessage(), is(notNullValue()));

        assertThat(errors.get(2).getReason(), is(equalTo("life.UUIDConstraint.message")));
        assertThat(errors.get(2).getAttributeName(), is(equalTo("userId")));
        assertThat(errors.get(2).getAttributeValues().size(), is(equalTo(1)));
        assertThat(errors.get(2).getAttributeValues().get(0), is(nullValue()));
        assertThat(errors.get(2).getMessage(), is(notNullValue()));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void checkNotifyRequestStatusSecurity() throws Exception {
        // when
        String requestId = UUID.randomUUID().toString();

        // execute
        mockMvc.perform(
                get("/api/v1/notify/" + requestId))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {"PW_NS_NOTIFY"})
    public void checkNotifyRequestStatusNotFound() throws Exception {
        // when
        String requestId = UUID.randomUUID().toString();

        // execute
        mockMvc.perform(
                get("/api/v1/notify/" + requestId))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {"PW_NS_NOTIFY"})
    public void checkNotifyRequestStatusSuccessfully() throws Exception {
        // given
        UUID userId = UUID.fromString(DEFAULT_USER_ID_STRING);

        UUID requestId = this.notificationRepository.saveNotifyRequest(userId, NotificationFormat.PLAIN, "Hello");

        // when
        MvcResult result = mockMvc.perform(
                get("/api/v1/notify/" + requestId))
                .andExpect(status().isOk())
                .andReturn();

        // then
        NotifyRequestStatusResponse response = fromJson(result, NotifyRequestStatusResponse.class);

        assertThat(response, is(notNullValue()));
        assertThat(response.getRequestId(), is(equalTo(requestId)));
        assertThat(response.getUserId(), is(equalTo(userId)));
        assertThat(response.getFormat(), is(equalTo(NotificationFormat.PLAIN)));
        assertThat(response.getContent(), is(equalTo("Hello")));
        assertThat(response.getStatus(), is(equalTo(NotifyStatus.OPEN)));
        assertThat(response.getDeliveredOn(), is(nullValue()));
        assertThat(response.getExecuteOn(), is(notNullValue()));
        assertThat(response.getNumberOfDeliveryAttempts(), is(equalTo(0)));

    }
}
