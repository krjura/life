package org.krjura.life.notifications.controller;

import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.krjura.life.notifications.TestBase;
import org.krjura.life.notifications.controller.pojo.response.UserNotificationResponses;
import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.UserNotificationStatus;
import org.krjura.life.notifications.model.UserNotification;
import org.krjura.life.notifications.model.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserNotificationControllerTest extends TestBase {

    private static final String DEFAULT_USER_ID_STRING = "0c80a3c1-dc4e-45b7-9a91-fdfda5589da4";

    @Autowired
    private NotificationRepository notificationRepository;

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void checkUserNotificationsNotAuthenticated() throws Exception {
        // execute
        mockMvc.perform(
                get("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/notifications"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_NS_USER_NOTIFY"})
    public void checkUserNotificationsInvalidUserId() throws Exception {
        String userId = UUID.randomUUID().toString();

        // execute
        mockMvc.perform(
                get("/api/v1/users/" + userId + "/notifications"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {"PW_USER"})
    public void checkValidUserNotifications() throws Exception {

        // given
        UserNotification userNotification = new UserNotification();
        userNotification.setUserId(UUID.fromString(DEFAULT_USER_ID_STRING));
        userNotification.setMessageId(UUID.randomUUID());
        userNotification.setStatus(UserNotificationStatus.UNREAD);
        userNotification.setDeliveredOn(ZonedDateTime.now());
        userNotification.setFormat(NotificationFormat.PLAIN.name());
        userNotification.setContent("Hello");

        this.notificationRepository.saveUserNotification(userNotification);

        // when
        MvcResult mvcResult = mockMvc.perform(
                get("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/notifications"))
                .andExpect(status().isOk())
                .andReturn();

        // then

        UserNotificationResponses responses = fromJson(mvcResult, UserNotificationResponses.class);
        assertThat(responses, is(notNullValue()));
        assertThat(responses.getContent(), is(notNullValue()));
        assertThat(responses.getContent().size(), is(equalTo(1)));
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {})
    public void updateUserNotificationStatusNotAuthenticated() throws Exception {
        String messageId = UUID.randomUUID().toString();

        // execute
        mockMvc.perform(
                put("/api/v1/users/" + DEFAULT_USER_ID_STRING + "/notifications/" + messageId))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_NS_USER_NOTIFY"})
    public void updateUserNotificationStatusInvalidUserId() throws Exception {
        String userId = UUID.randomUUID().toString();
        String messageId = UUID.randomUUID().toString();

        String content = contentOf("requests/valid/UserNotificationStatusUpdateRequest-v1-valid.json");

        // execute
        mockMvc.perform(
                put("/api/v1/users/" + userId + "/notifications/" + messageId)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockJwt(userId = DEFAULT_USER_ID_STRING, keyGrants = {}, grants = {"PW_USER"})
    public void updateUserNotificationStatusSuccessfully() throws Exception {

        // given

        String userId = DEFAULT_USER_ID_STRING;
        String messageId = UUID.randomUUID().toString();

        UserNotification userNotification = new UserNotification();
        userNotification.setUserId(UUID.fromString(userId));
        userNotification.setMessageId(UUID.fromString(messageId));
        userNotification.setStatus(UserNotificationStatus.UNREAD);
        userNotification.setDeliveredOn(ZonedDateTime.now());
        userNotification.setFormat(NotificationFormat.PLAIN.name());
        userNotification.setContent("Hello");

        this.notificationRepository.saveUserNotification(userNotification);

        // when

        String content = contentOf("requests/valid/UserNotificationStatusUpdateRequest-v1-valid.json");

        // execute
        mockMvc.perform(
                put("/api/v1/users/" + userId + "/notifications/" + messageId)
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isNoContent());

        // then

        Optional<UserNotification> fromDbOptional = this.notificationRepository
                .findUserNotificationByMessageId(UUID.fromString(userId), UUID.fromString(messageId));

        assertThat(fromDbOptional.isPresent(), is(equalTo(true)));

        UserNotification fromDb = fromDbOptional.get();

        assertThat(fromDb.getUserId(), is(equalTo(userNotification.getUserId())));
        assertThat(fromDb.getMessageId(), is(equalTo(userNotification.getMessageId())));
        assertThat(fromDb.getStatus(), is(equalTo(UserNotificationStatus.READ)));
        assertThat(fromDb.getDeliveredOn(), is(notNullValue()));
        assertThat(fromDb.getFormat(), is(equalTo(userNotification.getFormat())));
        assertThat(fromDb.getContent(), is(equalTo(userNotification.getContent())));
    }
}
