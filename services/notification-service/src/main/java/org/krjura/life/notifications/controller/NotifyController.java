package org.krjura.life.notifications.controller;

import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPostMapping;
import org.krjura.life.notifications.controller.pojo.request.NotifyRequest;
import org.krjura.life.notifications.controller.pojo.response.NotifyRequestStatusResponse;
import org.krjura.life.notifications.controller.pojo.response.NotifyResponse;
import org.krjura.life.notifications.model.NotificationRequest;
import org.krjura.life.notifications.services.NotificationService;
import org.krjura.life.notifications.utils.NotificationFormattingUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Controller
public class NotifyController {

    private final NotificationService notificationService;

    public NotifyController(NotificationService notificationService) {
        Assert.notNull(notificationService, "NotificationService cannot be null");

        this.notificationService = notificationService;
    }

    @JsonPostMapping(path = "/api/v1/notify")
    public ResponseEntity<NotifyResponse> notify(
            @RequestBody @Valid NotifyRequest request) {

        UUID userId = UUID.fromString(request.getUserId());

        UUID requestId = notificationService.saveNotifyRequest(userId, request.getFormat(), request.getContent());

        return ResponseEntity.ok(new NotifyResponse(requestId));
    }

    @JsonGetMapping(path = "/api/v1/notify/{requestId}")
    public ResponseEntity<NotifyRequestStatusResponse> checkNotifyStatus(
            @PathVariable(value = "requestId") UUID requestId) {

        Optional<NotificationRequest> requestOptional = this
                .notificationService.findNotificationByRequestId(requestId);

        if(!requestOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        NotificationRequest request = requestOptional.get();

        NotifyRequestStatusResponse response = new NotifyRequestStatusResponse()
                .requestId(request.getRequestId())
                .userId(request.getUserId())
                .format(NotificationFormattingUtils.toNotificationFormat(request.getFormat()))
                .content(request.getContent())
                .status(request.getStatus())
                .executeOn(NotificationFormattingUtils.toEpocMillis(request.getExecuteOn()))
                .deliveredOn(NotificationFormattingUtils.toEpocMillis(request.getDeliveredOn()))
                .numberOfDeliveryAttempts(request.getNumberOfDeliveryAttempts());

        return ResponseEntity.ok(response);
    }
}
