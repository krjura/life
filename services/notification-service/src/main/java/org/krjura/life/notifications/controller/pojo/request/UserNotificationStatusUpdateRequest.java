package org.krjura.life.notifications.controller.pojo.request;

import org.krjura.life.notifications.enums.UserNotificationStatus;
import org.krjura.life.validations.BasicEnumConstraint;

public class UserNotificationStatusUpdateRequest {

    @BasicEnumConstraint
    private UserNotificationStatus status;

    public UserNotificationStatus getStatus() {
        return status;
    }

    public void setStatus(UserNotificationStatus status) {
        this.status = status;
    }
}
