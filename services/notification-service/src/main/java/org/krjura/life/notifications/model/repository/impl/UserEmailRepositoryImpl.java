package org.krjura.life.notifications.model.repository.impl;

import org.krjura.life.notifications.model.UserEmailConfiguration;
import org.krjura.life.notifications.model.repository.UserEmailRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Repository
public class UserEmailRepositoryImpl implements UserEmailRepository {

    private static final Logger logger = LoggerFactory.getLogger(UserEmailRepositoryImpl.class);

    private final EntityManager em;

    public UserEmailRepositoryImpl(EntityManager em) {
        Assert.notNull(em, "EntityManager cannot be null");

        this.em = em;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserEmailConfiguration> findByEmail(String email) {
        Objects.requireNonNull(email);

        Query query = em.createNamedQuery("UserEmailConfiguration.findByEmail");

        query.setParameter("userEmail", email);

        try {
            return Optional.ofNullable((UserEmailConfiguration) query.getSingleResult());
        } catch ( NoResultException | NonUniqueResultException e) {
            logger.trace("no single record found", e);
            return Optional.empty();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserEmailConfiguration> findByUserId(UUID userId) {
        Objects.requireNonNull(userId);

        Query query = em.createNamedQuery("UserEmailConfiguration.findByUserId");

        query.setParameter("userId", userId);

        try {
            return Optional.ofNullable((UserEmailConfiguration) query.getSingleResult());
        } catch ( NoResultException | NonUniqueResultException e) {
            logger.trace("no single record found", e);
            return Optional.empty();
        }
    }

    @Override
    @Transactional
    public void save(UserEmailConfiguration newConfiguration) {
        em.persist(newConfiguration);
    }

    @Override
    @Transactional
    public void deleteByUserId(UUID userId) {
        Objects.requireNonNull(userId);

        Query query = em.createNamedQuery("UserEmailConfiguration.deleteByUserId");

        query.setParameter("userId", userId);

        query.executeUpdate();
    }
}
