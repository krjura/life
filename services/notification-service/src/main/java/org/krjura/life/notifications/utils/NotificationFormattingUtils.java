package org.krjura.life.notifications.utils;

import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.NotifyStatus;

import java.time.ZonedDateTime;

public class NotificationFormattingUtils {

    public NotificationFormattingUtils() {
        // utils
    }

    public static NotificationFormat toNotificationFormat(String format) {
        return format == null ? null : NotificationFormat.valueOf(format);
    }

    public static NotifyStatus toNotifyStatus(String status) {
        return status == null ? null : NotifyStatus.valueOf(status);
    }

    public static Long toEpocMillis(ZonedDateTime dateTime) {
        return dateTime == null ? null : dateTime.toInstant().toEpochMilli();
    }
}
