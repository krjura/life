package org.krjura.life.notifications.controller.pojo.request;

import org.hibernate.validator.constraints.Email;

public class UserNotificationEmailRequest {

    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
