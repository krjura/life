package org.krjura.life.notifications.controller;

import biz.db.dev.life.exceptions.ServiceException;
import biz.db.dev.life.exceptions.UserIsForbiddenException;
import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPostMapping;
import org.krjura.life.notifications.controller.pojo.request.UserNotificationEmailRequest;
import org.krjura.life.notifications.controller.pojo.response.UserNotificationEmailResponse;
import org.krjura.life.notifications.model.UserEmailConfiguration;
import org.krjura.life.notifications.services.UserEmailService;
import org.krjura.life.notifications.utils.JwtUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Controller
public class UserEmailController {

    private final UserEmailService userEmailService;

    public UserEmailController(UserEmailService userEmailService) {
        Assert.notNull(userEmailService, "UserEmailService cannot be null");

        this.userEmailService = userEmailService;
    }

    @JsonPostMapping(path = "/api/v1/users/{userId}/emails")
    public ResponseEntity<Void> updateUserEmail(
            @PathVariable(value = "userId") UUID userId,
            @RequestBody @Valid UserNotificationEmailRequest request) throws ServiceException {

        UUID userIdFromAuth = JwtUtils.extractUserId();

        if(!userId.equals(userIdFromAuth)) {
            throw new UserIsForbiddenException();
        }

        this.userEmailService.setUserEmail(userId, request.getEmail());

        return ResponseEntity.noContent().build();
    }

    @JsonGetMapping(path = "/api/v1/users/{userId}/emails")
    public ResponseEntity<UserNotificationEmailResponse> readUserEmail(
            @PathVariable(value = "userId") UUID userId) {

        UUID userIdFromAuth = JwtUtils.extractUserId();

        if(!userId.equals(userIdFromAuth)) {
            throw new UserIsForbiddenException();
        }

        Optional<UserEmailConfiguration> emailOptional = this.userEmailService.findEmail(userId);

        if(emailOptional.isPresent()) {
            return ResponseEntity.ok(new UserNotificationEmailResponse(emailOptional.get().getUserEmail()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "/api/v1/users/{userId}/emails")
    public ResponseEntity<Void> deleteUserEmail(
            @PathVariable(value = "userId") UUID userId) {

        UUID userIdFromAuth = JwtUtils.extractUserId();

        if(!userId.equals(userIdFromAuth)) {
            throw new UserIsForbiddenException();
        }

        this.userEmailService.deleteEmail(userId);

        return ResponseEntity.noContent().build();
    }
}
