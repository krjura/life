package org.krjura.life.notifications.controller.pojo.request;

import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.validations.NotifyContentConstraint;
import org.krjura.life.validations.BasicEnumConstraint;
import org.krjura.life.validations.UUIDConstraint;

public class NotifyRequest {

    @UUIDConstraint
    private String userId;

    @BasicEnumConstraint
    private NotificationFormat format;

    @NotifyContentConstraint
    private String content;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public NotificationFormat getFormat() {
        return format;
    }

    public void setFormat(NotificationFormat format) {
        this.format = format;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
