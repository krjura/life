package org.krjura.life.notifications.controller.pojo.response;

import org.krjura.life.notifications.enums.UserNotificationStatus;
import org.krjura.life.notifications.model.UserNotification;
import org.krjura.life.notifications.utils.NotificationFormattingUtils;

import java.util.UUID;

public class UserNotificationResponse {

    private UUID messageId;

    private long deliveredOn;

    private UserNotificationStatus status;

    private String format;

    private String content;

    public UserNotificationResponse() {

    }

    public UserNotificationResponse(UserNotification userNotification) {
        this.messageId = userNotification.getMessageId();
        this.deliveredOn = NotificationFormattingUtils.toEpocMillis(userNotification.getDeliveredOn());
        this.status = userNotification.getStatus();
        this.format = userNotification.getFormat();
        this.content = userNotification.getContent();
    }

    public UUID getMessageId() {
        return messageId;
    }

    public void setMessageId(UUID messageId) {
        this.messageId = messageId;
    }

    public long getDeliveredOn() {
        return deliveredOn;
    }

    public void setDeliveredOn(long deliveredOn) {
        this.deliveredOn = deliveredOn;
    }

    public UserNotificationStatus getStatus() {
        return status;
    }

    public void setStatus(UserNotificationStatus status) {
        this.status = status;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
