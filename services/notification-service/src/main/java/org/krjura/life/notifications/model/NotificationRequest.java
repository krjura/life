package org.krjura.life.notifications.model;

import org.krjura.life.notifications.enums.NotifyStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name = "notification_request")
@NamedQueries({
        @NamedQuery(
                name = "NotificationRequest.findNotificationByRequestId",
                query = "FROM NotificationRequest " +
                        "WHERE requestId = :requestId"
        )
})
public class NotificationRequest {

    @Id
    @Column( name = "request_id", nullable = false)
    private UUID requestId;

    @Column( name = "user_id", nullable = false)
    private UUID userId;

    @Column( name = "format", nullable = false, length = 16)
    private String format;

    @Lob
    @Column( name = "content", nullable = false)
    private String content;

    @Column( name = "delivered_on", nullable = false)
    private ZonedDateTime deliveredOn;

    @Column( name = "execute_on", nullable = false)
    private ZonedDateTime executeOn;

    @Column( name = "number_of_delivery_attempts", nullable = false)
    private Integer numberOfDeliveryAttempts;

    @Enumerated(EnumType.STRING)
    @Column( name = "status", nullable = false, length = 16)
    private NotifyStatus status;

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getDeliveredOn() {
        return deliveredOn;
    }

    public void setDeliveredOn(ZonedDateTime deliveredOn) {
        this.deliveredOn = deliveredOn;
    }

    public ZonedDateTime getExecuteOn() {
        return executeOn;
    }

    public void setExecuteOn(ZonedDateTime executeOn) {
        this.executeOn = executeOn;
    }

    public Integer getNumberOfDeliveryAttempts() {
        return numberOfDeliveryAttempts;
    }

    public void setNumberOfDeliveryAttempts(Integer numberOfDeliveryAttempts) {
        this.numberOfDeliveryAttempts = numberOfDeliveryAttempts;
    }

    public NotifyStatus getStatus() {
        return status;
    }

    public void setStatus(NotifyStatus status) {
        this.status = status;
    }

    // fluent

    public NotificationRequest requestId(final UUID requestId) {
        setRequestId(requestId);
        return this;
    }

    public NotificationRequest userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public NotificationRequest format(final String format) {
        setFormat(format);
        return this;
    }

    public NotificationRequest content(final String content) {
        setContent(content);
        return this;
    }

    public NotificationRequest deliveredOn(final ZonedDateTime deliveredOn) {
        setDeliveredOn(deliveredOn);
        return this;
    }

    public NotificationRequest executeOn(final ZonedDateTime executeOn) {
        setExecuteOn(executeOn);
        return this;
    }

    public NotificationRequest numberOfDeliveryAttempts(final Integer numberOfDeliveryAttempts) {
        setNumberOfDeliveryAttempts(numberOfDeliveryAttempts);
        return this;
    }

    public NotificationRequest status(final NotifyStatus status) {
        setStatus(status);
        return this;
    }
}
