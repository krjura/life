package org.krjura.life.notifications.validations;

import org.hibernate.validator.constraints.Length;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = { })
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention( RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@NotNull
@Length( min = 1)
public @interface NotifyContentConstraint {

    String message() default "life.NotifyContentConstraint.message";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
