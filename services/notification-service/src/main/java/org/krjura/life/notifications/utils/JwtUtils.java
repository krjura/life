package org.krjura.life.notifications.utils;

import biz.db.dev.life.exceptions.UserIsForbiddenException;
import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.JwtDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.UUID;

public class JwtUtils {

    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    public JwtUtils() {
        // util
    }

    public static UUID extractUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if( ! (authentication instanceof JwtAuthenticationToken) ) {
            logger.debug("authentication is not of type JwtAuthenticationToken but {}",
                    authentication.getClass().getSimpleName());

            throw new UserIsForbiddenException();
        }

        return ((JwtDetails) authentication.getDetails()).getUserId();
    }
}
