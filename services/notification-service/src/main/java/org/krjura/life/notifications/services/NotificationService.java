package org.krjura.life.notifications.services;

import org.krjura.life.commons.dao.Pageable;
import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.UserNotificationStatus;
import org.krjura.life.notifications.model.NotificationRequest;
import org.krjura.life.notifications.model.UserNotification;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NotificationService {

    UUID saveNotifyRequest(UUID userId, NotificationFormat format, String content);

    Optional<NotificationRequest> findNotificationByRequestId(UUID requestId);

    List<UserNotification> findUserNotifications(UUID userId, Pageable pagingRequest);

    void updateUserNotificationStatus(UUID userId, UUID messageId, UserNotificationStatus status);
}
