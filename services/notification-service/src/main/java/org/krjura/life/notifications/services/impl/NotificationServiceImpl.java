package org.krjura.life.notifications.services.impl;

import org.krjura.life.commons.dao.Pageable;
import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.UserNotificationStatus;
import org.krjura.life.notifications.model.NotificationRequest;
import org.krjura.life.notifications.model.UserNotification;
import org.krjura.life.notifications.model.repository.NotificationRepository;
import org.krjura.life.notifications.services.NotificationService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository notificationRepository;

    public NotificationServiceImpl(NotificationRepository notificationRepository) {
        Assert.notNull(notificationRepository, "NotificationRepository cannot be null");

        this.notificationRepository = notificationRepository;
    }

    @Override
    // @Transactional -- simple service not needed
    public UUID saveNotifyRequest(UUID userId, NotificationFormat format, String content) {
        return this.notificationRepository.saveNotifyRequest(userId, format, content);
    }

    @Override
    // @Transactional -- simple service not needed
    public Optional<NotificationRequest> findNotificationByRequestId(UUID requestId) {
        return this.notificationRepository.findNotificationByRequestId(requestId);
    }

    @Override
    // @Transactional -- simple service not needed
    public List<UserNotification> findUserNotifications(UUID userId, Pageable pagingRequest) {
        return this.notificationRepository.findUserNotifications(userId, pagingRequest);
    }

    @Override
    // @Transactional -- simple service not needed
    public void updateUserNotificationStatus(UUID userId, UUID messageId, UserNotificationStatus status) {
        this.notificationRepository.updateUserNotificationStatus(userId, messageId, status);
    }
}
