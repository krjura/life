package org.krjura.life.notifications.controller.pojo.response;

import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.NotifyStatus;

import java.util.UUID;

public class NotifyRequestStatusResponse {

    private UUID requestId;

    private UUID userId;

    private NotificationFormat format;

    private String content;

    private NotifyStatus status;

    private Long deliveredOn;

    private Long executeOn;

    private Integer numberOfDeliveryAttempts;

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public NotificationFormat getFormat() {
        return format;
    }

    public void setFormat(NotificationFormat format) {
        this.format = format;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NotifyStatus getStatus() {
        return status;
    }

    public void setStatus(NotifyStatus status) {
        this.status = status;
    }

    public Long getDeliveredOn() {
        return deliveredOn;
    }

    public void setDeliveredOn(Long deliveredOn) {
        this.deliveredOn = deliveredOn;
    }

    public Long getExecuteOn() {
        return executeOn;
    }

    public void setExecuteOn(Long executeOn) {
        this.executeOn = executeOn;
    }

    public Integer getNumberOfDeliveryAttempts() {
        return numberOfDeliveryAttempts;
    }

    public void setNumberOfDeliveryAttempts(Integer numberOfDeliveryAttempts) {
        this.numberOfDeliveryAttempts = numberOfDeliveryAttempts;
    }

    // fluent

    public NotifyRequestStatusResponse requestId(final UUID requestId) {
        setRequestId(requestId);
        return this;
    }

    public NotifyRequestStatusResponse userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public NotifyRequestStatusResponse format(final NotificationFormat format) {
        setFormat(format);
        return this;
    }

    public NotifyRequestStatusResponse content(final String content) {
        setContent(content);
        return this;
    }

    public NotifyRequestStatusResponse status(final NotifyStatus status) {
        setStatus(status);
        return this;
    }

    public NotifyRequestStatusResponse deliveredOn(final Long deliveredOn) {
        setDeliveredOn(deliveredOn);
        return this;
    }

    public NotifyRequestStatusResponse executeOn(final Long executeOn) {
        setExecuteOn(executeOn);
        return this;
    }

    public NotifyRequestStatusResponse numberOfDeliveryAttempts(final Integer numberOfDeliveryAttempts) {
        setNumberOfDeliveryAttempts(numberOfDeliveryAttempts);
        return this;
    }
}
