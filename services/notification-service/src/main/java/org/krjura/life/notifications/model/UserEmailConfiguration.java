package org.krjura.life.notifications.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "user_email_configuration")
@NamedQueries({
        @NamedQuery(
                name = "UserEmailConfiguration.findByEmail",
                query = "FROM UserEmailConfiguration " +
                        "WHERE userEmail = :userEmail"
        ),
        @NamedQuery(
                name = "UserEmailConfiguration.findByUserId",
                query = "FROM UserEmailConfiguration " +
                        "WHERE userId = :userId"
        ),
        @NamedQuery(
                name = "UserEmailConfiguration.deleteByUserId",
                query = "DELETE FROM UserEmailConfiguration " +
                        "WHERE userId = :userId"
        )

})
public class UserEmailConfiguration {

    @Id
    @Column(name = "user_email", nullable = false)
    private String userEmail;

    @Column( name = "user_id", nullable = false)
    private UUID userId;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    // fluent

    public UserEmailConfiguration userEmail(final String userEmail) {
        setUserEmail(userEmail);
        return this;
    }

    public UserEmailConfiguration userId(final UUID userId) {
        setUserId(userId);
        return this;
    }
}
