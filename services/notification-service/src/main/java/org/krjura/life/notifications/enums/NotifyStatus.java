package org.krjura.life.notifications.enums;

public enum  NotifyStatus {

    OPEN,
    PROCESSED,
    DELIVERY_FAILED
}
