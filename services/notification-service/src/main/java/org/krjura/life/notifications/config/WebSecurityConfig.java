package org.krjura.life.notifications.config;

import biz.db.dev.life.jwt.core.config.JwtProps;
import biz.db.dev.life.jwt.core.filters.JwtAuthenticationFilter;
import org.krjura.life.notifications.enums.Privileges;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.util.Objects;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtProps jwtProps;

    private final AuthenticationManager authenticationManager;

    public WebSecurityConfig(JwtProps jwtProps, AuthenticationManager authenticationManager) {
        Objects.requireNonNull(jwtProps);
        Objects.requireNonNull(authenticationManager);

        this.jwtProps = jwtProps;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/management/health").permitAll()
                    .antMatchers("/api/v1/notify").hasAuthority(Privileges.PW_NS_NOTIFY.name())
                    .antMatchers("/api/v1/notify/**").hasAuthority(Privileges.PW_NS_NOTIFY.name())
                    .antMatchers("/api/v1/users/**").hasAuthority(Privileges.PW_USER.name());

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);

        http.addFilterBefore(
                new JwtAuthenticationFilter(this.jwtProps, this.authenticationManager),
                BasicAuthenticationFilter.class);

        http
                .csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());

        http
                .httpBasic()
                .authenticationEntryPoint(new Http403ForbiddenEntryPoint());

        http.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
    }
}