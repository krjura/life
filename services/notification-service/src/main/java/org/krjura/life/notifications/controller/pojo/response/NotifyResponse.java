package org.krjura.life.notifications.controller.pojo.response;

import java.util.UUID;

public class NotifyResponse {

    private UUID requestId;

    public NotifyResponse() {
        // mappers
    }

    public NotifyResponse(UUID requestId) {
        this.requestId = requestId;
    }

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(UUID requestId) {
        this.requestId = requestId;
    }
}
