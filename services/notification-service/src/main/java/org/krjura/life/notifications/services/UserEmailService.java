package org.krjura.life.notifications.services;

import biz.db.dev.life.exceptions.ServiceException;
import org.krjura.life.notifications.model.UserEmailConfiguration;

import java.util.Optional;
import java.util.UUID;

public interface UserEmailService {

    void setUserEmail(UUID userId, String email) throws ServiceException;

    Optional<UserEmailConfiguration> findEmail(UUID userId);

    void deleteEmail(UUID userId);
}
