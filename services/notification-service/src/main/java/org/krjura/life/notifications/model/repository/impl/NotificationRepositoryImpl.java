package org.krjura.life.notifications.model.repository.impl;

import org.krjura.life.commons.dao.Pageable;
import org.krjura.life.notifications.enums.NotificationFormat;
import org.krjura.life.notifications.enums.NotifyStatus;
import org.krjura.life.notifications.enums.UserNotificationStatus;
import org.krjura.life.notifications.model.NotificationRequest;
import org.krjura.life.notifications.model.UserNotification;
import org.krjura.life.notifications.model.repository.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class NotificationRepositoryImpl implements NotificationRepository {

    private static final Logger logger = LoggerFactory.getLogger(NotificationRepositoryImpl.class);

    private final EntityManager em;

    public NotificationRepositoryImpl(EntityManager em) {
        Assert.notNull(em, "EntityManager cannot be null");

        this.em = em;
    }

    @Override
    @Transactional
    public UUID saveNotifyRequest(UUID userId, NotificationFormat format, String content) {

        Assert.notNull(userId, "userId cannot be null");
        Assert.notNull(format, "format cannot be null");
        Assert.notNull(content, "content cannot be null");

        NotificationRequest request = new NotificationRequest();

        request.setRequestId(UUID.randomUUID());
        request.setUserId(userId);
        request.setFormat(format.name());
        request.setContent(content);
        request.setStatus(NotifyStatus.OPEN);
        request.setExecuteOn(ZonedDateTime.now());
        request.setDeliveredOn(null);
        request.setNumberOfDeliveryAttempts(0);

        this.em.persist(request);

        return request.getRequestId();
    }

    @Override
    @Transactional
    public void saveUserNotification(UserNotification userNotification) {
        this.em.persist(userNotification);
    }

    @Override
    @Transactional
    public Optional<NotificationRequest> findNotificationByRequestId(UUID requestId) {

        Assert.notNull(requestId, "requestId cannot be null");

        Query query = em.createNamedQuery("NotificationRequest.findNotificationByRequestId");

        query.setParameter("requestId", requestId);

        try {
            return Optional.ofNullable((NotificationRequest) query.getSingleResult());
        } catch ( NoResultException | NonUniqueResultException e) {
            logger.trace("no single record found", e);
            return Optional.empty();
        }
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<UserNotification> findUserNotifications(UUID userId, Pageable pagingRequest) {

        Assert.notNull(userId, "userId cannot be null");
        Assert.notNull(pagingRequest, "pagingRequest cannot be null");

        Query query = em.createNamedQuery("UserNotification.findUserNotifications");

        query.setParameter("userId", userId);

        query.setMaxResults(pagingRequest.getPageSize());
        query.setFirstResult(pagingRequest.getOffset());

        return query.getResultList();
    }

    @Override
    @Transactional
    public void updateUserNotificationStatus(UUID userId, UUID messageId, UserNotificationStatus status) {
        Assert.notNull(userId, "userId cannot be null");
        Assert.notNull(messageId, "messageId cannot be null");
        Assert.notNull(status, "status cannot be null");

        Query query = em.createNamedQuery("UserNotification.updateUserNotificationStatus");

        query.setParameter("status", status);
        query.setParameter("userId", userId);
        query.setParameter("messageId", messageId);

        query.executeUpdate();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserNotification> findUserNotificationByMessageId(UUID userId, UUID messageId) {
        Assert.notNull(userId, "userId cannot be null");
        Assert.notNull(messageId, "messageId cannot be null");

        Query query = em.createNamedQuery("UserNotification.findUserNotificationByMessageId");

        query.setParameter("userId", userId);
        query.setParameter("messageId", messageId);

        try {
            return Optional.ofNullable((UserNotification) query.getSingleResult());
        } catch ( NoResultException | NonUniqueResultException e) {
            logger.trace("no single record found", e);
            return Optional.empty();
        }
    }
}
