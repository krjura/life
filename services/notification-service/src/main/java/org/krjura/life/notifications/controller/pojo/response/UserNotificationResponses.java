package org.krjura.life.notifications.controller.pojo.response;

import org.krjura.life.notifications.model.UserNotification;

import java.util.List;
import java.util.stream.Collectors;

public class UserNotificationResponses {

    private List<UserNotificationResponse> content;

    public UserNotificationResponses() {

    }

    public UserNotificationResponses(List<UserNotification> notifications) {
        this.content = notifications
                .stream()
                .map(UserNotificationResponse::new)
                .collect(Collectors.toList());
    }

    public List<UserNotificationResponse> getContent() {
        return content;
    }

    public void setContent(List<UserNotificationResponse> content) {
        this.content = content;
    }
}
