package org.krjura.life.notifications.model.repository;

import org.krjura.life.notifications.model.UserEmailConfiguration;

import java.util.Optional;
import java.util.UUID;

public interface UserEmailRepository {

    Optional<UserEmailConfiguration> findByEmail(String email);

    Optional<UserEmailConfiguration> findByUserId(UUID userId);

    void save(UserEmailConfiguration newConfiguration);

    void deleteByUserId(UUID userId);
}
