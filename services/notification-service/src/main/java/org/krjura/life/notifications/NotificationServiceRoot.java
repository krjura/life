package org.krjura.life.notifications;

import biz.db.dev.life.mvc.error.handlers.MvcErrorHandlers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan( basePackageClasses = {
        NotificationServiceRoot.class,
        MvcErrorHandlers.class
})
@EnableTransactionManagement
public class NotificationServiceRoot {

    public static void main(String[] args) {
        NotificationServiceRoot application = new NotificationServiceRoot();
        application.runSpring(args);
    }

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(NotificationServiceRoot.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }
}
