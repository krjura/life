package org.krjura.life.notifications.model;

import org.krjura.life.notifications.enums.UserNotificationStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name = "user_notification")
@NamedQueries({
        @NamedQuery(
                name = "UserNotification.findUserNotifications",
                query = "FROM UserNotification " +
                        "WHERE userId = :userId"
        ),
        @NamedQuery(
                name = "UserNotification.findUserNotificationByMessageId",
                query = "FROM UserNotification " +
                        "WHERE userId = :userId " +
                        "AND messageId = :messageId"
        ),
        @NamedQuery(
                name = "UserNotification.updateUserNotificationStatus",
                query = "UPDATE UserNotification " +
                        "SET status = :status " +
                        "WHERE userId = :userId " +
                        "AND messageId = :messageId"
        )
})
public class UserNotification {

    @Id
    @Column( name = "message_id", nullable = false)
    private UUID messageId;

    @Column( name = "user_id", nullable = false)
    private UUID userId;

    @Column( name = "delivered_on", nullable = false)
    private ZonedDateTime deliveredOn;

    @Enumerated(EnumType.STRING)
    @Column( name = "status", nullable = false, length = 16)
    private UserNotificationStatus status;

    @Column( name = "format", nullable = false, length = 16)
    private String format;

    @Lob
    @Column( name = "content", nullable = false)
    private String content;

    public UUID getMessageId() {
        return messageId;
    }

    public void setMessageId(UUID messageId) {
        this.messageId = messageId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public ZonedDateTime getDeliveredOn() {
        return deliveredOn;
    }

    public void setDeliveredOn(ZonedDateTime deliveredOn) {
        this.deliveredOn = deliveredOn;
    }

    public UserNotificationStatus getStatus() {
        return status;
    }

    public void setStatus(UserNotificationStatus status) {
        this.status = status;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    // fluent

    public UserNotification messageId(final UUID messageId) {
        setMessageId(messageId);
        return this;
    }

    public UserNotification userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public UserNotification deliveredOn(final ZonedDateTime deliveredOn) {
        setDeliveredOn(deliveredOn);
        return this;
    }

    public UserNotification status(final UserNotificationStatus status) {
        setStatus(status);
        return this;
    }

    public UserNotification format(final String format) {
        setFormat(format);
        return this;
    }

    public UserNotification content(final String content) {
        setContent(content);
        return this;
    }
}
