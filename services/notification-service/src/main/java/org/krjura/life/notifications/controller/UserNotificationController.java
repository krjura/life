package org.krjura.life.notifications.controller;

import biz.db.dev.life.exceptions.UserIsForbiddenException;
import org.krjura.life.commons.dao.DefaultPagingRequest;
import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPutMapping;
import org.krjura.life.notifications.controller.pojo.request.UserNotificationStatusUpdateRequest;
import org.krjura.life.notifications.controller.pojo.response.UserNotificationResponses;
import org.krjura.life.notifications.model.UserNotification;
import org.krjura.life.notifications.services.NotificationService;
import org.krjura.life.notifications.utils.JwtUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Controller
public class UserNotificationController {

    private final NotificationService notificationService;

    public UserNotificationController(NotificationService notificationService) {
        Assert.notNull(notificationService, "NotificationService cannot be null");

        this.notificationService = notificationService;
    }

    @JsonGetMapping(path = "/api/v1//users/{userId}/notifications")
    public ResponseEntity<UserNotificationResponses> userNotifications(
            @PathVariable(value = "userId") UUID userId,
            DefaultPagingRequest pagingRequest) {

        UUID userIdFromAuth = JwtUtils.extractUserId();

        if(!userId.equals(userIdFromAuth)) {
            throw new UserIsForbiddenException();
        }

        List<UserNotification> requests = this.notificationService.findUserNotifications(userIdFromAuth, pagingRequest);

        return ResponseEntity.ok(new UserNotificationResponses(requests));
    }

    @JsonPutMapping(path = "/api/v1/users/{userId}/notifications/{messageId}")
    public ResponseEntity<UserNotificationResponses> userNotificationStatusUpdate(
            @PathVariable(value = "userId") UUID userId,
            @PathVariable(value = "messageId") UUID messageId,
            @RequestBody @Valid UserNotificationStatusUpdateRequest request) {

        UUID userIdFromAuth = JwtUtils.extractUserId();

        if(!userId.equals(userIdFromAuth)) {
            throw new UserIsForbiddenException();
        }

        this.notificationService.updateUserNotificationStatus(userId, messageId, request.getStatus());

        return ResponseEntity.noContent().build();
    }
}
