package org.krjura.life.notifications.services.impl;

import biz.db.dev.life.exceptions.ResourceAlreadyExists;
import biz.db.dev.life.exceptions.ServiceException;
import org.krjura.life.notifications.i18n.LocalizationKeys;
import org.krjura.life.notifications.model.UserEmailConfiguration;
import org.krjura.life.notifications.model.repository.UserEmailRepository;
import org.krjura.life.notifications.services.UserEmailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserEmailServiceImpl implements UserEmailService {

    private final UserEmailRepository userEmailRepository;

    public UserEmailServiceImpl(UserEmailRepository userEmailRepository) {
        Assert.notNull(userEmailRepository, "UserEmailRepository cannot be null");

        this.userEmailRepository = userEmailRepository;
    }

    @Override
    @Transactional
    public void setUserEmail(UUID userId, String email) throws ServiceException {

        Optional<UserEmailConfiguration> userEmailOptional = this.userEmailRepository.findByEmail(email);

        if(userEmailOptional.isPresent()) {
            throw new ResourceAlreadyExists(
                    "username",
                    email,
                    LocalizationKeys.EMAIL_ALREADY_EXISTS,
                    "email " + email + " already exists");
        }

        userEmailOptional = this
                .userEmailRepository.findByUserId(userId);

        if(userEmailOptional.isPresent()) {
            this.userEmailRepository.deleteByUserId(userId);
        }

        UserEmailConfiguration newConfiguration = new UserEmailConfiguration();
        newConfiguration.setUserId(userId);
        newConfiguration.setUserEmail(email);

        this.userEmailRepository.save(newConfiguration);
    }

    @Override
    // @Transactional - not needed simple fetch
    public Optional<UserEmailConfiguration> findEmail(UUID userId) {
        return this.userEmailRepository.findByUserId(userId);
    }

    @Override
    // @Transactional - not needed simple delete
    public void deleteEmail(UUID userId) {
        this.userEmailRepository.deleteByUserId(userId);
    }
}
