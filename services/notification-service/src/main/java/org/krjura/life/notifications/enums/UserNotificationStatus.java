package org.krjura.life.notifications.enums;

public enum UserNotificationStatus {

    UNREAD,
    READ
}
