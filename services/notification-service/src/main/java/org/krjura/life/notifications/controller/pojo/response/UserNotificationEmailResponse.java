package org.krjura.life.notifications.controller.pojo.response;

public class UserNotificationEmailResponse {

    private String email;

    public UserNotificationEmailResponse() {

    }

    public UserNotificationEmailResponse(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
