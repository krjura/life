package org.krjura.life.notifications.enums;

public enum NotificationFormat {

    PLAIN,
    HTML
}
