#!/bin/bash

set -o xtrace

source etc/init/postgres/client-docker.sh

psql -f etc/liquidbase/init/create-databases-in-memory.sql

