--liquibase formatted sql

--changeset kjurasovic:v20180215195400
CREATE TABLE notification_request (
  request_id  UUID NOT NULL,

  user_id UUID NOT NULL,

  format VARCHAR(16) NOT NULL,
  content TEXT NOT NULL,

  CONSTRAINT pk_notification_request PRIMARY KEY (request_id)
);

--changeset kjurasovic:v20180217182400
ALTER TABLE notification_request ADD COLUMN delivered_on TIMESTAMP WITH TIME ZONE NULL;
ALTER TABLE notification_request ADD COLUMN execute_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW();
ALTER TABLE notification_request ADD COLUMN number_of_delivery_attempts INTEGER NOT NULL DEFAULT 0;
ALTER TABLE notification_request ADD COLUMN status VARCHAR(16) NOT NULL DEFAULT 'OPEN';

--changeset kjurasovic:v20180218111100
CREATE TABLE user_notification (
  message_id  UUID NOT NULL,
  user_id UUID NOT NULL,

  delivered_on TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  status VARCHAR(16) NOT NULL,

  format VARCHAR(16) NOT NULL,
  content TEXT NOT NULL,

  CONSTRAINT pk_user_notification PRIMARY KEY (message_id)
);

--changeset kjurasovic:v20180218164200
CREATE TABLE user_email_configuration (
  user_email VARCHAR(255) NOT NULL,

  user_id UUID NOT NULL,

  CONSTRAINT pk_user_email PRIMARY KEY (user_email)
);
