DROP DATABASE IF EXISTS notifications;
CREATE DATABASE notifications ENCODING 'UTF-8';

DROP DATABASE IF EXISTS notifications_tst;
CREATE DATABASE notifications_tst ENCODING 'UTF-8';

DROP USER IF EXISTS notifications;
CREATE USER notifications WITH PASSWORD 'notifications';

DROP USER IF EXISTS notifications_tst;
CREATE USER notifications_tst WITH PASSWORD 'notifications_tst';