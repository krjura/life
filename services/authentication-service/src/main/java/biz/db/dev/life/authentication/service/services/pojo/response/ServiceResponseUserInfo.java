package biz.db.dev.life.authentication.service.services.pojo.response;

import biz.db.dev.life.authentication.service.model.UserBasicInfo;

import java.util.List;

public class ServiceResponseUserInfo {

    private String username;

    private UserBasicInfo userBasicInfo;

    private List<String> roles;
    private List<String> privileges;
    private List<String> grants;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<String> privileges) {
        this.privileges = privileges;
    }

    public List<String> getGrants() {
        return grants;
    }

    public void setGrants(List<String> grants) {
        this.grants = grants;
    }
}
