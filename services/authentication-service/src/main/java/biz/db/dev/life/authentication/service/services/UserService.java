package biz.db.dev.life.authentication.service.services;

import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestChangePassword;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestRegisterUser;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestUpdateBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseCheckLogin;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseRegisterUser;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUpdateBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUserInfo;
import biz.db.dev.life.exceptions.InvalidRequestReceived;
import biz.db.dev.life.exceptions.ServiceException;

import java.util.UUID;

public interface UserService {

    ServiceResponseUpdateBasicInfo provision(ServiceRequestUpdateBasicInfo info) throws ServiceException;

    ServiceResponseRegisterUser provision(ServiceRequestRegisterUser serviceRequest) throws ServiceException;

    void provision(ServiceRequestChangePassword serviceRequest) throws InvalidRequestReceived;

    ServiceResponseCheckLogin checkLogin(String username, String password, UUID tenantId);

    ServiceResponseUserInfo retrieveUserInfo(UUID tenantId, UUID userId);
}
