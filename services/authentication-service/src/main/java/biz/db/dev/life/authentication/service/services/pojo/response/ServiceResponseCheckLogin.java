package biz.db.dev.life.authentication.service.services.pojo.response;

import biz.db.dev.life.authentication.service.model.User;

import java.util.List;

public class ServiceResponseCheckLogin {

    private boolean authenticated;

    private User user;

    private List<String> grants;

    public ServiceResponseCheckLogin(User user, List<String> grants) {
        this.authenticated = true;
        this.user = user;
        this.grants = grants;
    }

    public ServiceResponseCheckLogin() {
        this.authenticated = false;
    }

    public User getUser() {
        return user;
    }

    public List<String> getGrants() {
        return grants;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }
}
