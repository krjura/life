package biz.db.dev.life.authentication.service.services.pojo.response;

import biz.db.dev.life.authentication.service.model.UserBasicInfo;

public class ServiceResponseUpdateBasicInfo {

    private UserBasicInfo userBasicInfo;

    public ServiceResponseUpdateBasicInfo() {
        // mappers
    }

    public ServiceResponseUpdateBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    public UserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    // fluent
    public ServiceResponseUpdateBasicInfo userBasicInfo(final UserBasicInfo userBasicInfo) {
        setUserBasicInfo(userBasicInfo);
        return this;
    }
}
