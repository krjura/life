package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {

    @Query("FROM User")
    @Transactional(readOnly = true)
    List<User> findAllUsers();

    @Query("FROM User u WHERE u.username = :username AND u.tenantId = :tenantId")
    @Transactional(readOnly = true)
    Optional<User> findUserByUsernameAndTenant(@Param("username") String username, @Param("tenantId") UUID tenantId);

    @Query("FROM User u WHERE u.username = :username")
    @Transactional(readOnly = true)
    Optional<User> findUserByUsername(@Param("username") String username);

    @Query("FROM User u WHERE u.tenantId = :tenantId")
    @Transactional(readOnly = true)
    List<User> findUsersByTenantId(@Param("tenantId") UUID tenantId);
}
