package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "application_user_role")
public class UserRole {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    @Column( name = "user_id", nullable = false)
    private UUID userId;

    @Column( name = "tenant_role_id", nullable = false)
    private Integer tenantRoleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Integer getTenantRoleId() {
        return tenantRoleId;
    }

    public void setTenantRoleId(Integer tenantRoleId) {
        this.tenantRoleId = tenantRoleId;
    }

    // fluent

    public UserRole id(final Integer id) {
        setId(id);
        return this;
    }

    public UserRole userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public UserRole tenantRoleId(final Integer tenantRoleId) {
        setTenantRoleId(tenantRoleId);
        return this;
    }
}
