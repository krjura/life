package biz.db.dev.life.authentication.service.controllers;

import biz.db.dev.life.authentication.service.config.props.AuthenticationProps;
import biz.db.dev.life.authentication.service.controllers.pojo.requests.LoginRequest;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.AuthenticationResponse;
import biz.db.dev.life.authentication.service.enums.AuthenticationProviderType;
import biz.db.dev.life.authentication.service.services.AuthService;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceLoginResponse;
import biz.db.dev.life.authentication.service.utils.JwtUtils;
import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.JwtTokenInfo;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.utils.JwtConstants;
import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPostMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Objects;

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    private final AuthenticationProps props;

    private final AuthService authService;

    public LoginController(final AuthenticationProps props, AuthService authService) {
        Objects.requireNonNull(props);
        Objects.requireNonNull(authService);

        this.props = props;
        this.authService = authService;
    }

    @JsonGetMapping(
            path = "/api/v1/auth/renew"
    )
    public ResponseEntity<AuthenticationResponse> renew (
            HttpServletResponse servletResponse) throws JwtProcessingException {

        JwtDetails jwtDetails = JwtUtils.extractJwtDetails();

        JwtTokenInfo token = this.authService.renewToken(jwtDetails);
        addJwtCookieToServletResponse(servletResponse, token.getToken());

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/api/v1/auth/logout")
    public ResponseEntity<Void> logout(HttpServletResponse response) {

        try {
            String token = this.authService.logout();

            addJwtCookieToServletResponse(response, token);

            SecurityContextHolder.clearContext();
        } catch (JwtProcessingException e) {
            logger.warn("Cannot generate logout token", e);
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.noContent().build();
    }

    @JsonPostMapping(
            path = "/api/v1/auth/tenant/{tenantId}/login"
    )
    public ResponseEntity<AuthenticationResponse> login (
            HttpServletResponse servletResponse,
            @PathVariable("tenantId") String tenantId,
            @RequestBody @Valid LoginRequest request) throws JwtProcessingException {

        ServiceLoginResponse response = this
                .authService.login(request.getUsername(), request.getPassword(), tenantId);

        if(response.isAuthenticated()) {
            addJwtCookieToServletResponse(servletResponse, response.getJwtTokenInfo().getToken());
            return ResponseEntity.ok(fillAuthenticationResponse(tenantId, response));
        } else {
            return ResponseEntity.ok(new AuthenticationResponse());
        }
    }

    private AuthenticationResponse fillAuthenticationResponse(
            String tenantId, ServiceLoginResponse response) {

        return new AuthenticationResponse()
                .authorized(true)
                .providerType(AuthenticationProviderType.DATABASE)
                .tenantId(tenantId)
                .grants(response.getLoginInfo().getGrants())
                .validTo(response.getJwtTokenInfo().getValidTo().toInstant().toEpochMilli());
    }

    private void addJwtCookieToServletResponse(HttpServletResponse servletResponse, String token) {
        Cookie cookie = new Cookie(JwtConstants.COOKIE_PARAM_NAME, token);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        cookie.setMaxAge(-1);
        cookie.setSecure(props.isCookiesOverHttpsOnly());

        servletResponse.addCookie(cookie);
    }
}
