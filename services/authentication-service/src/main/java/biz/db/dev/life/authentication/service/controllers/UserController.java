package biz.db.dev.life.authentication.service.controllers;

import biz.db.dev.life.authentication.service.controllers.pojo.nested.GrantedAuthorities;
import biz.db.dev.life.authentication.service.controllers.pojo.nested.UserBasicInfoData;
import biz.db.dev.life.authentication.service.controllers.pojo.requests.RegisterUserRequest;
import biz.db.dev.life.authentication.service.controllers.pojo.requests.UserPasswordChangeRequest;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.LoggedInUserInfoResponse;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.RegisterUserResponse;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.UpdateUserBasicInfoResponse;
import biz.db.dev.life.authentication.service.services.UserService;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestChangePassword;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestRegisterUser;
import biz.db.dev.life.authentication.service.services.pojo.ServiceUserBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestUpdateBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseRegisterUser;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUpdateBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUserInfo;
import biz.db.dev.life.authentication.service.utils.JwtUtils;
import biz.db.dev.life.exceptions.ServiceException;
import org.krjura.life.mvc.helpers.annotations.JsonGetMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPostMapping;
import org.krjura.life.mvc.helpers.annotations.JsonPutMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        Assert.notNull(userService, "UserService cannot be null");

        this.userService = userService;
    }

    @JsonGetMapping(path = "/api/v1/auth/info")
    public ResponseEntity<LoggedInUserInfoResponse> info() {

        UUID userId = JwtUtils.extractUserId();
        UUID tenantId = JwtUtils.extractTenantId();

        ServiceResponseUserInfo serviceResponse = this.userService.retrieveUserInfo(tenantId, userId);

        LoggedInUserInfoResponse response = new LoggedInUserInfoResponse()
                .username(serviceResponse.getUsername())
                .grantedAuthorities(new GrantedAuthorities()
                        .roles(serviceResponse.getRoles())
                        .privileges(serviceResponse.getPrivileges())
                        .grants(serviceResponse.getGrants())
                );

        if(serviceResponse.getUserBasicInfo() != null) {
            response.basicInfo(new UserBasicInfoData()
                    .firstName(serviceResponse.getUserBasicInfo().getFirstName())
                    .middleName(serviceResponse.getUserBasicInfo().getMiddleName())
                    .lastName(serviceResponse.getUserBasicInfo().getLastName())
            );
        }

        return ResponseEntity.ok(response);
    }

    @JsonPostMapping(path = "/api/v1/auth/tenant/{tenantId}/users/register")
    public ResponseEntity<RegisterUserResponse> registerNewUser(
            @PathVariable("tenantId") String tenantId,
            @RequestBody @Valid RegisterUserRequest request) throws ServiceException {

        ServiceRequestRegisterUser serviceRequest = new ServiceRequestRegisterUser()
                .username(request.getUsername())
                .password(request.getPassword())
                .tenantId(request.getTenantId());

        // can be null if user does not want to provide it
        if( request.getUserBasicInfo() != null ) {
            serviceRequest.setUserBasicInfo(
                    new ServiceUserBasicInfo()
                    .firstName(request.getUserBasicInfo().getFirstName())
                    .lastName(request.getUserBasicInfo().getLastName())
                    .middleName(request.getUserBasicInfo().getMiddleName())
            );
        }

        ServiceResponseRegisterUser response = userService.provision(serviceRequest);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new RegisterUserResponse(response));
    }

    @JsonPutMapping(path = "/api/v1/auth/tenant/{tenantId}/users/basic-info")
    public ResponseEntity<UpdateUserBasicInfoResponse> updateBasicInfo(
            @PathVariable("tenantId") String tenantId,
            @RequestBody @Valid UserBasicInfoData info) throws ServiceException {

        UUID userId = JwtUtils.extractUserId();

        ServiceRequestUpdateBasicInfo basicInfo = new ServiceRequestUpdateBasicInfo()
                .firstName(info.getFirstName())
                .middleName(info.getMiddleName())
                .lastName(info.getLastName())
                .userId(userId);

        ServiceResponseUpdateBasicInfo response = userService.provision(basicInfo);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new UpdateUserBasicInfoResponse(response));
    }

    @JsonPutMapping(path = "/api/v1/auth/tenant/{tenantId}/users/change-password")
    public ResponseEntity<Void> changePassword (
            @PathVariable("tenantId") String tenantId,
            @RequestBody @Valid UserPasswordChangeRequest request) throws ServiceException {

        UUID userId = JwtUtils.extractUserId();

        ServiceRequestChangePassword serviceRequest = new ServiceRequestChangePassword()
                .userId(userId)
                .currentPassword(request.getCurrentPassword())
                .newPassword(request.getNewPassword())
                .newPasswordRepeated(request.getNewPasswordRepeated());

        userService.provision(serviceRequest);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
