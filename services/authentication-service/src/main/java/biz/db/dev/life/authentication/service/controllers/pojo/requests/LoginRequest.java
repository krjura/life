package biz.db.dev.life.authentication.service.controllers.pojo.requests;

import org.krjura.life.validations.PasswordConstraint;
import org.krjura.life.validations.UsernameConstraint;

public class LoginRequest {

    @UsernameConstraint
    private String username;

    @PasswordConstraint
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // fluent

    public LoginRequest username(final String username) {
        setUsername(username);
        return this;
    }

    public LoginRequest password(final String password) {
        setPassword(password);
        return this;
    }
}
