package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tenant_privilege")
public class TenantPrivilege {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    @Column( name = "tenant_role_id", nullable = false)
    private Integer tenantRoleId;

    @Column( name = "privilege_name", length = 64, nullable = false)
    private String privilegeName;

    @Column( name = "privilege_description", length = 100, nullable = false)
    private String privilegeDescription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTenantRoleId() {
        return tenantRoleId;
    }

    public void setTenantRoleId(Integer tenantRoleId) {
        this.tenantRoleId = tenantRoleId;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public String getPrivilegeDescription() {
        return privilegeDescription;
    }

    public void setPrivilegeDescription(String privilegeDescription) {
        this.privilegeDescription = privilegeDescription;
    }

    // fluid

    public TenantPrivilege id(final Integer id) {
        setId(id);
        return this;
    }

    public TenantPrivilege tenantRoleId(final Integer tenantRoleId) {
        setTenantRoleId(tenantRoleId);
        return this;
    }

    public TenantPrivilege privilegeName(final String privilegeName) {
        setPrivilegeName(privilegeName);
        return this;
    }

    public TenantPrivilege privilegeDescription(final String privilegeDescription) {
        setPrivilegeDescription(privilegeDescription);
        return this;
    }
}
