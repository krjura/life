package biz.db.dev.life.authentication.service.services.impl;

import biz.db.dev.life.authentication.service.services.JwtTokenService;
import biz.db.dev.life.jwt.core.providers.RevocationProvider;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class DatabaseRevocationProvider implements RevocationProvider {

    private final JwtTokenService jwtTokenService;

    public DatabaseRevocationProvider(JwtTokenService jwtTokenService) {
        Objects.requireNonNull(jwtTokenService);

        this.jwtTokenService = jwtTokenService;
    }

    @Override
    public boolean isTokenRevoked(String token) {
        Objects.requireNonNull(token);

        return jwtTokenService.isTokenRevoked(token);
    }
}
