package biz.db.dev.life.authentication.service.controllers.pojo.requests;

import org.krjura.life.validations.PasswordConstraint;

public class UserPasswordChangeRequest {

    @PasswordConstraint
    private String currentPassword;

    @PasswordConstraint
    private String newPassword;

    @PasswordConstraint
    private String newPasswordRepeated;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeated() {
        return newPasswordRepeated;
    }

    public void setNewPasswordRepeated(String newPasswordRepeated) {
        this.newPasswordRepeated = newPasswordRepeated;
    }

    // fluent

    public UserPasswordChangeRequest currentPassword(final String currentPassword) {
        setCurrentPassword(currentPassword);
        return this;
    }

    public UserPasswordChangeRequest newPassword(final String newPassword) {
        setNewPassword(newPassword);
        return this;
    }

    public UserPasswordChangeRequest newPasswordRepeated(final String newPasswordRepeated) {
        setNewPasswordRepeated(newPasswordRepeated);
        return this;
    }
}
