package biz.db.dev.life.authentication.service.services.pojo.requests;

import java.util.UUID;

public class ServiceRequestUpdateBasicInfo {

    private UUID userId;

    private String firstName;

    private String middleName;

    private String lastName;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // fluent
    public ServiceRequestUpdateBasicInfo userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public ServiceRequestUpdateBasicInfo firstName(final String firstName) {
        setFirstName(firstName);
        return this;
    }

    public ServiceRequestUpdateBasicInfo middleName(final String middleName) {
        setMiddleName(middleName);
        return this;
    }

    public ServiceRequestUpdateBasicInfo lastName(final String lastName) {
        setLastName(lastName);
        return this;
    }
}
