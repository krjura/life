package biz.db.dev.life.authentication.service.services.impl;

import biz.db.dev.life.authentication.service.services.AuthService;
import biz.db.dev.life.authentication.service.services.JwtTokenService;
import biz.db.dev.life.authentication.service.services.UserService;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceLoginResponse;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseCheckLogin;
import biz.db.dev.life.authentication.service.utils.JwtUtils;
import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.JwtTokenBuilder;
import biz.db.dev.life.jwt.core.JwtTokenInfo;
import biz.db.dev.life.jwt.core.config.JwtProps;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.utils.JwtHelper;
import org.krjura.life.commons.utils.ArrayUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    private final JwtProps jwtProps;

    private final UserService userService;

    private final JwtTokenService tokenService;

    public AuthServiceImpl(
            JwtProps jwtProps,
            UserService userService,
            JwtTokenService tokenService) {

        Objects.requireNonNull(jwtProps);
        Objects.requireNonNull(userService);
        Objects.requireNonNull(tokenService);

        this.jwtProps = jwtProps;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @Override
    public String logout() throws JwtProcessingException {

        String defaultKeyId = this.jwtProps.getKeyStore().getDefaultKeyId();
        String privateKey = this.jwtProps.getKeyStore().getKeys().get(defaultKeyId).getPrivateKey();
        String sanitizedPrivateKey = JwtHelper.sanitizePrivateKey(privateKey);

        JwtDetails existingJwtDetails = JwtUtils.extractJwtDetails();

        String token = JwtTokenBuilder
                .create()
                .expiry(60)
                // we need to generate new token otherwise gateway will see revoked token
                .tokenId(UUID.randomUUID().toString())
                .tenantId(existingJwtDetails.getTenantId())
                .userId(existingJwtDetails.getUserId())
                .grants(new String[] {})
                .keyId(defaultKeyId)
                .privateKey(sanitizedPrivateKey)
                .build()
                .getToken();

        this.tokenService.revokeToken(existingJwtDetails);

        return token;
    }

    @Override
    public ServiceLoginResponse login(String username, String password, String tenantId)
            throws JwtProcessingException {

        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        Objects.requireNonNull(tenantId);

        ServiceResponseCheckLogin response = userService
                .checkLogin(username, password, UUID.fromString(tenantId));

        if(response.isAuthenticated()) {
            return new ServiceLoginResponse(
                    response,
                    createToken(tenantId, response.getUser().getId(), response.getGrants())
            );
        } else {
            return new ServiceLoginResponse();
        }
    }

    @Override
    public JwtTokenInfo createToken(String tenantId, UUID userId, List<String> grants) throws JwtProcessingException {

        Objects.requireNonNull(tenantId);
        Objects.requireNonNull(userId);
        Objects.requireNonNull(grants);

        UUID tenantUuid = UUID.fromString(tenantId);

        String keyId = this.jwtProps.getKeyStore().getDefaultKeyId();
        String privateKey = this.jwtProps.getKeyStore().getKeys().get(keyId).getPrivateKey();
        String sanitizedPrivateKey = JwtHelper.sanitizePrivateKey(privateKey);

        return JwtTokenBuilder
                .create()
                .expiry(60)
                .tokenId(UUID.randomUUID().toString())
                .tenantId(tenantUuid)
                .userId(userId)
                .grants(ArrayUtils.toArray(grants))
                .keyId(keyId)
                .privateKey(sanitizedPrivateKey)
                .build();
    }

    @Override
    public JwtTokenInfo renewToken(JwtDetails jwtDetails) throws JwtProcessingException {
        Objects.requireNonNull(jwtDetails);


        String keyId = this.jwtProps.getKeyStore().getDefaultKeyId();
        String privateKey = this.jwtProps.getKeyStore().getKeys().get(keyId).getPrivateKey();
        String sanitizedPrivateKey = JwtHelper.sanitizePrivateKey(privateKey);

        return JwtTokenBuilder
                .create()
                .expiry(60)
                .tokenId(UUID.randomUUID().toString())
                .tenantId(jwtDetails.getTenantId())
                .userId(jwtDetails.getUserId())
                .grants(ArrayUtils.toArray(jwtDetails.getGrants()))
                .keyId(keyId)
                .privateKey(sanitizedPrivateKey)
                .build();
    }

}
