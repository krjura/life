package biz.db.dev.life.authentication.service.controllers.pojo.nested;

import java.util.List;

public class GrantedAuthorities {

    private List<String> roles;

    private List<String> privileges;

    private List<String> grants;

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<String> privileges) {
        this.privileges = privileges;
    }

    public List<String> getGrants() {
        return grants;
    }

    public void setGrants(List<String> grants) {
        this.grants = grants;
    }

    // fluent

    public GrantedAuthorities roles(final List<String> roles) {
        setRoles(roles);
        return this;
    }

    public GrantedAuthorities privileges(final List<String> privileges) {
        setPrivileges(privileges);
        return this;
    }

    public GrantedAuthorities grants(final List<String> grants) {
        setGrants(grants);
        return this;
    }
}
