package biz.db.dev.life.authentication.service.services.impl;

import biz.db.dev.life.authentication.service.config.props.AuthenticationProps;
import biz.db.dev.life.authentication.service.model.TokenRevocation;
import biz.db.dev.life.authentication.service.model.repository.TokenRevocationRepository;
import biz.db.dev.life.authentication.service.services.EventsService;
import biz.db.dev.life.authentication.service.services.JwtTokenService;
import biz.db.dev.life.jwt.core.JwtDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenServiceImpl.class);

    private final AuthenticationProps props;

    private final TokenRevocationRepository tokenRevocationRepository;

    private final EventsService eventsService;

    public JwtTokenServiceImpl(
            AuthenticationProps props,
            EventsService eventsService,
            TokenRevocationRepository tokenRevocationRepository) {

        Objects.requireNonNull(props);
        Objects.requireNonNull(eventsService);
        Objects.requireNonNull(tokenRevocationRepository);

        this.props = props;
        this.eventsService = eventsService;
        this.tokenRevocationRepository = tokenRevocationRepository;
    }

    @Scheduled(cron = "${life.auth.deleteRevokedTokensCron}")
    public void deleteRevokedTokens() {
        try {
            ZonedDateTime ts = ZonedDateTime.now().minusMinutes(props.getDeleteRevokedTokensAfter());

            int count = this.tokenRevocationRepository.deleteTokenBefore(ts);

            logger.info("deleted {} expired revoked tokens", count);
        } catch (Exception e) {
            logger.warn("cannot delete expired tokens", e);
        }
    }

    @Override
    @Transactional
    public void revokeToken(JwtDetails jwtDetails) {
        Objects.requireNonNull(jwtDetails);

        ZonedDateTime timestamp = ZonedDateTime.now();

        TokenRevocation tokenRevocation = new TokenRevocation()
                .token(UUID.fromString(jwtDetails.getTokenId()))
                .revokedAt(timestamp);

        this.tokenRevocationRepository.save(tokenRevocation);

        this.eventsService.revokeToken(jwtDetails, timestamp);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isTokenRevoked(String token) {
        Objects.requireNonNull(token);

        return tokenRevocationRepository.findByToken(UUID.fromString(token)).isPresent();
    }
}
