package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.UserBasicInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface BasicUserRepository extends CrudRepository<UserBasicInfo, Integer> {

    @Query("SELECT ubi FROM UserBasicInfo ubi WHERE ubi.userId = :userId")
    @Transactional(readOnly = true)
    Optional<UserBasicInfo> findBasicInfoByUserId(@Param("userId") UUID id);
}
