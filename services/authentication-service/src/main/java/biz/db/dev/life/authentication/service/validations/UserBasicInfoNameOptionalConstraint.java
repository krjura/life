package biz.db.dev.life.authentication.service.validations;

import org.hibernate.validator.constraints.Length;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = { })
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention( RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Length( max = 32)
public @interface UserBasicInfoNameOptionalConstraint {

    String message() default "life.UserBasicInfoNameOptionalConstraint.message";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
