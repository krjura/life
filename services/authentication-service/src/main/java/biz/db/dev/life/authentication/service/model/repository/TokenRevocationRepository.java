package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.TokenRevocation;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TokenRevocationRepository extends CrudRepository<TokenRevocation, UUID> {

    @Query("SELECT tr FROM TokenRevocation tr WHERE token = :token")
    @Transactional(readOnly = true)
    Optional<TokenRevocation> findByToken(@Param("token") UUID token);

    @Query("DELETE FROM TokenRevocation tr WHERE tr.revokedAt < :ts")
    @Modifying
    @Transactional
    int deleteTokenBefore(@Param("ts") ZonedDateTime ts);

    @Query("SELECT tr FROM TokenRevocation tr")
    List<TokenRevocation> findAll();
}
