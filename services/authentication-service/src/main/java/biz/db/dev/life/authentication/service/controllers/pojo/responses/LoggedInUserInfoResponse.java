package biz.db.dev.life.authentication.service.controllers.pojo.responses;

import biz.db.dev.life.authentication.service.controllers.pojo.nested.GrantedAuthorities;
import biz.db.dev.life.authentication.service.controllers.pojo.nested.UserBasicInfoData;

public class LoggedInUserInfoResponse {

    private String username;

    private UserBasicInfoData basicInfo;

    private GrantedAuthorities grantedAuthorities;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserBasicInfoData getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(UserBasicInfoData basicInfo) {
        this.basicInfo = basicInfo;
    }

    public GrantedAuthorities getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(GrantedAuthorities grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }


    // fluent
    public LoggedInUserInfoResponse username(final String username) {
        setUsername(username);
        return this;
    }

    public LoggedInUserInfoResponse basicInfo(final UserBasicInfoData basicInfo) {
        setBasicInfo(basicInfo);
        return this;
    }

    public LoggedInUserInfoResponse grantedAuthorities(final GrantedAuthorities grantedAuthorities) {
        setGrantedAuthorities(grantedAuthorities);
        return this;
    }
}
