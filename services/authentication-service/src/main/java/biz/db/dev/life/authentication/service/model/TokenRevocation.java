package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name = "token_revocation")
@NamedQueries({
        @NamedQuery(
                name = "TokenRevocation.findByToken",
                query = "FROM TokenRevocation WHERE token = :token"
        )
})
public class TokenRevocation {

    @Id
    private UUID token;

    @Column(name = "revoked_at", nullable = false)
    private ZonedDateTime revokedAt;

    public UUID getToken() {
        return token;
    }

    public void setToken(UUID token) {
        this.token = token;
    }

    public ZonedDateTime getRevokedAt() {
        return revokedAt;
    }

    public void setRevokedAt(ZonedDateTime revokedAt) {
        this.revokedAt = revokedAt;
    }

    // fluent

    public TokenRevocation token(final UUID token) {
        setToken(token);
        return this;
    }

    public TokenRevocation revokedAt(final ZonedDateTime revokedAt) {
        setRevokedAt(revokedAt);
        return this;
    }
}
