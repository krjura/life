package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "tenant_role")
public class TenantRole {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    @Column( name = "tenant_id", nullable = false)
    private UUID tenantId;

    @Column( name = "role_name", length = 64, nullable = false)
    private String roleName;

    @Column( name = "role_description", length = 100, nullable = false)
    private String roleDescription;

    @Column( name = "default_role", nullable = false)
    private boolean defaultRole;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tenantRoleId")
    private Set<TenantPrivilege> privileges;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getTenantId() {
        return tenantId;
    }

    public void setTenantId(UUID tenantId) {
        this.tenantId = tenantId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    public boolean isDefaultRole() {
        return defaultRole;
    }

    public void setDefaultRole(boolean defaultRole) {
        this.defaultRole = defaultRole;
    }

    public Set<TenantPrivilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<TenantPrivilege> privileges) {
        this.privileges = privileges;
    }

    // fluid

    public TenantRole id(final Integer id) {
        setId(id);
        return this;
    }

    public TenantRole tenantId(final UUID tenantId) {
        setTenantId(tenantId);
        return this;
    }

    public TenantRole roleName(final String roleName) {
        setRoleName(roleName);
        return this;
    }

    public TenantRole roleDescription(final String roleDescription) {
        setRoleDescription(roleDescription);
        return this;
    }

    public TenantRole defaultRole(final boolean defaultRole) {
        setDefaultRole(defaultRole);
        return this;
    }

    public TenantRole privileges(final Set<TenantPrivilege> privileges) {
        setPrivileges(privileges);
        return this;
    }
}
