package biz.db.dev.life.authentication.service.i18n;

public class LocalizationKeys {

    public static final String EX_USERNAME_ALREADY_EXISTS = "life.ex.username.exists";
    public static final String EX_TENANT_DOES_NOT_EXISTS = "life.ex.tenant.id.not.exists";
    public static final String EX_USER_ID_DOES_NOT_EXISTS = "life.ex.user.id.not.exists";
    public static final String EX_USER_PASSWORD_MISMATCH = "life.ex.user.password.mismatch";;
}
