package biz.db.dev.life.authentication.service.config;

import biz.db.dev.life.authentication.service.enums.QueueNames;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmqpConfig {

    @Bean
    @Qualifier(QueueNames.LOGIN_UPDATES_EXCHANGE)
    public Exchange loginUpdatesExchange() {
        return ExchangeBuilder
                .fanoutExchange(QueueNames.LOGIN_UPDATES_EXCHANGE)
                .durable(true)
                .build();
    }
}
