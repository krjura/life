package biz.db.dev.life.authentication.service.services.pojo.requests;

import java.util.UUID;

public class ServiceRequestChangePassword {

    private UUID userId;

    private String currentPassword;

    private String newPassword;

    private String newPasswordRepeated;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeated() {
        return newPasswordRepeated;
    }

    public void setNewPasswordRepeated(String newPasswordRepeated) {
        this.newPasswordRepeated = newPasswordRepeated;
    }

    // fluent

    public ServiceRequestChangePassword userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public ServiceRequestChangePassword currentPassword(final String currentPassword) {
        setCurrentPassword(currentPassword);
        return this;
    }

    public ServiceRequestChangePassword newPassword(final String newPassword) {
        setNewPassword(newPassword);
        return this;
    }

    public ServiceRequestChangePassword newPasswordRepeated(final String newPasswordRepeated) {
        setNewPasswordRepeated(newPasswordRepeated);
        return this;
    }
}
