package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "tenant")
public class Tenant {

    @Id
    @Column( name = "id", nullable = false)
    private UUID id;

    @Column( name = "tenant_name", length = 100, nullable = false)
    private String tenantName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tenantId")
    private Set<TenantRole> tenantRoles;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public Set<TenantRole> getTenantRoles() {
        return tenantRoles;
    }

    public void setTenantRoles(Set<TenantRole> tenantRoles) {
        this.tenantRoles = tenantRoles;
    }

    // fluent
    public Tenant id(final UUID id) {
        setId(id);
        return this;
    }

    public Tenant tenantName(final String tenantName) {
        setTenantName(tenantName);
        return this;
    }

    public Tenant tenantRoles(final Set<TenantRole> tenantRoles) {
        setTenantRoles(tenantRoles);
        return this;
    }
}
