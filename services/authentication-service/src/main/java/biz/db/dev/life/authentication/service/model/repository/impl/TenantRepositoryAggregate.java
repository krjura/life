package biz.db.dev.life.authentication.service.model.repository.impl;

import biz.db.dev.life.authentication.service.model.repository.TenantPrivilegeRepository;
import biz.db.dev.life.authentication.service.model.repository.TenantRepository;
import biz.db.dev.life.authentication.service.model.repository.TenantRoleRepository;
import org.springframework.stereotype.Service;

@Service
public class TenantRepositoryAggregate {

    private final TenantRepository tenantRepository;

    private final TenantRoleRepository tenantRoleRepository;

    private final TenantPrivilegeRepository tenantPrivilegeRepository;

    public TenantRepositoryAggregate(
            TenantRepository tenantRepository,
            TenantRoleRepository tenantRoleRepository,
            TenantPrivilegeRepository tenantPrivilegeRepository) {

        this.tenantRepository = tenantRepository;
        this.tenantRoleRepository = tenantRoleRepository;
        this.tenantPrivilegeRepository = tenantPrivilegeRepository;
    }

    public TenantRepository getTenantRepository() {
        return tenantRepository;
    }

    public TenantRoleRepository getTenantRoleRepository() {
        return tenantRoleRepository;
    }

    public TenantPrivilegeRepository getTenantPrivilegeRepository() {
        return tenantPrivilegeRepository;
    }
}
