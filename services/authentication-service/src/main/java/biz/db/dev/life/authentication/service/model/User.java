package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "application_user")
public class User {

    @Id
    @Column( name = "id", nullable = false)
    private UUID id;

    @Column( name = "tenant_id", nullable = false)
    private UUID tenantId;

    @Column(name = "username", length = 64, nullable = false)
    private String username;

    @Column(name = "password", length = 64, nullable = false)
    private String password;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userId")
    private Set<UserRole> roles;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "user")
    private UserBasicInfo userBasicInfo;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getTenantId() {
        return tenantId;
    }

    public void setTenantId(UUID tenantId) {
        this.tenantId = tenantId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public UserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    // fluid
    public User id(final UUID id) {
        setId(id);
        return this;
    }

    public User tenantId(final UUID tenantId) {
        setTenantId(tenantId);
        return this;
    }

    public User username(final String username) {
        setUsername(username);
        return this;
    }

    public User password(final String password) {
        setPassword(password);
        return this;
    }

    public User enabled(final boolean enabled) {
        setEnabled(enabled);
        return this;
    }

    public User roles(final Set<UserRole> roles) {
        setRoles(roles);
        return this;
    }

    public User userBasicInfo(final UserBasicInfo userBasicInfo) {
        setUserBasicInfo(userBasicInfo);
        return this;
    }
}
