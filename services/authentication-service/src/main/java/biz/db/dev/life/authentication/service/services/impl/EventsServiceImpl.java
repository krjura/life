package biz.db.dev.life.authentication.service.services.impl;

import biz.db.dev.life.authentication.service.enums.QueueNames;
import biz.db.dev.life.authentication.service.services.EventsService;
import biz.db.dev.life.exceptions.ConversionException;
import biz.db.dev.life.jwt.core.JwtDetails;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.krjura.life.commons.time.FormattingUtils;
import org.krjura.life.events.EventMetadata;
import org.krjura.life.events.TokenRevokedEvent;
import org.krjura.life.json.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

@Service
public class EventsServiceImpl implements EventsService {

    private static final Logger logger = LoggerFactory.getLogger(EventsServiceImpl.class);

    private final RabbitTemplate rabbitTemplate;

    public EventsServiceImpl(RabbitTemplate rabbitTemplate) {
        Objects.requireNonNull(rabbitTemplate);

        this.rabbitTemplate = rabbitTemplate;
    }

    public void revokeToken(JwtDetails jwtDetails, ZonedDateTime revokedAt) {

        try {
            String messageId = UUID.randomUUID().toString();
            String expiresAtString = FormattingUtils.from(jwtDetails.getExpiresAt());
            String revokedAtString = FormattingUtils.from(revokedAt);

            EventMetadata metadata = new EventMetadata(messageId, messageId, "1.0.0", revokedAtString);

            TokenRevokedEvent event = new TokenRevokedEvent(
                    metadata, jwtDetails.getTokenId(), expiresAtString, revokedAtString);

            MessageProperties properties = buildJsonMessageProperties(messageId, TokenRevokedEvent.class);

            sendMessage(event, properties);
        } catch (JsonProcessingException e) {
            logger.warn("Cannot generate body for revokeToken", e);
            throw new ConversionException("Cannot generate body for revokeToken", e);
        }
    }

    private void sendMessage(Object event, MessageProperties properties) throws JsonProcessingException {
        byte[] messageData = JsonUtils.from(event).getBytes();

        Message message = MessageBuilder
                .withBody(messageData)
                .andProperties(properties)
                .build();

        this.rabbitTemplate.send(QueueNames.LOGIN_UPDATES_EXCHANGE, QueueNames.LOGIN_UPDATES_RK, message);
    }

    private MessageProperties buildJsonMessageProperties(String messageId, Class<?> clazz) {
        return MessagePropertiesBuilder
                        .newInstance()
                        .setContentType(MediaType.APPLICATION_JSON_VALUE)
                        .setCorrelationId(messageId)
                        .setMessageId(messageId)
                        .setType(clazz.getSimpleName())
                        .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                        .build();
    }
}
