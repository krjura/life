package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.Tenant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TenantRepository extends CrudRepository<Tenant, UUID> {

    @Query("SELECT t FROM Tenant t")
    @Transactional(readOnly = true)
    List<Tenant> findAllTenants();

    @Query("SELECT t FROM Tenant t WHERE t.id = :id")
    @Transactional(readOnly = true)
    Optional<Tenant> findTenantById(@Param("id") UUID tenantId);
}
