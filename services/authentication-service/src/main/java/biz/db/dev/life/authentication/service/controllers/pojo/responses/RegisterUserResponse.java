package biz.db.dev.life.authentication.service.controllers.pojo.responses;

import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseRegisterUser;

import java.util.UUID;

public class RegisterUserResponse {

    private User user;

    private String role;

    private UserBasicInfo basicInfo;

    public RegisterUserResponse() {
        // mappers
    }

    public RegisterUserResponse(ServiceResponseRegisterUser response) {
        prepareUser(response);
        prepareRole(response);
        prepareBasicInfo(response);
    }

    private void prepareUser(ServiceResponseRegisterUser response) {
        this.user = new User()
                .id(response.getUser().getId())
                .tenantId(response.getUser().getTenantId())
                .username(response.getUser().getUsername())
                .enabled(response.getUser().isEnabled());
    }

    private void prepareRole(ServiceResponseRegisterUser response) {
        if( response.getUserRole() != null) {
            this.role = response.getTenantRole().getRoleName();
        }
    }

    private void prepareBasicInfo(ServiceResponseRegisterUser response) {
        if(response.getUserBasicInfo() != null) {
            this.basicInfo = new UserBasicInfo()
                    .firstName(response.getUserBasicInfo().getFirstName())
                    .middleName(response.getUserBasicInfo().getMiddleName())
                    .lastName(response.getUserBasicInfo().getLastName());
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserBasicInfo getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(UserBasicInfo basicInfo) {
        this.basicInfo = basicInfo;
    }

    public class UserBasicInfo {

        private String firstName;

        private String middleName;

        private String lastName;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        // fluent
        public UserBasicInfo firstName(final String firstName) {
            setFirstName(firstName);
            return this;
        }

        public UserBasicInfo middleName(final String middleName) {
            setMiddleName(middleName);
            return this;
        }

        public UserBasicInfo lastName(final String lastName) {
            setLastName(lastName);
            return this;
        }
    }

    public class User {

        private UUID id;

        private UUID tenantId;

        private String username;

        private boolean enabled;

        public UUID getId() {
            return id;
        }

        public void setId(UUID id) {
            this.id = id;
        }

        public UUID getTenantId() {
            return tenantId;
        }

        public void setTenantId(UUID tenantId) {
            this.tenantId = tenantId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }


        // fluent
        public User id(final UUID id) {
            setId(id);
            return this;
        }

        public User tenantId(final UUID tenantId) {
            setTenantId(tenantId);
            return this;
        }

        public User username(final String username) {
            setUsername(username);
            return this;
        }

        public User enabled(final boolean enabled) {
            setEnabled(enabled);
            return this;
        }
    }
}
