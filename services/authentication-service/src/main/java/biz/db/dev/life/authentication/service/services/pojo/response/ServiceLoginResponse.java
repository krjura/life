package biz.db.dev.life.authentication.service.services.pojo.response;

import biz.db.dev.life.jwt.core.JwtTokenInfo;

public class ServiceLoginResponse {

    private boolean authenticated;

    private final ServiceResponseCheckLogin loginInfo;

    private final JwtTokenInfo jwtTokenInfo;

    public ServiceLoginResponse() {
        this.authenticated = false;

        this.loginInfo = null;
        this.jwtTokenInfo = null;
    }

    public ServiceLoginResponse(ServiceResponseCheckLogin loginInfo, JwtTokenInfo jwtTokenInfo) {
        this.authenticated = true;
        this.loginInfo = loginInfo;
        this.jwtTokenInfo = jwtTokenInfo;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public ServiceResponseCheckLogin getLoginInfo() {
        return loginInfo;
    }

    public JwtTokenInfo getJwtTokenInfo() {
        return jwtTokenInfo;
    }
}