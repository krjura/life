package biz.db.dev.life.authentication.service.enums;

public class QueueNames {

    public static final String LOGIN_UPDATES_EXCHANGE = "exchange.login-updates";
    public static final String LOGIN_UPDATES_QUEUE = "auth.queue.login-updates";
    public static final String LOGIN_UPDATES_RK = "login.updates";
}
