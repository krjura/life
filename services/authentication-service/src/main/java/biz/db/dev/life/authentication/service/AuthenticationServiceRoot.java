package biz.db.dev.life.authentication.service;

import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.mvc.error.handlers.MvcErrorHandlers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfigurationProperties
@EnableScheduling
@EntityScan(basePackageClasses = {
        Tenant.class
})
@ComponentScan( basePackageClasses = {
        AuthenticationServiceRoot.class,
        MvcErrorHandlers.class
})
public class AuthenticationServiceRoot {

    public static void main(String[] args) {
        AuthenticationServiceRoot application = new AuthenticationServiceRoot();
        application.runSpring(args);
    }

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(AuthenticationServiceRoot.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }
}