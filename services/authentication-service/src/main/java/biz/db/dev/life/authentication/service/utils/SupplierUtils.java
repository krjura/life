package biz.db.dev.life.authentication.service.utils;

import biz.db.dev.life.exceptions.UserIsForbiddenException;

import java.util.function.Supplier;

public class SupplierUtils {

    private SupplierUtils() {
        // util
    }

    public static Supplier<UserIsForbiddenException> throwUserIsForbiddenException() {
        return UserIsForbiddenException::new;
    }
}
