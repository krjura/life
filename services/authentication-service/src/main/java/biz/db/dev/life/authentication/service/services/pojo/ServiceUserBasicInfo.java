package biz.db.dev.life.authentication.service.services.pojo;

public class ServiceUserBasicInfo {

    private String firstName;

    private String middleName;

    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // fluid

    public ServiceUserBasicInfo firstName(final String firstName) {
        setFirstName(firstName);
        return this;
    }

    public ServiceUserBasicInfo middleName(final String middleName) {
        setMiddleName(middleName);
        return this;
    }

    public ServiceUserBasicInfo lastName(final String lastName) {
        setLastName(lastName);
        return this;
    }
}
