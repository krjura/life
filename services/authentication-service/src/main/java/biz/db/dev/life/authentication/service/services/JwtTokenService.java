package biz.db.dev.life.authentication.service.services;

import biz.db.dev.life.jwt.core.JwtDetails;

public interface JwtTokenService {

    void deleteRevokedTokens();

    void revokeToken(JwtDetails jwtDetails);

    boolean isTokenRevoked(String token);

}
