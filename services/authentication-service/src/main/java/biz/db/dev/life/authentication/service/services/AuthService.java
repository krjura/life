package biz.db.dev.life.authentication.service.services;

import biz.db.dev.life.authentication.service.services.pojo.response.ServiceLoginResponse;
import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.JwtTokenInfo;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;

import java.util.List;
import java.util.UUID;

public interface AuthService {

    String logout() throws JwtProcessingException;

    ServiceLoginResponse login(String username, String password, String tenantId) throws JwtProcessingException;

    JwtTokenInfo createToken(String tenantId, UUID userId, List<String> grants) throws JwtProcessingException;

    JwtTokenInfo renewToken(JwtDetails jwtDetails) throws JwtProcessingException;
}
