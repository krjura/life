package biz.db.dev.life.authentication.service.config.props;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Configuration
@ConfigurationProperties(prefix = "life.auth")
@Validated
public class AuthenticationPropsImpl implements AuthenticationProps {

    @Min(value = 0)
    private int deleteRevokedTokensAfter = -1; // minutes

    @NotEmpty
    private String deleteRevokedTokensCron;

    private boolean cookiesOverHttpsOnly = false;

    @Override
    public long getDeleteRevokedTokensAfter() {
        return deleteRevokedTokensAfter;
    }

    public void setDeleteRevokedTokensAfter(int deleteRevokedTokensAfter) {
        this.deleteRevokedTokensAfter = deleteRevokedTokensAfter;
    }

    @Override
    public String getDeleteRevokedTokensCron() {
        return deleteRevokedTokensCron;
    }

    public void setDeleteRevokedTokensCron(String deleteRevokedTokensCron) {
        this.deleteRevokedTokensCron = deleteRevokedTokensCron;
    }

    @Override
    public boolean isCookiesOverHttpsOnly() {
        return cookiesOverHttpsOnly;
    }

    public void setCookiesOverHttpsOnly(boolean cookiesOverHttpsOnly) {
        this.cookiesOverHttpsOnly = cookiesOverHttpsOnly;
    }
}
