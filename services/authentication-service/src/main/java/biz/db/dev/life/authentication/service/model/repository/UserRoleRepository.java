package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    @Query("FROM UserRole ur WHERE ur.userId = :userId")
    @Transactional(readOnly = true)
    List<UserRole> findUserRoleByUserId( @Param("userId") UUID userId);

    @Query("FROM UserRole")
    @Transactional(readOnly = true)
    List<UserRole> findAllUserRoles();
}
