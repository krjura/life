package biz.db.dev.life.authentication.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "application_user_basic_info")
public class UserBasicInfo {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;

    @Column( name = "user_id", nullable = false)
    private UUID userId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", updatable = false, insertable = false, nullable = false)
    private User user;

    @Column( name = "first_name", length = 32, nullable = false)
    private String firstName;

    @Column( name = "middle_name", length = 32)
    private String middleName;

    @Column( name = "last_name", length = 32, nullable = false)
    private String lastName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // fluid
    public UserBasicInfo id(final Integer id) {
        setId(id);
        return this;
    }

    public UserBasicInfo userId(final UUID userId) {
        setUserId(userId);
        return this;
    }

    public UserBasicInfo firstName(final String firstName) {
        setFirstName(firstName);
        return this;
    }

    public UserBasicInfo middleName(final String middleName) {
        setMiddleName(middleName);
        return this;
    }

    public UserBasicInfo lastName(final String lastName) {
        setLastName(lastName);
        return this;
    }


}
