package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.TenantRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TenantRoleRepository extends CrudRepository<TenantRole, Integer> {

    @Query("SELECT tr FROM TenantRole tr")
    @Transactional(readOnly = true)
    List<TenantRole> findAllTenantRoles();

    @Query("FROM TenantRole tr WHERE tr.id = :id")
    @Transactional(readOnly = true)
    Optional<TenantRole> findTenantRoleById( @Param("id") Integer id);

    @Query("SELECT tr FROM TenantRole tr " +
            "LEFT JOIN FETCH tr.privileges " +
            "WHERE tr.tenantId = :tenantId AND tr.id IN :tenantRoles")
    @Transactional(readOnly = true)
    List<TenantRole> findTenantRolesById(
            @Param("tenantId") UUID tenantId, @Param("tenantRoles") List<Integer> tenantRoles);

    @Query("FROM TenantRole tr WHERE tr.tenantId = :tenantId")
    @Transactional(readOnly = true)
    List<TenantRole> findTenantRolesByTenantId(@Param("tenantId") UUID tenantId);

    @Query("FROM TenantRole tr WHERE tr.tenantId = :tenantId AND defaultRole = true")
    @Transactional(readOnly = true)
    Optional<TenantRole> findDefaultTenantRole(@Param("tenantId")UUID tenantId);
}
