package biz.db.dev.life.authentication.service.utils;

import biz.db.dev.life.exceptions.UserIsForbiddenException;
import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.JwtDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.UUID;

public class JwtUtils {

    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    public JwtUtils() {
        // util
    }

    public static UUID extractUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        checkForJwtAuthenticationToken(authentication);

        return ((JwtDetails) authentication.getDetails()).getUserId();
    }

    private static void checkForJwtAuthenticationToken(Authentication authentication) {
        if( ! (authentication instanceof JwtAuthenticationToken) ) {
            logger.debug("authentication is not of type JwtAuthenticationToken but {}",
                    authentication.getClass().getSimpleName());

            throw new UserIsForbiddenException();
        }
    }

    public static UUID extractTenantId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        checkForJwtAuthenticationToken(authentication);

        return ((JwtDetails) authentication.getDetails()).getTenantId();
    }

    public static JwtDetails extractJwtDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return ((JwtDetails) authentication.getDetails());
    }
}
