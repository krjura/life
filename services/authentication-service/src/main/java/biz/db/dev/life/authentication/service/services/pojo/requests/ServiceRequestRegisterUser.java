package biz.db.dev.life.authentication.service.services.pojo.requests;

import biz.db.dev.life.authentication.service.services.pojo.ServiceUserBasicInfo;

public class ServiceRequestRegisterUser {

    private String username;

    private String password;

    private String tenantId;

    private ServiceUserBasicInfo userBasicInfo;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public ServiceUserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(ServiceUserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    // fluid

    public ServiceRequestRegisterUser username(final String username) {
        setUsername(username);
        return this;
    }

    public ServiceRequestRegisterUser password(final String password) {
        setPassword(password);
        return this;
    }

    public ServiceRequestRegisterUser tenantId(final String tenantId) {
        setTenantId(tenantId);
        return this;
    }

    public ServiceRequestRegisterUser userBasicInfo(final ServiceUserBasicInfo userBasicInfo) {
        setUserBasicInfo(userBasicInfo);
        return this;
    }
}
