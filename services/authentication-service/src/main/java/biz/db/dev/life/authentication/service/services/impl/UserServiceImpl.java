package biz.db.dev.life.authentication.service.services.impl;

import biz.db.dev.life.authentication.service.i18n.LocalizationKeys;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantPrivilege;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserBasicInfo;
import biz.db.dev.life.authentication.service.model.UserRole;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import biz.db.dev.life.authentication.service.model.repository.impl.UserRepositoryAggregate;
import biz.db.dev.life.authentication.service.services.UserService;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestChangePassword;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestUpdateBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.requests.ServiceRequestRegisterUser;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseCheckLogin;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseRegisterUser;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUpdateBasicInfo;
import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUserInfo;
import biz.db.dev.life.authentication.service.utils.SupplierUtils;
import biz.db.dev.life.exceptions.InvalidRequestReceived;
import biz.db.dev.life.exceptions.ResourceAlreadyExists;
import biz.db.dev.life.exceptions.ResourceDoesNotExists;
import biz.db.dev.life.exceptions.ServiceException;
import biz.db.dev.life.exceptions.UserIsForbiddenException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepositoryAggregate userRepositoryAggregate;

    private final TenantRepositoryAggregate tenantRepositoryAggregate;

    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(
            UserRepositoryAggregate userRepositoryAggregate,
            TenantRepositoryAggregate tenantRepositoryAggregate,
            PasswordEncoder passwordEncoder) {

        Assert.notNull(userRepositoryAggregate, "UserRepositoryAggregate cannot be null");
        Assert.notNull(tenantRepositoryAggregate, "TenantRepositoryAggregate cannot be null");
        Assert.notNull(passwordEncoder, "PasswordEncoder cannot be null");

        this.userRepositoryAggregate = userRepositoryAggregate;
        this.tenantRepositoryAggregate = tenantRepositoryAggregate;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public ServiceResponseUpdateBasicInfo provision(ServiceRequestUpdateBasicInfo info) throws ServiceException {
        verifyUserDoesExist(info);

        Optional<UserBasicInfo> basicInfoOptional = this
                .userRepositoryAggregate.getBasicUserRepository().findBasicInfoByUserId(info.getUserId());

        UserBasicInfo basicInfo;

        if( basicInfoOptional.isPresent()) {
            basicInfo = basicInfoOptional.get();

            basicInfo.setFirstName(info.getFirstName());
            basicInfo.setLastName(info.getLastName());
            basicInfo.setMiddleName(info.getMiddleName());
        } else {
            basicInfo = new UserBasicInfo()
                    .firstName(info.getFirstName())
                    .middleName(info.getMiddleName())
                    .lastName(info.getLastName())
                    .userId(info.getUserId());

        }

        userRepositoryAggregate.getBasicUserRepository().save(basicInfo);
        return new ServiceResponseUpdateBasicInfo(basicInfo);
    }

    @Override
    @Transactional
    public ServiceResponseRegisterUser provision(ServiceRequestRegisterUser serviceRequest) throws ServiceException {

        verifyUserDoesNotExists(serviceRequest);
        verifyTenantExists(serviceRequest);

        Optional<TenantRole> tenantRoleOptional  = this
                .tenantRepositoryAggregate
                .getTenantRoleRepository().findDefaultTenantRole(UUID.fromString(serviceRequest.getTenantId()));

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(UUID.fromString(serviceRequest.getTenantId()))
                .username(serviceRequest.getUsername())
                .password(passwordEncoder.encode(serviceRequest.getPassword()))
                .enabled(true);

        UserRole userRole = null;
        UserBasicInfo userBasicInfo = null;
        TenantRole tenantRole = null;

        this.userRepositoryAggregate.getUserRepository().save(user);

        if(tenantRoleOptional.isPresent()) {
            userRole = new UserRole()
                    .userId(user.getId())
                    .tenantRoleId(tenantRoleOptional.get().getId());
            tenantRole = tenantRoleOptional.get();

            this.userRepositoryAggregate.getUserRoleRepository().save(userRole);
        }

        if(serviceRequest.getUserBasicInfo() != null) {
            userBasicInfo = new UserBasicInfo()
                    .userId(user.getId())
                    .firstName(serviceRequest.getUserBasicInfo().getFirstName())
                    .middleName(serviceRequest.getUserBasicInfo().getMiddleName())
                    .lastName(serviceRequest.getUserBasicInfo().getLastName());

            this.userRepositoryAggregate.getBasicUserRepository().save(userBasicInfo);
        }

        return new ServiceResponseRegisterUser()
                .user(user)
                .userRole(userRole)
                .tenantRole(tenantRole)
                .userBasicInfo(userBasicInfo);
    }

    @Override
    @Transactional
    public void provision(ServiceRequestChangePassword serviceRequest) throws InvalidRequestReceived {
        Assert.notNull(serviceRequest, "ServiceRequestChangePassword cannot be null");
        Assert.notNull(serviceRequest.getCurrentPassword(), "Current password not defined");
        Assert.notNull(serviceRequest.getNewPassword(), "New password not defined");
        Assert.notNull(serviceRequest.getNewPasswordRepeated(), "New password repeated not defined");
        Assert.notNull(serviceRequest.getUserId(), "User id not defined");

        // there should be user with this id
        // if not someone is trying to break in or session has expired
        User user = this
                .userRepositoryAggregate
                .getUserRepository()
                .findById(serviceRequest.getUserId())
                .orElseThrow(SupplierUtils.throwUserIsForbiddenException());

        // current password must match
        if (!this.passwordEncoder.matches(serviceRequest.getCurrentPassword(), user.getPassword()) ) {
            throw new UserIsForbiddenException();
        }

        if(!serviceRequest.getNewPassword().equals(serviceRequest.getNewPasswordRepeated())) {
            throw new InvalidRequestReceived(
                    "Passwords do not match",
                    "password",
                    "HIDDEN",
                    LocalizationKeys.EX_USER_PASSWORD_MISMATCH
            );
        }

        user.setPassword(passwordEncoder.encode(serviceRequest.getNewPassword()));
        this.userRepositoryAggregate.getUserRepository().save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public ServiceResponseCheckLogin checkLogin(String username, String password, UUID tenantId) {
        Assert.notNull(username, "username cannot be null");
        Assert.notNull(password, "password cannot be null");
        Assert.notNull(tenantId, "tenantId cannot be null");

        User user = this
                .userRepositoryAggregate
                .getUserRepository()
                .findUserByUsernameAndTenant(username, tenantId)
                .orElseThrow(SupplierUtils.throwUserIsForbiddenException());

        if( passwordEncoder.matches(password, user.getPassword())) {
            List<String> grants = resolveGrants(tenantId, user);
            return new ServiceResponseCheckLogin(user, grants);
        } else {
            return new ServiceResponseCheckLogin();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ServiceResponseUserInfo retrieveUserInfo(UUID tenantId, UUID userId) {
        Assert.notNull(tenantId, "tenantId cannot be null");
        Assert.notNull(userId, "userId cannot be null");

        User user = this
                .userRepositoryAggregate
                .getUserRepository().findById(userId)
                .orElseThrow(SupplierUtils.throwUserIsForbiddenException());

        Optional<UserBasicInfo> basicInfoOptional = this
                .userRepositoryAggregate
                .getBasicUserRepository()
                .findBasicInfoByUserId(userId);

        List<UserRole> userRoles = this.userRepositoryAggregate.getUserRoleRepository().findUserRoleByUserId(userId);
        List<Integer> tenantRoleIds = userRoles.stream().map(UserRole::getTenantRoleId).collect(Collectors.toList());
        List<TenantRole> tenantRoles = tenantRoleIds.size() == 0 ? Collections.emptyList() :
                tenantRepositoryAggregate.getTenantRoleRepository().findTenantRolesById(tenantId, tenantRoleIds);

        List<String> roles = new ArrayList<>();
        List<String> privileges = new ArrayList<>();
        List<String> grants = new ArrayList<>();

        for(TenantRole role : tenantRoles) {
            List<String> rolePrivileges = role
                    .getPrivileges()
                    .stream()
                    .map(TenantPrivilege::getPrivilegeName)
                    .collect(Collectors.toList());

            roles.add(role.getRoleName());
            privileges.addAll(rolePrivileges);

            grants.add(role.getRoleName());
            grants.addAll(rolePrivileges);
        }

        ServiceResponseUserInfo response = new ServiceResponseUserInfo();

        response.setUsername(user.getUsername());
        response.setRoles(roles);
        response.setPrivileges(privileges);
        response.setGrants(grants);

        basicInfoOptional.ifPresent(response::setUserBasicInfo);

        return response;
    }

    private List<String> resolveGrants(UUID tenantId, User user) {
        List<Integer> tenantRoles = this
                .userRepositoryAggregate
                .getUserRoleRepository()
                .findUserRoleByUserId(user.getId())
                .stream()
                .map(UserRole::getTenantRoleId)
                .collect(Collectors.toList());

        List<TenantRole> roles = this
                .tenantRepositoryAggregate.getTenantRoleRepository().findTenantRolesById(tenantId, tenantRoles);

        List<String> grants = new ArrayList<>();
        for(TenantRole role : roles) {
            grants.add(role.getRoleName());
            grants.addAll(role
                    .getPrivileges()
                    .stream()
                    .map(TenantPrivilege::getPrivilegeName)
                    .collect(Collectors.toList()));
        }

        return grants;
    }

    private void verifyUserDoesExist(ServiceRequestUpdateBasicInfo info) throws ResourceDoesNotExists {
        Optional<User> userOptional = this
                .userRepositoryAggregate
                .getUserRepository()
                .findById(info.getUserId());

        // username should not exist in the database
        if(!userOptional.isPresent()) {
            throw new ResourceDoesNotExists(
                    "userId",
                    info.getUserId().toString(),
                    LocalizationKeys.EX_USER_ID_DOES_NOT_EXISTS,
                    "User with id of " + info.getUserId().toString() + " does exist"
            );
        }
    }

    private void verifyTenantExists(ServiceRequestRegisterUser serviceRequest) throws ResourceDoesNotExists {
        Optional<Tenant> tenantOptional = this
                .tenantRepositoryAggregate
                .getTenantRepository().findTenantById(UUID.fromString(serviceRequest.getTenantId()));

        if(! tenantOptional.isPresent()) {
            throw new ResourceDoesNotExists(
                    "tenantId",
                    serviceRequest.getTenantId(),
                    LocalizationKeys.EX_TENANT_DOES_NOT_EXISTS,
                    "Tenant with id of " + serviceRequest.getTenantId() + " does not exists");
        }
    }

    private void verifyUserDoesNotExists(ServiceRequestRegisterUser serviceRequest) throws ResourceAlreadyExists {
        Optional<User> userOptional = this
                .userRepositoryAggregate
                .getUserRepository()
                .findUserByUsername(serviceRequest.getUsername());

        // username should not exist in the database
        if(userOptional.isPresent()) {
            throw new ResourceAlreadyExists(
                    "username",
                    serviceRequest.getUsername(),
                    LocalizationKeys.EX_USERNAME_ALREADY_EXISTS,
                    "User with username " + serviceRequest.getUsername() + " already exists");
        }
    }
}
