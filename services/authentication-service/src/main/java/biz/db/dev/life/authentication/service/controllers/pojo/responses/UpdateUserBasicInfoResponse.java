package biz.db.dev.life.authentication.service.controllers.pojo.responses;

import biz.db.dev.life.authentication.service.services.pojo.response.ServiceResponseUpdateBasicInfo;

public class UpdateUserBasicInfoResponse {

    private UserBasicInfo basicInfo;

    public UpdateUserBasicInfoResponse() {
        // mappers
    }

    public UpdateUserBasicInfoResponse(ServiceResponseUpdateBasicInfo response) {
        this.basicInfo = new UserBasicInfo()
                .firstName(response.getUserBasicInfo().getFirstName())
                .middleName(response.getUserBasicInfo().getMiddleName())
                .lastName(response.getUserBasicInfo().getLastName());
    }

    public UserBasicInfo getBasicInfo() {
        return basicInfo;
    }

    public void setBasicInfo(UserBasicInfo basicInfo) {
        this.basicInfo = basicInfo;
    }

    public class UserBasicInfo {

        private String firstName;

        private String middleName;

        private String lastName;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        // fluent
        public UserBasicInfo firstName(final String firstName) {
            setFirstName(firstName);
            return this;
        }

        public UserBasicInfo middleName(final String middleName) {
            setMiddleName(middleName);
            return this;
        }

        public UserBasicInfo lastName(final String lastName) {
            setLastName(lastName);
            return this;
        }
    }
}
