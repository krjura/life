package biz.db.dev.life.authentication.service.controllers;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class DocumentationController {

    private static final String CONST_DOCS_API = "docs/api/main.html";
    private static final String CONST_DOCS_SWAGGER = "swagger/swagger.yml";
    private static final String CONST_DOCS_SWAGGER_UI_INDEX = "/api/v1/auth/docs/swagger-ui/index.html";

    private static final String CONST_EXTENSTION_MAP = ".map";
    private static final String CONST_EXTENSION_PNG = ".png";
    private static final String CONST_EXTENSION_HTML = ".html";
    private static final String CONST_EXTENSION_CSS = ".css";
    private static final String CONST_EXTENSION_JS = ".js";

    private static final String HTTP_HEADERS_LOCATION = "Location";

    private static final MediaType HTTP_MT_CSS = MediaType.parseMediaType("text/css");
    private static final MediaType HTTP_MT_JS = MediaType.parseMediaType("text/javascript");

    private static final List<String> swaggerFileAllowList = List.of(
            "favicon-16x16.png", "favicon-32x32.png",
            "index.html", "swagger-ui.css", "swagger-ui-bundle.js", "swagger-ui-standalone-preset.js"
    );

    @GetMapping(path = "/api/v1/auth/docs/api")
    public ResponseEntity<Resource> docsIndex() {
        Path path = Paths.get("docs/api/main.html");

        if(!Files.exists(path)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new FileSystemResource(CONST_DOCS_API));
    }

    @GetMapping(path = "/api/v1/auth/docs/swagger")
    public ResponseEntity<Resource> swagger() {
        return ResponseEntity
                .ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body(new ClassPathResource(CONST_DOCS_SWAGGER));
    }

    @GetMapping(path = "/api/v1/auth/docs/swagger-ui")
    public ResponseEntity<Void> swaggerUiRoot() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HTTP_HEADERS_LOCATION, CONST_DOCS_SWAGGER_UI_INDEX);

        return new ResponseEntity<>(headers , HttpStatus.FOUND);
    }

    @GetMapping(path = "/api/v1/auth/docs/swagger-ui/{file}")
    public ResponseEntity<Resource> swaggerUiRoot(@PathVariable("file") String file) {

        if(!swaggerFileAllowList.contains(file)) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity
                .ok()
                .contentType(resolveMediaType(file))
                .body(new ClassPathResource("swagger-ui/" + file));
    }

    private MediaType resolveMediaType(String file) {

        if(file.endsWith(CONST_EXTENSION_PNG)) {
            return MediaType.IMAGE_PNG;
        } else if(file.endsWith(CONST_EXTENSION_HTML)) {
            return MediaType.TEXT_HTML;
        } else if(file.endsWith(CONST_EXTENSION_CSS)) {
            return HTTP_MT_CSS;
        } else if(file.endsWith(CONST_EXTENSION_JS)) {
            return HTTP_MT_JS;
        } else {
            return MediaType.TEXT_PLAIN;
        }
    }
}
