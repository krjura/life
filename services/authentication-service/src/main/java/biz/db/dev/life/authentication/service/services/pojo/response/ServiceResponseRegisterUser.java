package biz.db.dev.life.authentication.service.services.pojo.response;

import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserBasicInfo;
import biz.db.dev.life.authentication.service.model.UserRole;

public class ServiceResponseRegisterUser {

    private UserRole userRole;

    private TenantRole tenantRole;

    private UserBasicInfo userBasicInfo;

    private User user;

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public TenantRole getTenantRole() {
        return tenantRole;
    }

    public void setTenantRole(TenantRole tenantRole) {
        this.tenantRole = tenantRole;
    }

    public UserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    // fluid

    public ServiceResponseRegisterUser userRole(final UserRole userRole) {
        setUserRole(userRole);
        return this;
    }

    public ServiceResponseRegisterUser tenantRole(final TenantRole tenantRole) {
        setTenantRole(tenantRole);
        return this;
    }

    public ServiceResponseRegisterUser userBasicInfo(final UserBasicInfo userBasicInfo) {
        setUserBasicInfo(userBasicInfo);
        return this;
    }

    public ServiceResponseRegisterUser user(final User user) {
        setUser(user);
        return this;
    }


}
