package biz.db.dev.life.authentication.service.controllers.pojo.responses;

import biz.db.dev.life.authentication.service.enums.AuthenticationProviderType;

import java.util.List;

public class AuthenticationResponse {

    private boolean authorized;

    private AuthenticationProviderType providerType;

    private String tenantId;

    private List<String> grants;

    private long validTo;

    public AuthenticationResponse() {
        this.authorized = false;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public AuthenticationProviderType getProviderType() {
        return providerType;
    }

    public void setProviderType(AuthenticationProviderType providerType) {
        this.providerType = providerType;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public List<String> getGrants() {
        return grants;
    }

    public void setGrants(List<String> grants) {
        this.grants = grants;
    }

    public long getValidTo() {
        return validTo;
    }

    public void setValidTo(long validTo) {
        this.validTo = validTo;
    }

    // fluent

    public AuthenticationResponse authorized(final boolean authorized) {
        setAuthorized(authorized);
        return this;
    }

    public AuthenticationResponse providerType(final AuthenticationProviderType providerType) {
        setProviderType(providerType);
        return this;
    }

    public AuthenticationResponse tenantId(final String tenantId) {
        setTenantId(tenantId);
        return this;
    }

    public AuthenticationResponse grants(final List<String> grants) {
        setGrants(grants);
        return this;
    }

    public AuthenticationResponse validTo(final long validTo) {
        setValidTo(validTo);
        return this;
    }
}
