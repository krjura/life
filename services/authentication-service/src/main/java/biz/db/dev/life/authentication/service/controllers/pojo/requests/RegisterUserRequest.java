package biz.db.dev.life.authentication.service.controllers.pojo.requests;

import biz.db.dev.life.authentication.service.controllers.pojo.nested.UserBasicInfoData;
import org.krjura.life.validations.PasswordConstraint;
import org.krjura.life.validations.UUIDConstraint;
import org.krjura.life.validations.UsernameConstraint;

import javax.validation.Valid;

public class RegisterUserRequest {

    @UsernameConstraint
    private String username;

    @PasswordConstraint
    private String password;

    @UUIDConstraint
    private String tenantId;

    @Valid
    private UserBasicInfoData userBasicInfo;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public UserBasicInfoData getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfoData userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    // fluent

    public RegisterUserRequest username(final String username) {
        setUsername(username);
        return this;
    }

    public RegisterUserRequest password(final String password) {
        setPassword(password);
        return this;
    }

    public RegisterUserRequest tenantId(final String tenantId) {
        setTenantId(tenantId);
        return this;
    }

    public RegisterUserRequest userBasicInfo(final UserBasicInfoData userBasicInfo) {
        setUserBasicInfo(userBasicInfo);
        return this;
    }
}
