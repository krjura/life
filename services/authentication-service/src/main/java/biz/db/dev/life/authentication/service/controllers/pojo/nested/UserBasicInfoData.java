package biz.db.dev.life.authentication.service.controllers.pojo.nested;

import biz.db.dev.life.authentication.service.validations.UserBasicInfoNameConstraint;
import biz.db.dev.life.authentication.service.validations.UserBasicInfoNameOptionalConstraint;

public class UserBasicInfoData {

    @UserBasicInfoNameConstraint
    private String firstName;

    @UserBasicInfoNameOptionalConstraint
    private String middleName;

    @UserBasicInfoNameConstraint
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // fluent

    public UserBasicInfoData firstName(final String firstName) {
        setFirstName(firstName);
        return this;
    }

    public UserBasicInfoData middleName(final String middleName) {
        setMiddleName(middleName);
        return this;
    }

    public UserBasicInfoData lastName(final String lastName) {
        setLastName(lastName);
        return this;
    }
}
