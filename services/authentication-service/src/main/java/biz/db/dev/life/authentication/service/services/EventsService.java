package biz.db.dev.life.authentication.service.services;

import biz.db.dev.life.jwt.core.JwtDetails;

import java.time.ZonedDateTime;

public interface EventsService {

    void revokeToken(JwtDetails jwtDetails, ZonedDateTime revokedAt);
}
