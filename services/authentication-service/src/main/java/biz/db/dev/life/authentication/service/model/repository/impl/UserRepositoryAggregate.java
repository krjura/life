package biz.db.dev.life.authentication.service.model.repository.impl;

import biz.db.dev.life.authentication.service.model.repository.BasicUserRepository;
import biz.db.dev.life.authentication.service.model.repository.UserRepository;
import biz.db.dev.life.authentication.service.model.repository.UserRoleRepository;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserRepositoryAggregate {

    private final UserRepository userRepository;

    private final BasicUserRepository basicUserRepository;

    private final UserRoleRepository userRoleRepository;

    public UserRepositoryAggregate(
            UserRepository userRepository,
            BasicUserRepository basicUserRepository,
            UserRoleRepository userRoleRepository) {

        Objects.requireNonNull(userRepository);
        Objects.requireNonNull(basicUserRepository);
        Objects.requireNonNull(userRepository);

        this.userRepository = userRepository;
        this.basicUserRepository = basicUserRepository;
        this.userRoleRepository = userRoleRepository;
    }

    public BasicUserRepository getBasicUserRepository() {
        return basicUserRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public UserRoleRepository getUserRoleRepository() {
        return userRoleRepository;
    }
}
