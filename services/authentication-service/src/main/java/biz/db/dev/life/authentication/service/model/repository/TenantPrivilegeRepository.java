package biz.db.dev.life.authentication.service.model.repository;

import biz.db.dev.life.authentication.service.model.TenantPrivilege;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface TenantPrivilegeRepository extends CrudRepository<TenantPrivilege, Integer> {

    @Query("SELECT tp FROM TenantPrivilege tp")
    @Transactional(readOnly = true)
    List<TenantPrivilege> findAllTenantPrivileges();

    @Query("SELECT tp FROM TenantPrivilege tp WHERE tp.id = :id")
    @Transactional(readOnly = true)
    Optional<TenantPrivilege> findTenantPrivilegeById(@Param("id") Integer id);

    @Query("FROM TenantPrivilege tp WHERE tp.tenantRoleId = :tenantRoleId")
    @Transactional(readOnly = true)
    List<TenantPrivilege> findTenantPrivilegesByTenantRoleId(@Param("tenantRoleId") Integer tenantRoleId);
}
