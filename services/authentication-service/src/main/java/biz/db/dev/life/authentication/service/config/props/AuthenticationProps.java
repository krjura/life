package biz.db.dev.life.authentication.service.config.props;

public interface AuthenticationProps {

    long getDeleteRevokedTokensAfter();

    String getDeleteRevokedTokensCron();

    boolean isCookiesOverHttpsOnly();
}
