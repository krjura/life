package biz.db.dev.life.authentication.service.integration.services;

import biz.db.dev.life.authentication.service.support.TestBase;
import biz.db.dev.life.authentication.service.config.props.AuthenticationProps;
import biz.db.dev.life.authentication.service.model.TokenRevocation;
import biz.db.dev.life.authentication.service.model.repository.TokenRevocationRepository;
import biz.db.dev.life.authentication.service.services.JwtTokenService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class JwtTokenServiceTest extends TestBase {

    @Autowired
    private TokenRevocationRepository tokenRevocationRepository;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private AuthenticationProps props;

    @Test
    public void mustDeleteExpiredRevokedTokensSuccessfully() {

        // given
        ZonedDateTime ts = ZonedDateTime.now().minusMinutes(this.props.getDeleteRevokedTokensAfter()).minusMinutes(1);
        this.tokenRevocationRepository.save(new TokenRevocation().token(UUID.randomUUID()).revokedAt(ts));

        // when
        this.jwtTokenService.deleteRevokedTokens();

        // then
        List<TokenRevocation> tokens = this.tokenRevocationRepository.findAll();

        assertThat(tokens).hasSize(0);
    }

    @Test
    public void mustNotDeleteExpiredRevokedTokens() {

        // given
        ZonedDateTime ts = ZonedDateTime.now().minusMinutes(this.props.getDeleteRevokedTokensAfter()).plusMinutes(2);
        this.tokenRevocationRepository.save(new TokenRevocation().token(UUID.randomUUID()).revokedAt(ts));

        // when
        this.jwtTokenService.deleteRevokedTokens();

        // then
        List<TokenRevocation> tokens = this.tokenRevocationRepository.findAll();

        assertThat(tokens).hasSize(1);
    }
}
