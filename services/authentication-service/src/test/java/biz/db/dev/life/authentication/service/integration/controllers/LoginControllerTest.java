package biz.db.dev.life.authentication.service.integration.controllers;

import biz.db.dev.life.authentication.service.swagger.SwaggerTools;
import biz.db.dev.life.authentication.service.support.TestBase;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.AuthenticationResponse;
import biz.db.dev.life.authentication.service.enums.AuthenticationProviderType;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantPrivilege;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.TokenRevocation;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserRole;
import biz.db.dev.life.authentication.service.model.repository.TokenRevocationRepository;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import biz.db.dev.life.authentication.service.model.repository.impl.UserRepositoryAggregate;
import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.JwtTokenBuilder;
import biz.db.dev.life.jwt.core.config.JwtProps;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import biz.db.dev.life.jwt.core.utils.JwtConstants;
import biz.db.dev.life.jwt.core.utils.JwtHelper;
import org.junit.Test;
import org.krjura.life.events.TokenRevokedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

public class LoginControllerTest extends TestBase {

    @Autowired
    private TenantRepositoryAggregate tenantRepositoryAggregate;

    @Autowired
    private UserRepositoryAggregate userRepositoryAggregate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtProps jwtProps;

    @Autowired
    private TokenRevocationRepository revocationRepository;

    @Test
    public void checkLogin() throws Exception {

        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("d2220c6c-f8bf-46b3-8d8b-88b604f5342f"))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        TenantPrivilege tenantPrivilege = new TenantPrivilege()
                .tenantRoleId(tenantRole.getId())
                .privilegeName("PRIV_READ")
                .privilegeDescription("Read all");

        this.tenantRepositoryAggregate.getTenantPrivilegeRepository().save(tenantPrivilege);

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(tenant.getId())
                .username("admin@example.com")
                .password(this.passwordEncoder.encode("adminadmin"))
                .enabled(true);

        UserRole userRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantRole.getId());

        this.userRepositoryAggregate.getUserRepository().save(user);
        this.userRepositoryAggregate.getUserRoleRepository().save(userRole);

        // when

        String content = contentOf("requests/valid/LoginRequest-valid-1.json");

        MvcResult result = mockMvc.perform(
                post("/api/v1/auth/tenant/d2220c6c-f8bf-46b3-8d8b-88b604f5342f/login")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        AuthenticationResponse response = fromJson(result, AuthenticationResponse.class);
        assertThat(response.isAuthorized(), is(equalTo(true)));
        assertThat(response.getProviderType(), is(equalTo(AuthenticationProviderType.DATABASE)));
        assertThat(response.getTenantId(), is(equalTo(tenant.getId().toString())));
        assertThat(response.getGrants(), hasSize(2));
        assertThat(response.getGrants(), hasItem("ROLE_ADMIN"));
        assertThat(response.getGrants(), hasItem("PRIV_READ"));

        // check jwt token
        Cookie cookie = result.getResponse().getCookie(JwtConstants.COOKIE_PARAM_NAME);
        String token = cookie.getValue();

        String keyId = this.jwtProps.getKeyStore().getDefaultKeyId();
        String publicKey = this.jwtProps.getKeyStore().getKeys().get(keyId).getPublicKey();
        String sanitizedPublicKey = JwtHelper.sanitizePublicKey(publicKey);

        JwtDetails jwtDetails = JwtTokenBuilder.parse(sanitizedPublicKey, token);
        assertThat(jwtDetails.getTokenId(), is(notNullValue()));
        assertThat(jwtDetails.getUserId(), is(equalTo(user.getId())));
        assertThat(jwtDetails.getGrants(), hasSize(2));
        assertThat(jwtDetails.getGrants(), hasItem("ROLE_ADMIN"));
        assertThat(jwtDetails.getGrants(), hasItem("PRIV_READ"));
    }

    @Test
    public void checkLoginUserNotFound() throws Exception {
        // when
        String content = contentOf("requests/valid/LoginRequest-valid-1.json");

        mockMvc.perform(
                post("/api/v1/auth/tenant/d2220c6c-f8bf-46b3-8d8b-88b604f5342f/login")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    public void checkLoginInvalidPassword() throws Exception {

        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("d2220c6c-f8bf-46b3-8d8b-88b604f5342f"))
                .tenantName("Test");

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(tenant.getId())
                .username("admin@example.com")
                .password(this.passwordEncoder.encode("adminadmin"))
                .enabled(true);

        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        String content = contentOf("requests/valid/LoginRequest-valid-2.json");

        MvcResult result = mockMvc.perform(
                post("/api/v1/auth/tenant/d2220c6c-f8bf-46b3-8d8b-88b604f5342f/login")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        AuthenticationResponse response = fromJson(result, AuthenticationResponse.class);
        assertThat(response.isAuthorized(), is(equalTo(false)));
        assertThat(response.getProviderType(), is(nullValue()));
        assertThat(response.getTenantId(), is(nullValue()));
        assertThat(response.getGrants(), is(nullValue()));
    }

    @Test
    public void checkLoginWrongTenant() throws Exception {

        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("0bcd684e-281c-4fa9-ab53-4dae26810eae"))
                .tenantName("Test");

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(tenant.getId())
                .username("admin@example.com")
                .password(this.passwordEncoder.encode("admin"))
                .enabled(true);

        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        String content = contentOf("requests/valid/LoginRequest-valid-2.json");

        mockMvc.perform(
                post("/api/v1/auth/tenant/d2220c6c-f8bf-46b3-8d8b-88b604f5342f/login")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    public void checkLoginInvalidRequest() throws Exception {
        // when
        String content = contentOf("requests/invalid/LoginRequest-invalid-1.json");

        mockMvc.perform(
                post("/api/v1/auth/tenant/d2220c6c-f8bf-46b3-8d8b-88b604f5342f/login")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void renewTokenNotAuthenticated() throws Exception {
        mockMvc.perform(
                get("/api/v1/auth/renew"))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    @WithMockJwt(
            userId = "58a6f86f-4eb1-46fd-bea7-fe2b72b8339f",
            tenantId = "ea832610-9239-4caa-badd-94c52ec42487",
            grants = {"ROLE_ADMIN", "ROLE_USER"}
    )
    public void renewToken() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/api/v1/auth/renew"))
                .andExpect(status().isNoContent())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        // check jwt token
        String keyId = this.jwtProps.getKeyStore().getDefaultKeyId();
        String publicKey = this.jwtProps.getKeyStore().getKeys().get(keyId).getPublicKey();
        String sanitizedPublicKey = JwtHelper.sanitizePublicKey(publicKey);

        Cookie cookie = result.getResponse().getCookie(JwtConstants.COOKIE_PARAM_NAME);
        String token = cookie.getValue();

        JwtDetails jwtDetails = JwtTokenBuilder.parse(sanitizedPublicKey, token);
        assertThat(jwtDetails.getTokenId(), is(notNullValue()));
        assertThat(jwtDetails.getTenantId(), is(equalTo(UUID.fromString("ea832610-9239-4caa-badd-94c52ec42487"))));
        assertThat(jwtDetails.getUserId(), is(equalTo(UUID.fromString("58a6f86f-4eb1-46fd-bea7-fe2b72b8339f"))));
        assertThat(jwtDetails.getGrants(), hasSize(2));
        assertThat(jwtDetails.getGrants(), hasItem("ROLE_ADMIN"));
        assertThat(jwtDetails.getGrants(), hasItem("ROLE_USER"));

        // do it again
        // must wait one second since jwt truncates ms
        Thread.sleep(1000);

        MvcResult resultTwo = mockMvc.perform(
                get("/api/v1/auth/renew"))
                .andExpect(status().isNoContent())
                .andReturn();

        Cookie cookieTwo = resultTwo.getResponse().getCookie(JwtConstants.COOKIE_PARAM_NAME);
        String tokenTwo = cookieTwo.getValue();

        JwtDetails jwtDetailsTwo = JwtTokenBuilder.parse(sanitizedPublicKey, tokenTwo);

        assertThat(jwtDetailsTwo.getIssuedAt(), is(greaterThan(jwtDetails.getIssuedAt())));
        assertThat(jwtDetailsTwo.getExpiresAt(), is(greaterThan(jwtDetails.getExpiresAt())));
    }

    @Test
    @WithMockJwt(
            tokenId = "7315eb96-247f-42e6-b9ab-7c2c8f72ee14",
            userId = "11e298b8-5351-4597-8cc4-45101d5a7244",
            tenantId = "59b8e876-25c2-48b7-bbf0-843f96e20508",
            grants = {"ROLE_ADMIN", "ROLE_USER"}
    )
    public void testLogout() throws Exception {
        mockMvc.perform(delete("/api/v1/auth/logout").with(csrf()))
                .andExpect(status().isNoContent())
                .andExpect(SwaggerTools.instance().getMatcher());

        Optional<TokenRevocation> tokenRevocationOptional = revocationRepository
                .findByToken(UUID.fromString("7315eb96-247f-42e6-b9ab-7c2c8f72ee14"));

        assertThat(tokenRevocationOptional.isPresent(), is(equalTo(true)));
        assertThat(tokenRevocationOptional.get().getToken(), is(equalTo(UUID.fromString("7315eb96-247f-42e6-b9ab-7c2c8f72ee14"))));

        TokenRevokedEvent event = waitUntilThereIsAMessageInLoginUpdatesQueue(TokenRevokedEvent.class);

        assertThat(event.getExpiresAt(), is(notNullValue()));
        assertThat(event.getRevokedAt(), is(notNullValue()));
        assertThat(event.getTokenId(), is(equalTo("7315eb96-247f-42e6-b9ab-7c2c8f72ee14")));
        assertThat(event.getMetadata().getCorrelationId(), is(notNullValue()));
        assertThat(event.getMetadata().getMessageId(), is(notNullValue()));
        assertThat(event.getMetadata().getTimestamp(), is(notNullValue()));
        assertThat(event.getMetadata().getVersion(), is(notNullValue()));
    }
}