package biz.db.dev.life.authentication.service.integration.model.reository.impl;

import biz.db.dev.life.authentication.service.support.TestBase;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserRole;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import biz.db.dev.life.authentication.service.model.repository.impl.UserRepositoryAggregate;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

public class UserRepositoryImplTest extends TestBase {

    @Autowired
    private TenantRepositoryAggregate tenantRepositoryAggregate;

    @Autowired
    private UserRepositoryAggregate userRepositoryAggregate;

    @Test
    public void addUser() {
        // given
        Tenant tenant = createDefaultTenant();
        tenantRepositoryAggregate.getTenantRepository().save(tenant);

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(tenant.getId())
                .username("admin")
                .password("admin")
                .enabled(true);

        // when
        userRepositoryAggregate.getUserRepository().save(user);

        // then
        verifyAllUsers(tenant, user);
        verifyUsersById(tenant, user);
        verifyUsersByTenant(tenant, user);
    }

    private void verifyUsersByTenant(Tenant tenant, User user) {
        List<User> users = userRepositoryAggregate.getUserRepository().findUsersByTenantId(tenant.getId());

        assertThat(users, hasSize(1));

        User dbUser = users.get(0);
        assertThat(dbUser.getId(), is(equalTo(user.getId())));
        assertThat(dbUser.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(dbUser.getUsername(), is(equalTo(user.getUsername())));
        assertThat(dbUser.getPassword(), is(equalTo(user.getPassword())));
        assertThat(dbUser.isEnabled(), is(equalTo(user.isEnabled())));
    }

    private void verifyUsersById(Tenant tenant, User user) {
        Optional<User> userOptional = userRepositoryAggregate.getUserRepository().findById(user.getId());

        assertThat(userOptional.isPresent(), is(equalTo(true)));
        assertThat(userOptional.get().getId(), is(equalTo(user.getId())));
        assertThat(userOptional.get().getTenantId(), is(equalTo(tenant.getId())));
        assertThat(userOptional.get().getUsername(), is(equalTo(user.getUsername())));
        assertThat(userOptional.get().getPassword(), is(equalTo(user.getPassword())));
        assertThat(userOptional.get().isEnabled(), is(equalTo(user.isEnabled())));
    }

    private void verifyAllUsers(Tenant tenant, User user) {
        List<User> users = userRepositoryAggregate.getUserRepository().findAllUsers();
        assertThat(users, hasSize(1));

        User dbUser = users.get(0);
        assertThat(dbUser.getId(), is(equalTo(user.getId())));
        assertThat(dbUser.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(dbUser.getUsername(), is(equalTo(user.getUsername())));
        assertThat(dbUser.getPassword(), is(equalTo(user.getPassword())));
        assertThat(dbUser.isEnabled(), is(equalTo(user.isEnabled())));
    }

    @Test
    public void addUserRoles() {
        // given
        Tenant tenant = createDefaultTenant();
        tenantRepositoryAggregate.getTenantRepository().save(tenant);

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator");
        tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(tenant.getId())
                .username("admin")
                .password("admin")
                .enabled(true);

        UserRole firstRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantRole.getId());

        UserRole secondRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantRole.getId());
        // when
        userRepositoryAggregate.getUserRepository().save(user);
        userRepositoryAggregate.getUserRoleRepository().save(firstRole);
        userRepositoryAggregate.getUserRoleRepository().save(secondRole);

        // then
        verifyAllUserRoles(tenantRole, user, firstRole, secondRole);
        verifyUserRolesByUserId(tenantRole, user, firstRole, secondRole);
    }

    private void verifyUserRolesByUserId(TenantRole tenantRole, User user, UserRole firstRole, UserRole secondRole) {
        List<UserRole> userRoles = userRepositoryAggregate.getUserRoleRepository().findUserRoleByUserId(user.getId());
        assertThat(userRoles, hasSize(2));

        UserRole dbFirstRole = userRoles.get(0);
        assertThat(dbFirstRole.getId(), is(equalTo(firstRole.getId())));
        assertThat(dbFirstRole.getUserId(), is(equalTo(user.getId())));
        assertThat(dbFirstRole.getTenantRoleId(), is(equalTo(tenantRole.getId())));

        UserRole dbSecondRole = userRoles.get(1);
        assertThat(dbSecondRole.getId(), is(equalTo(secondRole.getId())));
        assertThat(dbSecondRole.getUserId(), is(equalTo(user.getId())));
        assertThat(dbSecondRole.getTenantRoleId(), is(equalTo(tenantRole.getId())));
    }

    private void verifyAllUserRoles(TenantRole tenantRole, User user, UserRole firstRole, UserRole secondRole) {
        List<UserRole> userRoles = userRepositoryAggregate.getUserRoleRepository().findAllUserRoles();
        assertThat(userRoles, hasSize(2));

        UserRole dbFirstRole = userRoles.get(0);
        assertThat(dbFirstRole.getId(), is(equalTo(firstRole.getId())));
        assertThat(dbFirstRole.getUserId(), is(equalTo(user.getId())));
        assertThat(dbFirstRole.getTenantRoleId(), is(equalTo(tenantRole.getId())));

        UserRole dbSecondRole = userRoles.get(1);
        assertThat(dbSecondRole.getId(), is(equalTo(secondRole.getId())));
        assertThat(dbSecondRole.getUserId(), is(equalTo(user.getId())));
        assertThat(dbSecondRole.getTenantRoleId(), is(equalTo(tenantRole.getId())));
    }

    private Tenant createDefaultTenant() {
        return new Tenant()
                .id(UUID.randomUUID())
                .tenantName("Test");
    }
}
