package biz.db.dev.life.authentication.service.support;

import biz.db.dev.life.authentication.service.enums.QueueNames;
import biz.db.dev.life.authentication.service.integration.services.DatabaseCleanerServiceImpl;
import biz.db.dev.life.exceptions.WaitingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(SpringJUnit4ClassRunner.class)
@ProjectTest
public abstract class TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestBase.class);

    @Autowired
    private DatabaseCleanerServiceImpl databaseCleanerService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    protected WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    protected MockMvc mockMvc;

    @Before
    public void beforeTestBase() {
        this.databaseCleanerService.deleteAll();

        flushQueue(QueueNames.LOGIN_UPDATES_QUEUE);

        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    public void flushQueue(String queueName) {

        Message message;

        while( ( message = this.rabbitTemplate.receive(queueName) ) != null) {
            logger.info("read message during flush queue {}", message);
        }
    }

    protected String contentOf(String fileName) throws URISyntaxException, IOException {
        return new String(Files.readAllBytes(Paths.get(getClass().getClassLoader()
                .getResource(fileName)
                .toURI())));
    }

    protected <T> T fromJson(MvcResult result, Class<T> clazz) throws IOException {
        return fromJson(result.getResponse().getContentAsString(), clazz);
    }

    protected <T> T fromJson(String content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz);
    }

    protected <T> T fromJson(byte[] content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz);
    }

    protected  <T> T waitUntilThereIsAMessageInLoginUpdatesQueue(Class<T> clazz) throws IOException {
        Message message = this.rabbitTemplate.receive(QueueNames.LOGIN_UPDATES_QUEUE, 30000);

        if(message == null) {
            throw new WaitingException();
        }

        return fromJson(message.getBody(), clazz);
    }
}
