package biz.db.dev.life.authentication.service.documentation.api;

import biz.db.dev.life.authentication.service.integration.services.DatabaseCleanerServiceImpl;
import biz.db.dev.life.authentication.service.support.ProjectTest;
import biz.db.dev.life.authentication.service.support.ex.TestException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ProjectTest
public abstract class DocumentationMvcTestBase {

    @Autowired
    private DatabaseCleanerServiceImpl databaseCleanerService;

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void beforeTestBase() {
        this.databaseCleanerService.deleteAll();

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    public MockMvc getMockMvc() {
        return mockMvc;
    }

    String toJson(Object object) {
        try {
            return this.objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new TestException("cannot converto to json", e);
        }
    }
}
