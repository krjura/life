package biz.db.dev.life.authentication.service.documentation.api;

import biz.db.dev.life.authentication.service.controllers.pojo.requests.LoginRequest;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantPrivilege;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserRole;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import biz.db.dev.life.authentication.service.model.repository.impl.UserRepositoryAggregate;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.headers.HeaderDocumentation;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.RequestDocumentation;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LoginControllerDocumentation extends DocumentationMvcTestBase {

    private static final String CONST_TENANT_ID = "cb9b0a9b-f540-400a-87d7-3f21fa302f45";
    private static final String CONST_USERNAME = "admin@example.com";
    private static final String CONST_PASSWORD = "adminadmin";

    @Autowired
    private TenantRepositoryAggregate tenantRepositoryAggregate;

    @Autowired
    private UserRepositoryAggregate userRepositoryAggregate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void loginSuccessfully() throws Exception {
        supposeUserExists();

        LoginRequest loginRequest = new LoginRequest()
                .username(CONST_USERNAME)
                .password(CONST_PASSWORD);

        String content = toJson(loginRequest);

        getMockMvc()
                .perform(
                        post("/api/v1/auth/tenant/{tenantId}/login", CONST_TENANT_ID)
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andDo(document(
                        "successful_login",
                        RequestDocumentation.pathParameters(
                                RequestDocumentation
                                        .parameterWithName("tenantId")
                                        .description("Id of the tenant user belongs to")
                        ),
                        PayloadDocumentation.requestFields(
                                PayloadDocumentation
                                        .fieldWithPath("username")
                                        .type(JsonFieldType.STRING)
                                        .description("Username used to login to the system"),
                                PayloadDocumentation
                                        .fieldWithPath("password")
                                        .type(JsonFieldType.STRING)
                                        .description("Password used to login to the system")
                        ),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("authorized")
                                        .type(JsonFieldType.BOOLEAN)
                                        .description("Was user successfully authorized"),
                                PayloadDocumentation
                                        .fieldWithPath("providerType")
                                        .type(JsonFieldType.STRING)
                                        .description("How was the user authenticated. Possible options are: DATABASE"),
                                PayloadDocumentation
                                        .fieldWithPath("tenantId")
                                        .type(JsonFieldType.STRING)
                                        .attributes()
                                        .description("Tenant ID to whom the user was autentication to"),
                                PayloadDocumentation
                                        .fieldWithPath("grants")
                                        .type(JsonFieldType.ARRAY)
                                        .description("Grants assigned to this user. Grant allow access to specific parts of the system"),
                                PayloadDocumentation
                                        .fieldWithPath("validTo")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Time when the assigned token expires")
                        ),
                        HeaderDocumentation.responseHeaders(
                                HeaderDocumentation.headerWithName("Set-Cookie").description("Used to set cookie with jwt token")
                        )
                        ));
    }

    @Test
    public void checkLoginInvalidPassword() throws Exception {
        supposeUserExists();

        LoginRequest loginRequest = new LoginRequest()
                .username(CONST_USERNAME)
                .password("not_a_real_password");

        String content = toJson(loginRequest);

        getMockMvc()
                .perform(
                        post("/api/v1/auth/tenant/{tenantId}/login", CONST_TENANT_ID)
                                .content(content)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .with(csrf())
                )
                .andExpect(status().isOk())
                .andDo(document(
                        "unsuccessful_login",
                        RequestDocumentation.pathParameters(
                                RequestDocumentation
                                        .parameterWithName("tenantId")
                                        .description("Id of the tenant user belongs to")
                        ),
                        PayloadDocumentation.requestFields(
                                PayloadDocumentation
                                        .fieldWithPath("username")
                                        .type(JsonFieldType.STRING)
                                        .description("Username used to login to the system"),
                                PayloadDocumentation
                                        .fieldWithPath("password")
                                        .type(JsonFieldType.STRING)
                                        .description("Password used to login to the system")
                        ),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("authorized")
                                        .type(JsonFieldType.BOOLEAN)
                                        .description("Will be false for unsuccessful login"),
                                PayloadDocumentation
                                        .fieldWithPath("providerType")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("How was the user authenticated. Will be null for unsuccessful login"),
                                PayloadDocumentation
                                        .fieldWithPath("tenantId")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Tenant ID to whom the user was autentication to. Will be null for unsuccessful login"),
                                PayloadDocumentation
                                        .fieldWithPath("grants")
                                        .type(JsonFieldType.ARRAY)
                                        .optional()
                                        .description("Grants assigned to this user. Will be null for unsuccessful login"),
                                PayloadDocumentation
                                        .fieldWithPath("validTo")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Time when the assigned token expires")
                        )
                ));
    }

    @Test
    @WithMockJwt(
            userId = "58a6f86f-4eb1-46fd-bea7-fe2b72b8339f",
            tenantId = "ea832610-9239-4caa-badd-94c52ec42487",
            grants = {"ROLE_ADMIN", "ROLE_USER"}
    )
    public void logoutSuccessfully() throws Exception {
        getMockMvc()
                .perform(delete("/api/v1/auth/logout", CONST_TENANT_ID))
                .andDo(document(
                        "successful_logout"));
    }

    private void supposeUserExists() {
        Tenant tenant = new Tenant()
                .id(UUID.fromString(CONST_TENANT_ID))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        TenantPrivilege tenantPrivilege = new TenantPrivilege()
                .tenantRoleId(tenantRole.getId())
                .privilegeName("PRIV_READ")
                .privilegeDescription("Read all");

        this.tenantRepositoryAggregate.getTenantPrivilegeRepository().save(tenantPrivilege);

        User user = new User()
                .id(UUID.randomUUID())
                .tenantId(tenant.getId())
                .username(CONST_USERNAME)
                .password(this.passwordEncoder.encode(CONST_PASSWORD))
                .enabled(true);

        UserRole userRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantRole.getId());

        this.userRepositoryAggregate.getUserRepository().save(user);
        this.userRepositoryAggregate.getUserRoleRepository().save(userRole);
    }
}
