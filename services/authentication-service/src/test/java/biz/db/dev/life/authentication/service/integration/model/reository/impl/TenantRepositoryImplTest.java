package biz.db.dev.life.authentication.service.integration.model.reository.impl;

import biz.db.dev.life.authentication.service.support.TestBase;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantPrivilege;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

public class TenantRepositoryImplTest extends TestBase {

    @Autowired
    private TenantRepositoryAggregate tenantRepositoryAggregate;

    @Test
    public void addNewTenant() {
        // given
        Tenant tenant = createDefaultTenant();

        // when
        tenantRepositoryAggregate.getTenantRepository().save(tenant);

        // then

        List<Tenant> tenants = tenantRepositoryAggregate.getTenantRepository().findAllTenants();
        assertThat(tenants, hasSize(1));

        Tenant dbTenant = tenants.get(0);
        assertThat(dbTenant.getId(), is(equalTo(tenant.getId())));
        assertThat(dbTenant.getTenantName(), is(equalTo(tenant.getTenantName())));

        Optional<Tenant> tenantOptional = tenantRepositoryAggregate.getTenantRepository().findTenantById(tenant.getId());

        assertThat(tenantOptional.isPresent(), is(equalTo(true)));
        assertThat(tenantOptional.get().getId(), is(equalTo(tenant.getId())));
        assertThat(tenantOptional.get().getTenantName(), is(equalTo(tenant.getTenantName())));
    }

    @Test
    public void addTenantRoles() {

        // given
        Tenant tenant = createDefaultTenant();

        TenantRole firstRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator");

        TenantRole secondRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_USER")
                .roleDescription("User");

        // when
        tenant = tenantRepositoryAggregate.getTenantRepository().save(tenant);
        firstRole = tenantRepositoryAggregate.getTenantRoleRepository().save(firstRole);
        secondRole = tenantRepositoryAggregate.getTenantRoleRepository().save(secondRole);

        // then

        // verify find all
        verifyAllTenantRoles(tenant, firstRole, secondRole);
        // verify by tenant id
        verifyTenantRolesByTenant(tenant, firstRole, secondRole);
        // verify by id
        verifyTenantRolesById(tenant, firstRole, secondRole);
    }

    private void verifyTenantRolesById(Tenant tenant, TenantRole firstRole, TenantRole secondRole) {
        Optional<TenantRole> firstTenantRoleOptional = tenantRepositoryAggregate
                .getTenantRoleRepository().findTenantRoleById(firstRole.getId());

        assertThat(firstTenantRoleOptional.isPresent(), is(equalTo(true)));
        assertThat(firstTenantRoleOptional.get().getTenantId(), is(equalTo(tenant.getId())));
        assertThat(firstTenantRoleOptional.get().getRoleName(), is(equalTo(firstRole.getRoleName())));
        assertThat(firstTenantRoleOptional.get().getRoleDescription(), is(equalTo(firstRole.getRoleDescription())));

        Optional<TenantRole> secondTenantRoleOptional = tenantRepositoryAggregate
                .getTenantRoleRepository().findTenantRoleById(secondRole.getId());

        assertThat(secondTenantRoleOptional.isPresent(), is(equalTo(true)));
        assertThat(secondTenantRoleOptional.get().getTenantId(), is(equalTo(tenant.getId())));
        assertThat(secondTenantRoleOptional.get().getRoleName(), is(equalTo(secondRole.getRoleName())));
        assertThat(secondTenantRoleOptional.get().getRoleDescription(), is(equalTo(secondRole.getRoleDescription())));
    }

    private void verifyTenantRolesByTenant(Tenant tenant, TenantRole firstRole, TenantRole secondRole) {
        List<TenantRole> tenantRoles = tenantRepositoryAggregate
                .getTenantRoleRepository().findTenantRolesByTenantId(tenant.getId());
        assertThat(tenantRoles, hasSize(2));

        TenantRole dbFirstRole = tenantRoles.get(0);
        assertThat(dbFirstRole.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(dbFirstRole.getRoleName(), is(equalTo(firstRole.getRoleName())));
        assertThat(dbFirstRole.getRoleDescription(), is(equalTo(firstRole.getRoleDescription())));

        TenantRole dbSecondRole = tenantRoles.get(1);
        assertThat(dbSecondRole.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(dbSecondRole.getRoleName(), is(equalTo(secondRole.getRoleName())));
        assertThat(dbSecondRole.getRoleDescription(), is(equalTo(secondRole.getRoleDescription())));
    }

    private void verifyAllTenantRoles(Tenant tenant, TenantRole firstRole, TenantRole secondRole) {
        List<TenantRole> tenantRoles = tenantRepositoryAggregate.getTenantRoleRepository().findAllTenantRoles();
        assertThat(tenantRoles, hasSize(2));

        TenantRole dbFirstRole = tenantRoles.get(0);
        assertThat(dbFirstRole.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(dbFirstRole.getRoleName(), is(equalTo(firstRole.getRoleName())));
        assertThat(dbFirstRole.getRoleDescription(), is(equalTo(firstRole.getRoleDescription())));

        TenantRole dbSecondRole = tenantRoles.get(1);
        assertThat(dbSecondRole.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(dbSecondRole.getRoleName(), is(equalTo(secondRole.getRoleName())));
        assertThat(dbSecondRole.getRoleDescription(), is(equalTo(secondRole.getRoleDescription())));
    }

    @Test
    public void addTenantPrivileges() {
        // given
        Tenant tenant = createDefaultTenant();

        TenantRole role = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator");

        // must be created so id is assigned
        tenantRepositoryAggregate.getTenantRepository().save(tenant);
        tenantRepositoryAggregate.getTenantRoleRepository().save(role);

        TenantPrivilege firstPrivilege = new TenantPrivilege()
                .tenantRoleId(role.getId())
                .privilegeName("PRIV_WO")
                .privilegeDescription("Write Own");

        TenantPrivilege secondPrivilege = new TenantPrivilege()
                .tenantRoleId(role.getId())
                .privilegeName("PRIV_RO")
                .privilegeDescription("Read Own");

        // when
        tenantRepositoryAggregate.getTenantPrivilegeRepository().save(firstPrivilege);
        tenantRepositoryAggregate.getTenantPrivilegeRepository().save(secondPrivilege);

        // given

        // verify all
        verifyAllTenantPrivileges(role, firstPrivilege, secondPrivilege);
        // verify by role id
        verifyTenantPrivilegesByRoleId(role, firstPrivilege, secondPrivilege);
        // verify by id
        verifyTenantPrivilegesById(role, firstPrivilege, secondPrivilege);
    }

    private void verifyTenantPrivilegesById(
            TenantRole role, TenantPrivilege firstPrivilege, TenantPrivilege secondPrivilege) {

        Optional<TenantPrivilege> firstPrivilegeOptional = tenantRepositoryAggregate
                .getTenantPrivilegeRepository().findTenantPrivilegeById(firstPrivilege.getId());

        assertThat(firstPrivilegeOptional.isPresent(), is(equalTo(true)));
        assertThat(firstPrivilegeOptional.get().getTenantRoleId(), is(equalTo(role.getId())));
        assertThat(firstPrivilegeOptional.get().getPrivilegeName(), is(equalTo(firstPrivilege.getPrivilegeName())));
        assertThat(firstPrivilegeOptional.get().getPrivilegeDescription(), is(equalTo(firstPrivilege.getPrivilegeDescription())));

        Optional<TenantPrivilege> secondPrivilegeOptional = tenantRepositoryAggregate
                .getTenantPrivilegeRepository().findTenantPrivilegeById(secondPrivilege.getId());
        assertThat(secondPrivilegeOptional.get().getTenantRoleId(), is(equalTo(role.getId())));
        assertThat(secondPrivilegeOptional.get().getPrivilegeName(), is(equalTo(secondPrivilege.getPrivilegeName())));
        assertThat(secondPrivilegeOptional.get().getPrivilegeDescription(), is(equalTo(secondPrivilege.getPrivilegeDescription())));
    }

    private void verifyTenantPrivilegesByRoleId(
            TenantRole role, TenantPrivilege firstPrivilege, TenantPrivilege secondPrivilege) {

        List<TenantPrivilege> privileges = tenantRepositoryAggregate
                .getTenantPrivilegeRepository().findTenantPrivilegesByTenantRoleId(role.getId());

        assertThat(privileges, hasSize(2));

        TenantPrivilege dbFirstPrivilege = privileges.get(0);
        assertThat(dbFirstPrivilege.getTenantRoleId(), is(equalTo(role.getId())));
        assertThat(dbFirstPrivilege.getPrivilegeName(), is(equalTo(firstPrivilege.getPrivilegeName())));
        assertThat(dbFirstPrivilege.getPrivilegeDescription(), is(equalTo(firstPrivilege.getPrivilegeDescription())));

        TenantPrivilege dbSecondPrivilege = privileges.get(1);
        assertThat(dbSecondPrivilege.getTenantRoleId(), is(equalTo(role.getId())));
        assertThat(dbSecondPrivilege.getPrivilegeName(), is(equalTo(secondPrivilege.getPrivilegeName())));
        assertThat(dbSecondPrivilege.getPrivilegeDescription(), is(equalTo(secondPrivilege.getPrivilegeDescription())));
    }

    private void verifyAllTenantPrivileges(TenantRole role, TenantPrivilege firstPrivilege, TenantPrivilege secondPrivilege) {
        List<TenantPrivilege> privileges = tenantRepositoryAggregate
                .getTenantPrivilegeRepository().findAllTenantPrivileges();

        assertThat(privileges, hasSize(2));

        TenantPrivilege dbFirstPrivilege = privileges.get(0);
        assertThat(dbFirstPrivilege.getTenantRoleId(), is(equalTo(role.getId())));
        assertThat(dbFirstPrivilege.getPrivilegeName(), is(equalTo(firstPrivilege.getPrivilegeName())));
        assertThat(dbFirstPrivilege.getPrivilegeDescription(), is(equalTo(firstPrivilege.getPrivilegeDescription())));

        TenantPrivilege dbSecondPrivilege = privileges.get(1);
        assertThat(dbSecondPrivilege.getTenantRoleId(), is(equalTo(role.getId())));
        assertThat(dbSecondPrivilege.getPrivilegeName(), is(equalTo(secondPrivilege.getPrivilegeName())));
        assertThat(dbSecondPrivilege.getPrivilegeDescription(), is(equalTo(secondPrivilege.getPrivilegeDescription())));
    }

    private Tenant createDefaultTenant() {
        return new Tenant()
                .id(UUID.randomUUID())
                .tenantName("Test");
    }
}
