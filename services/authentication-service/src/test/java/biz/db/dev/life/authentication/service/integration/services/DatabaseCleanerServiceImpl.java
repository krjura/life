package biz.db.dev.life.authentication.service.integration.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
public class DatabaseCleanerServiceImpl {

    @Autowired
    private EntityManager em;

    @Transactional
    public void deleteAll() {
        em.createNativeQuery("TRUNCATE tenant CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE tenant_privilege CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE tenant_role CASCADE").executeUpdate();

        em.createNativeQuery("TRUNCATE application_user CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE application_user_role CASCADE").executeUpdate();
        em.createNativeQuery("TRUNCATE application_user_basic_info CASCADE").executeUpdate();

        em.createNativeQuery("TRUNCATE token_revocation CASCADE").executeUpdate();
    }
}