package biz.db.dev.life.authentication.service.support.config;

import biz.db.dev.life.authentication.service.enums.QueueNames;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmqpTestConfig {

    @Bean
    @Qualifier(QueueNames.LOGIN_UPDATES_EXCHANGE)
    public Queue loginUpdatesQueue() {
        return QueueBuilder
                .durable(QueueNames.LOGIN_UPDATES_QUEUE)
                .build();
    }

    @Bean
    public Binding loginUpdatesBinding(
            @Qualifier(QueueNames.LOGIN_UPDATES_EXCHANGE) Exchange exchange) {

        return BindingBuilder
                .bind(loginUpdatesQueue())
                .to(exchange)
                .with(QueueNames.LOGIN_UPDATES_RK)
                .noargs();
    }
}
