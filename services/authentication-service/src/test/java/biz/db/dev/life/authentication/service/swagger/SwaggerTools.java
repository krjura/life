package biz.db.dev.life.authentication.service.swagger;

import org.springframework.test.web.servlet.ResultMatcher;

public class SwaggerTools {

    private ResultMatcher matcher;

    private static SwaggerTools instance;

    private SwaggerTools() {
        this.matcher = new OpenApiMatchers().isValid("swagger/swagger.yml");
    }

    public static SwaggerTools instance() {
        if(instance == null) {
            instance = new SwaggerTools();
        }

        return instance;
    }

    public ResultMatcher getMatcher() {
        return matcher;
    }
}
