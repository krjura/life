package biz.db.dev.life.authentication.service.documentation.api;

import biz.db.dev.life.authentication.service.controllers.pojo.nested.UserBasicInfoData;
import biz.db.dev.life.authentication.service.controllers.pojo.requests.RegisterUserRequest;
import biz.db.dev.life.authentication.service.controllers.pojo.requests.UserPasswordChangeRequest;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserBasicInfo;
import biz.db.dev.life.authentication.service.model.UserRole;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import biz.db.dev.life.authentication.service.model.repository.impl.UserRepositoryAggregate;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.RequestDocumentation;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerDocumentation extends DocumentationMvcTestBase {

    private static final String CONST_TENANT_ID = "a847a209-b472-4342-9a3f-a819c09216ef";
    private static final String CONST_USER_ID = "daad598d-6bc5-4862-8ea7-b17c17074dbe";
    private static final String CONST_PASSWORD = "password123";
    private static final String CONST_USERNAME = "demo@example.com";

    @Autowired
    private TenantRepositoryAggregate tenantRepositoryAggregate;

    @Autowired
    private UserRepositoryAggregate userRepositoryAggregate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private TestContext tc;

    @Before
    public void beforeTest() {
        this.tc = new TestContext();
    }

    @Test
    public void registerNewUser() throws Exception {
        setupTenant();

        RegisterUserRequest request = new RegisterUserRequest()
                .username("admin@example.com")
                .password("adminadmin")
                .tenantId(CONST_TENANT_ID)
                .userBasicInfo(new UserBasicInfoData().firstName("John").lastName("Doe"));

        String content = toJson(request);

        getMockMvc().perform(
                post("/api/v1/auth/tenant/{tenantId}/users/register", CONST_TENANT_ID)
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andDo(document(
                        "successful_register_user",
                        RequestDocumentation.pathParameters(
                                RequestDocumentation
                                        .parameterWithName("tenantId")
                                        .description("ID of the tenant user should belong to")),
                        PayloadDocumentation.requestFields(
                                PayloadDocumentation
                                        .fieldWithPath("username")
                                        .type(JsonFieldType.STRING)
                                        .description("Expected username of the user"),
                                PayloadDocumentation
                                        .fieldWithPath("password")
                                        .type(JsonFieldType.STRING)
                                        .description("Password user should use to login to the system"),
                                PayloadDocumentation
                                        .fieldWithPath("tenantId")
                                        .type(JsonFieldType.STRING)
                                        .description("ID of the tenant user should belong to"),
                                PayloadDocumentation
                                        .fieldWithPath("userBasicInfo.firstName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("First name of the user. Field is optional."),
                                PayloadDocumentation
                                        .fieldWithPath("userBasicInfo.middleName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Middle name of the user. Field is optional."),
                                PayloadDocumentation
                                        .fieldWithPath("userBasicInfo.lastName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Last name of the user. Field is optional.")
                        ),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("user.id")
                                        .type(JsonFieldType.STRING)
                                        .description("Internal ID of the user"),
                                PayloadDocumentation
                                        .fieldWithPath("user.tenantId")
                                        .type(JsonFieldType.STRING)
                                        .description("ID of the tenant user should belong to"),
                                PayloadDocumentation
                                        .fieldWithPath("user.username")
                                        .type(JsonFieldType.STRING)
                                        .description("Accepted username of the user"),
                                PayloadDocumentation
                                        .fieldWithPath("user.enabled")
                                        .type(JsonFieldType.BOOLEAN)
                                        .description("Is user enabled by default"),
                                PayloadDocumentation
                                        .fieldWithPath("role")
                                        .type(JsonFieldType.STRING)
                                        .description("Role assigned to the user"),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.firstName")
                                        .optional()
                                        .type(JsonFieldType.STRING)
                                        .description("First name of the user. Field is optional."),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.middleName")
                                        .optional()
                                        .type(JsonFieldType.STRING)
                                        .description("Middle name of the user. Field is optional."),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.lastName")
                                        .optional()
                                        .type(JsonFieldType.STRING)
                                        .description("Last name of the user. Field is optional.")
                        )
                    ));
    }

    @Test
    @WithMockJwt( userId = CONST_USER_ID, tenantId = CONST_TENANT_ID)
    public void userInfo() throws Exception {
        setupTenant();
        setupUser();

        getMockMvc().perform(
                get("/api/v1/auth/info"))
                .andExpect(status().isOk())
                .andDo(document(
                        "successful_user_info",
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("username")
                                        .type(JsonFieldType.STRING)
                                        .description("Username of the user"),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.firstName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("First name of the user. Field is optional"),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.lastName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Last name of the user. Field is optional"),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.middleName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Midle name of the user. Field is optional"),
                                PayloadDocumentation
                                        .fieldWithPath("grantedAuthorities.roles")
                                        .type(JsonFieldType.ARRAY)
                                        .optional()
                                        .description("List of user roles"),
                                PayloadDocumentation
                                        .fieldWithPath("grantedAuthorities.privileges")
                                        .type(JsonFieldType.ARRAY)
                                        .optional()
                                        .description("List of user privileges"),
                                PayloadDocumentation
                                        .fieldWithPath("grantedAuthorities.grants")
                                        .type(JsonFieldType.ARRAY)
                                        .optional()
                                        .description("List of user grants")
                        )
                ));
    }

    @Test
    @WithMockJwt( userId = CONST_USER_ID)
    public void updateUserBasicInfo() throws Exception {
        setupTenant();
        setupUser();

        UserBasicInfoData userBasicInfoData = new UserBasicInfoData()
                .firstName("Ivan")
                .middleName("Petar")
                .lastName("Petrić");

        String content = toJson(userBasicInfoData);

        getMockMvc().perform(
                put("/api/v1/auth/tenant/{tenantId}/users/basic-info", CONST_TENANT_ID)
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andDo(document(
                        "successful_update_user_basic_info",
                        RequestDocumentation.pathParameters(
                                RequestDocumentation
                                        .parameterWithName("tenantId")
                                        .description("ID of the tenant user should belong to")),
                        PayloadDocumentation.requestFields(
                                PayloadDocumentation
                                        .fieldWithPath("firstName")
                                        .type(JsonFieldType.STRING)
                                        .description("First name of the user"),
                                PayloadDocumentation
                                        .fieldWithPath("middleName")
                                        .type(JsonFieldType.STRING)
                                        .description("Middle name of the user. Field is optional."),
                                PayloadDocumentation
                                        .fieldWithPath("lastName")
                                        .type(JsonFieldType.STRING)
                                        .description("Last name of the user")),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.firstName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("First name of the user"),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.middleName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Middle name of the user. Field is optional"),
                                PayloadDocumentation
                                        .fieldWithPath("basicInfo.lastName")
                                        .type(JsonFieldType.STRING)
                                        .optional()
                                        .description("Last name of the user")
                        )
                ));
    }

    @Test
    @WithMockJwt( userId = CONST_USER_ID)
    public void changeUserPassword() throws Exception {
        setupTenant();
        setupUser();

        UserPasswordChangeRequest request = new UserPasswordChangeRequest()
                .currentPassword(CONST_PASSWORD)
                .newPassword("password1234")
                .newPasswordRepeated("password1234");

        String content = toJson(request);

        getMockMvc().perform(
                put("/api/v1/auth/tenant/{tenantId}/users/change-password", CONST_TENANT_ID)
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isNoContent())
                .andDo(document(
                        "successful_change_password",
                        RequestDocumentation.pathParameters(
                                RequestDocumentation
                                        .parameterWithName("tenantId")
                                        .description("ID of the tenant user should belong to")),
                        PayloadDocumentation.requestFields(
                                PayloadDocumentation
                                        .fieldWithPath("currentPassword")
                                        .type(JsonFieldType.STRING)
                                        .description("Current user's password"),
                                PayloadDocumentation
                                        .fieldWithPath("newPassword")
                                        .type(JsonFieldType.STRING)
                                        .description("New password"),
                                PayloadDocumentation
                                        .fieldWithPath("newPasswordRepeated")
                                        .type(JsonFieldType.STRING)
                                        .description("New password repeated again by the user")
                        )
                ));
    }

    private void setupUser() {
        User user = new User()
                .id(UUID.fromString(CONST_USER_ID))
                .tenantId(UUID.fromString(CONST_TENANT_ID))
                .username(CONST_USERNAME)
                .password(this.passwordEncoder.encode(CONST_PASSWORD))
                .enabled(true);

        UserBasicInfo userBasicInfo = new UserBasicInfo()
                .firstName("BeforeFirst")
                .middleName("BeforeMiddle")
                .lastName("BeforeLast")
                .userId(UUID.fromString(CONST_USER_ID));

        UserRole adminRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(this.tc.tenantAdminRole.getId());

        this.tc.user = this.userRepositoryAggregate.getUserRepository().save(user);
        this.userRepositoryAggregate.getBasicUserRepository().save(userBasicInfo);
        this.userRepositoryAggregate.getUserRoleRepository().save(adminRole);
    }

    private void setupTenant() {
        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString(CONST_TENANT_ID))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tc.tenant = this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tc.tenantAdminRole = this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);
    }

    private class TestContext {
        private Tenant tenant;
        private TenantRole tenantAdminRole;
        private User user;
    }
}
