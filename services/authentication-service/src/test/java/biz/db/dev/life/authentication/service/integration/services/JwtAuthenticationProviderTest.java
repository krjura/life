package biz.db.dev.life.authentication.service.integration.services;

import biz.db.dev.life.authentication.service.support.TestBase;
import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.JwtTokenBuilder;
import biz.db.dev.life.jwt.core.config.JwtProps;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.keystore.DefaultKeyStoreProvider;
import biz.db.dev.life.jwt.core.keystore.JwtKeyStore;
import biz.db.dev.life.jwt.core.providers.JwtAuthenticationProvider;
import biz.db.dev.life.jwt.core.providers.RevocationProvider;
import biz.db.dev.life.jwt.core.utils.JwtHelper;
import com.auth0.jwt.exceptions.JWTDecodeException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;

import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JwtAuthenticationProviderTest extends TestBase {

    @Autowired
    private JwtProps jwtProps;

    private JwtAuthenticationProvider provider;

    private RevocationProvider revocationProvider;

    @Before
    public void init() {
        JwtKeyStore jwtKeyStore = new DefaultKeyStoreProvider(this.jwtProps.getKeyStore());
        this.revocationProvider = mock(RevocationProvider.class);
        this.provider = new JwtAuthenticationProvider(jwtKeyStore, this.revocationProvider);
    }

    @Test(expected = BadCredentialsException.class)
    public void testRevocationTrue() throws JwtProcessingException {

        String tokenId = UUID.randomUUID().toString();
        UUID tenantId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();

        String defaultKeyId = this.jwtProps.getKeyStore().getDefaultKeyId();
        String privateKey = this.jwtProps.getKeyStore().getKeys().get(defaultKeyId).getPrivateKey();
        String sanitizedPrivateKey = JwtHelper.sanitizePrivateKey(privateKey);

        String token = JwtTokenBuilder
                .create()
                .expiry(60)
                .tokenId(tokenId)
                .tenantId(tenantId)
                .userId(userId)
                .grants(new String[] {})
                .keyId(defaultKeyId)
                .privateKey(sanitizedPrivateKey)
                .build()
                .getToken();

        JwtAuthenticationToken jwtAuthenticationToken = mock(JwtAuthenticationToken.class);

        when(jwtAuthenticationToken.getToken()).thenReturn(token);
        when(this.revocationProvider.isTokenRevoked(tokenId)).thenReturn(true);

        this.provider.authenticate(jwtAuthenticationToken);
    }

    @Test(expected = JWTDecodeException.class) // JWTDecodeException because we did not provide a valid token
    public void testRevocationFalse() {

        String token = "not a token";

        JwtAuthenticationToken jwtAuthenticationToken = mock(JwtAuthenticationToken.class);

        when(jwtAuthenticationToken.getToken()).thenReturn(token);
        when(this.revocationProvider.isTokenRevoked(token)).thenReturn(false);

        this.provider.authenticate(jwtAuthenticationToken);
    }
}
