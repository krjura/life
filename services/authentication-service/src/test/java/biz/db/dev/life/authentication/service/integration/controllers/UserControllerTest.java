package biz.db.dev.life.authentication.service.integration.controllers;

import biz.db.dev.life.authentication.service.swagger.SwaggerTools;
import biz.db.dev.life.authentication.service.support.TestBase;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.LoggedInUserInfoResponse;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.RegisterUserResponse;
import biz.db.dev.life.authentication.service.controllers.pojo.responses.UpdateUserBasicInfoResponse;
import biz.db.dev.life.authentication.service.model.Tenant;
import biz.db.dev.life.authentication.service.model.TenantPrivilege;
import biz.db.dev.life.authentication.service.model.TenantRole;
import biz.db.dev.life.authentication.service.model.User;
import biz.db.dev.life.authentication.service.model.UserBasicInfo;
import biz.db.dev.life.authentication.service.model.UserRole;
import biz.db.dev.life.authentication.service.model.repository.impl.TenantRepositoryAggregate;
import biz.db.dev.life.authentication.service.model.repository.impl.UserRepositoryAggregate;
import biz.db.dev.life.jwt.core.test.WithMockJwt;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorDetails;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponse;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatus;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatusCode;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

public class UserControllerTest extends TestBase {

    @Autowired
    private TenantRepositoryAggregate tenantRepositoryAggregate;

    @Autowired
    private UserRepositoryAggregate userRepositoryAggregate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void registerNewUserValidationError() throws Exception {
        String content = contentOf("requests/invalid/RegisterUserRequest-invalid-1.json");

        MvcResult result = mockMvc.perform(
                post("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/register")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse errorResponse = fromJson(result, ErrorResponse.class);

        assertThat(errorResponse.getStatus(), is(equalTo(ErrorResponseStatus.ERROR)));
        assertThat(errorResponse.getStatusCode(), is(equalTo(ErrorResponseStatusCode.BAD_REQUEST.getCode())));
        assertThat(errorResponse.getStatusDescription(), is(equalTo("validation error occurred")));
        assertThat(errorResponse.getData(), is(notNullValue()));
        assertThat(errorResponse.getData().getDetails(), hasSize(5));

        List<ErrorDetails> errorDetails = errorResponse.getData().getDetails();
        assertThat(errorDetails.get(0).getReason(), is(equalTo("life.PasswordConstraint.message")));
        assertThat(errorDetails.get(0).getAttributeName(), is(equalTo("password")));
        assertThat(errorDetails.get(0).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(0).getAttributeValues().get(0), is(equalTo("")));
        assertThat(errorDetails.get(0).getMessage(), is(equalTo("life.PasswordConstraint.message")));

        assertThat(errorDetails.get(1).getReason(), is(equalTo("life.UUIDConstraint.message")));
        assertThat(errorDetails.get(1).getAttributeName(), is(equalTo("tenantId")));
        assertThat(errorDetails.get(1).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(1).getAttributeValues().get(0), is(equalTo("notuuid")));
        assertThat(errorDetails.get(1).getMessage(), is(equalTo("life.UUIDConstraint.message")));

        assertThat(errorDetails.get(2).getReason(), is(equalTo("life.UserBasicInfoNameConstraint.message")));
        assertThat(errorDetails.get(2).getAttributeName(), is(equalTo("userBasicInfo.firstName")));
        assertThat(errorDetails.get(2).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(2).getAttributeValues().get(0), is(equalTo("")));
        assertThat(errorDetails.get(2).getMessage(), is(equalTo("life.UserBasicInfoNameConstraint.message")));

        assertThat(errorDetails.get(3).getReason(), is(equalTo("life.UserBasicInfoNameConstraint.message")));
        assertThat(errorDetails.get(3).getAttributeName(), is(equalTo("userBasicInfo.lastName")));
        assertThat(errorDetails.get(3).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(3).getAttributeValues().get(0), is(equalTo("")));
        assertThat(errorDetails.get(3).getMessage(), is(equalTo("life.UserBasicInfoNameConstraint.message")));

        assertThat(errorDetails.get(4).getReason(), is(equalTo("life.UsernameConstraint.message")));
        assertThat(errorDetails.get(4).getAttributeName(), is(equalTo("username")));
        assertThat(errorDetails.get(4).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(4).getAttributeValues().get(0), is(equalTo("not email")));
        assertThat(errorDetails.get(4).getMessage(), is(equalTo("life.UsernameConstraint.message")));
    }

    @Test
    public void registerNewUserNoBasicDataNoRole() throws Exception {
        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("a847a209-b472-4342-9a3f-a819c09216ef"))
                .tenantName("Test");

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);

        // when
        String content = contentOf("requests/valid/RegisterUserRequest-valid-2.json");

        MvcResult result = mockMvc.perform(
                post("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/register")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        // verify response
        RegisterUserResponse response = fromJson(result, RegisterUserResponse.class);
        assertThat(response.getUser().getId(), is(notNullValue()));
        assertThat(response.getUser().getTenantId(), is(equalTo(tenant.getId())));
        assertThat(response.getUser().getUsername(), is(equalTo("user@example.com")));

        assertThat(response.getBasicInfo(), is(nullValue()));
        assertThat(response.getRole(),is(nullValue()));

        // verify user is created
        List<User> users = this
                .userRepositoryAggregate.getUserRepository().findAllUsers();
        assertThat(users, hasSize(1));

        User user = users.get(0);
        assertThat(user.getUsername(), is(equalTo("user@example.com")));
        assertThat(user.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(user.getId(), is(notNullValue()));
        assertThat(passwordEncoder.matches("password01", user.getPassword()), is(equalTo(true)));

        // verify user role
        List<UserRole> userRoles = this
                .userRepositoryAggregate.getUserRoleRepository().findUserRoleByUserId(user.getId());
        assertThat(userRoles, hasSize(0));

        // verify basic info
        Optional<UserBasicInfo> basicInfoOptional = this
                .userRepositoryAggregate.getBasicUserRepository().findBasicInfoByUserId(user.getId());
        assertThat(basicInfoOptional.isPresent(), is(equalTo(false)));
    }

    @Test
    public void registerNewUserNoBasicData() throws Exception {
        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("a847a209-b472-4342-9a3f-a819c09216ef"))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        // when
        String content = contentOf("requests/valid/RegisterUserRequest-valid-2.json");

        MvcResult result = mockMvc.perform(
                post("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/register")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        // verify response
        RegisterUserResponse response = fromJson(result, RegisterUserResponse.class);
        assertThat(response.getUser().getId(), is(notNullValue()));
        assertThat(response.getUser().getTenantId(), is(equalTo(tenant.getId())));
        assertThat(response.getUser().getUsername(), is(equalTo("user@example.com")));

        assertThat(response.getBasicInfo(), is(nullValue()));
        assertThat(response.getRole(),is(equalTo(tenantRole.getRoleName())));

        // verify user is created
        List<User> users = this
                .userRepositoryAggregate.getUserRepository().findAllUsers();
        assertThat(users, hasSize(1));

        User user = users.get(0);
        assertThat(user.getUsername(), is(equalTo("user@example.com")));
        assertThat(user.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(user.getId(), is(notNullValue()));
        assertThat(passwordEncoder.matches("password01", user.getPassword()), is(equalTo(true)));

        // verify user role
        List<UserRole> userRoles = this
                .userRepositoryAggregate.getUserRoleRepository().findUserRoleByUserId(user.getId());
        assertThat(userRoles, hasSize(1));

        UserRole userRole = userRoles.get(0);
        assertThat(userRole.getTenantRoleId(), is(equalTo(tenantRole.getId())));
        assertThat(userRole.getUserId(), is(equalTo(user.getId())));

        // verify basic info
        Optional<UserBasicInfo> basicInfoOptional = this
                .userRepositoryAggregate.getBasicUserRepository().findBasicInfoByUserId(user.getId());
        assertThat(basicInfoOptional.isPresent(), is(equalTo(false)));
    }

    @Test
    public void registerNewUser() throws Exception {
        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("a847a209-b472-4342-9a3f-a819c09216ef"))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        // when
        String content = contentOf("requests/valid/RegisterUserRequest-valid-1.json");

        MvcResult result = mockMvc.perform(
                post("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/register")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isCreated())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        // verify response
        RegisterUserResponse response = fromJson(result, RegisterUserResponse.class);
        assertThat(response.getUser().getId(), is(notNullValue()));
        assertThat(response.getUser().getTenantId(), is(equalTo(tenant.getId())));
        assertThat(response.getUser().getUsername(), is(equalTo("user@example.com")));

        assertThat(response.getBasicInfo().getFirstName(), is(equalTo("user")));
        assertThat(response.getBasicInfo().getLastName(), is(equalTo("test")));
        assertThat(response.getBasicInfo().getMiddleName(), is(nullValue()));

        assertThat(response.getRole(),is(equalTo(tenantRole.getRoleName())));

        // verify user is created
        List<User> users = this.userRepositoryAggregate.getUserRepository().findAllUsers();
        assertThat(users, hasSize(1));

        User user = users.get(0);
        assertThat(user.getUsername(), is(equalTo("user@example.com")));
        assertThat(user.getTenantId(), is(equalTo(tenant.getId())));
        assertThat(user.getId(), is(notNullValue()));
        assertThat(passwordEncoder.matches("password01", user.getPassword()), is(equalTo(true)));

        // verify user role
        List<UserRole> userRoles = this
                .userRepositoryAggregate.getUserRoleRepository().findUserRoleByUserId(user.getId());
        assertThat(userRoles, hasSize(1));

        UserRole userRole = userRoles.get(0);
        assertThat(userRole.getTenantRoleId(), is(equalTo(tenantRole.getId())));
        assertThat(userRole.getUserId(), is(equalTo(user.getId())));

        // verify basic info
        Optional<UserBasicInfo> basicInfoOptional = this
                .userRepositoryAggregate.getBasicUserRepository().findBasicInfoByUserId(user.getId());
        assertThat(basicInfoOptional.isPresent(), is(equalTo(true)));
        assertThat(basicInfoOptional.get().getUserId(), is(equalTo(user.getId())));
        assertThat(basicInfoOptional.get().getFirstName(), is(equalTo("user")));
        assertThat(basicInfoOptional.get().getMiddleName(), is(nullValue()));
        assertThat(basicInfoOptional.get().getLastName(), is(equalTo("test")));
    }

    @Test
    @WithMockJwt( userId = "3baf83ca-b339-4844-883f-2e0d4092171e")
    public void updateUserBasicInfo() throws Exception {
        // given

        Tenant tenant = new Tenant()
                .id(UUID.fromString("a847a209-b472-4342-9a3f-a819c09216ef"))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        User user = new User()
                .id(UUID.fromString("3baf83ca-b339-4844-883f-2e0d4092171e"))
                .tenantId(tenant.getId())
                .username("demo@example.com")
                .password("demo")
                .enabled(true);

        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        String content = contentOf("requests/valid/UserBasicInfo-valid-1.json");

        MvcResult result = mockMvc.perform(
                put("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/basic-info")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        UpdateUserBasicInfoResponse response = fromJson(result, UpdateUserBasicInfoResponse.class);
        assertThat(response.getBasicInfo(), is(notNullValue()));
        assertThat(response.getBasicInfo().getFirstName(), is(equalTo("First")));
        assertThat(response.getBasicInfo().getMiddleName(), is(equalTo("Middle")));
        assertThat(response.getBasicInfo().getLastName(), is(equalTo("Last")));

        Optional<UserBasicInfo> basicInfoOptional = userRepositoryAggregate
                .getBasicUserRepository().findBasicInfoByUserId(UUID.fromString("3baf83ca-b339-4844-883f-2e0d4092171e"));

        assertThat(basicInfoOptional.isPresent(), is(equalTo(true)));
        UserBasicInfo basicInfoFromDatabase = basicInfoOptional.get();
        assertThat(basicInfoFromDatabase.getFirstName(), is(equalTo("First")));
        assertThat(basicInfoFromDatabase.getMiddleName(), is(equalTo("Middle")));
        assertThat(basicInfoFromDatabase.getLastName(), is(equalTo("Last")));
    }

    @Test
    @WithMockJwt( userId = "3baf83ca-b339-4844-883f-2e0d4092171e")
    public void updateExistingUserBasicInfo() throws Exception {
        // given

        Tenant tenant = new Tenant()
                .id(UUID.fromString("a847a209-b472-4342-9a3f-a819c09216ef"))
                .tenantName("Test");

        TenantRole tenantRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantRole);

        UUID userId = UUID.fromString("3baf83ca-b339-4844-883f-2e0d4092171e");

        User user = new User()
                .id(userId)
                .tenantId(tenant.getId())
                .username("demo@example.com")
                .password("demo")
                .enabled(true);

        UserBasicInfo userBasicInfo = new UserBasicInfo()
                .firstName("BeforeFirst")
                .middleName("BeforeMiddle")
                .lastName("BeforeLast")
                .userId(userId);

        this.userRepositoryAggregate.getUserRepository().save(user);
        this.userRepositoryAggregate.getBasicUserRepository().save(userBasicInfo);

        // when
        String content = contentOf("requests/valid/UserBasicInfo-valid-1.json");

        MvcResult result = mockMvc.perform(
                put("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/basic-info")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        UpdateUserBasicInfoResponse response = fromJson(result, UpdateUserBasicInfoResponse.class);
        assertThat(response.getBasicInfo(), is(notNullValue()));
        assertThat(response.getBasicInfo().getFirstName(), is(equalTo("First")));
        assertThat(response.getBasicInfo().getMiddleName(), is(equalTo("Middle")));
        assertThat(response.getBasicInfo().getLastName(), is(equalTo("Last")));

        Optional<UserBasicInfo> basicInfoOptional = userRepositoryAggregate
                .getBasicUserRepository().findBasicInfoByUserId(UUID.fromString("3baf83ca-b339-4844-883f-2e0d4092171e"));

        assertThat(basicInfoOptional.isPresent(), is(equalTo(true)));
        UserBasicInfo basicInfoFromDatabase = basicInfoOptional.get();
        assertThat(basicInfoFromDatabase.getFirstName(), is(equalTo("First")));
        assertThat(basicInfoFromDatabase.getMiddleName(), is(equalTo("Middle")));
        assertThat(basicInfoFromDatabase.getLastName(), is(equalTo("Last")));
    }

    @Test
    public void updateUserBasicInfoNotAuth() throws Exception {
        String content = contentOf("requests/valid/UserBasicInfo-valid-1.json");

        mockMvc.perform(
                put("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/basic-info")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    @WithMockJwt( userId = "3baf83ca-b339-4844-883f-2e0d4092171e")
    public void updateUserBasicInfoInvalid() throws Exception {
        String content = contentOf("requests/invalid/UserBasicInfo-invalid-1.json");

        MvcResult result = mockMvc.perform(
                put("/api/v1/auth/tenant/a847a209-b472-4342-9a3f-a819c09216ef/users/basic-info")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse errorResponse = fromJson(result, ErrorResponse.class);

        assertThat(errorResponse.getStatus(), is(equalTo(ErrorResponseStatus.ERROR)));
        assertThat(errorResponse.getStatusCode(), is(equalTo(ErrorResponseStatusCode.BAD_REQUEST.getCode())));
        assertThat(errorResponse.getStatusDescription(), is(equalTo("validation error occurred")));
        assertThat(errorResponse.getData(), is(notNullValue()));
        assertThat(errorResponse.getData().getDetails(), hasSize(2));

        List<ErrorDetails> errorDetails = errorResponse.getData().getDetails();
        assertThat(errorDetails.get(0).getReason(), is(equalTo("life.UserBasicInfoNameConstraint.message")));
        assertThat(errorDetails.get(0).getAttributeName(), is(equalTo("firstName")));
        assertThat(errorDetails.get(0).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(0).getAttributeValues().get(0), is(nullValue()));
        assertThat(errorDetails.get(0).getMessage(), is(equalTo("life.UserBasicInfoNameConstraint.message")));

        assertThat(errorDetails.get(1).getReason(), is(equalTo("life.UserBasicInfoNameConstraint.message")));
        assertThat(errorDetails.get(1).getAttributeName(), is(equalTo("lastName")));
        assertThat(errorDetails.get(1).getAttributeValues(), hasSize(1));
        assertThat(errorDetails.get(1).getAttributeValues().get(0), is(nullValue()));
        assertThat(errorDetails.get(1).getMessage(), is(equalTo("life.UserBasicInfoNameConstraint.message")));
    }

    @Test
    @WithMockJwt( userId = "7ec3715c-e5b9-4e5e-b21d-6323c11958af")
    public void changeUserPassword() throws Exception {

        // given
        Tenant tenant = basicTenantForPasswordChange();
        User user = basicUserForPasswordChange(tenant);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        String content = contentOf("requests/valid/UserPasswordChangeRequest-valid-1.json");

        mockMvc.perform(
                put("/api/v1/auth/tenant/a5befd48-280d-49e3-85e3-30e9e733b525/users/change-password")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isNoContent())
                .andExpect(SwaggerTools.instance().getMatcher());

        Optional<User> userOptional = userRepositoryAggregate.getUserRepository().findById(user.getId());

        assertThat(userOptional.isPresent(), is(equalTo(true)));
        assertThat(passwordEncoder.matches("newpassword", userOptional.get().getPassword()), is(equalTo(true)));
    }

    @Test
    @WithMockJwt( userId = "7ec3715c-e5b9-4e5e-b21d-6323c11958af")
    public void changeUserPasswordCurrentNoMatch() throws Exception {

        // given
        Tenant tenant = basicTenantForPasswordChange();
        User user = basicUserForPasswordChange(tenant);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        String content = contentOf("requests/invalid/UserPasswordChangeRequest-invalid-2.json");

        mockMvc.perform(
                put("/api/v1/auth/tenant/a5befd48-280d-49e3-85e3-30e9e733b525/users/change-password")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    @WithMockJwt( userId = "7ec3715c-e5b9-4e5e-b21d-6323c11958af")
    public void changeUserPasswordPasswordMismatch() throws Exception {

        // given
        Tenant tenant = basicTenantForPasswordChange();
        User user = basicUserForPasswordChange(tenant);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        String content = contentOf("requests/invalid/UserPasswordChangeRequest-invalid-1.json");

        mockMvc.perform(
                put("/api/v1/auth/tenant/a5befd48-280d-49e3-85e3-30e9e733b525/users/change-password")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    @WithMockJwt( userId = "7ec3715c-e5b9-4e5e-b21d-6323c11958af")
    public void changeUserPasswordInvalidRequest() throws Exception {
        // when
        String content = contentOf("requests/invalid/UserPasswordChangeRequest-invalid-3.json");

        mockMvc.perform(
                put("/api/v1/auth/tenant/a5befd48-280d-49e3-85e3-30e9e733b525/users/change-password")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .with(csrf()))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockJwt( userId = "be86a7c8-e6d8-449a-9ad2-b32ffa0adcb7")
    public void changeUserPasswordInvalidUserInAuth() throws Exception {

        // given
        Tenant tenant = basicTenantForPasswordChange();
        User user = basicUserForPasswordChange(tenant);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.userRepositoryAggregate.getUserRepository().save(user);


        // when
        String content = contentOf("requests/valid/UserPasswordChangeRequest-valid-1.json");

        mockMvc.perform(
                put("/api/v1/auth/tenant/a5befd48-280d-49e3-85e3-30e9e733b525/users/change-password")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    @WithMockJwt( userId = "daad598d-6bc5-4862-8ea7-b17c17074dbe", tenantId = "c00ef9d8-4cbd-45ed-bcb6-b62bc448018a")
    public void userInfo() throws Exception {

        // given

        Tenant tenant = new Tenant()
                .id(UUID.fromString("c00ef9d8-4cbd-45ed-bcb6-b62bc448018a"))
                .tenantName("Test");

        TenantRole tenantAdminRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(false);

        TenantRole tenantUserRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_USER")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantAdminRole);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantUserRole);

        TenantPrivilege tenantAdminPrivilege = new TenantPrivilege()
                .tenantRoleId(tenantAdminRole.getId())
                .privilegeName("PRIV_WRITE")
                .privilegeDescription("Write All");

        TenantPrivilege tenantUserPrivilege = new TenantPrivilege()
                .tenantRoleId(tenantUserRole.getId())
                .privilegeName("PRIV_READ")
                .privilegeDescription("Read All");

        this.tenantRepositoryAggregate.getTenantPrivilegeRepository().save(tenantAdminPrivilege);
        this.tenantRepositoryAggregate.getTenantPrivilegeRepository().save(tenantUserPrivilege);

        UUID userId = UUID.fromString("daad598d-6bc5-4862-8ea7-b17c17074dbe");

        User user = new User()
                .id(userId)
                .tenantId(tenant.getId())
                .username("demo@example.com")
                .password("demo")
                .enabled(true);

        UserBasicInfo userBasicInfo = new UserBasicInfo()
                .firstName("BeforeFirst")
                .middleName("BeforeMiddle")
                .lastName("BeforeLast")
                .userId(userId);

        UserRole adminRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantAdminRole.getId());

        UserRole userRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantUserRole.getId());

        this.userRepositoryAggregate.getUserRepository().save(user);
        this.userRepositoryAggregate.getBasicUserRepository().save(userBasicInfo);
        this.userRepositoryAggregate.getUserRoleRepository().save(adminRole);
        this.userRepositoryAggregate.getUserRoleRepository().save(userRole);

        // when

        MvcResult result = mockMvc.perform(
                get("/api/v1/auth/info"))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then

        LoggedInUserInfoResponse response = fromJson(result, LoggedInUserInfoResponse.class);

        assertThat(response.getUsername(), is(equalTo(user.getUsername())));
        assertThat(response.getBasicInfo(), is(notNullValue()));
        assertThat(response.getBasicInfo().getFirstName(), is(equalTo(userBasicInfo.getFirstName())));
        assertThat(response.getBasicInfo().getMiddleName(), is(equalTo(userBasicInfo.getMiddleName())));
        assertThat(response.getBasicInfo().getLastName(), is(equalTo(userBasicInfo.getLastName())));
        assertThat(response.getGrantedAuthorities(), is(notNullValue()));
        assertThat(response.getGrantedAuthorities().getRoles(), hasSize(2));
        assertThat(response.getGrantedAuthorities().getRoles(), hasItem("ROLE_ADMIN"));
        assertThat(response.getGrantedAuthorities().getRoles(), hasItem("ROLE_USER"));
        assertThat(response.getGrantedAuthorities().getPrivileges(), hasSize(2));
        assertThat(response.getGrantedAuthorities().getPrivileges(), hasItem("PRIV_WRITE"));
        assertThat(response.getGrantedAuthorities().getPrivileges(), hasItem("PRIV_READ"));
        assertThat(response.getGrantedAuthorities().getGrants(), hasSize(4));
        assertThat(response.getGrantedAuthorities().getGrants(), hasItem("ROLE_ADMIN"));
        assertThat(response.getGrantedAuthorities().getGrants(), hasItem("ROLE_USER"));
        assertThat(response.getGrantedAuthorities().getGrants(), hasItem("PRIV_WRITE"));
        assertThat(response.getGrantedAuthorities().getGrants(), hasItem("PRIV_READ"));
    }

    @Test
    @WithMockJwt
    public void userInfoUserNotFound() throws Exception {
        mockMvc.perform(
                get("/api/v1/auth/info"))
                .andExpect(status().isForbidden())
                .andExpect(SwaggerTools.instance().getMatcher());
    }

    @Test
    @WithMockJwt( userId = "daad598d-6bc5-4862-8ea7-b17c17074dbe", tenantId = "daad598d-6bc5-4862-8ea7-b17c17074dbe")
    public void userInfoTenantDoesNotMatch() throws Exception {
        // tenant id is matched by tenant

        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("c00ef9d8-4cbd-45ed-bcb6-b62bc448018a"))
                .tenantName("Test");

        TenantRole tenantAdminRole = new TenantRole()
                .tenantId(tenant.getId())
                .roleName("ROLE_ADMIN")
                .roleDescription("Administrator")
                .defaultRole(true);

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);
        this.tenantRepositoryAggregate.getTenantRoleRepository().save(tenantAdminRole);

        UUID userId = UUID.fromString("daad598d-6bc5-4862-8ea7-b17c17074dbe");

        User user = new User()
                .id(userId)
                .tenantId(tenant.getId())
                .username("demo@example.com")
                .password("demo")
                .enabled(true);

        UserRole adminRole = new UserRole()
                .userId(user.getId())
                .tenantRoleId(tenantAdminRole.getId());

        this.userRepositoryAggregate.getUserRepository().save(user);
        this.userRepositoryAggregate.getUserRoleRepository().save(adminRole);

        // when
        MvcResult result = mockMvc.perform(
                get("/api/v1/auth/info"))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        LoggedInUserInfoResponse response = fromJson(result, LoggedInUserInfoResponse.class);

        assertThat(response.getUsername(), is(equalTo(user.getUsername())));
        assertThat(response.getBasicInfo(), is(nullValue()));
        assertThat(response.getGrantedAuthorities(), is(notNullValue()));
        assertThat(response.getGrantedAuthorities().getRoles(), hasSize(0));
        assertThat(response.getGrantedAuthorities().getPrivileges(), hasSize(0));
        assertThat(response.getGrantedAuthorities().getGrants(), hasSize(0));
    }

    @Test
    @WithMockJwt( userId = "daad598d-6bc5-4862-8ea7-b17c17074dbe", tenantId = "c00ef9d8-4cbd-45ed-bcb6-b62bc448018a")
    public void userInfoNoBasicInfo() throws Exception {

        // given
        Tenant tenant = new Tenant()
                .id(UUID.fromString("c00ef9d8-4cbd-45ed-bcb6-b62bc448018a"))
                .tenantName("Test");

        this.tenantRepositoryAggregate.getTenantRepository().save(tenant);

        UUID userId = UUID.fromString("daad598d-6bc5-4862-8ea7-b17c17074dbe");

        User user = new User()
                .id(userId)
                .tenantId(tenant.getId())
                .username("demo@example.com")
                .password("demo")
                .enabled(true);

        this.userRepositoryAggregate.getUserRepository().save(user);

        // when
        MvcResult result = mockMvc.perform(
                get("/api/v1/auth/info"))
                .andExpect(status().isOk())
                .andExpect(SwaggerTools.instance().getMatcher())
                .andReturn();

        // then
        LoggedInUserInfoResponse response = fromJson(result, LoggedInUserInfoResponse.class);

        assertThat(response.getUsername(), is(equalTo(user.getUsername())));
        assertThat(response.getBasicInfo(), is(nullValue()));
        assertThat(response.getGrantedAuthorities(), is(notNullValue()));
        assertThat(response.getGrantedAuthorities().getRoles(), hasSize(0));
        assertThat(response.getGrantedAuthorities().getPrivileges(), hasSize(0));
        assertThat(response.getGrantedAuthorities().getGrants(), hasSize(0));
    }

    private User basicUserForPasswordChange(Tenant tenant) {
        return new User()
                .id(UUID.fromString("7ec3715c-e5b9-4e5e-b21d-6323c11958af"))
                .tenantId(tenant.getId())
                .username("demo@example.com")
                .password(passwordEncoder.encode("oldpassword"))
                .enabled(true);
    }

    private Tenant basicTenantForPasswordChange() {
        return new Tenant()
                .id(UUID.fromString("a5befd48-280d-49e3-85e3-30e9e733b525"))
                .tenantName("Test");
    }
}
