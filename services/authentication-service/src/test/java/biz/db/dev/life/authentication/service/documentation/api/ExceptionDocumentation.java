package biz.db.dev.life.authentication.service.documentation.api;

import biz.db.dev.life.authentication.service.controllers.pojo.requests.LoginRequest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.RequestDocumentation;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

public class ExceptionDocumentation extends DocumentationMvcTestBase {

    private static final String CONST_TENANT_ID = "cb9b0a9b-f540-400a-87d7-3f21fa302f45";

    @Test
    public void loginWithBadRequest() throws Exception {
        LoginRequest loginRequest = new LoginRequest();

        String content = toJson(loginRequest);

        getMockMvc()
                .perform(
                        post("/api/v1/auth/tenant/{tenantId}/login", CONST_TENANT_ID)
                                .content(content)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .with(csrf())
                )
                .andExpect(status().isBadRequest())
                .andDo(document(
                        "errors_bad_request",
                        RequestDocumentation.pathParameters(
                                RequestDocumentation
                                        .parameterWithName("tenantId")
                                        .description("Id of the tenant user belongs to")
                        ),
                        PayloadDocumentation.responseFields(
                                PayloadDocumentation
                                        .fieldWithPath("status")
                                        .type(JsonFieldType.STRING)
                                        .description("Status of the request. Can be ERROR or SUCCESS"),
                                PayloadDocumentation
                                        .fieldWithPath("statusCode")
                                        .type(JsonFieldType.NUMBER)
                                        .description("Status code of the request"),
                                PayloadDocumentation
                                        .fieldWithPath("statusDescription")
                                        .type(JsonFieldType.STRING)
                                        .attributes()
                                        .description("Human readable description of the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data")
                                        .type(JsonFieldType.OBJECT)
                                        .description("Wrapper object for error details"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[]")
                                        .type(JsonFieldType.ARRAY)
                                        .description("List of all error details"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].reason")
                                        .type(JsonFieldType.STRING)
                                        .description("Localized key of the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].attributeName")
                                        .type(JsonFieldType.STRING)
                                        .description("Name of the attribute causing the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].attributeValues")
                                        .type(JsonFieldType.ARRAY)
                                        .description("List of values causing the error"),
                                PayloadDocumentation
                                        .fieldWithPath("data.details[].message")
                                        .type(JsonFieldType.STRING)
                                        .description("Human readable description of the errors")
                        )
                ));
    }

    @Test
    public void userInfo() throws Exception {
        getMockMvc().perform(
                get("/api/v1/auth/info"))
                .andDo(document(
                        "error_forbidden"
                ))
                .andExpect(status().isForbidden());
    }
}
