#!/usr/bin/env bash

curl -l -v -X PUT --data-binary '@src/main/resources/application.yml' http://localhost:8500/v1/kv/config/authentication-service/application.yml

curl -l -v -X PUT --data-binary '@src/main/resources/logback.xml' http://localhost:8500/v1/kv/config/authentication-service/logback.xml