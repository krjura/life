--changeset v20171022151900
CREATE TABLE tenant (
  id  UUID NOT NULL,

  tenant_name VARCHAR(100) NOT NULL,

  CONSTRAINT pk_tenant PRIMARY KEY (id)
);

CREATE TABLE tenant_role (
  id  SERIAL NOT NULL,

  tenant_id  UUID NOT NULL,

  role_name VARCHAR(64) NOT NULL,
  role_description VARCHAR(100) NOT NULL,

  CONSTRAINT pk_tenant_role PRIMARY KEY (id),
  CONSTRAINT fk_tenant_role_tenant FOREIGN KEY (tenant_id) REFERENCES tenant (id)
);

CREATE TABLE tenant_privilege (
  id  SERIAL NOT NULL,

  tenant_role_id INT NOT NULL,

  privilege_name VARCHAR(64) NOT NULL,
  privilege_description VARCHAR(100) NOT NULL,

  CONSTRAINT pk_tenant_privilege PRIMARY KEY (id),
  CONSTRAINT fk_tenant_privilege_tenant_role FOREIGN KEY (tenant_role_id) REFERENCES tenant_role (id)
);

CREATE TABLE application_user (
  id  UUID NOT NULL,

  tenant_id UUID NOT NULL,

  username VARCHAR(64) NOT NULL,
  password VARCHAR(64) NOT NULL,

  enabled BOOLEAN NOT NULL,

  CONSTRAINT pk_application_user PRIMARY KEY (id),
  CONSTRAINT fk_application_user_tenant FOREIGN KEY (tenant_id) REFERENCES tenant (id)
);

CREATE TABLE application_user_role (
  id  SERIAL NOT NULL,

  user_id UUID NOT NULL,
  tenant_role_id INT NOT NULL,

  CONSTRAINT pk_application_user_role PRIMARY KEY (id),
  CONSTRAINT fk_application_user_role_user_id FOREIGN KEY (user_id) REFERENCES application_user (id),
  CONSTRAINT fk_application_user_role_tenant_role FOREIGN KEY (tenant_role_id) REFERENCES tenant_role (id)
);
--rollback DROP TABLE tenant
--rollback DROP TABLE tenant_role
--rollback DROP TABLE tenant_privilege
--rollback DROP TABLE application_user
--rollback DROP TABLE application_user_role

--changeset v20172711214300
ALTER TABLE tenant_role ADD COLUMN default_role BOOLEAN NOT NULL DEFAULT false;
--rollback ALTER TABLE tenant_role DROP COLUMN default_role

--changeset v20172711222100
CREATE TABLE application_user_basic_info (
  id  SERIAL NOT NULL,

  user_id UUID NOT NULL,

  first_name VARCHAR(32) NOT NULL,
  middle_name VARCHAR(32),
  last_name VARCHAR(32) NOT NULL,

  CONSTRAINT pk_application_user_basic_info PRIMARY KEY (id),
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES application_user (id)
);
--rollback DROP TABLE application_user_basic_info

--changeset v20171215194000
CREATE TABLE token_revocation (
  token UUID NOT NULL,
  revoked_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  CONSTRAINT pk_token_revocation PRIMARY KEY (token)
)
--rollback DROP TABLE token_revocation
