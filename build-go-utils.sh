#!/usr/bin/env bash

cd infrastructure/go-utils/build
go build -o build-utils
mv build-utils ../../build-utils