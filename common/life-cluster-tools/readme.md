# JDBC lock

Added ability to make a lock using a database. 
This lock can be used cluster wide if your database supports "serializable" isolation level in its transactions.

In order to use it:

* Add the dependency to this library
* Add database migration schema in file etc/init/postgres/lock_database.sql
* enable lock using property "life.cluster.tools.locks.enabled" with value "true"