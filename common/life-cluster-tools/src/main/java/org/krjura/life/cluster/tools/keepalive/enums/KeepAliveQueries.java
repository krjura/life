package org.krjura.life.cluster.tools.keepalive.enums;

public class KeepAliveQueries {

    public static final String QUERY_KEEP_ALIVE_UPDATE =
            "UPDATE cluster_tools_keep_alive SET last_updated = ? WHERE node_id = ?";

    public static final String QUERY_KEEP_ALIVE_LIST =
            "SELECT node_id, last_updated FROM cluster_tools_keep_alive";

    public static final String QUERY_KEEP_ALIVE_INSERT =
            "INSERT INTO cluster_tools_keep_alive (node_id, last_updated) VALUES (?, ?)";

    public static final String QUERY_KEEP_ALIVE_DELETE =
            "DELETE FROM cluster_tools_keep_alive WHERE last_updated < ?";

    public static final String QUERY_KEEP_ALIVE_TRUNCATE = "TRUNCATE cluster_tools_keep_alive";
}
