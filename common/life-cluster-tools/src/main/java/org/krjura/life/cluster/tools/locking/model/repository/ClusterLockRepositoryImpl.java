package org.krjura.life.cluster.tools.locking.model.repository;

import org.krjura.life.cluster.tools.locking.enums.LockingQueries;
import org.krjura.life.cluster.tools.locking.model.ClusterLock;
import org.krjura.life.cluster.tools.utils.RepositoryUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class ClusterLockRepositoryImpl implements ClusterLockRepository {

    private static final Logger logger = LoggerFactory.getLogger(ClusterLockRepositoryImpl.class);

    private static final ResultRowMapper ROW_MAPPER_CLUSTER_LOCK = new ResultRowMapper();

    private final TransactionTemplate transactionTemplate;

    private final JdbcTemplate jdbcTemplate;

    public ClusterLockRepositoryImpl(PlatformTransactionManager transactionManager, JdbcTemplate jdbcTemplate) {
        Objects.requireNonNull(transactionManager);
        Objects.requireNonNull(jdbcTemplate);

        this.transactionTemplate = new TransactionTemplate(transactionManager);
        this.transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Optional<String> saveLock(String lockKey, String nodeId) {
        return transactionTemplate.execute(status -> {

            String clientKey = UUID.randomUUID().toString();
            Timestamp created_date = new Timestamp(System.currentTimeMillis());

            int count = this
                    .jdbcTemplate.update(LockingQueries.LOCK_QUERY, lockKey, clientKey, nodeId, created_date);

            if(count == 0) {
                return Optional.empty();
            }

            logger.trace("Obtained cluster lock for lockKey {} with {}", lockKey, clientKey);
            return Optional.of(clientKey);
        });
    }

    @Override
    public void unlockClusterKey(String clientKey) {
        if (clientKey == null) {
            return;
        }

        transactionTemplate.execute(new TransactionCallbackWithoutResult() {

            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                jdbcTemplate.update(LockingQueries.UNLOCK_QUERY, clientKey);
            }
        });
    }

    @Override
    public List<ClusterLock> findAllLocks() {
        return this.jdbcTemplate.query(LockingQueries.LOCK_READ_ALL_QUERY, new Object[] {}, ROW_MAPPER_CLUSTER_LOCK);
    }

    @Override
    public int deleteObsoleteLocks(long olderThen) {
        ZonedDateTime ts = ZonedDateTime.now().minusSeconds(olderThen);

        int count = 0;
        count += this.jdbcTemplate.update(LockingQueries.DELETE_OBSOLETE_WITH_KEEP_ALIVE, RepositoryUtils.toTimestamp(ts));
        count += this.jdbcTemplate.update(LockingQueries.DELETE_OBSOLETE_NO_KEEP_ALIVE);

        return count;
    }

    @Override
    public void truncateClusterLock() {
        this.jdbcTemplate.update(LockingQueries.TRUNCATE_CLUSTER_LOCK);
    }

    private static class ResultRowMapper implements RowMapper<ClusterLock> {

        @Override
        public ClusterLock mapRow(ResultSet rs, int rowNum) throws SQLException, DataAccessException {

            return new ClusterLock(
                    rs.getString("lock_key"),
                    rs.getString("client_id"),
                    rs.getString("node_id"),
                    RepositoryUtils.toZonedDateTime(rs.getTimestamp("created_date"))
            );
        }
    }
}
