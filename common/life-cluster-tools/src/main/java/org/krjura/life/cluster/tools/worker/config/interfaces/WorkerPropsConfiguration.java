package org.krjura.life.cluster.tools.worker.config.interfaces;

public interface WorkerPropsConfiguration {

    boolean isEnabled();

    RequestQueueConfig getRequestQueue();

    long getCompletedCleanupPeriod();
}