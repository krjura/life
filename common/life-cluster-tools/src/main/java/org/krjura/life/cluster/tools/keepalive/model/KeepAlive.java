package org.krjura.life.cluster.tools.keepalive.model;

import java.time.ZonedDateTime;

public class KeepAlive {

    private String nodeId;

    private ZonedDateTime lastUpdated;

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    // fluent

    public KeepAlive nodeId(final String nodeId) {
        setNodeId(nodeId);
        return this;
    }

    public KeepAlive lastUpdated(final ZonedDateTime lastUpdated) {
        setLastUpdated(lastUpdated);
        return this;
    }
}
