package org.krjura.life.cluster.tools.worker.utils;

import java.time.ZonedDateTime;
import java.util.Objects;

public class RetryPolicy {

    private RetryPolicy() {
        // utils
    }

    public static ZonedDateTime nextExponential(ZonedDateTime timestamp, Integer numberOfRetries) {
        Objects.requireNonNull(timestamp);
        Objects.requireNonNull(numberOfRetries);

        if( numberOfRetries >= 5) {
            return timestamp.plusHours(24);
        }

        double offset = Math.pow(2, numberOfRetries);

        return timestamp.plusHours((long) offset);
    }
}
