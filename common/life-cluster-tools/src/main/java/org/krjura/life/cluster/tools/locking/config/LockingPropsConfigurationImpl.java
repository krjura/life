package org.krjura.life.cluster.tools.locking.config;

import org.krjura.life.cluster.tools.locking.config.interfaces.LockingPropsConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "life.cluster.tools.locks")
public class LockingPropsConfigurationImpl implements LockingPropsConfiguration {

    private long duration = 120; // seconds

    private String deleteObsoleteCron = "0 * * * * ?";

    @Override
    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String getDeleteObsoleteCron() {
        return deleteObsoleteCron;
    }

    public void setDeleteObsoleteCron(String deleteObsoleteCron) {
        this.deleteObsoleteCron = deleteObsoleteCron;
    }
}
