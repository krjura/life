package org.krjura.life.cluster.tools.worker.config.interfaces;

public interface RequestQueueConfig {

    Integer getCorePoolSize();

    Integer getMaximumPoolSize();

    Integer getPoolKeepAlive();

    Integer getQueueSize();

    Integer getDeleteOlderThen();
}
