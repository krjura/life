package org.krjura.life.cluster.tools.worker.enums;

public enum RequestQueueStatus {

    OPEN, ASSIGNED, FAILED, COMPLETED
}
