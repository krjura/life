package org.krjura.life.cluster.tools.worker.config.pojo;

import org.krjura.life.cluster.tools.worker.config.interfaces.RequestQueueConfig;

public class RequestQueueConfigObject implements RequestQueueConfig {

    private Integer corePoolSize = 1;

    private Integer maximumPoolSize = 5;

    private Integer poolKeepAlive = 10; // minutes

    private Integer queueSize = 100;

    private Integer deleteOlderThen = 1; // minutes

    @Override
    public Integer getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(Integer corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    @Override
    public Integer getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(Integer maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    @Override
    public Integer getPoolKeepAlive() {
        return poolKeepAlive;
    }

    public void setPoolKeepAlive(Integer poolKeepAlive) {
        this.poolKeepAlive = poolKeepAlive;
    }

    @Override
    public Integer getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(Integer queueSize) {
        this.queueSize = queueSize;
    }

    @Override
    public Integer getDeleteOlderThen() {
        return deleteOlderThen;
    }

    public void setDeleteOlderThen(Integer deleteOlderThen) {
        this.deleteOlderThen = deleteOlderThen;
    }
}
