package org.krjura.life.cluster.tools.worker.services;

import org.krjura.life.cluster.tools.worker.model.Request;

import java.time.ZonedDateTime;

public interface WorkerRequestQueueService {

    Request addRequest(String type, String data, String metadata, ZonedDateTime executeOn);

    Request addRequest(String type, String data, String metadata);

    void searchForRequests();

    void deleteCompletedRequests();
}
