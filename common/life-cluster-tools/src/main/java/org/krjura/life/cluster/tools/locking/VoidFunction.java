package org.krjura.life.cluster.tools.locking;

public interface VoidFunction {

    void apply() throws Exception;
}
