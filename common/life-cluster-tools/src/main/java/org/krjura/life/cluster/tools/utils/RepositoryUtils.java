package org.krjura.life.cluster.tools.utils;

import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class RepositoryUtils {

    private RepositoryUtils() {
        // util
    }

    public static ZonedDateTime toZonedDateTime(Timestamp timestamp) {
        if(timestamp == null) {
            return null;
        }

        return ZonedDateTime.ofInstant(timestamp.toInstant(), ZoneOffset.UTC);
    }

    public static Timestamp toTimestamp(ZonedDateTime ts) {
        return new Timestamp(ts.toInstant().toEpochMilli());
    }
}
