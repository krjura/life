package org.krjura.life.cluster.tools.worker.ex;

public class WorkerException extends Exception {

    public WorkerException(String s) {
        super(s);
    }

    public WorkerException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
