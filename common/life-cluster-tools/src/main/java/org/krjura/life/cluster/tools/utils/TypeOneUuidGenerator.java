package org.krjura.life.cluster.tools.utils;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;

import java.util.UUID;

public class TypeOneUuidGenerator {

    private static final NoArgGenerator uuidGenerator = Generators.timeBasedGenerator();

    private TypeOneUuidGenerator() {
        // factory
    }

    public static UUID generateId() {
        return uuidGenerator.generate();
    }
}
