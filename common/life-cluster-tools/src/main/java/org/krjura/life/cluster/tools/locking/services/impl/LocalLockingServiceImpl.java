package org.krjura.life.cluster.tools.locking.services.impl;

import org.krjura.life.cluster.tools.ex.LockingException;
import org.krjura.life.cluster.tools.locking.VoidFunction;
import org.krjura.life.cluster.tools.locking.config.interfaces.LockingPropsConfiguration;
import org.krjura.life.cluster.tools.locking.model.repository.ClusterLockRepository;
import org.krjura.life.cluster.tools.locking.services.LocalLockingService;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.Random;

public class LocalLockingServiceImpl implements LocalLockingService {

    private static final Logger logger = LoggerFactory.getLogger(LocalLockingServiceImpl.class);

    private static final long LOCK_WAIT_TIME = 500;

    private final LockingPropsConfiguration lockingPropsConfiguration;

    private final ClusterLockRepository clusterLockRepository;

    private final ClusterToolNodeInfoService nodeInfoService;

    private final Random random;

    public LocalLockingServiceImpl(
            LockingPropsConfiguration lockingPropsConfiguration,
            ClusterLockRepository clusterLockRepository,
            ClusterToolNodeInfoService nodeInfoService) {

        Assert.notNull(lockingPropsConfiguration, "LockingPropsConfiguration cannot be null");
        Assert.notNull(clusterLockRepository, "ClusterLockRepository cannot be null");
        Assert.notNull(nodeInfoService, "ClusterToolNodeInfoService cannot be null");

        this.lockingPropsConfiguration  = lockingPropsConfiguration;
        this.clusterLockRepository = clusterLockRepository;
        this.nodeInfoService = nodeInfoService;

        this.random = new Random();
    }

    public String obtainClusterLock(String lockKey) throws LockingException {

        long lockStartTime = System.currentTimeMillis();

        do {
            try {
                Optional<String> clientKey = this
                        .clusterLockRepository.saveLock(lockKey, this.nodeInfoService.getNodeId());

                // if present return otherwise let it fail
                if(clientKey.isPresent()) {
                    return clientKey.get();
                }
            } catch (Exception e) {
                logger.trace("Unable to obtain cluster lock for {}. Will try again", lockKey);
            }

            // randomize sleep to try to avoid collisions
            sleep(50 + random.nextInt(100));
        } while ((System.currentTimeMillis()) - lockStartTime < LOCK_WAIT_TIME);

        throw new LockingException("unable to obtain cluster lock in " + LOCK_WAIT_TIME);
    }

    @Override
    public void unlockClusterKey(String clientKey) {
        if (clientKey == null) {
            return;
        }

        this.clusterLockRepository.unlockClusterKey(clientKey);
    }

    @Scheduled(cron = "${life.cluster.tools.locks.deleteObsoleteCron}" )
    public void deleteObsoleteLocks() {
        int count = this.clusterLockRepository.deleteObsoleteLocks(this.lockingPropsConfiguration.getDuration());

        if(logger.isDebugEnabled()) {
            logger.debug("deleted {} obsolete records", count);
        }
    }

    private void sleep(long duration) {
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            logger.trace("interrupted from sleep", e);
        }
    }

    @Override
    public void doWithLock(String lockKey, VoidFunction function) throws Exception {
        String clientLockKey = obtainClusterLock(lockKey);

        try {
            function.apply();
        } finally {
            unlockClusterKey(clientLockKey);
        }
    }
}
