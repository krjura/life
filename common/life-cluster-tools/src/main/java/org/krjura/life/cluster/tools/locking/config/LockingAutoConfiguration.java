package org.krjura.life.cluster.tools.locking.config;

import org.krjura.life.cluster.tools.locking.config.interfaces.LockingPropsConfiguration;
import org.krjura.life.cluster.tools.locking.model.repository.ClusterLockRepository;
import org.krjura.life.cluster.tools.locking.model.repository.ClusterLockRepositoryImpl;
import org.krjura.life.cluster.tools.locking.services.LocalLockingService;
import org.krjura.life.cluster.tools.locking.services.impl.LocalLockingServiceImpl;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@ConditionalOnProperty(value = "life.cluster.tools.locks.enabled", havingValue = "true")
@ConditionalOnClass({Environment.class, JdbcTemplate.class})
public class LockingAutoConfiguration {

    @Bean
    public LockingPropsConfiguration lockingPropsConfiguration() {
        return new LockingPropsConfigurationImpl();
    }

    @Bean
    public ClusterLockRepository clusterLockRepository(
            PlatformTransactionManager transactionManager, JdbcTemplate jdbcTemplate) {

        return new ClusterLockRepositoryImpl(transactionManager, jdbcTemplate);
    }

    @Bean
    public LocalLockingService localLockingService(
            LockingPropsConfiguration lockingPropsConfiguration,
            ClusterLockRepository clusterLockRepository,
            ClusterToolNodeInfoService nodeInfoService) {

        return new LocalLockingServiceImpl(lockingPropsConfiguration, clusterLockRepository, nodeInfoService);
    }
}