package org.krjura.life.cluster.tools.worker.model;

import org.krjura.life.cluster.tools.worker.enums.RequestQueueStatus;

import java.time.ZonedDateTime;
import java.util.UUID;

public class Request {

    private UUID id;

    private String requestType;

    private byte[] requestData;

    private byte[] requestMetadata;

    private String nodeId;

    private ZonedDateTime assignedOn;

    private RequestQueueStatus state;

    private ZonedDateTime executeOn;

    private Integer retries;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public byte[] getRequestData() {
        return requestData;
    }

    public void setRequestData(byte[] requestData) {
        this.requestData = requestData;
    }

    public byte[] getRequestMetadata() {
        return requestMetadata;
    }

    public void setRequestMetadata(byte[] requestMetadata) {
        this.requestMetadata = requestMetadata;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public ZonedDateTime getAssignedOn() {
        return assignedOn;
    }

    public void setAssignedOn(ZonedDateTime assignedOn) {
        this.assignedOn = assignedOn;
    }

    public RequestQueueStatus getState() {
        return state;
    }

    public void setState(RequestQueueStatus state) {
        this.state = state;
    }

    public ZonedDateTime getExecuteOn() {
        return executeOn;
    }

    public void setExecuteOn(ZonedDateTime execute_on) {
        this.executeOn = execute_on;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    // fluent

    public Request id(final UUID id) {
        setId(id);
        return this;
    }

    public Request requestType(final String requestType) {
        setRequestType(requestType);
        return this;
    }

    public Request requestData(final byte[] requestData) {
        setRequestData(requestData);
        return this;
    }

    public Request requestMetadata(final byte[] requestMetadata) {
        setRequestMetadata(requestMetadata);
        return this;
    }

    public Request nodeId(final String nodeId) {
        setNodeId(nodeId);
        return this;
    }

    public Request assignedOn(final ZonedDateTime assignedOn) {
        setAssignedOn(assignedOn);
        return this;
    }

    public Request state(final RequestQueueStatus state) {
        setState(state);
        return this;
    }

    public Request executeOn(final ZonedDateTime executeOn) {
        setExecuteOn(executeOn);
        return this;
    }

    public Request retries(final Integer retries) {
        setRetries(retries);
        return this;
    }
}
