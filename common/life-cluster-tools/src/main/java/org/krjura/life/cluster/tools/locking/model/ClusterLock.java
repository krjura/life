package org.krjura.life.cluster.tools.locking.model;

import java.time.ZonedDateTime;

public class ClusterLock {

    private final String lockKey;

    private final String clientId;

    private final String nodeId;

    private ZonedDateTime createdDate;

    public ClusterLock(String lockKey, String clientId, String nodeId, ZonedDateTime createdDate) {
        this.lockKey = lockKey;
        this.clientId = clientId;
        this.nodeId = nodeId;
        this.createdDate = createdDate;
    }

    public String getLockKey() {
        return lockKey;
    }

    public String getClientId() {
        return clientId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }
}