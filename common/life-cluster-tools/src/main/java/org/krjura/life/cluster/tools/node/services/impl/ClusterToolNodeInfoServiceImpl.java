package org.krjura.life.cluster.tools.node.services.impl;

import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.UUID;

public class ClusterToolNodeInfoServiceImpl implements ClusterToolNodeInfoService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(ClusterToolNodeInfoServiceImpl.class);

    private final Environment environment;

    private String nodeId;

    public ClusterToolNodeInfoServiceImpl(Environment environment) {
        Objects.requireNonNull(environment);

        this.environment = environment;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String applicationName = environment.getProperty("spring.application.name", "anonymous");
        String hostname = getHostName();
        String port = environment.getProperty("server.port", "-1");

        this.nodeId = applicationName + "@" + hostname + ":" + port + "@" + UUID.randomUUID().toString();

        logger.info("node id is {}", this.nodeId);
    }

    private String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch ( UnknownHostException e) {
            return "localhost";
        }
    }

    @Override
    public String getNodeId() {
        return nodeId;
    }
}
