package org.krjura.life.cluster.tools.worker.services.impl;

import org.krjura.life.cluster.tools.locking.services.LocalLockingService;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.krjura.life.cluster.tools.worker.config.interfaces.WorkerPropsConfiguration;
import org.krjura.life.cluster.tools.worker.enums.RequestExecutionState;
import org.krjura.life.cluster.tools.worker.ex.WorkerException;
import org.krjura.life.cluster.tools.worker.interfaces.WorkerTaskProcessor;
import org.krjura.life.cluster.tools.worker.model.Request;
import org.krjura.life.cluster.tools.worker.services.WorkerRepository;
import org.krjura.life.cluster.tools.worker.services.WorkerRequestQueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.Assert;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WorkerRequestQueueServiceImpl implements WorkerRequestQueueService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(WorkerRequestQueueServiceImpl.class);

    private static final String LOCK_KEY = "LOCK_WORKER_REQUEST_QUEUE";

    private final WorkerPropsConfiguration configuration;

    private final WorkerRepository workerRepository;

    private final LocalLockingService lockingService;

    private final ClusterToolNodeInfoService nodeInfoService;

    private final List<WorkerTaskProcessor> processors;

    private final LinkedBlockingQueue<Runnable> queue;

    private final ExecutorService executorService;

    private Map<String, WorkerTaskProcessor> processorMap;

    public WorkerRequestQueueServiceImpl(
            WorkerPropsConfiguration configuration,
            WorkerRepository workerRepository,
            LocalLockingService lockingService,
            ClusterToolNodeInfoService nodeInfoService,
            List<WorkerTaskProcessor> processors) {

        Assert.notNull(configuration, "NodePropsConfiguration cannot be null");
        Assert.notNull(workerRepository, "WorkerRepository cannot be null");
        Assert.notNull(lockingService, "LocalLockingService cannot be null");
        Assert.notNull(processors, "WorkerTaskProcessor cannot be null");

        this.configuration = configuration;
        this.workerRepository = workerRepository;
        this.nodeInfoService = nodeInfoService;
        this.lockingService = lockingService;
        this.processors = processors;

        this.queue = new LinkedBlockingQueue<>();
        this.executorService = setupExecutorService();
        this.processorMap = new HashMap<>();
    }

    private ThreadPoolExecutor setupExecutorService() {
        return new ThreadPoolExecutor(
                this.configuration.getRequestQueue().getCorePoolSize(),
                this.configuration.getRequestQueue().getMaximumPoolSize(),
                this.configuration.getRequestQueue().getPoolKeepAlive(),
                TimeUnit.MINUTES, queue);
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        for( WorkerTaskProcessor processor : this.processors ) {

            if( processor.supports() == null) {
                continue;
            }

            for(String type : processor.supports()) {

                if(this.processorMap.containsKey(type)) {
                    throw new WorkerException("type " + type + " already set");
                }

                this.processorMap.put(type, processor);
            }
        }
    }

    @Override
    public Request addRequest(String type, String data, String metadata, ZonedDateTime executeOn) {
        return this.workerRepository.addRequest(type, data.getBytes(), metadata.getBytes(), executeOn);
    }

    @Override
    public Request addRequest(String type, String data, String metadata) {
        return addRequest(type, data, metadata, ZonedDateTime.now());
    }

    @Scheduled(fixedRate = 60000)
    public void deleteCompletedRequests() {

        try {
            ZonedDateTime when = ZonedDateTime.now().minusMinutes(this.configuration.getCompletedCleanupPeriod());

            int count = this.workerRepository.deletedCompletedRequests(when);

            logger.info("deleted {} messages older then {}", count, when);
        } catch (Exception e) {
            logger.warn("cannot deleted old records", e);
        }
    }

    @Scheduled(fixedRate = 1000, initialDelay = 20000)
    public void searchForRequests() {
        if (this.queue.size() == 0) {
            logger.trace("queue is empty. Will try to load more");
        } else {
            logger.trace("queue has {} entries. Waiting until it is empty", queue.size());
            return;
        }

        try {
            this.lockingService.doWithLock(LOCK_KEY, this::assignNewTasks);
        } catch (Exception e) {
            logger.warn("Cannot assign new tasks", e);
        }
    }

    private void assignNewTasks() {
        String nodeId = this.nodeInfoService.getNodeId();

        int limit = this.configuration.getRequestQueue().getQueueSize();
        List<Request> messages = workerRepository.findOpenNotAssignedRequests(limit, nodeId);

        logger.trace("found {} messages in need of sending", messages.size());

        messages.forEach(task -> {
            this.executorService.submit(() -> {
                try {
                    WorkerTaskProcessor processor = processorMap.get(task.getRequestType());

                    if( processor == null ) {
                        logger.warn("cannot find worker processor for task of type {}", task.getRequestType());
                        return;
                    }

                    RequestExecutionState result = processor.execute(task);

                    switch (result) {
                        case COMPLETED:
                            workerRepository.markRequestAsCompleted(task);
                            break;
                        case FAILED_PERMANENT:
                            workerRepository.markRequestAsFailed(task);
                            break;
                        case FAILED_RETRY_POSSIBLE:
                            workerRepository.markRequestAsAttempted(task);
                            break;
                        default:
                            logger.warn("unknown worker request execution state {}", result);
                    }

                    logger.debug("task with id of {} completed with result {}", task.getId(), result);
                } catch ( Exception e) {
                    logger.warn("task execution failed", e);
                }
            });
        });
    }
}
