package org.krjura.life.cluster.tools.keepalive.config;

import org.krjura.life.cluster.tools.keepalive.services.KeepAliveService;
import org.krjura.life.cluster.tools.keepalive.config.interfaces.KeepAlivePropsConfiguration;
import org.krjura.life.cluster.tools.keepalive.model.repository.KeepAliveRepository;
import org.krjura.life.cluster.tools.keepalive.model.repository.impl.KeepAliveRepositoryImpl;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.ScheduledExecutorService;

@Configuration
public class KeepAliveAutoConfiguration {

    @Bean
    public KeepAlivePropsConfiguration keepAlivePropsConfiguration() {
        return new KeepAlivePropsConfigurationImpl();
    }

    @Bean
    public KeepAliveRepository keepAliveRepository(
            JdbcTemplate jdbcTemplate, ClusterToolNodeInfoService nodeInfoService) {

        return new KeepAliveRepositoryImpl(jdbcTemplate, nodeInfoService);
    }

    @Bean
    public KeepAliveService keepAliveService(
            KeepAlivePropsConfiguration configuration,
            KeepAliveRepository keepAliveRepository,
            ScheduledExecutorService scheduledExecutorService) {

        return new KeepAliveService(configuration, keepAliveRepository, scheduledExecutorService);
    }
}
