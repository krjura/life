package org.krjura.life.cluster.tools.keepalive.model.repository;

import org.krjura.life.cluster.tools.keepalive.model.KeepAlive;

import java.time.ZonedDateTime;
import java.util.List;

public interface KeepAliveRepository {

    void keepAlive();

    List<KeepAlive> findAllKeepAlives();

    void addKeepAlive(String node, ZonedDateTime before);

    void deleteObsoleteNodeIds(long olderThen);

    void truncateKeepAlive();
}
