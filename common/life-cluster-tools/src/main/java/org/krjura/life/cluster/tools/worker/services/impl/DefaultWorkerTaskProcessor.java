package org.krjura.life.cluster.tools.worker.services.impl;

import org.krjura.life.cluster.tools.worker.enums.RequestExecutionState;
import org.krjura.life.cluster.tools.worker.interfaces.WorkerTaskProcessor;
import org.krjura.life.cluster.tools.worker.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class DefaultWorkerTaskProcessor implements WorkerTaskProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DefaultWorkerTaskProcessor.class);

    private static final String DEFAULT = "DEFAULT";

    @Override
    public List<String> supports() {
        return Collections.singletonList(DEFAULT);
    }

    @Override
    public RequestExecutionState execute(Request request) {
        logger.info("completed default task with id of {}", request.getId());
        return RequestExecutionState.COMPLETED;
    }
}
