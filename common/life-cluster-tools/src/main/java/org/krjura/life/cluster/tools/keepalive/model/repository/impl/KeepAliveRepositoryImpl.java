package org.krjura.life.cluster.tools.keepalive.model.repository.impl;

import org.krjura.life.cluster.tools.keepalive.enums.KeepAliveQueries;
import org.krjura.life.cluster.tools.keepalive.model.KeepAlive;
import org.krjura.life.cluster.tools.keepalive.model.repository.KeepAliveRepository;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.krjura.life.cluster.tools.utils.RepositoryUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class KeepAliveRepositoryImpl implements KeepAliveRepository {

    private static final Logger logger = LoggerFactory.getLogger(KeepAliveRepositoryImpl.class);

    private static final KeepAliveRowMapper KEEP_ALIVE_ROW_MAPPER = new KeepAliveRowMapper();

    private final JdbcTemplate jdbcTemplate;

    private final ClusterToolNodeInfoService nodeInfoService;

    public KeepAliveRepositoryImpl(JdbcTemplate jdbcTemplate, ClusterToolNodeInfoService nodeInfoService) {
        Objects.requireNonNull(jdbcTemplate);
        Objects.requireNonNull(nodeInfoService);

        this.jdbcTemplate = jdbcTemplate;
        this.nodeInfoService = nodeInfoService;
    }

    @Override
    public void keepAlive() {
        String nodeId = this.nodeInfoService.getNodeId();

        logger.info("updating keep alive for node {}", nodeId);

        Timestamp lastUpdated = new Timestamp(ZonedDateTime.now().toInstant().toEpochMilli());

        int count = this.jdbcTemplate.update(KeepAliveQueries.QUERY_KEEP_ALIVE_UPDATE, lastUpdated, nodeId);

        if(count == 0) {
            logger.info("nodeId not defined. Using insert instead");

            this.jdbcTemplate.update(KeepAliveQueries.QUERY_KEEP_ALIVE_INSERT, nodeId, lastUpdated);
        }
    }

    @Override
    public List<KeepAlive> findAllKeepAlives() {
        return this.jdbcTemplate.query(
                KeepAliveQueries.QUERY_KEEP_ALIVE_LIST,
                new Object[] {},
                KEEP_ALIVE_ROW_MAPPER
        );
    }

    @Override
    public void addKeepAlive(String node, ZonedDateTime before) {
        Timestamp lastUpdated = new Timestamp(before.toInstant().toEpochMilli());

        this.jdbcTemplate.update(KeepAliveQueries.QUERY_KEEP_ALIVE_INSERT, node, lastUpdated);
    }

    @Override
    public void deleteObsoleteNodeIds(long olderThen) {
        ZonedDateTime period = ZonedDateTime.now().minusSeconds(olderThen);

        int count = this
                .jdbcTemplate
                .update(KeepAliveQueries.QUERY_KEEP_ALIVE_DELETE, RepositoryUtils.toTimestamp(period));

        logger.info("found {} obsolete nodeId records", count);
    }

    @Override
    public void truncateKeepAlive() {
        this.jdbcTemplate.update(KeepAliveQueries.QUERY_KEEP_ALIVE_TRUNCATE);
    }

    private static class KeepAliveRowMapper implements RowMapper<KeepAlive> {

        @Override
        public KeepAlive mapRow(ResultSet rs, int rowNum) throws SQLException, DataAccessException {
            return new KeepAlive()
                    .nodeId(rs.getString("node_id"))
                    .lastUpdated(RepositoryUtils.toZonedDateTime(rs.getTimestamp("last_updated")));
        }
    }

}
