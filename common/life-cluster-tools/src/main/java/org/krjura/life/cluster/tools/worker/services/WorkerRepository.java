package org.krjura.life.cluster.tools.worker.services;

import org.krjura.life.cluster.tools.worker.model.Request;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

public interface WorkerRepository {

    Request addRequest(String requestType, byte[] requestData, byte[] requestMetadata, ZonedDateTime executeOn);

    Request getRequest(UUID id);

    void truncateClusterRequestQueue();

    List<Request> findOpenNotAssignedRequests(int limit, String nodeId);

    int deletedCompletedRequests(ZonedDateTime when);

    void markRequestAsCompleted(Request request);

    void markRequestAsFailed(Request request);

    void markRequestAsAttempted(Request request);

    List<Request> findAllRequests();
}
