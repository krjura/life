package org.krjura.life.cluster.tools.locking.model.repository;

import org.krjura.life.cluster.tools.locking.model.ClusterLock;

import java.util.List;
import java.util.Optional;

public interface ClusterLockRepository {

    Optional<String> saveLock(String lockKey, String nodeId);

    void unlockClusterKey(String clientKey);

    List<ClusterLock> findAllLocks();

    int deleteObsoleteLocks(long olderThen);

    void truncateClusterLock(); // test only
}
