package org.krjura.life.cluster.tools.worker.config;

import org.krjura.life.cluster.tools.worker.config.interfaces.WorkerPropsConfiguration;
import org.krjura.life.cluster.tools.worker.config.interfaces.RequestQueueConfig;
import org.krjura.life.cluster.tools.worker.config.pojo.RequestQueueConfigObject;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "life.cluster.tools.workers")
public class WorkerPropsConfigurationImpl implements WorkerPropsConfiguration {

    private boolean enabled;

    private long completedCleanupPeriod = 60; // minutes

    private RequestQueueConfig requestQueue;

    public WorkerPropsConfigurationImpl() {
        this.requestQueue = new RequestQueueConfigObject();
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public RequestQueueConfig getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(RequestQueueConfigObject requestQueue) {
        this.requestQueue = requestQueue;
    }

    @Override
    public long getCompletedCleanupPeriod() {
        return completedCleanupPeriod;
    }

    public void setCompletedCleanupPeriod(long completedCleanupPeriod) {
        this.completedCleanupPeriod = completedCleanupPeriod;
    }
}