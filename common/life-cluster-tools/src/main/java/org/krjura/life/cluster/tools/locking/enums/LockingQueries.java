package org.krjura.life.cluster.tools.locking.enums;

public class LockingQueries {

    private LockingQueries() {
        // enum like
    }

    public static final String LOCK_QUERY =
            "INSERT INTO cluster_lock (lock_key, client_id, node_id, created_date) VALUES (?, ?, ?, ?) ON CONFLICT DO NOTHING";

    public static final String LOCK_READ_ALL_QUERY =
            "SELECT lock_key, client_id, node_id, created_date FROM cluster_lock ";

    public static final String UNLOCK_QUERY = "DELETE FROM cluster_lock WHERE client_id = ?";

    public static final String DELETE_OBSOLETE_WITH_KEEP_ALIVE =
            "DELETE FROM cluster_lock cl WHERE EXISTS " +
                    "(SELECT node_id FROM cluster_tools_keep_alive ka WHERE ka.node_id = cl.node_id AND ka.last_updated < ?)";

    public static final String DELETE_OBSOLETE_NO_KEEP_ALIVE =
            "DELETE FROM cluster_lock cl WHERE NOT EXISTS " +
                    "(SELECT node_id FROM cluster_tools_keep_alive ka WHERE ka.node_id = cl.node_id)";

    public static final String TRUNCATE_CLUSTER_LOCK = "TRUNCATE cluster_lock";
}
