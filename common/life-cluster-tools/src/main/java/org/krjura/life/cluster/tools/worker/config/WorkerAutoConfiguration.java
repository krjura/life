package org.krjura.life.cluster.tools.worker.config;

import org.krjura.life.cluster.tools.locking.services.LocalLockingService;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.krjura.life.cluster.tools.worker.config.interfaces.WorkerPropsConfiguration;
import org.krjura.life.cluster.tools.worker.enums.WorkerQualifiers;
import org.krjura.life.cluster.tools.worker.interfaces.WorkerTaskProcessor;
import org.krjura.life.cluster.tools.worker.services.WorkerRepository;
import org.krjura.life.cluster.tools.worker.services.WorkerRequestQueueService;
import org.krjura.life.cluster.tools.worker.services.impl.DefaultWorkerTaskProcessor;
import org.krjura.life.cluster.tools.worker.services.impl.WorkerRepositoryImpl;
import org.krjura.life.cluster.tools.worker.services.impl.WorkerRequestQueueServiceImpl;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@ConditionalOnProperty(value = "life.cluster.tools.workers.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({Environment.class, JdbcTemplate.class})
public class WorkerAutoConfiguration {

    @Bean
    public WorkerPropsConfigurationImpl nodePropsConfiguration() {
        return new WorkerPropsConfigurationImpl();
    }

    @Bean
    public WorkerRepository workerRepository(
            JdbcTemplate jdbcTemplate,
            WorkerPropsConfiguration configuration,
            ClusterToolNodeInfoService nodeInfoService) {

        return new WorkerRepositoryImpl(configuration, jdbcTemplate, nodeInfoService);
    }

    @Bean(destroyMethod = "shutdown")
    @Qualifier(WorkerQualifiers.SPRING_QUALIFIER)
    public ScheduledExecutorService workerScheduledExecutorService() {
        return Executors.newScheduledThreadPool(2);
    }

    @Bean
    public WorkerTaskProcessor defaultWorkerTaskProcessor() {
        return new DefaultWorkerTaskProcessor();
    }

    @Bean
    public WorkerRequestQueueService workerRequestQueueService(
            WorkerPropsConfiguration configuration,
            WorkerRepository workerRepository,
            LocalLockingService lockingService,
            ClusterToolNodeInfoService nodeInfoService,
            List<WorkerTaskProcessor> processors) {

        return new WorkerRequestQueueServiceImpl(
                configuration, workerRepository, lockingService, nodeInfoService, processors);
    }
}