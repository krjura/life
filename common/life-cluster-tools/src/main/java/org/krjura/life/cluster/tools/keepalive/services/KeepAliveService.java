package org.krjura.life.cluster.tools.keepalive.services;

import org.krjura.life.cluster.tools.keepalive.config.interfaces.KeepAlivePropsConfiguration;
import org.krjura.life.cluster.tools.keepalive.model.repository.KeepAliveRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class KeepAliveService {

    private static final Logger logger = LoggerFactory.getLogger(KeepAliveService.class);

    private final KeepAlivePropsConfiguration configuration;

    private final KeepAliveRepository keepAliveRepository;

    private final ScheduledExecutorService scheduledExecutorService;

    public KeepAliveService(
            KeepAlivePropsConfiguration configuration,
            KeepAliveRepository keepAliveRepository,
            ScheduledExecutorService scheduledExecutorService) {

        Assert.notNull(configuration, "KeepAlivePropsConfiguration cannot be null");
        Assert.notNull(keepAliveRepository, "KeepAliveRepository cannot be null");
        Assert.notNull(scheduledExecutorService, "ScheduledExecutorService cannot be null");

        this.configuration = configuration;
        this.keepAliveRepository = keepAliveRepository;
        this.scheduledExecutorService = scheduledExecutorService;

        schedule();
    }

    private void schedule() {
        this
                .scheduledExecutorService
                .scheduleAtFixedRate(
                        this::runKeepAlive,
                        this.configuration.getScheduleDelay(),
                        this.configuration.getKeepAlivePeriod(),
                        TimeUnit.SECONDS);

        this
                .scheduledExecutorService
                .scheduleAtFixedRate(
                        this::deleteObsoleteNodeIds,
                        this.configuration.getScheduleDelay(),
                        this.configuration.getObsoleteSchedulingPeriod(),
                        TimeUnit.SECONDS);
    }

    public void deleteObsoleteNodeIds() {
        try {
            this.keepAliveRepository.deleteObsoleteNodeIds(this.configuration.getObsoleteDeletePeriod());
        } catch ( Exception e) {
            logger.warn("Failure while executing deleteObsoleteNodeIds", e);
        }
    }

    public void runKeepAlive() {
        try {
            this.keepAliveRepository.keepAlive();
        } catch ( Exception e) {
            logger.warn("Failure while executing keep alive", e);
        }
    }
}
