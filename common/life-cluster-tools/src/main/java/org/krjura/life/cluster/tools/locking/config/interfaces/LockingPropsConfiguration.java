package org.krjura.life.cluster.tools.locking.config.interfaces;

public interface LockingPropsConfiguration {

    long getDuration();

    String getDeleteObsoleteCron();
}
