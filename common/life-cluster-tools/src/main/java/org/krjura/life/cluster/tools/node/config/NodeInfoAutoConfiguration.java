package org.krjura.life.cluster.tools.node.config;

import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.krjura.life.cluster.tools.node.services.impl.ClusterToolNodeInfoServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@ConditionalOnClass({Environment.class})
public class NodeInfoAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(ClusterToolNodeInfoService.class)
    public ClusterToolNodeInfoService clusterToolNodeInfoService(Environment environment) {
        return new ClusterToolNodeInfoServiceImpl(environment);
    }
}