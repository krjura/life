package org.krjura.life.cluster.tools.worker.enums;

public class WorkerQueries {

    public static final String QUERY_REQUEST_INSERT =
            "INSERT INTO cluster_tools_request_queue " +
                    "(id, request_type, request_data, request_metadata, state, execute_on, retries ) " +
                    "VALUES ( ?, ?, ?, ?, ?, ?, ? )";

    public static final String QUERY_REQUEST_SELECT = "SELECT * FROM cluster_tools_request_queue WHERE id = ?";

    public static final String QUERY_REQUEST_FIND_OPEN =
            "SELECT id, request_type, request_data, request_metadata, node_id, assigned_on, state, execute_on, retries " +
                    "FROM cluster_tools_request_queue " +
                    "WHERE node_id is NULL and state = 'OPEN' AND execute_on < ? LIMIT ?";

    public static final String QUERY_REQUEST_MARK = "UPDATE cluster_tools_request_queue " +
            "SET node_id = NULL, assigned_on = NULL, state = ?, execute_on = ?, retries = ? " +
            "WHERE id = ?";

    public static final String QUERY_DELETE_COMPLETED = "DELETE FROM cluster_tools_request_queue " +
            "WHERE state IN ('COMPLETED', 'FAILED' ) AND execute_on < ?";

    public static final String QUERY_REQUEST_ALL =
            "SELECT id, request_type, request_data, request_metadata, node_id, assigned_on, state, execute_on, retries " +
            "FROM cluster_tools_request_queue ";

    public static final String QUERY_TRUNCATE_REQUEST_QUEUE = "TRUNCATE cluster_tools_request_queue";
}
