package org.krjura.life.cluster.tools.locking.services;

import org.krjura.life.cluster.tools.ex.LockingException;
import org.krjura.life.cluster.tools.locking.VoidFunction;

public interface LocalLockingService {

    String obtainClusterLock(String lockKey) throws LockingException;

    void unlockClusterKey(String clientKey);

    void doWithLock(String lockKey, VoidFunction function) throws Exception;

    void deleteObsoleteLocks();
}
