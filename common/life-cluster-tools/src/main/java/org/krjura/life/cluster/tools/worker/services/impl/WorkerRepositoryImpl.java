package org.krjura.life.cluster.tools.worker.services.impl;

import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.krjura.life.cluster.tools.utils.TypeOneUuidGenerator;
import org.krjura.life.cluster.tools.worker.config.interfaces.WorkerPropsConfiguration;
import org.krjura.life.cluster.tools.worker.enums.RequestQueueStatus;
import org.krjura.life.cluster.tools.worker.enums.WorkerQueries;
import org.krjura.life.cluster.tools.worker.model.Request;
import org.krjura.life.cluster.tools.worker.services.WorkerRepository;
import org.krjura.life.cluster.tools.utils.RepositoryUtils;
import org.krjura.life.cluster.tools.worker.utils.RetryPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class WorkerRepositoryImpl implements WorkerRepository {

    private static final Logger logger = LoggerFactory.getLogger(WorkerRepositoryImpl.class);

    private static final ResultRowMapper RESULT_ROW_MAPPER = new ResultRowMapper();

    private final JdbcTemplate jdbcTemplate;

    public WorkerRepositoryImpl(
            WorkerPropsConfiguration configuration,
            JdbcTemplate jdbcTemplate,
            ClusterToolNodeInfoService nodeInfoService) {

        Objects.requireNonNull(configuration);
        Objects.requireNonNull(jdbcTemplate);
        Objects.requireNonNull(nodeInfoService);

        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Request addRequest(String requestType, byte[] requestData, byte[] requestMetadata, ZonedDateTime when) {
        UUID id = TypeOneUuidGenerator.generateId();
        Timestamp executeOn = new Timestamp(when.toInstant().toEpochMilli());

        this.jdbcTemplate.update(
                WorkerQueries.QUERY_REQUEST_INSERT,
                id, requestType, requestData, requestMetadata, RequestQueueStatus.OPEN.name(), executeOn, 0);

        return getRequest(id);
    }

    @Override
    public Request getRequest(UUID id) {
        return this.jdbcTemplate.queryForObject(
                WorkerQueries.QUERY_REQUEST_SELECT,
                new Object[] {id},
                RESULT_ROW_MAPPER);
    }

    @Override
    public void truncateClusterRequestQueue() {
        this.jdbcTemplate.update(WorkerQueries.QUERY_TRUNCATE_REQUEST_QUEUE);
    }

    @Override
    public List<Request> findOpenNotAssignedRequests(int limit, String nodeId) {
        Timestamp now = new Timestamp(ZonedDateTime.now().toInstant().toEpochMilli());

        List<Request> requests = this.jdbcTemplate.query(
                WorkerQueries.QUERY_REQUEST_FIND_OPEN, new Object[] {now, limit}, RESULT_ROW_MAPPER);

        if( requests.size() == 0) {
            return Collections.emptyList();
        }

        List<String> placeholders = requests.stream().map(request -> "?").collect(Collectors.toList());
        String inClause = String.join(",", placeholders);

        String assignQuery = "UPDATE cluster_tools_request_queue " +
                "SET node_id = ?, state = ?, assigned_on = ? WHERE id IN (" + inClause + ")";

        Object[] params = new Object[requests.size() + 3];
        params[0] = nodeId;
        params[1] = RequestQueueStatus.ASSIGNED.name();
        params[2] = now;

        for( int i = 0; i < requests.size(); i++) {
            params[i+3] = requests.get(i).getId();
        }

        this.jdbcTemplate.update(assignQuery, params);

        return requests;
    }

    @Override
    public int deletedCompletedRequests(ZonedDateTime when) {
        Timestamp timestamp = new Timestamp(when.toInstant().toEpochMilli());

        return this.jdbcTemplate.update(WorkerQueries.QUERY_DELETE_COMPLETED, timestamp);
    }

    @Override
    public void markRequestAsCompleted(Request request) {
        RequestQueueStatus status = RequestQueueStatus.COMPLETED;
        Timestamp executeOn = new Timestamp(request.getExecuteOn().toInstant().toEpochMilli());
        Integer retries = request.getRetries();
        UUID id = request.getId();

        this.jdbcTemplate.update(WorkerQueries.QUERY_REQUEST_MARK, status.name(), executeOn, retries, id);
    }

    @Override
    public void markRequestAsFailed(Request request) {
        RequestQueueStatus status = RequestQueueStatus.FAILED;
        Timestamp executeOn = new Timestamp(request.getExecuteOn().toInstant().toEpochMilli());
        Integer retries = request.getRetries() + 1;
        UUID id = request.getId();

        this.jdbcTemplate.update(WorkerQueries.QUERY_REQUEST_MARK, status.name(), executeOn, retries, id);
    }

    @Override
    public void markRequestAsAttempted(Request request) {
        RequestQueueStatus status = RequestQueueStatus.OPEN;
        ZonedDateTime retryTime = RetryPolicy.nextExponential(request.getExecuteOn(), request.getRetries());
        Timestamp executeOn = new Timestamp(retryTime.toInstant().toEpochMilli());
        Integer retries = request.getRetries() + 1;
        UUID id = request.getId();

        this.jdbcTemplate.update(WorkerQueries.QUERY_REQUEST_MARK, status.name(), executeOn, retries, id);
    }

    @Override
    public List<Request> findAllRequests() {
        return this.jdbcTemplate.query(WorkerQueries.QUERY_REQUEST_ALL, new Object[] {}, RESULT_ROW_MAPPER);
    }

    private static class ResultRowMapper implements RowMapper<Request> {

        @Override
        public Request mapRow(ResultSet rs, int rowNum) throws SQLException, DataAccessException {
           return new Request()
                    .id((UUID) rs.getObject("id"))
                    .requestType(rs.getString("request_type"))
                    .requestData(rs.getBytes("request_data"))
                    .requestMetadata(rs.getBytes("request_metadata"))
                    .nodeId(rs.getString("node_id"))
                    .assignedOn(RepositoryUtils.toZonedDateTime(rs.getTimestamp("assigned_on")))
                    .state(toRequestQueueStatus(rs.getString( "state")))
                    .executeOn(RepositoryUtils.toZonedDateTime(rs.getTimestamp("execute_on")))
                    .retries(rs.getInt("retries"));
        }
    }

    private static RequestQueueStatus toRequestQueueStatus(String status) {
        return status == null ? null : RequestQueueStatus.valueOf(status);
    }
}