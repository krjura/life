package org.krjura.life.cluster.tools.ex;

public class LockingException extends Exception {

    public LockingException(String message) {
        super(message);
    }

    public LockingException(String message, Throwable cause) {
        super(message, cause);
    }
}


