package org.krjura.life.cluster.tools.worker.enums;

public enum RequestExecutionState {

    COMPLETED, FAILED_RETRY_POSSIBLE, FAILED_PERMANENT
}
