package org.krjura.life.cluster.tools.worker.interfaces;

import org.krjura.life.cluster.tools.worker.enums.RequestExecutionState;
import org.krjura.life.cluster.tools.worker.model.Request;

import java.util.List;

public interface WorkerTaskProcessor {

    List<String> supports();

    RequestExecutionState execute(Request request);
}
