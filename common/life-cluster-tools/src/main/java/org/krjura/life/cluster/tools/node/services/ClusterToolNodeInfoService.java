package org.krjura.life.cluster.tools.node.services;

public interface ClusterToolNodeInfoService {

    String getNodeId();
}
