package org.krjura.life.cluster.tools.keepalive.config;

import org.krjura.life.cluster.tools.keepalive.config.interfaces.KeepAlivePropsConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "life.cluster.tools.keep-alive")
public class KeepAlivePropsConfigurationImpl implements KeepAlivePropsConfiguration {

    private long keepAlivePeriod = 30; // seconds

    private long scheduleDelay = 20; // seconds

    private long obsoleteSchedulingPeriod = 30; // seconds

    private long obsoleteDeletePeriod = 120; // 30s for keep alive if server is not alive for 120 delete

    @Override
    public long getKeepAlivePeriod() {
        return keepAlivePeriod;
    }

    public void setKeepAlivePeriod(long keepAlivePeriod) {
        this.keepAlivePeriod = keepAlivePeriod;
    }

    @Override
    public long getScheduleDelay() {
        return scheduleDelay;
    }

    public void setScheduleDelay(long scheduleDelay) {
        this.scheduleDelay = scheduleDelay;
    }

    @Override
    public long getObsoleteSchedulingPeriod() {
        return obsoleteSchedulingPeriod;
    }

    public void setObsoleteSchedulingPeriod(long obsoleteSchedulingPeriod) {
        this.obsoleteSchedulingPeriod = obsoleteSchedulingPeriod;
    }

    @Override
    public long getObsoleteDeletePeriod() {
        return obsoleteDeletePeriod;
    }

    public void setObsoleteDeletePeriod(long obsoleteDeletePeriod) {
        this.obsoleteDeletePeriod = obsoleteDeletePeriod;
    }
}
