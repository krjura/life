package org.krjura.life.cluster.tools.keepalive.config.interfaces;

public interface KeepAlivePropsConfiguration {

    long getKeepAlivePeriod();

    long getScheduleDelay();

    long getObsoleteSchedulingPeriod();

    long getObsoleteDeletePeriod();
}
