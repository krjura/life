package org.krjura.life.cluster.tools.worker.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.krjura.life.cluster.tools.TestBase;
import org.krjura.life.cluster.tools.ex.WaitingException;
import org.krjura.life.cluster.tools.services.TestWorkerTaskProcessor;
import org.krjura.life.cluster.tools.worker.enums.RequestExecutionState;
import org.krjura.life.cluster.tools.worker.enums.RequestQueueStatus;
import org.krjura.life.cluster.tools.worker.model.Request;
import org.krjura.life.cluster.tools.worker.services.WorkerRepository;
import org.krjura.life.cluster.tools.worker.services.WorkerRequestQueueService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

public class WorkerRequestQueueServiceTest extends TestBase {

    @Autowired
    private WorkerRequestQueueService workerRequestQueueService;

    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private TestWorkerTaskProcessor taskProcessor;

    @Before
    public void init() {
        this.workerRepository.truncateClusterRequestQueue();
        this.taskProcessor.reset();
    }

    @Test
    public void addAndCompleteRequestSuccessfully() throws WaitingException {
        Request request = this.workerRequestQueueService.addRequest("test", "data", "metadata");
        this.workerRequestQueueService.searchForRequests();

        waitUntil(
                is(equalTo(RequestQueueStatus.COMPLETED)), () -> workerRepository.getRequest(request.getId()).getState());

        assertThat(this.taskProcessor.getRequests().size(), is(equalTo(1)));

        Request fromDb = workerRepository.getRequest(request.getId());

        assertThat(fromDb.getId(), is(equalTo(request.getId())));
        assertThat(fromDb.getRequestType(), is(equalTo("test")));
        assertThat(fromDb.getRequestData(), is(equalTo("data".getBytes())));
        assertThat(fromDb.getRequestMetadata(), is(equalTo("metadata".getBytes())));
        assertThat(fromDb.getNodeId(), is(nullValue()));
        assertThat(fromDb.getAssignedOn(), is(nullValue()));
        assertThat(fromDb.getState(), is(equalTo(RequestQueueStatus.COMPLETED)));
        assertThat(fromDb.getExecuteOn(), is(notNullValue()));
        assertThat(fromDb.getRetries(), is(equalTo(0)));
    }

    @Test
    public void addAndFailRequestSuccessfully() throws WaitingException {
        this.taskProcessor.setReturnValue(RequestExecutionState.FAILED_PERMANENT);

        Request request = this.workerRequestQueueService.addRequest("test", "data", "metadata");
        this.workerRequestQueueService.searchForRequests();

        waitUntil(
                is(equalTo(RequestQueueStatus.FAILED)), () -> workerRepository.getRequest(request.getId()).getState());

        assertThat(this.taskProcessor.getRequests().size(), is(equalTo(1)));

        Request fromDb = workerRepository.getRequest(request.getId());

        assertThat(fromDb.getId(), is(equalTo(request.getId())));
        assertThat(fromDb.getRequestType(), is(equalTo("test")));
        assertThat(fromDb.getRequestData(), is(equalTo("data".getBytes())));
        assertThat(fromDb.getRequestMetadata(), is(equalTo("metadata".getBytes())));
        assertThat(fromDb.getNodeId(), is(nullValue()));
        assertThat(fromDb.getAssignedOn(), is(nullValue()));
        assertThat(fromDb.getState(), is(equalTo(RequestQueueStatus.FAILED)));
        assertThat(fromDb.getExecuteOn(), is(notNullValue()));
        assertThat(fromDb.getRetries(), is(equalTo(1)));
    }

    @Test
    public void addAndRetryRequestSuccessfully() throws WaitingException {
        this.taskProcessor.setReturnValue(RequestExecutionState.FAILED_RETRY_POSSIBLE);

        Request request = this.workerRequestQueueService.addRequest("test", "data", "metadata");
        this.workerRequestQueueService.searchForRequests();

        waitUntil(
                is(equalTo(RequestQueueStatus.OPEN)), () -> workerRepository.getRequest(request.getId()).getState());

        assertThat(this.taskProcessor.getRequests().size(), is(equalTo(1)));

        Request fromDb = workerRepository.getRequest(request.getId());

        assertThat(fromDb.getId(), is(equalTo(request.getId())));
        assertThat(fromDb.getRequestType(), is(equalTo("test")));
        assertThat(fromDb.getRequestData(), is(equalTo("data".getBytes())));
        assertThat(fromDb.getRequestMetadata(), is(equalTo("metadata".getBytes())));
        assertThat(fromDb.getNodeId(), is(nullValue()));
        assertThat(fromDb.getAssignedOn(), is(nullValue()));
        assertThat(fromDb.getState(), is(equalTo(RequestQueueStatus.OPEN)));
        assertThat(fromDb.getExecuteOn(), is(notNullValue()));
        assertThat(fromDb.getRetries(), is(equalTo(1)));
    }

    @Test
    public void cleanupCompletedSuccessfully() {
        ZonedDateTime executeOn = ZonedDateTime.now().minusDays(1);
        Request request = this.workerRequestQueueService.addRequest("test", "data", "metadata", executeOn);
        this.workerRepository.markRequestAsCompleted(request);

        this.workerRequestQueueService.deleteCompletedRequests();

        List<Request> requests = this.workerRepository.findAllRequests();
        assertThat(requests.size(), is(equalTo(0)));
    }
}
