package org.krjura.life.cluster.tools.services;

import org.krjura.life.cluster.tools.worker.enums.RequestExecutionState;
import org.krjura.life.cluster.tools.worker.interfaces.WorkerTaskProcessor;
import org.krjura.life.cluster.tools.worker.model.Request;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class TestWorkerTaskProcessor implements WorkerTaskProcessor {

    private List<Request> requests;

    private RequestExecutionState result;

    public TestWorkerTaskProcessor() {
        this.requests = new ArrayList<>();
        this.result = RequestExecutionState.COMPLETED;
    }

    @Override
    public List<String> supports() {
        return Collections.singletonList("test");
    }

    @Override
    public RequestExecutionState execute(Request request) {
        requests.add(request);

        return this.result;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void reset() {
        this.requests.clear();
        this.result = RequestExecutionState.COMPLETED;
    }

    public void setReturnValue(RequestExecutionState result) {
        this.result = result;
    }
}
