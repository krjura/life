package org.krjura.life.cluster.tools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan( basePackageClasses = {
        ClusterToolsRoot.class
})
@EnableTransactionManagement
@EnableScheduling
public class ClusterToolsTestRoot {

    public static void main(String[] args) {

        ClusterToolsTestRoot application = new ClusterToolsTestRoot();
        application.runSpring(args);
    }

    private void runSpring(String[] args) {
        SpringApplication springApplication = new SpringApplication(ClusterToolsRoot.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
    }
}
