package org.krjura.life.cluster.tools.keepalive.services;

import org.junit.Test;
import org.krjura.life.cluster.tools.TestBase;
import org.krjura.life.cluster.tools.keepalive.services.KeepAliveService;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.krjura.life.cluster.tools.keepalive.model.KeepAlive;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

public class KeepAliveServiceTest extends TestBase {

    @Autowired
    private KeepAliveService keepAliveService;

    @Autowired
    private ClusterToolNodeInfoService nodeInfoService;

    @Test
    public void keepAliveIsRegistered() {
        this.keepAliveService.runKeepAlive();

        List<KeepAlive> entries = getKeepAliveRepository().findAllKeepAlives();
        assertThat(entries.size(), is(equalTo(1)));

        KeepAlive keepAlive = entries.get(0);

        assertThat(keepAlive.getNodeId(), is(equalTo(this.nodeInfoService.getNodeId())));
        assertThat(keepAlive.getLastUpdated(), is(notNullValue()));
    }

    @Test
    public void cleanupSuccessfully() {
        ZonedDateTime before = ZonedDateTime.now().minusYears(10);
        getKeepAliveRepository().addKeepAlive("node", before);

        keepAliveService.deleteObsoleteNodeIds();

        List<KeepAlive> keepAlives = getKeepAliveRepository().findAllKeepAlives();
        assertThat(keepAlives.size(), is(equalTo(0)));

    }
}
