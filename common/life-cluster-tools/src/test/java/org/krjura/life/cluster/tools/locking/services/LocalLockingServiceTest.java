package org.krjura.life.cluster.tools.locking.services;

import org.junit.Test;
import org.krjura.life.cluster.tools.TestBase;
import org.krjura.life.cluster.tools.ex.LockingException;
import org.krjura.life.cluster.tools.keepalive.model.repository.KeepAliveRepository;
import org.krjura.life.cluster.tools.locking.config.interfaces.LockingPropsConfiguration;
import org.krjura.life.cluster.tools.locking.model.ClusterLock;
import org.krjura.life.cluster.tools.locking.model.repository.ClusterLockRepository;
import org.krjura.life.cluster.tools.node.services.ClusterToolNodeInfoService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZonedDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

public class LocalLockingServiceTest extends TestBase {

    private static final String LOCK_KEY = "LocalLockingServiceTest";

    @Autowired
    private LocalLockingService localLockingService;

    @Autowired
    private ClusterLockRepository lockRepository;

    @Autowired
    private LockingPropsConfiguration lockingPropsConfiguration;

    @Autowired
    private ClusterToolNodeInfoService nodeInfoService;

    @Autowired
    private KeepAliveRepository keepAliveRepository;

    @Test
    public void mustLockAndUnlockByLockKeySuccessfully() throws LockingException {
        String clientKey = localLockingService.obtainClusterLock(LOCK_KEY);

        List<ClusterLock> locks = lockRepository.findAllLocks();

        assertThat(locks.size(), is(equalTo(1)));

        ClusterLock lock = locks.get(0);

        assertThat(lock.getLockKey(), is(equalTo(LOCK_KEY)));
        assertThat(lock.getClientId(), is(equalTo(clientKey)));
        assertThat(lock.getNodeId(), is(equalTo(nodeInfoService.getNodeId())));
        assertThat(lock.getCreatedDate(), is(notNullValue()));

        this.localLockingService.unlockClusterKey(clientKey);

        List<ClusterLock> unlockedLocks = lockRepository.findAllLocks();

        assertThat(unlockedLocks.size(), is(equalTo(0)));
    }

    @Test(expected = LockingException.class)
    public void mustNotBeAbleToLockAlreadyLockedKey() throws LockingException {
        localLockingService.obtainClusterLock(LOCK_KEY);
        localLockingService.obtainClusterLock(LOCK_KEY);
    }

    @Test
    public void mustDeleteObsoleteRecords() {
        // given
        lockRepository.saveLock("lockKey", "nodeId");

        ZonedDateTime keepAliveTimestamp = ZonedDateTime
                .now()
                .minusSeconds(this.lockingPropsConfiguration.getDuration()) // reduce minus from configuration used by the mechanism
                .minusSeconds(1); // and one more seconds so it gets deleted

        keepAliveRepository.addKeepAlive("nodeId", keepAliveTimestamp);

        // when
        localLockingService.deleteObsoleteLocks();

        // then
        List<ClusterLock> locks = this.lockRepository.findAllLocks();
        assertThat(locks.size(), is(equalTo(0)));

    }

    @Test
    public void mustNotDeleteObsoleteRecords() {
        // given
        lockRepository.saveLock("lockKey", "nodeId");
        keepAliveRepository.addKeepAlive("nodeId", ZonedDateTime.now());

        // when
        localLockingService.deleteObsoleteLocks();

        // then
        List<ClusterLock> locks = this.lockRepository.findAllLocks();
        assertThat(locks.size(), is(equalTo(1)));

    }

    @Test
    public void mustNotDeleteObsoleteRecordsWhenKeepAliveNotFound() {
        // given
        lockRepository.saveLock("lockKey", "nodeId");

        // when
        localLockingService.deleteObsoleteLocks();

        // then
        List<ClusterLock> locks = this.lockRepository.findAllLocks();
        assertThat(locks.size(), is(equalTo(0)));

    }
}
