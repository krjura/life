package org.krjura.life.cluster.tools;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.krjura.life.cluster.tools.ex.WaitingException;
import org.krjura.life.cluster.tools.keepalive.model.repository.KeepAliveRepository;
import org.krjura.life.cluster.tools.locking.model.repository.ClusterLockRepository;
import org.krjura.life.cluster.tools.worker.services.WorkerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.function.Supplier;

@RunWith(SpringJUnit4ClassRunner.class)
@ProjectTest
public abstract class TestBase {

    private static final Logger logger = LoggerFactory.getLogger(TestBase.class);

    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private KeepAliveRepository keepAliveRepository;

    @Autowired
    private ClusterLockRepository clusterLockRepository;

    public WorkerRepository getWorkerRepository() {
        return workerRepository;
    }

    public KeepAliveRepository getKeepAliveRepository() {
        return keepAliveRepository;
    }

    @Before
    public void beforeTestBase() {
        this.keepAliveRepository.truncateKeepAlive();
        this.clusterLockRepository.truncateClusterLock();
    }

    public <T> void waitUntil(Matcher<T> matcher, Supplier<T> supplier) throws WaitingException {

        for( int i = 0; i < 600; i++) {

            logger.info("trying to match");

            T value = supplier.get();
            boolean result = matcher.matches(value);

            if(result) {
                return;
            }

            sleep();
        }

        throw new WaitingException("Cannot wait any longer");
    }

    private void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            logger.trace("interrupted while sleeping", e);
        }
    }
}
