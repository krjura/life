package org.krjura.life.cluster.tools.ex;

public class WaitingException extends Exception {

    public WaitingException(String s) {
        super(s);
    }

    public WaitingException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
