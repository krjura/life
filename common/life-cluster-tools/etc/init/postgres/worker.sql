CREATE TABLE cluster_tools_keep_alive  (
	node_id VARCHAR(255) NOT NULL,
	last_updated TIMESTAMP WITH TIME ZONE,

	CONSTRAINT pk_cluster_tools_keep_alive PRIMARY KEY (node_id)
);

CREATE TABLE cluster_tools_request_queue (
  id UUID NOT NULL,

  request_type VARCHAR(100) NOT NULL,
  request_data BYTEA NOT NULL,
  request_metadata BYTEA NOT NULL,

  node_id VARCHAR(255),
  assigned_on TIMESTAMP WITH TIME ZONE,

  state VARCHAR(16) NOT NULL,
  execute_on TIMESTAMP WITH TIME ZONE NOT NULL,
  retries INT NOT NULL,

  CONSTRAINT pk_cluster_tools_request_queue PRIMARY KEY (id)
);

CREATE UNIQUE INDEX idx_cluster_tools_request_queue_execute ON cluster_tools_request_queue (state, execute_on);