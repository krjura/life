/*
Used for cluster wide operations
*/
CREATE TABLE cluster_lock  (
	lock_key VARCHAR(36),
	client_id VARCHAR(36),
	created_date TIMESTAMP WITH TIME ZONE NOT NULL,
	constraint pk_cluster_lock primary key (lock_key)
);