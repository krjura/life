DROP DATABASE IF EXISTS cluster_tools_tst;
CREATE DATABASE cluster_tools_tst ENCODING 'UTF-8';

DROP USER IF EXISTS cluster_tools_tst;
CREATE USER cluster_tools_tst WITH PASSWORD 'cluster_tools_tst';