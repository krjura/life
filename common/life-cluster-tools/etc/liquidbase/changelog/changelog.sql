--liquibase formatted sql

--changeset kjurasovic:v20180301220100
CREATE TABLE cluster_lock  (
	lock_key VARCHAR(36),
	client_id VARCHAR(36),
	created_date TIMESTAMP WITH TIME ZONE NOT NULL,
	constraint pk_cluster_lock primary key (lock_key)
);

CREATE TABLE cluster_tools_keep_alive  (
	node_id VARCHAR(255) NOT NULL,
	last_updated TIMESTAMP WITH TIME ZONE,

	CONSTRAINT pk_cluster_tools_keep_alive PRIMARY KEY (node_id)
);

CREATE TABLE cluster_tools_request_queue (
  id UUID NOT NULL,

  request_type VARCHAR(100) NOT NULL,
  request_data BYTEA NOT NULL,
  request_metadata BYTEA NOT NULL,

  node_id VARCHAR(255) NULL,
  assigned_on TIMESTAMP WITH TIME ZONE NULL,

  state VARCHAR(16) NOT NULL,
  execute_on TIMESTAMP WITH TIME ZONE NOT NULL,
  retries INT NOT NULL,

  CONSTRAINT pk_cluster_tools_request_queue PRIMARY KEY (id)
);

CREATE UNIQUE INDEX idx_cluster_tools_request_queue_execute ON cluster_tools_request_queue (state, execute_on);

--changeset kjurasovic:v20180911205900
ALTER TABLE cluster_lock ADD COLUMN node_id VARCHAR(255) NULL;

--changeset kjurasovic:v20180918094500
DROP INDEX idx_cluster_tools_request_queue_execute;
CREATE INDEX idx_cluster_tools_request_queue_execute ON cluster_tools_request_queue (state, execute_on);