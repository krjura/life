package org.krjura.life.json.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.krjura.life.json.utils.JsonUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "org.krjura.life.json.jackson.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({ObjectMapper.class})
public class JsonConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return JsonUtils.objectMapper();
    }
}
