package biz.db.dev.life.mvc.error.handlers.handler.response;

public class ErrorResponse {

    private ErrorResponseStatus status;

    private Integer statusCode;

    private String statusDescription;

    private ErrorResponseData data;

    public ErrorResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ErrorResponseStatus status) {
        this.status = status;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public ErrorResponseData getData() {
        if( data == null ) {
            this.data = new ErrorResponseData();
        }

        return data;
    }

    public void setData(ErrorResponseData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "status=" + status +
                ", statusCode='" + statusCode + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                ", data=" + data +
                '}';
    }
}
