package biz.db.dev.life.mvc.error.handlers.handler;

import biz.db.dev.life.exceptions.InvalidRequestReceived;
import biz.db.dev.life.exceptions.ResourceAlreadyExists;
import biz.db.dev.life.exceptions.ResourceDoesNotExists;
import biz.db.dev.life.exceptions.UserIsForbiddenException;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponse;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseBuilder;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatus;
import biz.db.dev.life.mvc.error.handlers.handler.response.ErrorResponseStatusCode;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@ControllerAdvice
public class GlobalErrorHandler {

    private static final String STATUS_VALIDATION_ERROR_OCCURRED = "validation error occurred";

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceDoesNotExists.class)
    @ResponseBody
    public ErrorResponse handleResourceDoesNotExists(ResourceDoesNotExists ex) {
        ErrorResponseBuilder builder = ErrorResponseBuilder
                .builder()
                .status(ErrorResponseStatus.ERROR)
                .statusCode(ErrorResponseStatusCode.BAD_REQUEST.getCode())
                .statusDescription(ex.getMessage());

        builder
                .detail()
                .message(ex.getMessage()) // need default message source
                .reason(ex.getLocalizationKey())
                .attributeName(ex.getResourceName())
                .attributeValue(ex.getResourceValue())
                .build();

        return builder.build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ResourceAlreadyExists.class)
    @ResponseBody
    public ErrorResponse handleResourceAlreadyExists(ResourceAlreadyExists ex) {
        ErrorResponseBuilder builder = ErrorResponseBuilder
                .builder()
                .status(ErrorResponseStatus.ERROR)
                .statusCode(ErrorResponseStatusCode.BAD_REQUEST.getCode())
                .statusDescription(ex.getMessage());

        builder
                .detail()
                .message(ex.getMessage()) // need default message source
                .reason(ex.getLocalizationKey())
                .attributeName(ex.getResourceName())
                .attributeValue(ex.getResourceValue())
                .build();

        return builder.build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        ErrorResponseBuilder builder = ErrorResponseBuilder
                .builder()
                .status(ErrorResponseStatus.ERROR)
                .statusCode(ErrorResponseStatusCode.BAD_REQUEST.getCode())
                .statusDescription(STATUS_VALIDATION_ERROR_OCCURRED);

        List<FieldError> fieldErrors = new ArrayList<>(ex.getBindingResult().getFieldErrors());
        fieldErrors.sort(Comparator.comparing(FieldError::getField));

        for(FieldError fieldError : fieldErrors ) {

            builder
                    .detail()
                    .message(fieldError.getDefaultMessage()) // need default message source
                    .reason(fieldError.getDefaultMessage())
                    .attributeName(fieldError.getField())
                    .attributeValue(getObjectValue(fieldError.getRejectedValue()))
                    .build();
        }

        return builder.build();
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(UserIsForbiddenException.class)
    @ResponseBody
    public ErrorResponse handleUserIsForbiddenException(UserIsForbiddenException ex) {
        ErrorResponseBuilder builder = ErrorResponseBuilder
                .builder()
                .status(ErrorResponseStatus.ERROR)
                .statusCode(ErrorResponseStatusCode.FORBIDDEN.getCode())
                .statusDescription(ex.getMessage());

        builder
                .detail()
                .message("User is not authorized")
                .reason(ex.getMessage())
                .attributeName("session")
                .attributeValue(null)
                .build();

        return builder.build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidRequestReceived.class)
    @ResponseBody
    public ErrorResponse handleInvalidRequestReceived(InvalidRequestReceived ex) {
        ErrorResponseBuilder builder = ErrorResponseBuilder
                .builder()
                .status(ErrorResponseStatus.ERROR)
                .statusCode(ErrorResponseStatusCode.BAD_REQUEST.getCode())
                .statusDescription(ex.getMessage());

        builder
                .detail()
                .message(ex.getMessage())
                .reason(ex.getLocalizationKey())
                .attributeName(ex.getResourceName())
                .attributeValue(ex.getResourceValue())
                .build();

        return builder.build();
    }

    private String getObjectValue(Object value) {
        return value == null ? null : value.toString();
    }
}
