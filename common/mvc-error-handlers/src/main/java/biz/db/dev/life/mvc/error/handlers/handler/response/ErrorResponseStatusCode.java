package biz.db.dev.life.mvc.error.handlers.handler.response;

public enum ErrorResponseStatusCode {

    OK(200),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    RESOURCE_NOT_FOUND(404),
    INTERNAL_SERVER_ERROR(500)
    ;

    private Integer code;

    private ErrorResponseStatusCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
