package biz.db.dev.life.mvc.error.handlers.handler.response;

public enum ErrorResponseStatus {

    SUCCESS,
    ERROR
}
