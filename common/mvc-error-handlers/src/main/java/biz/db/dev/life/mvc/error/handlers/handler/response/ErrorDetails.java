package biz.db.dev.life.mvc.error.handlers.handler.response;

import java.util.ArrayList;
import java.util.List;

public class ErrorDetails {

    // required
    private String reason;

    // optional
    private String attributeName;

    // optional
    private List<String> attributeValues;

    // optional
    private String message;

    public ErrorDetails() {
        this.attributeValues = new ArrayList<>(1);
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public List<String> getAttributeValues() {
        if( this.attributeValues == null ) {
            this.attributeValues = new ArrayList<>(1);
        }

        return attributeValues;
    }

    public void setAttributeValues(List<String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorDetails{" +
                "reason='" + reason + '\'' +
                ", attributeName='" + attributeName + '\'' +
                ", attributeValues=" + attributeValues +
                ", message='" + message + '\'' +
                '}';
    }

    public void addAttributeValue(String attributeValue) {
        getAttributeValues().add(attributeValue);
    }
}
