package biz.db.dev.life.mvc.error.handlers.handler.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.util.List;

public class ErrorResponseBuilder {

    private static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    private ErrorResponse errorResponse;

    private ErrorResponseBuilder() {
        this.errorResponse = new ErrorResponse();
    }

    public static ErrorResponseBuilder builder() {
        return new ErrorResponseBuilder();
    }

    public ErrorResponseBuilder status(ErrorResponseStatus status) {
        this.errorResponse.setStatus(status);

        return this;
    }

    public ErrorResponseBuilder statusCode(Integer statusCode) {
        this.errorResponse.setStatusCode(statusCode);

        return this;
    }

    public ErrorResponseBuilder statusDescription(String statusDescription) {
        this.errorResponse.setStatusDescription(statusDescription);

        return this;
    }

    public ErrorDetailsBuilder detail() {
        return new ErrorDetailsBuilder(this);
    }

    public ErrorResponseBuilder detail(ErrorDetails errorDetails) {
        return this
                .detail()
                .reason(errorDetails.getReason())
                .attributeName(errorDetails.getAttributeName())
                .attributeValues(errorDetails.getAttributeValues())
                .message(errorDetails.getMessage())
                .build();
    }

    public ErrorResponse build() {
        return this.errorResponse;
    }

    public String buildAsJson() throws JsonProcessingException {
        return objectMapper.writeValueAsString(this.errorResponse);
    }

    public ErrorResponseBuilder load(String content) throws IOException {
        this.errorResponse = objectMapper.readValue(content, ErrorResponse.class);

        return this;
    }

    public class ErrorDetailsBuilder {

        private ErrorResponseBuilder errorResponseBuilder;

        private ErrorDetails errorDetails;

        private ErrorDetailsBuilder(ErrorResponseBuilder errorResponseBuilder) {
            this.errorResponseBuilder = errorResponseBuilder;
            this.errorDetails = new ErrorDetails();
        }

        public ErrorDetailsBuilder reason(String reason) {
            this.errorDetails.setReason(reason);

            return this;
        }

        public ErrorDetailsBuilder attributeName(String attributeName) {
            this.errorDetails.setAttributeName(attributeName);

            return this;
        }

        public ErrorDetailsBuilder attributeValue(String attributeValue) {
            this.errorDetails.addAttributeValue(attributeValue);

            return this;
        }

        public ErrorDetailsBuilder attributeValues(Object[] values) {

            if( values == null || values.length == 0) {
                return this;
            }

            for( Object value : values ) {
                if( value == null ) {
                    continue;
                }

                this.errorDetails.addAttributeValue(value.toString());
            }

            return this;
        }

        public ErrorDetailsBuilder attributeValues(List<?> values) {
            if( values == null || values.isEmpty() ) {
                return this;
            }

            for( Object value : values ) {
                if( value == null) {
                    continue;
                }

                this.errorDetails.addAttributeValue(value.toString());
            }

            return this;
        }

        public ErrorDetailsBuilder message(String message) {
            this.errorDetails.setMessage(message);

            return this;
        }

        public ErrorResponseBuilder build() {

            this
                    .errorResponseBuilder
                    .errorResponse
                    .getData()
                    .addDetail( this.errorDetails );

            return this.errorResponseBuilder;
        }
    }
}
