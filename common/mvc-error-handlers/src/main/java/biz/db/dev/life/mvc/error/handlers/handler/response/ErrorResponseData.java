package biz.db.dev.life.mvc.error.handlers.handler.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponseData {

    private List<ErrorDetails> details;

    public List<ErrorDetails> getDetails() {
        if( this.details == null ) {
            this.details = new ArrayList<>();
        }

        return details;
    }

    public void setDetails(List<ErrorDetails> details) {
        this.details = details;
    }

    public void addDetail(ErrorDetails errorDetails) {
        getDetails().add(errorDetails);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return getDetails().isEmpty();
    }

    @Override
    public String toString() {
        return "ErrorResponseData{" +
                "details=" + details +
                '}';
    }
}
