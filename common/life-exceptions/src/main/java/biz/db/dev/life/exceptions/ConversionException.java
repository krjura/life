package biz.db.dev.life.exceptions;

public class ConversionException extends RuntimeException {

    public ConversionException(String message) {
        super(message);
    }

    public ConversionException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
