package biz.db.dev.life.exceptions;

public class InvalidRequestReceived extends ServiceException {

    private String resourceName;

    private String resourceValue;

    private String localizationKey;

    public InvalidRequestReceived(String message, String resourceName, String resourceValue, String localizationKey) {
        super(message);
        this.resourceName = resourceName;
        this.resourceValue = resourceValue;
        this.localizationKey = localizationKey;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getResourceValue() {
        return resourceValue;
    }

    public String getLocalizationKey() {
        return localizationKey;
    }
}
