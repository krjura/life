package biz.db.dev.life.exceptions;

public class ResourceAlreadyExists extends ServiceException {

    private String resourceName;
    private String resourceValue;
    private String localizationKey;

    public ResourceAlreadyExists(String resourceName, String resourceValue, String localizationKey, String message) {
        super(message);

        this.resourceName = resourceName;
        this.resourceValue = resourceValue;
        this.localizationKey = localizationKey;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getResourceValue() {
        return resourceValue;
    }

    public String getLocalizationKey() {
        return localizationKey;
    }
}
