package biz.db.dev.life.exceptions;

public class UserIsForbiddenException extends RuntimeException {

    private static final String MESSAGE_KEY = "life.ex.UserIsForbiddenException";

    public UserIsForbiddenException() {
        super(MESSAGE_KEY);
    }

    public UserIsForbiddenException(Throwable throwable) {
        super(MESSAGE_KEY, throwable);
    }
}
