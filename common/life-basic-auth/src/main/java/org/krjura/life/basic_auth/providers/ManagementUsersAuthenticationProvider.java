package org.krjura.life.basic_auth.providers;

import org.krjura.life.basic_auth.config.ManagementUsersConfig;
import org.krjura.life.basic_auth.config.pojo.UserInfo;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ManagementUsersAuthenticationProvider implements AuthenticationProvider {

    private static final String CONST_HIDDEN = "HIDDEN";

    private final ManagementUsersConfig config;

    private final PasswordEncoder passwordEncoder;

    public ManagementUsersAuthenticationProvider(ManagementUsersConfig config) {

        Objects.requireNonNull(config);

        this.config = config;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;

        String username = (String) token.getPrincipal();
        String password = (String) token.getCredentials();

        Optional<UserInfo> userInfoOptional = findUserByUsername(username);

        if(! userInfoOptional.isPresent()) {
            return null;
        }

        UserInfo userInfo = userInfoOptional.get();

        if(!passwordEncoder.matches(password, userInfo.getPassword())) {
            throw new BadCredentialsException("invalid username or password");
        }

        List<GrantedAuthority> grantedAuthorities = userInfo
                .getPrivileges()
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(token.getPrincipal(), CONST_HIDDEN, grantedAuthorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private Optional<UserInfo> findUserByUsername(String username) {
        for(UserInfo userInfo : this.config.getUserInfos()) {
            if(userInfo.getUsername().equals(username)) {
                return Optional.of(userInfo);
            }
        }

        return Optional.empty();
    }
}
