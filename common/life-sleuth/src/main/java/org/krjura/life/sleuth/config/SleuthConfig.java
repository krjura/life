package org.krjura.life.sleuth.config;

import brave.sampler.Sampler;
import org.krjura.life.sleuth.CustomSleuthReporter;
import org.krjura.life.sleuth.config.props.SleuthProps;
import org.springframework.cloud.sleuth.DefaultSpanNamer;
import org.springframework.cloud.sleuth.SpanNamer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import zipkin2.Span;
import zipkin2.reporter.Reporter;

@Configuration
public class SleuthConfig {

    @Bean
    public Reporter<Span> sleuthSpanReporter(SleuthProps sleuthProps) {
        return new CustomSleuthReporter(sleuthProps);
    }

    @Bean
    public Sampler sleuthTraceSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }

    @Bean
    public SpanNamer sleuthSpanNamer() {
        return new DefaultSpanNamer();
    }
}
