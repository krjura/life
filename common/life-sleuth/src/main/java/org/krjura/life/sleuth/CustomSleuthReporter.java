package org.krjura.life.sleuth;

import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.Metrics;
import org.krjura.life.sleuth.config.props.SleuthProps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zipkin2.Endpoint;
import zipkin2.Span;
import zipkin2.reporter.Reporter;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CustomSleuthReporter implements Reporter<Span> {

    private static final Logger logger = LoggerFactory.getLogger(CustomSleuthReporter.class);

    private static final String CONST_TRACER = "life.sleuth.latency";
    private static final String CONST_TAG_SOURCE = "class_source";
    private static final String CONST_UNKNOWN = "unknown";
    private static final String CONST_HTTP_PATH = "http.path";
    private static final int CONST_UNKNOWN_PORT = -1;
    private static final String CONST_DOT = ".";

    private static final String[] classTags = {"class", "mvc.controller.class"};
    private static final String[] methodTags = {"method", "mvc.controller.method"};

    private Map<String, DistributionSummary> metricsCache;

    private final SleuthProps sleuthProps;

    public CustomSleuthReporter(SleuthProps sleuthProps) {
        this.sleuthProps = sleuthProps;

        this.metricsCache = new ConcurrentHashMap<>();
    }

    @Override
    public void report(Span span) {
        String metricName = calculateMetricName(span);

        if(sleuthProps.isIncludedInLogging(metricName)) {
            logger.debug(
                    "name: {}, metricName: {}, traceId: {}, spanId: {}, duration: {}, kind: {}, local: {}, remote: {}, tags: {}",
                    span.name(), metricName, span.traceId(), span.id(),
                    span.duration(), span.kind(), printEndpoint(span.localEndpoint()), printEndpoint(span.remoteEndpoint()), span.tags());
        }

        recordDuration(span, metricName);
    }

    private String calculateMetricName(Span span) {
        if(span == null) {
            return null;
        }

        return calculateMetricNameBasedOnClass(span);
    }

    private String calculateMetricNameBasedOnClass(Span span) {
        String clazz = findClassTag(span);
        String method = findMethodTag(span);

        String metricName = null;

        if(clazz != null && method != null) {
            metricName = clazz.toLowerCase() + CONST_DOT + method.toLowerCase();
        }

        return metricName;
    }

    private boolean containsPath(Span span) {
        return span.tags().containsKey(CONST_HTTP_PATH);
    }

    private boolean containsClass(Span span) {
        for(String classTag : classTags) {
            if(span.tags().containsKey(classTag)) {
                return true;
            }
        }

        return false;
    }

    private void recordDuration(Span span, String metricName) {
        if(metricName == null) {
            return;
        }

        if(span.duration() == null) {
            return;
        }

        findMetric(metricName).record(span.duration());
    }

    private DistributionSummary findMetric(String name) {
        return this.metricsCache.computeIfAbsent(name, s -> newDistributionSummary(name));
    }

    private DistributionSummary newDistributionSummary(String name) {
        return DistributionSummary
                .builder(CONST_TRACER)
                .baseUnit("us")
                .percentilePrecision(2)
                .publishPercentiles(0.5, 0.95)
                .distributionStatisticExpiry(Duration.ofHours(1))
                .sla(50_000, 100_000, 500_000, 1000_000, 10000_000)
                .tag(CONST_TAG_SOURCE, name)
                .register(Metrics.globalRegistry);
    }

    private String printEndpoint(Endpoint endpoint) {
        if(endpoint == null) {
            return CONST_UNKNOWN;
        }

        if(endpoint.ipv4() == null && endpoint.port() == null) {
            return CONST_UNKNOWN;
        }

        String ip = endpoint.ipv4() == null ? CONST_UNKNOWN : endpoint.ipv4();
        Integer port = endpoint.port() == null ? CONST_UNKNOWN_PORT : endpoint.port();

        return ip + ":" + port;
    }

    private String findMethodTag(Span span) {
        if(span.tags() == null) {
            return null;
        }

        for(String methodTag : methodTags) {
            if(span.tags().containsKey(methodTag)) {
                return span.tags().get(methodTag);
            }
        }

        return null;
    }

    private String findClassTag(Span span) {
        if(span.tags() == null) {
            return null;
        }

        for(String classTag : classTags) {
            if(span.tags().containsKey(classTag)) {
                return span.tags().get(classTag);
            }
        }

        return null;
    }
}
