package org.krjura.life.sleuth.config.props;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties( prefix = "life.sleuth")
@ConditionalOnProperty(value = "life.sleuth.enabled", havingValue = "true", matchIfMissing = true)
public class SleuthProps {

    private List<String> excludedMetricsFromLogging;

    public SleuthProps() {
        this.excludedMetricsFromLogging = new ArrayList<>();
    }

    public List<String> getExcludedMetricsFromLogging() {
        return excludedMetricsFromLogging;
    }

    public void setExcludedMetricsFromLogging(List<String> excludedMetricsFromLogging) {
        this.excludedMetricsFromLogging = excludedMetricsFromLogging;
    }

    public boolean isIncludedInLogging(String metricName) {
        if( metricName == null ) {
            return true;
        }

        return ! this.excludedMetricsFromLogging.contains(metricName);
    }
}
