package org.krjura.life.autoconfigure.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@ConditionalOnProperty(value = "org.krjura.life.autoconfigure.executors.enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({ScheduledExecutorService.class})
public class ExecutorsConfig {

    @Bean
    @Primary
    public ScheduledExecutorService defaultScheduledExecutor() {
        return Executors.newScheduledThreadPool(2);
    }
}
