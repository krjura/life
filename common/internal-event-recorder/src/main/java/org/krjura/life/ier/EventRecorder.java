package org.krjura.life.ier;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EventRecorder {

    private Map<String, Recorder> recorders;

    private static EventRecorder instance;

    private EventRecorder() {
        this.recorders = new HashMap<>();
    }

    public static EventRecorder instance() {
        if(EventRecorder.instance == null) {
            EventRecorder.instance = new EventRecorder();
        }

        return EventRecorder.instance;
    }

    public Recorder record() {
        String id = UUID.randomUUID().toString();

        return recorders.computeIfAbsent(id, s -> new Recorder(id, this));
    }

    void unSubscribe(String id) {
        this.recorders.remove(id);
    }

    public void publish(String name, Object value) {
        this
                .recorders
                .values()
                .forEach(recorder -> recorder.receive(new InternalEvent(name, value)));
    }
}
