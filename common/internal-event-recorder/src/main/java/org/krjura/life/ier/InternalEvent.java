package org.krjura.life.ier;

public class InternalEvent {

    private final String name;

    private final Object value;

    public InternalEvent(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
