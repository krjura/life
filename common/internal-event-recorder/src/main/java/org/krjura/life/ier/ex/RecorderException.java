package org.krjura.life.ier.ex;

public class RecorderException extends RuntimeException {

    public RecorderException(String s) {
        super(s);
    }

    public RecorderException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
