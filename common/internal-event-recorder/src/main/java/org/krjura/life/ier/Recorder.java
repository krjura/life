package org.krjura.life.ier;


import org.krjura.life.ier.ex.RecorderException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Recorder {

    private final String id;

    private final EventRecorder eventRecorder;

    private final List<InternalEvent> events;

    public Recorder(String id, EventRecorder eventRecorder) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(eventRecorder);

        this.eventRecorder = eventRecorder;
        this.id = id;
        this.events = new ArrayList<>();
    }

    public void unsubscribe() {
        this.eventRecorder.unSubscribe(id);
    }

    public void reset() {
        this.events.clear();
    }

    public void receive(InternalEvent internalEvent) {
        this.events.add(internalEvent);
    }

    public List<InternalEvent> getEvents() {
        return events;
    }

    public void waitUntilThereAreEvents(int count) {

        for(int i = 0; i < 50; i++) {

            if(this.events.size() >= count) {
                return;
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // i do not care
            }
        }

        throw new RecorderException("timeout");
    }
}
