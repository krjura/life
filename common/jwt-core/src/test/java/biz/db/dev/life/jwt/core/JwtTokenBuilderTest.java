package biz.db.dev.life.jwt.core;

import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.providers.PrivateRsaKeyProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class JwtTokenBuilderTest {

    private String privateKey;
    private String publicKey;

    @Before
    public void init() throws NoSuchAlgorithmException {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance("RSA");
        rsaGenerator.initialize(2048);
        KeyPair rsaKeyPair = rsaGenerator.generateKeyPair();

        this.publicKey = Base64.getEncoder().encodeToString(rsaKeyPair.getPublic().getEncoded());
        this.privateKey = Base64.getEncoder().encodeToString(rsaKeyPair.getPrivate().getEncoded());
    }

    @Test
    public void mustParseKeyIdCorrectly() throws JwtProcessingException {
        long expiry = 10;
        String keyId = "1";
        String tokenId = UUID.randomUUID().toString();
        UUID userId = UUID.randomUUID();
        UUID tenantId = UUID.randomUUID();
        String[] grants = {"ROLE_ADMIN", "ROLE_USER"};

        JwtTokenInfo tokenInfo = JwtTokenBuilder
                .create()
                .expiry(expiry)
                .tokenId(tokenId)
                .tenantId(tenantId)
                .userId(userId)
                .grants(grants)
                .privateKey(privateKey)
                .keyId(keyId)
                .build();

        String parsedKeyId = JwtTokenBuilder.parseKeyId(tokenInfo.getToken());
        assertThat(parsedKeyId, is(equalTo(keyId)));
    }


    @Test
    public void mustEncodeAndDecodeTokenSuccessfully() throws JwtProcessingException {

        long expiry = 10;
        String tokenId = UUID.randomUUID().toString();
        UUID userId = UUID.randomUUID();
        UUID tenantId = UUID.randomUUID();
        String[] grants = {"ROLE_ADMIN", "ROLE_USER"};

        JwtTokenInfo tokenInfo = JwtTokenBuilder
                .create()
                .expiry(expiry)
                .tokenId(tokenId)
                .tenantId(tenantId)
                .userId(userId)
                .grants(grants)
                .privateKey(privateKey)
                .keyId("1")
                .build();

        JwtDetails details = JwtTokenBuilder.parse(publicKey, tokenInfo.getToken());
        assertThat(details, is(notNullValue()));
        assertThat(details.getGrants(), contains(grants));
        assertThat(details.getTokenId(), is(equalTo(tokenId)));
        assertThat(details.getUserId(), is(equalTo(userId)));
        assertThat(details.getIssuedAt(), is(notNullValue()));
        assertThat(details.getExpiresAt(), is(notNullValue()));
    }

    @Test(expected = TokenExpiredException.class)
    public void mustFailOnExpire() throws JwtProcessingException {

        long expiry = -10;
        String tokenId = UUID.randomUUID().toString();
        UUID userId = UUID.randomUUID();
        UUID tenantId = UUID.randomUUID();
        String[] grants = {"ROLE_ADMIN", "ROLE_USER"};

        JwtTokenInfo tokenInfo = JwtTokenBuilder
                .create()
                .expiry(expiry)
                .tokenId(tokenId)
                .tenantId(tenantId)
                .userId(userId)
                .grants(grants)
                .privateKey(privateKey)
                .keyId("")
                .build();

        JwtTokenBuilder.parse(publicKey, tokenInfo.getToken());
    }

    @Test( expected = JwtProcessingException.class)
    public void mustFailWhenIssuedAtWasNotSet() throws JwtProcessingException {
        String tokenId = UUID.randomUUID().toString();
        Integer userId = 1;
        String[] grants = {"ROLE_ADMIN", "ROLE_USER"};
        String[] identifiers = {"1", "3", "2"};

        PrivateRsaKeyProvider jwtKeyProvider = new PrivateRsaKeyProvider("1", this.privateKey);
        Algorithm algorithm = Algorithm.RSA256(jwtKeyProvider);

        String token = JWT
                .create()
                .withJWTId(tokenId)
                .withClaim("userId", userId)
                .withArrayClaim("identifiers", identifiers)
                .withArrayClaim("roles", grants)
                .sign(algorithm);

        JwtTokenBuilder.parse(publicKey, token);
    }

    @Test( expected = JwtProcessingException.class)
    public void mustFailWhenExpiresAtWasNotSet() throws JwtProcessingException {
        String tokenId = UUID.randomUUID().toString();
        Integer userId = 1;
        String[] grants = {"ROLE_ADMIN", "ROLE_USER"};

        PrivateRsaKeyProvider jwtKeyProvider = new PrivateRsaKeyProvider("1", this.privateKey);
        Algorithm algorithm = Algorithm.RSA256(jwtKeyProvider);

        ZonedDateTime expiration = ZonedDateTime.now().plusMinutes(0);

        String token = JWT
                .create()
                .withJWTId(tokenId)
                .withIssuedAt(Date.from(expiration.toInstant()))
                .withClaim("userId", userId)
                .withArrayClaim("roles", grants)
                .sign(algorithm);

        JwtTokenBuilder.parse(publicKey, token);
    }
}