package biz.db.dev.life.jwt.core.providers;

import biz.db.dev.life.jwt.core.ex.JwtRuntimeException;
import com.auth0.jwt.interfaces.RSAKeyProvider;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class PrivateRsaKeyProvider implements RSAKeyProvider {

    private static final String PARAM_RSA = "RSA";

    private String privateKey;

    private String keyId;

    public PrivateRsaKeyProvider(String keyId, String privateKey) {
        this.keyId = keyId;
        this.privateKey = privateKey;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public RSAPublicKey getPublicKeyById(String keyId) {
        throw new UnsupportedOperationException("public keys are not supported");
    }

    @Override
    public RSAPrivateKey getPrivateKey() {

        try {
            byte[] keyBytes = Base64.getDecoder().decode(this.privateKey);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance(PARAM_RSA);

            return (RSAPrivateKey) kf.generatePrivate(keySpec);
        } catch ( NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JwtRuntimeException("Cannot generate Private Key", e);
        }
    }

    @Override
    public String getPrivateKeyId() {
        return this.keyId;
    }
}