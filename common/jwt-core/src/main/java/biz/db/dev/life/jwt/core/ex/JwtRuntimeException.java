package biz.db.dev.life.jwt.core.ex;

public class JwtRuntimeException extends RuntimeException {

    public JwtRuntimeException(String message) {
        super(message);
    }

    public JwtRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}