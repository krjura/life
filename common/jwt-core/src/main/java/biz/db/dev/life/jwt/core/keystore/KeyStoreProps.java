package biz.db.dev.life.jwt.core.keystore;

import biz.db.dev.life.jwt.core.config.pojo.KeyInfo;

import java.util.Map;

public interface KeyStoreProps {

    Map<String, KeyInfo> getKeys();

    String getDefaultKeyId();

    Long getDefaultExpiry();
}