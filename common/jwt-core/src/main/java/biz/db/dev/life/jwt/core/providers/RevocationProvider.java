package biz.db.dev.life.jwt.core.providers;

public interface RevocationProvider {

    boolean isTokenRevoked(String token);
}
