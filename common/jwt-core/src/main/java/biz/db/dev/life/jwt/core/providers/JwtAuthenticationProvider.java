package biz.db.dev.life.jwt.core.providers;

import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.keystore.JwtKeyStore;
import biz.db.dev.life.jwt.core.utils.JwtHelper;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthenticationProvider implements AuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    private static final String CONST_ANONYMOUS = "anonymous";
    private static final List<SimpleGrantedAuthority> CONST_ROLE_ANONYMOUS =
            Collections.singletonList(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));

    private final JwtKeyStore jwtKeyStore;

    private final RevocationProvider revocationProvider;

    public JwtAuthenticationProvider(JwtKeyStore jwtKeyStore, RevocationProvider revocationProvider) {
        Assert.notNull(jwtKeyStore, "JwtKeyStore cannot be null");
        Assert.notNull(revocationProvider, "RevocationProvider cannot be null");

        this.jwtKeyStore = jwtKeyStore;
        this.revocationProvider = revocationProvider;

        logger.info("started JwtAuthenticationProvider");
    }

    @Override
    public Authentication authenticate(Authentication authentication) {

        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String token = jwtAuthenticationToken.getToken();

        JwtDetails jwtDetails;
        try {
            jwtDetails = JwtHelper.processToken(token, this.jwtKeyStore);
        } catch (TokenExpiredException e) {
            return new AnonymousAuthenticationToken(CONST_ANONYMOUS, CONST_ANONYMOUS, CONST_ROLE_ANONYMOUS);
        } catch (JwtProcessingException e) {
            throw new JWTDecodeException("token is invalid. " + e.getMessage());
        }

        if(revocationProvider.isTokenRevoked(jwtDetails.getTokenId())) {
            throw new BadCredentialsException("token " + jwtDetails.getTokenId() + " has been revoked");
        }

        List<GrantedAuthority> grants = jwtDetails
                .getGrants()
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        if(logger.isDebugEnabled()) {
            logger.debug("JWT token found and verified with details {}", jwtDetails);
        }

        return new JwtAuthenticationToken(grants, jwtDetails);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
