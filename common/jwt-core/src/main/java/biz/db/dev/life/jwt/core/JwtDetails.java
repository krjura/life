package biz.db.dev.life.jwt.core;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class JwtDetails implements Serializable {

    private String tokenId;

    private UUID tenantId;

    private UUID userId;

    private List<String> grants;

    private Long expiresAt;

    private Long issuedAt;

    public JwtDetails(String tokenId, UUID tenantId, UUID userId, List<String> grants, Long issuedAt, Long expiresAt) {
        this.tokenId = tokenId;
        this.tenantId = tenantId;
        this.userId = userId;
        this.grants = grants;
        this.issuedAt = issuedAt;
        this.expiresAt = expiresAt;
    }

    public UUID getTenantId() {
        return tenantId;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public List<String> getGrants() {
        return grants;
    }

    public Long getExpiresAt() {
        return expiresAt;
    }

    public Long getIssuedAt() {
        return issuedAt;
    }

    @Override
    public String toString() {
        return "JwtDetails{" +
                "tokenId='" + tokenId + '\'' +
                ", tenantId=" + tenantId +
                ", userId=" + userId +
                ", grants=" + grants +
                ", expiresAt=" + expiresAt +
                ", issuedAt=" + issuedAt +
                '}';
    }
}