package biz.db.dev.life.jwt.core.providers;

public class AlwaysAcceptRevocationProvider implements RevocationProvider {

    @Override
    public boolean isTokenRevoked(String token) {
        return false;
    }
}
