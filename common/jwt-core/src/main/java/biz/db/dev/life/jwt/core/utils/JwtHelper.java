package biz.db.dev.life.jwt.core.utils;

import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.JwtDetails;
import biz.db.dev.life.jwt.core.JwtTokenBuilder;
import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.keystore.JwtKeyStore;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class JwtHelper {

    private static final String PUBLIC_KEY_BEGIN = "-----BEGIN PUBLIC KEY-----";
    private static final String PUBLIC_KEY_END = "-----END PUBLIC KEY-----";
    private static final String PRIVATE_KEY_BEGIN = "-----BEGIN PRIVATE KEY-----";
    private static final String PRIVATE_KEY_END = "-----END PRIVATE KEY-----";
    private static final String EMPTY_STRING = "";
    private static final String BLANK_SPACE = " ";
    private static final String NEW_LINE = "\n";

    private static final int CONST_PROPERTIES_MAX_SIZE = 64;

    private JwtHelper() {
        // util class
    }

    public static boolean hasGrants(String grant, JwtDetails jwtDetails) {
        return jwtDetails.getGrants().contains(grant);
    }

    public static Optional<UUID> userId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if( authentication == null) {
            return Optional.empty();
        } else if( authentication instanceof JwtAuthenticationToken) {
            return Optional.of( ( (JwtDetails) authentication.getDetails()).getUserId() );
        } else {
            return Optional.empty();
        }
    }

    public static String sanitizePublicKey(String key) {
        String sanitized = key.replace(PUBLIC_KEY_BEGIN, EMPTY_STRING);
        sanitized = sanitized.replace(PUBLIC_KEY_END, EMPTY_STRING);
        sanitized = sanitized.replace(BLANK_SPACE, EMPTY_STRING);
        sanitized = sanitized.replace(NEW_LINE, EMPTY_STRING);

        return sanitized;
    }

    public static String sanitizePrivateKey(String key) {
        String sanitized = key.replace(PRIVATE_KEY_BEGIN, EMPTY_STRING);
        sanitized = sanitized.replace(PRIVATE_KEY_END, EMPTY_STRING);
        sanitized = sanitized.replace(BLANK_SPACE, EMPTY_STRING);
        sanitized = sanitized.replace(NEW_LINE, EMPTY_STRING);

        return sanitized;
    }

    public static JwtDetails processToken(String token, JwtKeyStore jwtKeyStore) throws JwtProcessingException {
        String keyId = JwtTokenBuilder.parseKeyId(token);
        String publicKey = jwtKeyStore.getPublicKeyById(keyId);
        String sanitizedPublicKey = JwtHelper.sanitizePublicKey(publicKey);
        List<String> keyGrants = jwtKeyStore.getKeyGrantsById(keyId);

        checkPublicKey(keyId, sanitizedPublicKey);

        return JwtTokenBuilder.parse(sanitizedPublicKey, token, keyGrants);
    }

    private static void checkPublicKey(String keyId, String publicKey) throws JwtProcessingException {
        if(publicKey == null) {
            throw new JwtProcessingException("public key not found with id " + keyId);
        }
    }


    public static void main(String[] args) throws NoSuchAlgorithmException {
        KeyPairGenerator rsaGenerator = KeyPairGenerator.getInstance("RSA");
        rsaGenerator.initialize(2048);
        KeyPair rsaKeyPair = rsaGenerator.generateKeyPair();

        String publicKey = Base64.getEncoder().encodeToString(rsaKeyPair.getPublic().getEncoded());
        String privateKey = Base64.getEncoder().encodeToString(rsaKeyPair.getPrivate().getEncoded());

        StringBuilder publicKeyBuilder = new StringBuilder();
        publicKeyBuilder.append(PUBLIC_KEY_BEGIN).append(NEW_LINE);

        int publicKeyLength = publicKey.length();
        int publicKeyCurrent = 0;
        int publicKeyNext = 0;

        while ( publicKeyCurrent < publicKeyLength){

            publicKeyNext = Math.min(publicKeyCurrent + CONST_PROPERTIES_MAX_SIZE, publicKeyLength);
            publicKeyBuilder.append(publicKey, publicKeyCurrent, publicKeyNext);
            publicKeyBuilder.append(NEW_LINE);

            publicKeyCurrent = publicKeyNext;

        }
        publicKeyBuilder.append(PUBLIC_KEY_END);

        System.out.println(publicKeyBuilder.toString());

        StringBuilder privateKeyBuilder = new StringBuilder();
        privateKeyBuilder.append(PRIVATE_KEY_BEGIN).append(NEW_LINE);

        int privateKeyLength = privateKey.length();
        int privateKeyCurrent = 0;
        int privateKeyNext = 0;

        while ( privateKeyCurrent < privateKeyLength){

            privateKeyNext = Math.min(privateKeyCurrent + CONST_PROPERTIES_MAX_SIZE, privateKeyLength);
            privateKeyBuilder.append(privateKey, privateKeyCurrent, privateKeyNext);
            privateKeyBuilder.append(NEW_LINE);

            privateKeyCurrent = privateKeyNext;

        }
        privateKeyBuilder.append(PRIVATE_KEY_END);

        System.out.println(privateKeyBuilder.toString());
    }

}