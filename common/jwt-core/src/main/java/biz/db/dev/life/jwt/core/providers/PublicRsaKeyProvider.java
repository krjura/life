package biz.db.dev.life.jwt.core.providers;

import biz.db.dev.life.jwt.core.ex.JwtRuntimeException;
import com.auth0.jwt.interfaces.RSAKeyProvider;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class PublicRsaKeyProvider implements RSAKeyProvider {

    private static final String PARAM_RSA = "RSA";

    private String publicKey;

    public PublicRsaKeyProvider(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public RSAPublicKey getPublicKeyById(String keyId) {

        try {
            byte[] keyBytes = Base64.getDecoder().decode(this.publicKey);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance(PARAM_RSA);

            return ( RSAPublicKey) kf.generatePublic(keySpec);
        } catch ( NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new JwtRuntimeException("Cannot generate Public Key", e);
        }
    }

    @Override
    public RSAPrivateKey getPrivateKey() {
        throw new UnsupportedOperationException("public keys are not supported");
    }

    @Override
    public String getPrivateKeyId() {
        throw new UnsupportedOperationException("public keys are not supported");
    }
}