package biz.db.dev.life.jwt.core.config.pojo;

import java.util.ArrayList;
import java.util.List;

public class KeyInfo {

    private String publicKey;

    private String privateKey;

    private List<String> keyGrants;

    public KeyInfo() {
        this.keyGrants = new ArrayList<>();
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public List<String> getKeyGrants() {
        return keyGrants;
    }

    public void setKeyGrants(List<String> keyGrants) {
        this.keyGrants = keyGrants;
    }

    @Override
    public String toString() {
        return "KeyInfo{" +
                "publicKey='" + publicKey + '\'' +
                ", privateKey='" + privateKey + '\'' +
                ", keyGrants=" + keyGrants +
                '}';
    }
}
