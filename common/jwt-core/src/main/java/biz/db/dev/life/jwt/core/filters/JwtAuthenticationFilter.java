package biz.db.dev.life.jwt.core.filters;

import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.config.JwtProps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    private final JwtProps jwtProps;

    private final AuthenticationManager authenticationManager;

    public JwtAuthenticationFilter(JwtProps jwtProps, AuthenticationManager authenticationManager) {
        Objects.requireNonNull(jwtProps);
        Objects.requireNonNull(authenticationManager);

        this.jwtProps = jwtProps;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if(logger.isTraceEnabled()) {
            logger.trace("checking for JWT authentication");
        }

        switch (this.jwtProps.getTokenLocation()) {
            case COOKIE:
                useCookieBasedJwt(request, response, filterChain);
                break;
            case HEADER:
                useHeaderBasedJwt(request, response, filterChain);
                break;
            default:
                useCookieBasedJwt(request, response, filterChain);
                break;
        }
    }

    private void useHeaderBasedJwt(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        String authToken = request.getHeader(this.jwtProps.getTokenParamName());

        if(authToken == null) {

            if(logger.isTraceEnabled()) {
                logger.trace("JWT token not found in header");
            }

            filterChain.doFilter(request, response);
            return;
        }

        logger.info("using header based jwt auth");

        handleJwtAuth(authToken, request, response, filterChain);
    }

    private void useCookieBasedJwt(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        Cookie cookie = findJwtCookie(request);

        if(cookie == null) {

            if(logger.isTraceEnabled()) {
                logger.trace("JWT token not found in cookie");
            }

            filterChain.doFilter(request, response);
            return;
        }

        logger.info("using cookie based jwt auth");

        String authToken = cookie.getValue();

        handleJwtAuth(authToken, request, response, filterChain);
    }

    private Cookie findJwtCookie(HttpServletRequest request) {

        if( request.getCookies() == null ) {
            return null;
        }

        for( Cookie cookie : request.getCookies()) {
            if( cookie.getName().equals(this.jwtProps.getTokenParamName())) {
                return cookie;
            }
        }

        return null;
    }

    private void handleJwtAuth(
            String authToken, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        try {
            JwtAuthenticationToken authRequest = new JwtAuthenticationToken(authToken);

            Authentication authResult = this.authenticationManager.authenticate(authRequest);

            SecurityContextHolder.getContext().setAuthentication(authResult);

            if(logger.isDebugEnabled()) {
                logger.debug("JWT authentication was successful with result: {}", authResult);
            }
        } catch (AuthenticationException e) {
            logger.warn("JWT token parsing failed", e);

            SecurityContextHolder.clearContext();
        }

        filterChain.doFilter(request, response);
    }
}