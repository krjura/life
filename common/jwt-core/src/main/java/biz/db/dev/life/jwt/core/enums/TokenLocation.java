package biz.db.dev.life.jwt.core.enums;

public enum TokenLocation {

    COOKIE, HEADER
}
