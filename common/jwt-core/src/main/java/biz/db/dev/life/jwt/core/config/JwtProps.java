package biz.db.dev.life.jwt.core.config;

import biz.db.dev.life.jwt.core.enums.TokenLocation;
import biz.db.dev.life.jwt.core.keystore.KeyStoreProps;
import biz.db.dev.life.jwt.core.keystore.KeyStorePropsImpl;
import biz.db.dev.life.jwt.core.utils.JwtConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties( prefix = "life.jwt")
@ConditionalOnProperty(value = "life.jwt.enabled", havingValue = "true")
public class JwtProps {

    private static final Logger logger = LoggerFactory.getLogger(JwtProps.class);

    private KeyStoreProps keyStore;

    private TokenLocation tokenLocation = TokenLocation.COOKIE;

    private String tokenParamName = JwtConstants.COOKIE_PARAM_NAME;

    public JwtProps() {
        this.keyStore = new KeyStorePropsImpl();

        logger.info("initializing JwtProps");
    }

    public KeyStoreProps getKeyStore() {
        return keyStore;
    }

    public void setKeyStore(KeyStoreProps keyStore) {
        this.keyStore = keyStore;
    }

    public TokenLocation getTokenLocation() {
        return tokenLocation;
    }

    public void setTokenLocation(TokenLocation tokenLocation) {
        this.tokenLocation = tokenLocation;
    }

    public String getTokenParamName() {
        return tokenParamName;
    }

    public void setTokenParamName(String tokenParamName) {
        this.tokenParamName = tokenParamName;
    }
}