package biz.db.dev.life.jwt.core.test;

import biz.db.dev.life.jwt.core.JwtAuthenticationToken;
import biz.db.dev.life.jwt.core.JwtDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class WithMockJwtContextFactory implements WithSecurityContextFactory<WithMockJwt> {

    @Override
    public SecurityContext createSecurityContext(WithMockJwt annotation) {

        List<GrantedAuthority> grantedAuthorities =
                annotation.keyGrants().length == 0
                        ? toGrantedAuthority(annotation.grants()) : toGrantedAuthority(annotation.keyGrants());;

        UUID userId = UUID.fromString(annotation.userId());
        UUID tenantId = UUID.fromString(annotation.tenantId());

        Long issuedAt = System.currentTimeMillis();
        Long expiresAt = ZonedDateTime.now().plusMinutes(10).toInstant().toEpochMilli();

        JwtDetails jwtDetails = new JwtDetails(
                annotation.tokenId(), tenantId, userId, Arrays.asList(annotation.grants()), issuedAt, expiresAt);

        JwtAuthenticationToken jwtAuthenticationToken = new JwtAuthenticationToken(grantedAuthorities, jwtDetails);

        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(jwtAuthenticationToken);

        return context;
    }

    private List<GrantedAuthority> toGrantedAuthority(String[] grants) {
        return Arrays
                .stream(grants)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
