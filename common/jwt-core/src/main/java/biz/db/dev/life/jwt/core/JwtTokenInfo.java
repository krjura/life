package biz.db.dev.life.jwt.core;

import java.time.ZonedDateTime;

public class JwtTokenInfo {

    private String token;

    private ZonedDateTime validTo;

    public JwtTokenInfo(String token, ZonedDateTime validTo) {
        this.token = token;
        this.validTo = validTo;
    }

    public String getToken() {
        return token;
    }

    public ZonedDateTime getValidTo() {
        return validTo;
    }
}
