package biz.db.dev.life.jwt.core.config;

import biz.db.dev.life.jwt.core.keystore.DefaultKeyStoreProvider;
import biz.db.dev.life.jwt.core.keystore.JwtKeyStore;
import biz.db.dev.life.jwt.core.providers.AlwaysAcceptRevocationProvider;
import biz.db.dev.life.jwt.core.providers.JwtAuthenticationProvider;
import biz.db.dev.life.jwt.core.providers.RevocationProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;

import java.util.List;

@Configuration
@ConditionalOnProperty(value = "life.jwt.enabled", havingValue = "true")
@ConditionalOnClass({AuthenticationManager.class, AuthenticationProvider.class})
public class JwtBeanConfig {

    @Bean
    @ConditionalOnProperty(value = "life.jwt.provider.enabled", havingValue = "true")
    public JwtAuthenticationProvider jwtAuthenticationProvider(JwtProps jwtProps, RevocationProvider revocationProvider) {

        JwtKeyStore jwtKeyStore = new DefaultKeyStoreProvider(jwtProps.getKeyStore());

        return new JwtAuthenticationProvider(jwtKeyStore, revocationProvider);
    }

    @Bean
    @ConditionalOnProperty(value = "life.jwt.manager.enabled", havingValue = "true")
    public AuthenticationManager authenticationManager(List<AuthenticationProvider> providers) {
        return new ProviderManager(providers);
    }

    @Bean
    @ConditionalOnProperty(value = "life.jwt.provider.enabled", havingValue = "true")
    @ConditionalOnMissingBean(value = RevocationProvider.class)
    public RevocationProvider revocationProvider() {
        return new AlwaysAcceptRevocationProvider();
    }
}
