package biz.db.dev.life.jwt.core.keystore;

import biz.db.dev.life.jwt.core.config.pojo.KeyInfo;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class DefaultKeyStoreProvider implements JwtKeyStore {

    private final KeyStoreProps keyStoreConfig;

    public DefaultKeyStoreProvider(KeyStoreProps keyStoreConfig) {
        Objects.requireNonNull(keyStoreConfig, "KeyStoreProps cannot be null");

        this.keyStoreConfig = keyStoreConfig;
    }

    @Override
    public String getPublicKeyById(String keyId) {
        return getKeyInfo(keyId).map(KeyInfo::getPublicKey).orElse(null);
    }

    @Override
    public String getPrivateKeyById(String keyId) {
        return getKeyInfo(keyId).map(KeyInfo::getPrivateKey).orElse(null);
    }

    @Override
    public List<String> getKeyGrantsById(String keyId) {
        return getKeyInfo(keyId).map(KeyInfo::getKeyGrants).orElse(Collections.emptyList());
    }

    private Optional<KeyInfo> getKeyInfo(String keyId) {
        return Optional.ofNullable(this.keyStoreConfig.getKeys().get(keyId));
    }

    @Override
    public String getDefaultKeyId() {
        return this.keyStoreConfig.getDefaultKeyId();
    }
}