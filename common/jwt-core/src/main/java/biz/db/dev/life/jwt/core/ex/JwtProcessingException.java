package biz.db.dev.life.jwt.core.ex;

public class JwtProcessingException extends Exception {

    public JwtProcessingException(String message) {
        super(message);
    }

    public JwtProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}