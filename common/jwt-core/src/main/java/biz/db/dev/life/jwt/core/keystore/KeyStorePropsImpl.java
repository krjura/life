package biz.db.dev.life.jwt.core.keystore;

import biz.db.dev.life.jwt.core.config.pojo.KeyInfo;

import java.util.HashMap;
import java.util.Map;

public class KeyStorePropsImpl implements KeyStoreProps {

    private Map<String, KeyInfo> keys;

    private String defaultKeyId;

    private Long defaultExpiry;

    public KeyStorePropsImpl() {
        this.keys = new HashMap<>();
    }

    @Override
    public Map<String, KeyInfo> getKeys() {
        return keys;
    }

    public void setKeys(Map<String, KeyInfo> keys) {
        this.keys = keys;
    }

    @Override
    public String getDefaultKeyId() {
        return defaultKeyId;
    }

    public void setDefaultKeyId(String defaultKeyId) {
        this.defaultKeyId = defaultKeyId;
    }

    @Override
    public Long getDefaultExpiry() {
        return defaultExpiry;
    }

    public void setDefaultExpiry(Long defaultExpiry) {
        this.defaultExpiry = defaultExpiry;
    }

    @Override
    public String toString() {
        return "KeyStoreConfig{" +
                "keys=" + keys +
                ", defaultKeyId='" + defaultKeyId + '\'' +
                ", defaultExpiry=" + defaultExpiry +
                '}';
    }
}