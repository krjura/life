package biz.db.dev.life.jwt.core.keystore;

import java.util.List;

public interface JwtKeyStore {

    String getPublicKeyById(String keyId);

    String getPrivateKeyById(String keyId);

    List<String> getKeyGrantsById(String keyId);

    String getDefaultKeyId();
}