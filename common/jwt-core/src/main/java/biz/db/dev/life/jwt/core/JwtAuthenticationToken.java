package biz.db.dev.life.jwt.core;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class JwtAuthenticationToken implements Authentication {

    private String token;

    private JwtDetails jwtDetails;

    private Collection<? extends GrantedAuthority> grants;

    public JwtAuthenticationToken(String token) {
        this.token = token;
    }

    public JwtAuthenticationToken(Collection<? extends GrantedAuthority> grants, JwtDetails jwtDetails) {
        this.jwtDetails = jwtDetails;
        this.grants = grants;
    }

    public String getToken() {
        return token;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.grants == null ? Collections.emptyList() : this.grants;
    }

    @Override
    public Object getCredentials() {
        return this.jwtDetails == null ? null : this.jwtDetails.getTokenId();
    }

    @Override
    public Object getDetails() {
        return this.jwtDetails;
    }

    @Override
    public Object getPrincipal() {
        return this.jwtDetails == null ? null : this.jwtDetails.getUserId();
    }

    @Override
    public boolean isAuthenticated() {
        return this.jwtDetails != null;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) {
        // not needed
    }

    @Override
    public String getName() {
        return this.jwtDetails.getUserId().toString();
    }

    @Override
    public String toString() {
        return "JwtAuthenticationToken{" +
                "token='" + token + '\'' +
                ", jwtDetails=" + jwtDetails +
                ", grants=" + grants +
                '}';
    }
}