package biz.db.dev.life.jwt.core;

import biz.db.dev.life.jwt.core.ex.JwtProcessingException;
import biz.db.dev.life.jwt.core.providers.PrivateRsaKeyProvider;
import biz.db.dev.life.jwt.core.providers.PublicRsaKeyProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.impl.PublicClaims;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class JwtTokenBuilder {

    private static final String PARAM_GRANTS = "grants";
    private static final String PARAM_USER_ID = "userId";
    private static final String PARAM_TENANT_ID = "tenantId";
    private static final int DEFAULT_LEEWAY = 30;

    private long expiry = 10;

    private String tokenId;

    private UUID tenantId;

    private UUID userId;

    private String[] grants;

    private String privateKey;

    private String keyId;

    private JwtTokenBuilder() {
        // builder
    }

    public static JwtTokenBuilder create() {
        return new JwtTokenBuilder();
    }

    public JwtTokenBuilder expiry(long expiry) {
        this.expiry = expiry;

        return this;
    }

    public JwtTokenBuilder tokenId(String id) {
        this.tokenId = id;

        return this;
    }

    public JwtTokenBuilder tenantId(UUID tenantId) {
        this.tenantId = tenantId;

        return this;
    }

    public JwtTokenBuilder userId(UUID userId) {
        this.userId = userId;

        return this;
    }

    public JwtTokenBuilder grants(String[] grants) {
        this.grants = grants;

        return this;
    }

    public JwtTokenBuilder privateKey(String privateKey) {
        this.privateKey = privateKey;

        return this;
    }

    public JwtTokenBuilder keyId(String keyId) {
        this.keyId = keyId;

        return this;
    }

    public JwtTokenInfo build() throws JwtProcessingException {
        Objects.requireNonNull(this.tokenId, "JWT tokenId cannot be null");
        Objects.requireNonNull(this.tenantId, "Tenant ID cannot be null");
        Objects.requireNonNull(this.userId, "User Id cannot be null");
        Objects.requireNonNull(this.grants, "Grants cannot be null");
        Objects.requireNonNull(this.privateKey, "Private key cannot be null");
        Objects.requireNonNull(this.keyId, "KeyId cannot be null");

        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime expiration = now.plusMinutes(this.expiry);

        try {
            PrivateRsaKeyProvider jwtKeyProvider = new PrivateRsaKeyProvider(this.keyId, this.privateKey);

            Algorithm algorithm = Algorithm.RSA256(jwtKeyProvider);

            String token = JWT
                    .create()
                    .withJWTId(this.tokenId)
                    .withClaim(PARAM_USER_ID, userId.toString())
                    .withClaim(PARAM_TENANT_ID, tenantId.toString())
                    .withExpiresAt(Date.from(expiration.toInstant()))
                    .withIssuedAt(Date.from(now.toInstant()))
                    .withArrayClaim(PARAM_GRANTS, this.grants)
                    .withKeyId(this.keyId)
                    .sign(algorithm);

            return new JwtTokenInfo(token, expiration);
        } catch ( IllegalArgumentException | JWTCreationException e) {
            throw new JwtProcessingException("Cannot encode jwt", e);
        }
    }

    public static String parseKeyId(String token) throws JwtProcessingException {
        try {
            return JWT.decode(token).getKeyId();
        } catch (JWTDecodeException e) {
            throw new JwtProcessingException("Cannot fetch key id", e);
        }
    }

    public static JwtDetails parse(String publicKeyString, String token) throws JwtProcessingException {
        return parse(publicKeyString, token, Collections.emptyList());
    }

    @SuppressWarnings("unchecked")
    public static JwtDetails parse(String publicKeyString, String token, List<String> keyGrants)
            throws JwtProcessingException {

        try {
            PublicRsaKeyProvider jwtKeyProvider = new PublicRsaKeyProvider(publicKeyString);

            Algorithm algorithm = Algorithm.RSA256(jwtKeyProvider);

            JWTVerifier verifier = JWT
                    .require(algorithm)
                    .acceptLeeway(DEFAULT_LEEWAY)
                    .build();
            DecodedJWT jwt = verifier.verify(token);


            List<String> grants = keyGrants.isEmpty() ? jwt.getClaim(PARAM_GRANTS).asList(String.class) : keyGrants;
            String tokenId = jwt.getId();

            UUID userId = uuidOrNull(jwt.getClaim(PARAM_USER_ID).asString());
            UUID tenantId = uuidOrNull(jwt.getClaim(PARAM_TENANT_ID).asString());

            Long issuedAt = dateOrNull(jwt.getIssuedAt());
            Long expiresAt = dateOrNull(jwt.getExpiresAt());

            verifyClaims(jwt);

            return new JwtDetails(tokenId, tenantId, userId, grants, issuedAt, expiresAt);
        } catch (TokenExpiredException e) {
            // we need special handling for this
            throw e;
        } catch ( JWTCreationException | JWTVerificationException e) {
            throw new JwtProcessingException("Cannot decode jwt. The following error occurred '" + e.getMessage() + "'", e);
        }
    }

    private static Long dateOrNull(Date date) {
        return date == null ? null : date.getTime();

    }

    private static UUID uuidOrNull(String uuid) {
        return uuid == null ? null : UUID.fromString(uuid);
    }

    private static void verifyClaims(DecodedJWT jwt) throws JwtProcessingException {
        Claim issuedAtClaim = jwt.getClaim(PublicClaims.ISSUED_AT);

        if( issuedAtClaim.isNull() ) {
            throw new JwtProcessingException("issued at claim cannot be null");
        }

        Claim expiresAt = jwt.getClaim(PublicClaims.EXPIRES_AT);

        if( expiresAt.isNull() ) {
            throw new JwtProcessingException("expires at claim cannot be null");
        }
    }
}