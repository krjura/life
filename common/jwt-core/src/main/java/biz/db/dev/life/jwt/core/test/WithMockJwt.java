package biz.db.dev.life.jwt.core.test;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@WithSecurityContext(factory = WithMockJwtContextFactory.class)
public @interface WithMockJwt {

    String tokenId() default "42246169-1bdc-4988-8438-2d4c53e7bc2b";

    String tenantId() default "92832068-7590-427f-a725-28c5308f0d93";

    String userId() default "68ad4723-8e99-4a57-97fe-fafb8aff5325";

    String[] grants() default {"PRIV_USER"};

    String[] keyGrants() default {};
}
