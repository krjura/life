package org.krjura.life.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.krjura.life.validations.UUIDConstraint;
import org.krjura.life.validations.VersionConstraint;
import org.krjura.life.validations.ZonedDateTimeConstraint;

public class EventMetadata {

    @UUIDConstraint
    private String messageId;

    @UUIDConstraint
    private String correlationId;

    @VersionConstraint
    private String version;

    @ZonedDateTimeConstraint
    private String timestamp;

    @JsonCreator
    public EventMetadata(
            @JsonProperty("messageId") String messageId,
            @JsonProperty("correlationId") String correlationId,
            @JsonProperty("version") String version,
            @JsonProperty("timestamp") String timestamp) {

        this.messageId = messageId;
        this.correlationId = correlationId;
        this.version = version;
        this.timestamp = timestamp;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public String getVersion() {
        return version;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
