package org.krjura.life.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.krjura.life.validations.UUIDConstraint;
import org.krjura.life.validations.ZonedDateTimeConstraint;

public class TokenRevokedEvent extends BasicEvent {

    public static final String NAME = TokenRevokedEvent.class.getSimpleName();

    @UUIDConstraint
    private String tokenId;

    @ZonedDateTimeConstraint
    private String expiresAt;

    @ZonedDateTimeConstraint
    private String revokedAt;

    @JsonCreator
    public TokenRevokedEvent(
            @JsonProperty("metadata") EventMetadata metadata,
            @JsonProperty("tokenId") String tokenId,
            @JsonProperty("expiresAt") String expiresAt,
            @JsonProperty("revokedAt") String revokedAt) {

        super(metadata);

        this.tokenId = tokenId;
        this.expiresAt = expiresAt;
        this.revokedAt = revokedAt;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public String getRevokedAt() {
        return revokedAt;
    }
}
