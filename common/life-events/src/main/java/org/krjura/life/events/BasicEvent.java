package org.krjura.life.events;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public abstract class BasicEvent {

    @Valid
    @NotNull
    private EventMetadata metadata;

    public BasicEvent(EventMetadata metadata) {
        this.metadata = metadata;
    }

    public EventMetadata getMetadata() {
        return metadata;
    }
}
