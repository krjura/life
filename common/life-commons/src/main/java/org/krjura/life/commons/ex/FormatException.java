package org.krjura.life.commons.ex;

public class FormatException extends RuntimeException{

    public FormatException(String message) {
        super(message);
    }

    public FormatException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
