package org.krjura.life.commons.dao;

public interface Pageable {

    int getPageNumber();

    int getPageSize();

    int getOffset();

    Pageable next();

    Pageable previousOrFirst();

    Pageable first();

    boolean hasPrevious();
}
