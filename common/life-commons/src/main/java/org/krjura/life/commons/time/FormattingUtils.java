package org.krjura.life.commons.time;

import org.krjura.life.commons.ex.FormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class FormattingUtils {

    private static final Logger logger = LoggerFactory.getLogger(FormattingUtils.class);

    private static final ZoneId utc = ZoneId.of("UTC");

    private FormattingUtils() {
        // util
    }

    public static String from(long timestamp) {
        Instant instant = Instant.ofEpochMilli(timestamp);

        return ZonedDateTime.ofInstant(instant, utc).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static String from(ZonedDateTime timestamp) {
        return timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static ZonedDateTime to(String timestamp) {
        return ZonedDateTime.parse(timestamp, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static LocalDate parseLocalDate(String date) throws FormatException {
        LocalDate localDate;

        try {
            localDate = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch ( DateTimeParseException e) {
            logger.warn("Cannot parse date time", e);

            throw new FormatException("Cannot parse date time", e);
        }

        return localDate;
    }

    public static LocalTime parseLocalTime(String time) throws FormatException {
        LocalTime localTime;

        try {
            localTime = LocalTime.parse(time, DateTimeFormatter.ISO_LOCAL_TIME);
        } catch ( DateTimeParseException e) {
            logger.warn("Cannot parse date time", e);

            throw new FormatException("Cannot parse date time", e);
        }

        return localTime;
    }
}
