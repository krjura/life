package org.krjura.life.commons.utils;

import java.util.List;

public class ArrayUtils {

    private ArrayUtils() {
        // util
    }

    public static String[] toArray(List<String> list) {

        String[] grantsArray = new String[list.size()];
        for( int i = 0; i < list.size(); i++) {
            grantsArray[i] = list.get(i);
        }

        return grantsArray;
    }
}
