package org.krjura.life.commons.dao;

public class DefaultPagingRequest implements Pageable {

    public static Pageable MAX_RESULTS = new DefaultPagingRequest(0, Integer.MAX_VALUE);

    private Integer maxPerPage;

    private Integer page;

    public DefaultPagingRequest() {
        setMaxPerPage(10);
        setPage(1);
    }

    public DefaultPagingRequest(Integer page, Integer maxPerPage) {
        setMaxPerPage(maxPerPage);
        setPage(page);
    }

    public Integer getMaxPerPage() {
        return maxPerPage;
    }

    public void setMaxPerPage(Integer maxPerPage) {
        this.maxPerPage = Math.max(maxPerPage, 10);
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        if(page < 0) {
            this.page = 0;
        } else if(page > 0) {
          this.page = page - 1;
        } else {
            this.page = 0;
        }
    }

    @Override
    public int getPageNumber() {
        return this.page;
    }

    @Override
    public int getPageSize() {
        return this.maxPerPage;
    }

    @Override
    public int getOffset() {
        return this.page * this.maxPerPage;
    }

    @Override
    public Pageable next() {
        return new DefaultPagingRequest(this.page + 1, this.getPageSize());
    }

    @Override
    public Pageable previousOrFirst() {
        if(this.page > 1) {
            return new DefaultPagingRequest(this.page - 1, this.getPageSize());
        } else {
            return new DefaultPagingRequest(0, this.getPageSize());
        }
    }

    @Override
    public Pageable first() {
        return new DefaultPagingRequest(0, this.getPageSize());
    }

    @Override
    public boolean hasPrevious() {
        return this.page > 1;
    }

    @Override
    public String toString() {
        return "DefaultPagingRequest{" +
                "maxPerPage=" + maxPerPage +
                ", page=" + page +
                '}';
    }
}
