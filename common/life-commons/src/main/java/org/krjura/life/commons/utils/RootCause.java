package org.krjura.life.commons.utils;

public class RootCause {

    private RootCause() {
        // util
    }

    public static boolean isRootCause(Throwable th, Class clazz) {

        Throwable cause = th;

        while (cause != null) {
            if(clazz.isInstance(cause)) {
                return true;
            }

            cause = cause.getCause();
        }

        return false;
    }
}
