package org.krjura.life.validations;

import org.junit.Before;
import org.junit.Test;
import org.krjura.life.validations.pojo.VersionObject;
import org.krjura.life.validations.utils.EventValidator;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class VersionConstraintTest {

    private EventValidator eventValidator;

    @Before
    public void init() {
        this.eventValidator = new EventValidator();
    }

    @Test
    public void all() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.0.0"));

        assertThat(violations).hasSize(0);
    }

    @Test
    public void onlyOneDigit() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1"));

        assertThat(violations).hasSize(0);
    }

    @Test
    public void twoDigit() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.0"));

        assertThat(violations).hasSize(0);
    }

    @Test
    public void threeDigit() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.0.0"));

        assertThat(violations).hasSize(0);
    }

    @Test
    public void fourDigit() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.0.0.0"));

        assertThat(violations).hasSize(1);
    }

    @Test
    public void oneWithDot() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1."));

        assertThat(violations).hasSize(1);
    }

    @Test
    public void twoWithDot() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.0."));

        assertThat(violations).hasSize(1);
    }

    @Test
    public void threeWithDot() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.0.0."));

        assertThat(violations).hasSize(1);
    }

    @Test
    public void withLetters() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("one"));

        assertThat(violations).hasSize(1);
    }

    @Test
    public void withMultipleLetters() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("one.two.three"));

        assertThat(violations).hasSize(1);
    }

    @Test
    public void withLettersAndNumbers() {
        Set<ConstraintViolation<VersionObject>> violations = this.eventValidator.validate(new VersionObject("1.a.b"));

        assertThat(violations).hasSize(1);
    }
}
