package org.krjura.life.validations.pojo;

import org.krjura.life.validations.VersionConstraint;

public class VersionObject {

    @VersionConstraint
    private String version;

    public VersionObject(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
