package org.krjura.life.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = { })
@Target({ FIELD, METHOD })
@Retention(RUNTIME)
@ReportAsSingleViolation
@NotNull
@Pattern(regexp = "^\\d+(\\.\\d+)?(\\.\\d+)?")
public @interface VersionConstraint {

    String message() default "life.VersionConstraint.message";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}