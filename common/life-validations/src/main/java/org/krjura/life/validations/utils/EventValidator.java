package org.krjura.life.validations.utils;

import org.krjura.life.validations.ex.ValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class EventValidator {

    private Validator validator;

    public EventValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public <T> Set<ConstraintViolation<T>> validate(T value) {
        return this.validator.validate(value);
    }

    public <T> void validateAndThrow(T value) {
        Set<ConstraintViolation<Object>> violations = validate(value);

        if(violations.size() != 0) {
            throw new ValidationException(
                    "cannot validate object of type " + value.getClass().getSimpleName(), violations);
        }
    }
}
