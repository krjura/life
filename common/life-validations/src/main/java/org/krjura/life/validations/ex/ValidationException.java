package org.krjura.life.validations.ex;

import javax.validation.ConstraintViolation;
import java.util.Set;

public class ValidationException extends RuntimeException {

    private Set<ConstraintViolation<Object>> violations;

    public ValidationException(String message, Set<ConstraintViolation<Object>> violations) {
        super(message);

        this.violations = violations;
    }

    public ValidationException(String message, Throwable throwable, Set<ConstraintViolation<Object>> violations) {
        super(message, throwable);

        this.violations = violations;
    }

    public Set<ConstraintViolation<Object>> getViolations() {
        return violations;
    }
}
