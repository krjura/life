package org.krjura.life.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = { })
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Pattern(regexp = "^(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])$")
public @interface TimeConstraint {

    String message() default "life.TimeConstraint.message";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
