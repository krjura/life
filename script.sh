#!/usr/bin/env bash

set -e

MODULE=$1
SCRIPT_NAME=$2

if [ -z "$MODULE" ]
then
  echo "module name not set"
  exit 1
fi

if [ -z "SCRIPT_NAME" ]
then
  echo "script name not set"
  exit 1
fi

cd $MODULE
echo "running $SCRIPT_NAME in $(pwd)"

$SCRIPT_NAME
